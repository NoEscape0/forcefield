﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ENGINE.Graphics;
using Microsoft.Xna.Framework.Content;
using ENGINE.GUI;
using GAME.MenuScreens;
using GAME;
using STORAGE;
using ENGINE.Logic;
using Microsoft.Xna.Framework.Media;


namespace Editor.Managers.MenuScreens
{
    public class EMainMenu : StandartScreen
    {
        public EMainMenu(ContentManager Content, object LastMenu)
        {
            Marker = new GameTexture(false, @"Textures\Advanced\point", new Vector2(-2000, -2000));


            Start = new Button(new GameTexture(false, @"Textures\Advanced\button", new Vector2(-2000, -2000)));
            Start.Text = "Редактор";
            Start.OnButtonClick += delegate(object sender)
            {
                GameManager.RecreateLevelController();
                GAME.GameManager.Map = WorldMap.Load(System.IO.Directory.GetCurrentDirectory() + @"\Levels\worldmap.map");
                GameManager.SetGame();


                ENGINE.Logic.Zones.StartFinishZone ob = new ENGINE.Logic.Zones.StartFinishZone(true);
                ob.Gtexture.Position = GameManager.LvlController.camera.CameraPosition;
                ob.Gtexture.Layer = 0.40f;
                ob.Startzone = true;

                GameManager.LvlController.ObjectArray.Add(ob);
                GameManager.LvlController.SortByLayer();


                SoundManager.StopAll(true);
            };
            Start.OnCursorUnderButton += delegate(object sender)
            {
                Marker.Position = new Vector2(Start.GTexture.TopLeft.X - (Marker.TextureResolution.width + 5), Start.GTexture.Position.Y);
            };

            
            //=======

            Loading = new Button(new GameTexture(false, @"Textures\Advanced\button", new Vector2(-2000, -2000)));
            Loading.Text = "";
            Loading.OnButtonClick += delegate(object sender)
            {
                
            };
            Loading.OnCursorUnderButton += delegate(object sender)
            {
                Marker.Position = new Vector2(Loading.GTexture.TopLeft.X - (Marker.TextureResolution.width + 5), Loading.GTexture.Position.Y);
            };

            //=======

            Settings = new Button(new GameTexture(false, @"Textures\Advanced\button", new Vector2(-2000, -2000)));
            Settings.Text = "";
            Settings.OnButtonClick += delegate(object sender)
            {
                
            };
            Settings.OnCursorUnderButton += delegate(object sender)
            {
                Marker.Position = new Vector2(Settings.GTexture.TopLeft.X - (Marker.TextureResolution.width + 5), Settings.GTexture.Position.Y);
            };

            //=======

            Exit = new Button(new GameTexture(false, @"Textures\Advanced\button", new Vector2(-2000, -2000)));
            Exit.Text = "Выход";
            Exit.OnButtonClick += delegate(object sender)
            {
                SoundManager.StopAll(false);
                Editor.Program.game.Exit();
            };
            Exit.OnCursorUnderButton += delegate(object sender)
            {
                Marker.Position = new Vector2(Exit.GTexture.TopLeft.X - (Marker.TextureResolution.width + 5), Exit.GTexture.Position.Y);
            };
            SoundManager.background.Play();
        }

        GameTexture Marker;
        public Button Start;
        public Button Loading;
        public Button Settings;
        public Button Exit;
        public string MenuName = "Editor : Main";


        public override void Draw(SpriteFont font)
        {
            Start.Draw(font);
            Loading.Draw(font);
            Settings.Draw(font);
            Exit.Draw(font);
            if (Marker.Position.X != 0 | Marker.Position.Y != 0)  Marker.Draw();
            Depository.SpriteBatch.DrawString(font, MenuName, new Vector2(Depository.ScreenResolution.X / 10, 50), new Color(30, 30, 30));
 	        base.Draw(font);
        }

        public override void Update(Microsoft.Xna.Framework.Vector2 MouseCoord, float camZoom, bool LbClick, Vector2 ScreenRes)
        {
            float Ystart = (ScreenRes.Y - 400) / 2;

            Start.Update(MouseCoord, camZoom, LbClick);
            Start.GTexture.Position =
                new Vector2(ScreenRes.X / 2 , Ystart);


            Loading.Update(MouseCoord, camZoom, LbClick);
            Loading.GTexture.Position =
                new Vector2(ScreenRes.X / 2 , Ystart + 100);


            Settings.Update(MouseCoord, camZoom, LbClick);
            Settings.GTexture.Position =
                new Vector2(ScreenRes.X / 2 , Ystart + 200);


            Exit.Update(MouseCoord, camZoom, LbClick);
            Exit.GTexture.Position =
                new Vector2(ScreenRes.X / 2 , Ystart + 300);

            base.Update(MouseCoord, camZoom, LbClick, ScreenRes);
        }
    }
}

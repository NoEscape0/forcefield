﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using STORAGE;
using Editor.Managers.MenuScreens;
using GAME.MenuScreens;
using GAME;
using Editor.FormGUI;
using ENGINE.Logic.Console;

namespace Editor.Managers
{
    public static class EDrawManager
    {
        public static object ActiveMenu;
        public static SpriteFont MenuFont;
        public static EditorMenu FORM_gameeditor;
        public static MapForm FORM_mapeditor;
        public static bool isDrawAllBody;

        public static void Load()
        {
            ActiveMenu = new EMainMenu(Depository.CommonContent, null);
            MenuFont = Depository.CommonContent.Load<SpriteFont>("Source\\Fonts\\menu");

            ENGINE.Logic.Library.LoadFromFile();

            FORM_gameeditor = new EditorMenu();
            FORM_gameeditor.Location = new System.Drawing.Point((int)(Depository.ScreenResolution.X - FORM_gameeditor.Width), 0);

            FORM_mapeditor = new MapForm();
            FORM_mapeditor.Location = new System.Drawing.Point((int)(Depository.ScreenResolution.X - FORM_mapeditor.Width), 0);




            FormGUI.Library_page.Load(Program.game);

            #region console commands for editor
            { // editor menu
                Command Command = new Command("editormenu");
                Command.help = "Enable(1) or disable(0) editor form";
                Command.OnCommandAction += delegate(string value)
                {
                    int d = -1;

                    try
                    {
                        d = System.Convert.ToInt32(value);
                    }
                    catch
                    {
                        Console.set_red();
                        Console.write_line("Incorrect value. Input 0 or 1");
                        Console.set_black();
                    }

                    if (d != -1)
                    {
                        Console.set_blue();

                        if (d == 1)
                        {
                            FORM_gameeditor.hided = false;
                            Console.write_line("Editor menu Enabled");
                        }
                        if (d == 0)
                        {
                            FORM_gameeditor.hided = true;
                            Console.write_line("Editor menu Disabled");
                        }

                        FORM_gameeditor.Location = FORM_gameeditor.location;

                        Console.set_black();
                    }
                };
                Console.ConsoleCommands.Add(Command);
            }




            { // map menu
                Command Command = new Command("mapmenu");
                Command.help = "Enable(1) or disable(0) map form";
                Command.OnCommandAction += delegate(string value)
                {
                    int d = -1;

                    try
                    {
                        d = System.Convert.ToInt32(value);
                    }
                    catch
                    {
                        Console.set_red();
                        Console.write_line("Incorrect value. Input 0 or 1");
                        Console.set_black();
                    }

                    if (d != -1)
                    {
                        Console.set_blue();

                        if (d == 1)
                        {
                            FORM_mapeditor.hided = false;

                            Console.write_line("mapmenu Enabled");
                        }
                        if (d == 0)
                        {
                            FORM_mapeditor.hided = true;
                            Console.write_line("mapmenu Disabled");
                        }

                        FORM_mapeditor.Location = FORM_mapeditor.location;

                        Console.set_black();
                    }
                };
                Console.ConsoleCommands.Add(Command);
            }
            #endregion
        }


        public static void Draw()
        {
            if (GameManager.GetGameStatus() == GameManager.gamestatus.game)//EDITOR 
            {
                Depository.GraphDevice.Clear(GameManager.LvlController.BackgroundFillColor);

                GameManager.LvlController.DrawAll();

                // DrawGrid(); //сетка привязки

                GUIManager.DrawGameGUI(GameManager.LvlController);


                Depository.SpriteBatch.Begin();
                Library_page.Draw();
                ENGINE.Logic.TextMessager.Draw();
                Depository.SpriteBatch.End();
                if (isDrawAllBody) { DrawAllBody(); ENGINE.Graphics.Grid.DrawBatch(GAME.GameManager.LvlController.camera); }
            }
            else Depository.GraphDevice.Clear(Color.Gray);


            if (GameManager.GetGameStatus() == GameManager.gamestatus.menu) // PAUSE
            {
                Depository.SpriteBatch.Begin();
                (ActiveMenu as StandartScreen).Draw(MenuFont);
                Depository.SpriteBatch.End();
            }

            if (GameManager.GetGameStatus() == GameManager.gamestatus.map) // MAP
            {
                GameManager.Map.Draw();
                GUIManager.DrawMapGUI();
            }

            GUIManager.DrawCommonGUI();
        }

        public static void Update()
        {
            if (GameManager.GetGameStatus() == GameManager.gamestatus.game)//EDITOR 
            {
                Depository.DrawViewport = new Rectangle(
     (int)(GameManager.LvlController.camera.CameraPosition.X - Depository.ScreenResolution.X / 2 / GameManager.LvlController.camera.Zoom),
     (int)(GameManager.LvlController.camera.CameraPosition.Y - Depository.ScreenResolution.Y / 2 / GameManager.LvlController.camera.Zoom),
     (int)(Depository.ScreenResolution.X / GameManager.LvlController.camera.Zoom),
     (int)(Depository.ScreenResolution.Y / GameManager.LvlController.camera.Zoom));

                Depository.ScreenRectangle = new Rectangle(0, 0, (int)Depository.ScreenResolution.X, (int)Depository.ScreenResolution.Y);


                if (!FORM_gameeditor.Visible)
                    FORM_gameeditor.Show();

                if (FORM_mapeditor.Visible)
                    FORM_mapeditor.Hide();

                Library_page.Update();

                GUIManager.UpdateGameGUI();

                GameManager.LvlController.UpdateAll();

                //update event handler

                if (STORAGE.Depository.keyboardState.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.LeftControl) &
                    STORAGE.Depository.keyboardState.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Z))
                {
                    if (canUnDo) ENGINE.Logic.EditorEventHandler.UndoEvent();
                    canUnDo = false;
                }
                else canUnDo = true;

                if (Input.IsNewKeyPressed(Microsoft.Xna.Framework.Input.Keys.Escape))
                {
                    GameManager.SetMenu();
                    Editor.Managers.EDrawManager.ActiveMenu = new EPauseM(Editor.Managers.EDrawManager.ActiveMenu);
                }

                ENGINE.Logic.TextMessager.Update();
            }

            if (GameManager.GetGameStatus() == GameManager.gamestatus.menu) // PAUSE
            {
                (ActiveMenu as StandartScreen).Update(CursorManager.ScreenPosition, 1.0f, CursorManager.LBclick, Depository.ScreenResolution);

                if (FORM_gameeditor.Visible)
                    FORM_gameeditor.Hide();

                if (FORM_mapeditor.Visible)
                    FORM_mapeditor.Hide();
            }

            if (GameManager.GetGameStatus() == GameManager.gamestatus.map) // MAP
            {
                if (FORM_gameeditor.Visible)
                    FORM_gameeditor.Hide();

                if (!FORM_mapeditor.Visible)
                    FORM_mapeditor.Show();

                GameManager.Map.Update();
                GUIManager.UpdateMapGUI();


                if (Input.IsNewKeyPressed(Microsoft.Xna.Framework.Input.Keys.Escape))
                {
                    GameManager.SetMenu();
                    Editor.Managers.EDrawManager.ActiveMenu = new Editor.Managers.MenuScreens.EPauseM(Editor.Managers.EDrawManager.ActiveMenu);
                }
            }

            GUIManager.UpdateCommonGUI();
        }

        static bool canUnDo = true;

        // DEBUG
        public static void DrawAllBody()
        {
            FarseerPhysics.Dynamics.World PhysicWorld = GAME.GameManager.LvlController.PhysicWorld;

            foreach (var body in PhysicWorld.BodyList)
            {
                foreach (var fxl in body.FixtureList)
                {
                    FarseerPhysics.Common.Transform t;
                    body.GetTransform(out t);
                    ENGINE.Graphics.Grid.BatchShape(fxl.Shape, t, Color.Green);
                }
            }
        }


        public static void DrawGrid()
        {
            ENGINE.Logic.Camera2d camera = GAME.GameManager.LvlController.camera;
            Vector2 start = new Vector2(-Config.Snap_to_grid, -Config.Snap_to_grid);
            start = Vector2.Transform(start, Matrix.Invert(camera.GetTransformation()));
            start.X = ((int)start.X / Config.Snap_to_grid) * Config.Snap_to_grid;
            start.Y = ((int)start.Y / Config.Snap_to_grid) * Config.Snap_to_grid;
            int lineX = (int)(Depository.ScreenResolution.X / Config.Snap_to_grid) + 1;
            int lineY = (int)(Depository.ScreenResolution.Y / Config.Snap_to_grid) + 1;

            float zoom = GAME.GameManager.LvlController.camera.Zoom;

            for (int i = 0; i < lineX / zoom; i++)
            {
                ENGINE.Graphics.Grid.BatchLine(start.X + i * Config.Snap_to_grid,
                                               start.Y - Config.Snap_to_grid,
                                               start.X + i * Config.Snap_to_grid,
                                               start.Y + Depository.ScreenResolution.Y / zoom + Config.Snap_to_grid, Color.Green);
            }

            for (int i = 0; i < lineY / zoom; i++)
            {
                ENGINE.Graphics.Grid.BatchLine(start.X - Config.Snap_to_grid,
                                              start.Y + i * Config.Snap_to_grid,
                                              start.X + Depository.ScreenResolution.X / zoom + Config.Snap_to_grid,
                                              start.Y + i * Config.Snap_to_grid, Color.Green);
            }
        }
    }
}

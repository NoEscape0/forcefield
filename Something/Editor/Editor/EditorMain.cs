﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Windows.Forms;
using GAME;
using STORAGE;
using Editor.Managers;
using ENGINE.Logic;

namespace Editor
{
    public class EMain : Microsoft.Xna.Framework.Game
    {
        public EMain()
        {
            Depository.CommonContent = new Microsoft.Xna.Framework.Content.ContentManager(this.Services, "Content");
            Depository.GameContent = new Microsoft.Xna.Framework.Content.ContentManager(this.Services, "Content");
            Depository.MainForm = Control.FromHandle(this.Window.Handle) as Form;
            Depository.Graphics = new GraphicsDeviceManager(this);
            Depository.Game = this;            
            IsFixedTimeStep = Config.isFixedTimeStep;
        }

        protected override void Initialize()
        {
            Config.EditorMode = true;
            Application.EnableVisualStyles();
            Depository.Adapter = Depository.Graphics.GraphicsDevice.Adapter;
            Depository.GraphDevice = Depository.Graphics.GraphicsDevice;
            Depository.BasicEffect = new BasicEffect(Depository.GraphDevice);
            ScreenParametrs.SetScreenParams();
            CursorManager.Initialize();
            base.Initialize();
            
            EDrawManager.Load();
        }

        protected override void LoadContent()
        {
            ProfilesManager.CheckDirectories();
            Depository.SpriteBatch = new SpriteBatch(GraphicsDevice);
            GUIManager.LoadContent(Editor.Program.game);
            IsMouseVisible = true;
            
        }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            if (this.IsActive) Depository.GameFormActive = true;
             else Depository.GameFormActive = false;

            Depository.keyboardState = Keyboard.GetState();

            Depository.mouseState = Mouse.GetState();
            Depository.Elapsed = gameTime.ElapsedGameTime.TotalSeconds;
            Depository.gameTime = gameTime;

            EDrawManager.Update();

            Depository.oldKeyboardState = Keyboard.GetState();
            Depository.oldMouseState = Mouse.GetState();

            SoundManager.Update();




            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            EDrawManager.Draw();
            base.Draw(gameTime);
        }
    }
}

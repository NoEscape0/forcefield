﻿using System;

namespace Editor
{
    public static class Program
    {
        public static EMain game;
        [STAThreadAttribute]
        public static void Main(string[] args)
        {
            using (game = new EMain())
            {
                game.Run();
            }
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GAME;
using STORAGE;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace Editor.FormGUI
{
    public partial class SoundSettings : Form
    {
        public SoundSettings(bool randomSound, object sound)
        {
            InitializeComponent();


            if (!randomSound)
            {
                label1.Text = "Sound";
                label2.Enabled = comboBox1.Enabled = button3.Enabled = button5.Enabled = false;
                s = sound as Sound;
            }
            else
            {
                label1.Text = "RandomSound";
                r = sound as RandomSound;
            }

            randomsound = randomSound;
        }

        bool randomsound;
        Sound s;
        RandomSound r = new RandomSound();


        private void SoundSettings_Load(object sender, EventArgs e)
        {
            if (randomsound)
            { //RandomSound
                RefreshRandomSoundForm();
            }
            else
            { //Sound
                label4_folder.Text = s.folder;
                label6_volume.Text = s.Volume.ToString();
            }
        }

        void RefreshRandomSoundForm()
        {
            comboBox1.Items.Clear();

            if (r.Sounds.Length == 0) return;

            for (int i = 0; i < r.Sounds.Length; ++i)
            {
                string s = r.Sounds[i].folder;
                s = s.Substring(s.LastIndexOf(@"\") + 1);
                comboBox1.Items.Add(i.ToString() + "  " + s);
            }

            currentRsound = r.Sounds[0];

            comboBox1.SelectedIndex = 0;
        }

        Sound currentRsound;

        public static void ShowSoundSettings(Sound sound)
        {
            SoundSettings s = new SoundSettings(false, sound);
            s.ShowDialog(Editor.Managers.EDrawManager.FORM_gameeditor);
        }
        public static void ShowRandomSoundSettings(RandomSound sound)
        {
            SoundSettings s = new SoundSettings(true, sound);
            s.ShowDialog(Editor.Managers.EDrawManager.FORM_gameeditor);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentRsound = r.Sounds[comboBox1.SelectedIndex];
            label4_folder.Text = currentRsound.folder;
            label6_volume.Text = currentRsound.Volume.ToString();
        }

        //change volume
        private void button2_Click(object sender, EventArgs e)
        {         
            if (randomsound)
            {
                try { currentRsound.Volume = (float)Convert.ToDouble(textBox1.Text); }
                catch { };
                label6_volume.Text = currentRsound.Volume.ToString();
            }
            else
            {
                try { s.Volume = (float)Convert.ToDouble(textBox1.Text); }
                catch { };
                label6_volume.Text = s.Volume.ToString();
            }

            textBox1.Text = "";
        }

        //change folder
        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Выберете файл звука xnb";
            dialog.Filter = "SoundEffect (*.xnb)|*.xnb";
            dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + @"Content\Sound\";

            DialogResult res = dialog.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {
                string file = dialog.FileName;

                file = file.Replace(".xnb", "");
                file = file.Replace(AppDomain.CurrentDomain.BaseDirectory, "");
                file = file.Substring(8);


                try
                {
                    Depository.CommonContent.Load<SoundEffect>(file);
                }
                catch
                {
                    MessageBox.Show("File error");
                    return;
                }


                if (randomsound)
                {
                    currentRsound.folder = file;
                    currentRsound.RestoreSound();
                    label4_folder.Text = currentRsound.folder;
                }
                else
                {
                    s.folder = file;
                    s.RestoreSound();
                    label4_folder.Text = s.folder;
                }
            }



             
        }

        //add
        private void button3_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Выберете файл звука xnb";
            dialog.Filter = "SoundEffect (*.xnb)|*.xnb";
            dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + @"Content\Sound\";

            DialogResult res = dialog.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {
                string file = dialog.FileName;

                file = file.Replace(".xnb", "");
                file = file.Replace(AppDomain.CurrentDomain.BaseDirectory, "");
                file = file.Substring(8);

                try { Depository.CommonContent.Load<SoundEffect>(file); }
                catch
                {
                    MessageBox.Show("File error");
                    return;
                }

                int le = 1;
                if (r == null) r = new RandomSound();
                le = r.Sounds.Length + 1;

                Sound[] nn = new Sound[le];

                for (int i = 0; i < r.Sounds.Length; ++i) nn[i] = r.Sounds[i];
                nn[nn.Length - 1] = new Sound(file);
                
               

                r.Sounds = nn;

                RefreshRandomSoundForm();
            }

        }

        //delete
        private void button5_Click(object sender, EventArgs e)
        {
            if (r.Sounds.Length <= 1)
            {
                r.Sounds = new Sound[0];
                RefreshRandomSoundForm();
                return;
            }
            
            Sound[] nn = new Sound[r.Sounds.Length - 1];

            int freeM = 0;

            for (int i = 0; i < r.Sounds.Length; ++i)
            {
                if (r.Sounds[i] != currentRsound)
                {
                    nn[freeM] = r.Sounds[i];
                    freeM++;
                }
            }

            r.Sounds = nn;

            RefreshRandomSoundForm();
        }
    }
}

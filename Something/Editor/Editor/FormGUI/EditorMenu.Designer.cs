﻿namespace Editor.FormGUI
{
    partial class EditorMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MainTabControl = new System.Windows.Forms.TabControl();
            this.Main_LevelPage = new System.Windows.Forms.TabPage();
            this.DrawAllBody_checkBox = new System.Windows.Forms.CheckBox();
            this.label53 = new System.Windows.Forms.Label();
            this.button29 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.currentBGSize_label = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.isSky_checkBox = new System.Windows.Forms.CheckBox();
            this.tileVertical_checkBox = new System.Windows.Forms.CheckBox();
            this.tileHorizontal_checkbox = new System.Windows.Forms.CheckBox();
            this.label66 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.layer_textBox = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.parallax_textBox = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.positionY_textBox = new System.Windows.Forms.TextBox();
            this.positionX_textBox = new System.Windows.Forms.TextBox();
            this.backgroundList_comboBox = new System.Windows.Forms.ComboBox();
            this.brightness_textbox = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button9 = new System.Windows.Forms.Button();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.button38 = new System.Windows.Forms.Button();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.backgroundColor = new System.Windows.Forms.Button();
            this.label151 = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this.gravity_textBox = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.physBody_label = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.Main_ObjectPage = new System.Windows.Forms.TabPage();
            this.deleteFarObject_button = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.AttributestabControl = new System.Windows.Forms.TabControl();
            this.StandartPage = new System.Windows.Forms.TabPage();
            this.button46 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.isShadowCaster_checkBox = new System.Windows.Forms.CheckBox();
            this.button45 = new System.Windows.Forms.Button();
            this.button43 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.label108 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.button25 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.label100 = new System.Windows.Forms.Label();
            this.rotation_textBox = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.button11 = new System.Windows.Forms.Button();
            this.label56 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.button13 = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.SelectPage = new System.Windows.Forms.TabPage();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.DragPage = new System.Windows.Forms.TabPage();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.button14 = new System.Windows.Forms.Button();
            this.label191 = new System.Windows.Forms.Label();
            this.label190 = new System.Windows.Forms.Label();
            this.AnimationPage = new System.Windows.Forms.TabPage();
            this.button58 = new System.Windows.Forms.Button();
            this.button57 = new System.Windows.Forms.Button();
            this.button53 = new System.Windows.Forms.Button();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.button26 = new System.Windows.Forms.Button();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.button56 = new System.Windows.Forms.Button();
            this.label187 = new System.Windows.Forms.Label();
            this.button55 = new System.Windows.Forms.Button();
            this.button50 = new System.Windows.Forms.Button();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.button54 = new System.Windows.Forms.Button();
            this.label186 = new System.Windows.Forms.Label();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.label184 = new System.Windows.Forms.Label();
            this.label174 = new System.Windows.Forms.Label();
            this.button27 = new System.Windows.Forms.Button();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.button44 = new System.Windows.Forms.Button();
            this.label182 = new System.Windows.Forms.Label();
            this.label183 = new System.Windows.Forms.Label();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.label181 = new System.Windows.Forms.Label();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.label185 = new System.Windows.Forms.Label();
            this.label180 = new System.Windows.Forms.Label();
            this.comboBox_fromTexture = new System.Windows.Forms.ComboBox();
            this.label179 = new System.Windows.Forms.Label();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.label178 = new System.Windows.Forms.Label();
            this.button51 = new System.Windows.Forms.Button();
            this.button52 = new System.Windows.Forms.Button();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.comboBox_frames = new System.Windows.Forms.ComboBox();
            this.label173 = new System.Windows.Forms.Label();
            this.label177 = new System.Windows.Forms.Label();
            this.label176 = new System.Windows.Forms.Label();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.label175 = new System.Windows.Forms.Label();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.label172 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.label171 = new System.Windows.Forms.Label();
            this.comboBox_animations = new System.Windows.Forms.ComboBox();
            this.label87 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.DrawPage = new System.Windows.Forms.TabPage();
            this.BarrierPage = new System.Windows.Forms.TabPage();
            this.isInstantPlatform_checkBox = new System.Windows.Forms.CheckBox();
            this.moveWaypoints_checkBox = new System.Windows.Forms.CheckBox();
            this.speed_label = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.setSpeed_textBox = new System.Windows.Forms.TextBox();
            this.isSensor_checkBox = new System.Windows.Forms.CheckBox();
            this.friction_label = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.setFriction_textBox = new System.Windows.Forms.TextBox();
            this.mass_label = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.setMass_textBox = new System.Windows.Forms.TextBox();
            this.isCircular_checkBox = new System.Windows.Forms.CheckBox();
            this.deleteWaypoint_button = new System.Windows.Forms.Button();
            this.addWaypoint_button = new System.Windows.Forms.Button();
            this.testMoving_checkBox = new System.Windows.Forms.CheckBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label102 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.dynamic_radioButton = new System.Windows.Forms.RadioButton();
            this.moving_radioButton = new System.Windows.Forms.RadioButton();
            this.staticBarrier_radioButton = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.isCircleBody_checkBox = new System.Windows.Forms.CheckBox();
            this.label40 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.button15 = new System.Windows.Forms.Button();
            this.label34 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.MovementHeroPage = new System.Windows.Forms.TabPage();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.button47 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.maxJumpSpeed_textBox = new System.Windows.Forms.TextBox();
            this.jumpStr_textBox = new System.Windows.Forms.TextBox();
            this.runSpeed_textBox = new System.Windows.Forms.TextBox();
            this.applyJumpParam_button = new System.Windows.Forms.Button();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.RpgCharacterPage = new System.Windows.Forms.TabPage();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.BonusPage = new System.Windows.Forms.TabPage();
            this.setBounceParam_button = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.period_textBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.amplitude_textBox = new System.Windows.Forms.TextBox();
            this.bounceBonus_checkBox = new System.Windows.Forms.CheckBox();
            this.button19 = new System.Windows.Forms.Button();
            this.label63 = new System.Windows.Forms.Label();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.button18 = new System.Windows.Forms.Button();
            this.label62 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.button17 = new System.Windows.Forms.Button();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label59 = new System.Windows.Forms.Label();
            this.PortalPage = new System.Windows.Forms.TabPage();
            this.isInteractive_checkBox = new System.Windows.Forms.CheckBox();
            this.isInstant_checkBox = new System.Windows.Forms.CheckBox();
            this.label90 = new System.Windows.Forms.Label();
            this.exitTime_textBox = new System.Windows.Forms.TextBox();
            this.label89 = new System.Windows.Forms.Label();
            this.transitionTime_textBox = new System.Windows.Forms.TextBox();
            this.isExit_checkBox = new System.Windows.Forms.CheckBox();
            this.twoSides_checkBox = new System.Windows.Forms.CheckBox();
            this.ToolTipPage = new System.Windows.Forms.TabPage();
            this.label99 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.omega_textBox = new System.Windows.Forms.TextBox();
            this.appearTime_textBox = new System.Windows.Forms.TextBox();
            this.label97 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.fontLoad_button = new System.Windows.Forms.Button();
            this.bgLoad_button = new System.Windows.Forms.Button();
            this.label96 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.ttText_textBox = new System.Windows.Forms.TextBox();
            this.isAlwaysShow_checkBox = new System.Windows.Forms.CheckBox();
            this.ttOffsetY_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.ttOffsetX_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.ttWidth_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.ttHeight_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.DeathZonePage = new System.Windows.Forms.TabPage();
            this.label104 = new System.Windows.Forms.Label();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label103 = new System.Windows.Forms.Label();
            this.button32 = new System.Windows.Forms.Button();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.DialogPage = new System.Windows.Forms.TabPage();
            this.setActorTexture_button = new System.Windows.Forms.Button();
            this.deleteDialogScreen_button = new System.Windows.Forms.Button();
            this.deleteAnswer_button = new System.Windows.Forms.Button();
            this.label158 = new System.Windows.Forms.Label();
            this.dialogText_textBox = new System.Windows.Forms.TextBox();
            this.newAnswer_button = new System.Windows.Forms.Button();
            this.screenId_label = new System.Windows.Forms.Label();
            this.label156 = new System.Windows.Forms.Label();
            this.addDialogScreen_button = new System.Windows.Forms.Button();
            this.label154 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this.nextDialogScreen_comboBox = new System.Windows.Forms.ComboBox();
            this.answers_listBox = new System.Windows.Forms.ListBox();
            this.label132 = new System.Windows.Forms.Label();
            this.dialogScreenList_comboBox = new System.Windows.Forms.ComboBox();
            this.currentAnswerText_textBox = new System.Windows.Forms.TextBox();
            this.LightPage = new System.Windows.Forms.TabPage();
            this.gammaLight_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label155 = new System.Windows.Forms.Label();
            this.directionAngle_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.innerAngle_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.outerAngle_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.lightColor_button = new System.Windows.Forms.Button();
            this.label142 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this.label138 = new System.Windows.Forms.Label();
            this.radius_textBox = new System.Windows.Forms.TextBox();
            this.label137 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.lightOffsetY_textBox = new System.Windows.Forms.TextBox();
            this.lightOffsetX_textBox = new System.Windows.Forms.TextBox();
            this.lights_comboBox = new System.Windows.Forms.ComboBox();
            this.TriggerPage = new System.Windows.Forms.TabPage();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.comboBox_fields_settings = new System.Windows.Forms.ComboBox();
            this.label168 = new System.Windows.Forms.Label();
            this.public_param_b_1 = new System.Windows.Forms.Button();
            this.label167 = new System.Windows.Forms.Label();
            this.label166 = new System.Windows.Forms.Label();
            this.label165 = new System.Windows.Forms.Label();
            this.label157 = new System.Windows.Forms.Label();
            this.label164 = new System.Windows.Forms.Label();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.button41 = new System.Windows.Forms.Button();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.label152 = new System.Windows.Forms.Label();
            this.radioButton_loop_Unlimited = new System.Windows.Forms.RadioButton();
            this.label163 = new System.Windows.Forms.Label();
            this.radioButton_loop_Once = new System.Windows.Forms.RadioButton();
            this.label162 = new System.Windows.Forms.Label();
            this.radioButton_loop_useCD = new System.Windows.Forms.RadioButton();
            this.button39 = new System.Windows.Forms.Button();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.label161 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label153 = new System.Windows.Forms.Label();
            this.radioButton_OnCollisionStart = new System.Windows.Forms.RadioButton();
            this.radioButton_OnBeing = new System.Windows.Forms.RadioButton();
            this.radioButton_OnLeaving = new System.Windows.Forms.RadioButton();
            this.radioButton_Always = new System.Windows.Forms.RadioButton();
            this.label160 = new System.Windows.Forms.Label();
            this.label159 = new System.Windows.Forms.Label();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.Main_SpecialObjects = new System.Windows.Forms.TabPage();
            this.label69 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.button34 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.SpecialObjectsTabControl = new System.Windows.Forms.TabControl();
            this.CreateEmitterPage = new System.Windows.Forms.TabPage();
            this.CreateCircleEmitterButton = new System.Windows.Forms.Button();
            this.CreateLineEmitterButton = new System.Windows.Forms.Button();
            this.CreateDotEmitterButton = new System.Windows.Forms.Button();
            this.EmitterPage = new System.Windows.Forms.TabPage();
            this.label169 = new System.Windows.Forms.Label();
            this.label170 = new System.Windows.Forms.Label();
            this.energyLoss_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.isSolid_checkBox = new System.Windows.Forms.CheckBox();
            this.mass_textBox = new System.Windows.Forms.TextBox();
            this.label143 = new System.Windows.Forms.Label();
            this.isInteractveEmitter_checkBox = new System.Windows.Forms.CheckBox();
            this.normalDistr_checkBox = new System.Windows.Forms.CheckBox();
            this.isBackLayer_checkBox = new System.Windows.Forms.CheckBox();
            this.Color_button = new System.Windows.Forms.Button();
            this.label85 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.tMaxAlpha_textBox = new System.Windows.Forms.TextBox();
            this.sizeVel_textBox = new System.Windows.Forms.TextBox();
            this.label76 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.bothWays_checkBox = new System.Windows.Forms.CheckBox();
            this.label193 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.ttl_textBox = new System.Windows.Forms.TextBox();
            this.label192 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.emittrtomega_textBox = new System.Windows.Forms.TextBox();
            this.angleVel_textBox = new System.Windows.Forms.TextBox();
            this.alphaVel_textBox = new System.Windows.Forms.TextBox();
            this.width_textBox = new System.Windows.Forms.TextBox();
            this.height_textBox = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.width_label75 = new System.Windows.Forms.Label();
            this.height_label76 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.maxSpeed_textBox = new System.Windows.Forms.TextBox();
            this.minSpeed_textBox = new System.Windows.Forms.TextBox();
            this.maxSize_textBox = new System.Windows.Forms.TextBox();
            this.minSize_textBox = new System.Windows.Forms.TextBox();
            this.label70_position = new System.Windows.Forms.Label();
            this.label70_secondaryAngle = new System.Windows.Forms.Label();
            this.label71_mainAngle = new System.Windows.Forms.Label();
            this.secondaryAngle_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.startAngle_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.mainAngle_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label70_maxSize = new System.Windows.Forms.Label();
            this.label71_minSize = new System.Windows.Forms.Label();
            this.label72_startSize = new System.Windows.Forms.Label();
            this.label72_maxSpeed = new System.Windows.Forms.Label();
            this.label71_minSpeed = new System.Windows.Forms.Label();
            this.label70_startSpeed = new System.Windows.Forms.Label();
            this.label70_pps = new System.Windows.Forms.Label();
            this.Pps_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.alphablend_checkBox = new System.Windows.Forms.CheckBox();
            this.EnemySettingsPage = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.button37 = new System.Windows.Forms.Button();
            this.label131 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.kamikaze_groupBox = new System.Windows.Forms.GroupBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label188 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label189 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.button36 = new System.Windows.Forms.Button();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label117 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.CreateEnemyPage = new System.Windows.Forms.TabPage();
            this.button35 = new System.Windows.Forms.Button();
            this.weatherPage = new System.Windows.Forms.TabPage();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.weather_Rain = new System.Windows.Forms.RadioButton();
            this.weather_Snow = new System.Windows.Forms.RadioButton();
            this.noWeather = new System.Windows.Forms.RadioButton();
            this.Main_library = new System.Windows.Forms.TabPage();
            this.Main_Events = new System.Windows.Forms.TabPage();
            this.button22 = new System.Windows.Forms.Button();
            this.label149 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.label147 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.label144 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.colorDialog1 = new ColorDialogEx(1000, 200);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.button48 = new System.Windows.Forms.Button();
            this.button49 = new System.Windows.Forms.Button();
            this.MainTabControl.SuspendLayout();
            this.Main_LevelPage.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.Main_ObjectPage.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.AttributestabControl.SuspendLayout();
            this.StandartPage.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.SelectPage.SuspendLayout();
            this.DragPage.SuspendLayout();
            this.AnimationPage.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.BarrierPage.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.MovementHeroPage.SuspendLayout();
            this.RpgCharacterPage.SuspendLayout();
            this.BonusPage.SuspendLayout();
            this.PortalPage.SuspendLayout();
            this.ToolTipPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ttOffsetY_numericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ttOffsetX_numericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ttWidth_numericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ttHeight_numericUpDown)).BeginInit();
            this.DeathZonePage.SuspendLayout();
            this.DialogPage.SuspendLayout();
            this.LightPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gammaLight_numericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.directionAngle_numericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.innerAngle_numericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.outerAngle_numericUpDown)).BeginInit();
            this.TriggerPage.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.Main_SpecialObjects.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SpecialObjectsTabControl.SuspendLayout();
            this.CreateEmitterPage.SuspendLayout();
            this.EmitterPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.energyLoss_numericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondaryAngle_numericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startAngle_numericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainAngle_numericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pps_numericUpDown)).BeginInit();
            this.EnemySettingsPage.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.CreateEnemyPage.SuspendLayout();
            this.weatherPage.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.Main_Events.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainTabControl
            // 
            this.MainTabControl.Controls.Add(this.Main_LevelPage);
            this.MainTabControl.Controls.Add(this.Main_ObjectPage);
            this.MainTabControl.Controls.Add(this.Main_SpecialObjects);
            this.MainTabControl.Controls.Add(this.Main_library);
            this.MainTabControl.Controls.Add(this.Main_Events);
            this.MainTabControl.Location = new System.Drawing.Point(1, -2);
            this.MainTabControl.Name = "MainTabControl";
            this.MainTabControl.SelectedIndex = 0;
            this.MainTabControl.Size = new System.Drawing.Size(501, 608);
            this.MainTabControl.TabIndex = 0;
            // 
            // Main_LevelPage
            // 
            this.Main_LevelPage.BackColor = System.Drawing.Color.White;
            this.Main_LevelPage.Controls.Add(this.DrawAllBody_checkBox);
            this.Main_LevelPage.Controls.Add(this.label53);
            this.Main_LevelPage.Controls.Add(this.button29);
            this.Main_LevelPage.Controls.Add(this.button28);
            this.Main_LevelPage.Controls.Add(this.button10);
            this.Main_LevelPage.Controls.Add(this.groupBox6);
            this.Main_LevelPage.Controls.Add(this.checkBox3);
            this.Main_LevelPage.Controls.Add(this.groupBox4);
            this.Main_LevelPage.Controls.Add(this.physBody_label);
            this.Main_LevelPage.Controls.Add(this.label20);
            this.Main_LevelPage.Controls.Add(this.label109);
            this.Main_LevelPage.Controls.Add(this.label19);
            this.Main_LevelPage.Controls.Add(this.groupBox2);
            this.Main_LevelPage.Location = new System.Drawing.Point(4, 22);
            this.Main_LevelPage.Name = "Main_LevelPage";
            this.Main_LevelPage.Padding = new System.Windows.Forms.Padding(3);
            this.Main_LevelPage.Size = new System.Drawing.Size(493, 582);
            this.Main_LevelPage.TabIndex = 1;
            this.Main_LevelPage.Text = "World";
            // 
            // DrawAllBody_checkBox
            // 
            this.DrawAllBody_checkBox.AutoSize = true;
            this.DrawAllBody_checkBox.Location = new System.Drawing.Point(233, 234);
            this.DrawAllBody_checkBox.Name = "DrawAllBody_checkBox";
            this.DrawAllBody_checkBox.Size = new System.Drawing.Size(200, 17);
            this.DrawAllBody_checkBox.TabIndex = 10;
            this.DrawAllBody_checkBox.Text = "Показывать все физические тела";
            this.DrawAllBody_checkBox.UseVisualStyleBackColor = true;
            this.DrawAllBody_checkBox.CheckedChanged += new System.EventHandler(this.DrawAllBody_checkBox_CheckedChanged);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(132, 40);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(11, 13);
            this.label53.TabIndex = 9;
            this.label53.Text = "*";
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(119, 6);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(99, 23);
            this.button29.TabIndex = 8;
            this.button29.Text = "Save as...";
            this.button29.UseVisualStyleBackColor = true;
            this.button29.Click += new System.EventHandler(this.button29_Click);
            // 
            // button28
            // 
            this.button28.Location = new System.Drawing.Point(14, 35);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(99, 23);
            this.button28.TabIndex = 7;
            this.button28.Text = "SaveLevel";
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.button28_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(14, 6);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(99, 23);
            this.button10.TabIndex = 6;
            this.button10.Text = "OpenLevel...";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.currentBGSize_label);
            this.groupBox6.Controls.Add(this.label145);
            this.groupBox6.Controls.Add(this.isSky_checkBox);
            this.groupBox6.Controls.Add(this.tileVertical_checkBox);
            this.groupBox6.Controls.Add(this.tileHorizontal_checkbox);
            this.groupBox6.Controls.Add(this.label66);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this.layer_textBox);
            this.groupBox6.Controls.Add(this.label65);
            this.groupBox6.Controls.Add(this.parallax_textBox);
            this.groupBox6.Controls.Add(this.label64);
            this.groupBox6.Controls.Add(this.label136);
            this.groupBox6.Controls.Add(this.positionY_textBox);
            this.groupBox6.Controls.Add(this.positionX_textBox);
            this.groupBox6.Controls.Add(this.backgroundList_comboBox);
            this.groupBox6.Controls.Add(this.brightness_textbox);
            this.groupBox6.Controls.Add(this.label67);
            this.groupBox6.Controls.Add(this.button20);
            this.groupBox6.Controls.Add(this.button21);
            this.groupBox6.Controls.Add(this.checkBox5);
            this.groupBox6.Controls.Add(this.checkBox4);
            this.groupBox6.Location = new System.Drawing.Point(17, 257);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(473, 228);
            this.groupBox6.TabIndex = 5;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Background";
            // 
            // currentBGSize_label
            // 
            this.currentBGSize_label.AutoSize = true;
            this.currentBGSize_label.Location = new System.Drawing.Point(261, 151);
            this.currentBGSize_label.Name = "currentBGSize_label";
            this.currentBGSize_label.Size = new System.Drawing.Size(90, 13);
            this.currentBGSize_label.TabIndex = 37;
            this.currentBGSize_label.Text = "WIDTHxHEIGHT";
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Location = new System.Drawing.Point(209, 151);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(46, 13);
            this.label145.TabIndex = 36;
            this.label145.Text = "Размер";
            // 
            // isSky_checkBox
            // 
            this.isSky_checkBox.AutoSize = true;
            this.isSky_checkBox.Location = new System.Drawing.Point(212, 121);
            this.isSky_checkBox.Name = "isSky_checkBox";
            this.isSky_checkBox.Size = new System.Drawing.Size(151, 17);
            this.isSky_checkBox.TabIndex = 35;
            this.isSky_checkBox.Text = "Это небо (не видно тени)";
            this.isSky_checkBox.UseVisualStyleBackColor = true;
            this.isSky_checkBox.CheckedChanged += new System.EventHandler(this.isSky_checkBox_CheckedChanged);
            // 
            // tileVertical_checkBox
            // 
            this.tileVertical_checkBox.AutoSize = true;
            this.tileVertical_checkBox.Location = new System.Drawing.Point(212, 95);
            this.tileVertical_checkBox.Name = "tileVertical_checkBox";
            this.tileVertical_checkBox.Size = new System.Drawing.Size(161, 17);
            this.tileVertical_checkBox.TabIndex = 34;
            this.tileVertical_checkBox.Text = "Размножить по вертикали";
            this.tileVertical_checkBox.UseVisualStyleBackColor = true;
            this.tileVertical_checkBox.CheckedChanged += new System.EventHandler(this.tileVertical_checkBox_CheckedChanged);
            // 
            // tileHorizontal_checkbox
            // 
            this.tileHorizontal_checkbox.AutoSize = true;
            this.tileHorizontal_checkbox.Location = new System.Drawing.Point(212, 72);
            this.tileHorizontal_checkbox.Name = "tileHorizontal_checkbox";
            this.tileHorizontal_checkbox.Size = new System.Drawing.Size(172, 17);
            this.tileHorizontal_checkbox.TabIndex = 33;
            this.tileHorizontal_checkbox.Text = "Размножить по горизонтали";
            this.tileHorizontal_checkbox.UseVisualStyleBackColor = true;
            this.tileHorizontal_checkbox.CheckedChanged += new System.EventHandler(this.tileHorizontal_checkbox_CheckedChanged);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(74, 121);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(14, 13);
            this.label66.TabIndex = 32;
            this.label66.Text = "Y";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(74, 95);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(14, 13);
            this.label26.TabIndex = 31;
            this.label26.Text = "X";
            // 
            // layer_textBox
            // 
            this.layer_textBox.Location = new System.Drawing.Point(94, 177);
            this.layer_textBox.Name = "layer_textBox";
            this.layer_textBox.Size = new System.Drawing.Size(100, 20);
            this.layer_textBox.TabIndex = 30;
            this.layer_textBox.TextChanged += new System.EventHandler(this.layer_textBox_TextChanged);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(8, 180);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(32, 13);
            this.label65.TabIndex = 29;
            this.label65.Text = "Слой";
            // 
            // parallax_textBox
            // 
            this.parallax_textBox.Location = new System.Drawing.Point(94, 151);
            this.parallax_textBox.Name = "parallax_textBox";
            this.parallax_textBox.Size = new System.Drawing.Size(100, 20);
            this.parallax_textBox.TabIndex = 27;
            this.parallax_textBox.TextChanged += new System.EventHandler(this.parallax_textBox_TextChanged);
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(6, 145);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(67, 26);
            this.label64.TabIndex = 26;
            this.label64.Text = "Расстояние\r\nдо камеры";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(6, 95);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(65, 13);
            this.label136.TabIndex = 25;
            this.label136.Text = "Положение";
            // 
            // positionY_textBox
            // 
            this.positionY_textBox.Location = new System.Drawing.Point(94, 118);
            this.positionY_textBox.Name = "positionY_textBox";
            this.positionY_textBox.Size = new System.Drawing.Size(100, 20);
            this.positionY_textBox.TabIndex = 24;
            this.positionY_textBox.TextChanged += new System.EventHandler(this.positionY_textBox_TextChanged);
            // 
            // positionX_textBox
            // 
            this.positionX_textBox.Location = new System.Drawing.Point(94, 92);
            this.positionX_textBox.Name = "positionX_textBox";
            this.positionX_textBox.Size = new System.Drawing.Size(100, 20);
            this.positionX_textBox.TabIndex = 22;
            this.positionX_textBox.TextChanged += new System.EventHandler(this.positionX_textBox_TextChanged);
            // 
            // backgroundList_comboBox
            // 
            this.backgroundList_comboBox.FormattingEnabled = true;
            this.backgroundList_comboBox.Location = new System.Drawing.Point(6, 42);
            this.backgroundList_comboBox.Name = "backgroundList_comboBox";
            this.backgroundList_comboBox.Size = new System.Drawing.Size(121, 21);
            this.backgroundList_comboBox.TabIndex = 20;
            this.backgroundList_comboBox.TextChanged += new System.EventHandler(this.backgroundList_comboBox_TextChanged);
            // 
            // brightness_textbox
            // 
            this.brightness_textbox.Location = new System.Drawing.Point(94, 69);
            this.brightness_textbox.Name = "brightness_textbox";
            this.brightness_textbox.Size = new System.Drawing.Size(100, 20);
            this.brightness_textbox.TabIndex = 16;
            this.brightness_textbox.TextChanged += new System.EventHandler(this.textBox22_TextChanged);
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(6, 72);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(82, 13);
            this.label67.TabIndex = 14;
            this.label67.Text = "Яркость фона:";
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(260, 40);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(121, 23);
            this.button20.TabIndex = 9;
            this.button20.Text = "Удалить текущую";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(133, 40);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(121, 23);
            this.button21.TabIndex = 8;
            this.button21.Text = "Добавить текстуру";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(135, 19);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(153, 17);
            this.checkBox5.TabIndex = 1;
            this.checkBox5.Text = "Отображать в редакторе";
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.checkBox5_CheckedChanged);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(6, 19);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(123, 17);
            this.checkBox4.TabIndex = 0;
            this.checkBox4.Text = "Отображать в игре";
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(17, 234);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(200, 17);
            this.checkBox3.TabIndex = 4;
            this.checkBox3.Text = "Показывать область препятствий";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button9);
            this.groupBox4.Controls.Add(this.checkBox8);
            this.groupBox4.Controls.Add(this.checkBox7);
            this.groupBox4.Controls.Add(this.button38);
            this.groupBox4.Controls.Add(this.textBox33);
            this.groupBox4.Controls.Add(this.backgroundColor);
            this.groupBox4.Controls.Add(this.label151);
            this.groupBox4.Controls.Add(this.label150);
            this.groupBox4.Controls.Add(this.gravity_textBox);
            this.groupBox4.Controls.Add(this.label50);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Location = new System.Drawing.Point(14, 114);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(473, 114);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "World Properties";
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(239, 79);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(227, 23);
            this.button9.TabIndex = 34;
            this.button9.Text = "скопировать героя из карты в игру";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click_1);
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(290, 21);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(34, 17);
            this.checkBox8.TabIndex = 33;
            this.checkBox8.Text = "G";
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.CheckedChanged += new System.EventHandler(this.checkBox8_CheckedChanged);
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(251, 21);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(33, 17);
            this.checkBox7.TabIndex = 32;
            this.checkBox7.Text = "E";
            this.checkBox7.UseVisualStyleBackColor = true;
            this.checkBox7.CheckedChanged += new System.EventHandler(this.checkBox7_CheckedChanged);
            // 
            // button38
            // 
            this.button38.Location = new System.Drawing.Point(330, 17);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(136, 23);
            this.button38.TabIndex = 31;
            this.button38.Text = "Фоновая музыка...";
            this.button38.UseVisualStyleBackColor = true;
            this.button38.Click += new System.EventHandler(this.button38_Click);
            // 
            // textBox33
            // 
            this.textBox33.Location = new System.Drawing.Point(76, 55);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(69, 20);
            this.textBox33.TabIndex = 29;
            this.textBox33.TextChanged += new System.EventHandler(this.textBox33_TextChanged);
            // 
            // backgroundColor
            // 
            this.backgroundColor.Location = new System.Drawing.Point(76, 84);
            this.backgroundColor.Name = "backgroundColor";
            this.backgroundColor.Size = new System.Drawing.Size(139, 23);
            this.backgroundColor.TabIndex = 28;
            this.backgroundColor.UseVisualStyleBackColor = true;
            this.backgroundColor.Click += new System.EventHandler(this.backgroundColor_Click);
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Location = new System.Drawing.Point(9, 89);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(61, 13);
            this.label151.TabIndex = 27;
            this.label151.Text = "Цвет фона";
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Location = new System.Drawing.Point(4, 58);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(66, 13);
            this.label150.TabIndex = 26;
            this.label150.Text = "Освещение";
            // 
            // gravity_textBox
            // 
            this.gravity_textBox.Location = new System.Drawing.Point(121, 19);
            this.gravity_textBox.Name = "gravity_textBox";
            this.gravity_textBox.Size = new System.Drawing.Size(95, 20);
            this.gravity_textBox.TabIndex = 25;
            this.gravity_textBox.TextChanged += new System.EventHandler(this.gravity_textBox_TextChanged);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(61, 22);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(41, 13);
            this.label50.TabIndex = 24;
            this.label50.Text = "label50";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Gravity";
            // 
            // physBody_label
            // 
            this.physBody_label.AutoSize = true;
            this.physBody_label.Location = new System.Drawing.Point(119, 561);
            this.physBody_label.Name = "physBody_label";
            this.physBody_label.Size = new System.Drawing.Size(41, 13);
            this.physBody_label.TabIndex = 2;
            this.physBody_label.Text = "label20";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(425, 561);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 13);
            this.label20.TabIndex = 2;
            this.label20.Text = "label20";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(12, 561);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(93, 13);
            this.label109.TabIndex = 1;
            this.label109.Text = "Физических тел:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(318, 561);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(101, 13);
            this.label19.TabIndex = 1;
            this.label19.Text = "Объектов в мире: ";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Location = new System.Drawing.Point(14, 66);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(473, 42);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Камера";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(392, 16);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 13);
            this.label18.TabIndex = 3;
            this.label18.Text = "label18";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(335, 16);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(27, 13);
            this.label17.TabIndex = 2;
            this.label17.Text = "Зум";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(63, 16);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "label16";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 16);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(51, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Позиция";
            // 
            // Main_ObjectPage
            // 
            this.Main_ObjectPage.Controls.Add(this.deleteFarObject_button);
            this.Main_ObjectPage.Controls.Add(this.label2);
            this.Main_ObjectPage.Controls.Add(this.groupBox1);
            this.Main_ObjectPage.Controls.Add(this.label1);
            this.Main_ObjectPage.Controls.Add(this.button2);
            this.Main_ObjectPage.Location = new System.Drawing.Point(4, 22);
            this.Main_ObjectPage.Name = "Main_ObjectPage";
            this.Main_ObjectPage.Padding = new System.Windows.Forms.Padding(3);
            this.Main_ObjectPage.Size = new System.Drawing.Size(493, 582);
            this.Main_ObjectPage.TabIndex = 0;
            this.Main_ObjectPage.Text = "Object";
            this.Main_ObjectPage.UseVisualStyleBackColor = true;
            // 
            // deleteFarObject_button
            // 
            this.deleteFarObject_button.Location = new System.Drawing.Point(103, 548);
            this.deleteFarObject_button.Name = "deleteFarObject_button";
            this.deleteFarObject_button.Size = new System.Drawing.Size(118, 23);
            this.deleteFarObject_button.TabIndex = 6;
            this.deleteFarObject_button.Text = "Удалить упавшие";
            this.deleteFarObject_button.UseVisualStyleBackColor = true;
            this.deleteFarObject_button.Click += new System.EventHandler(this.deleteFarObject_button_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(68, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "***";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.AttributestabControl);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Location = new System.Drawing.Point(10, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(480, 484);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Tag = "";
            this.groupBox1.Text = "Объект:";
            // 
            // AttributestabControl
            // 
            this.AttributestabControl.Controls.Add(this.StandartPage);
            this.AttributestabControl.Controls.Add(this.SelectPage);
            this.AttributestabControl.Controls.Add(this.DragPage);
            this.AttributestabControl.Controls.Add(this.AnimationPage);
            this.AttributestabControl.Controls.Add(this.DrawPage);
            this.AttributestabControl.Controls.Add(this.BarrierPage);
            this.AttributestabControl.Controls.Add(this.MovementHeroPage);
            this.AttributestabControl.Controls.Add(this.RpgCharacterPage);
            this.AttributestabControl.Controls.Add(this.BonusPage);
            this.AttributestabControl.Controls.Add(this.PortalPage);
            this.AttributestabControl.Controls.Add(this.ToolTipPage);
            this.AttributestabControl.Controls.Add(this.DeathZonePage);
            this.AttributestabControl.Controls.Add(this.DialogPage);
            this.AttributestabControl.Controls.Add(this.LightPage);
            this.AttributestabControl.Controls.Add(this.TriggerPage);
            this.AttributestabControl.Location = new System.Drawing.Point(1, 55);
            this.AttributestabControl.Name = "AttributestabControl";
            this.AttributestabControl.SelectedIndex = 0;
            this.AttributestabControl.Size = new System.Drawing.Size(473, 429);
            this.AttributestabControl.TabIndex = 6;
            // 
            // StandartPage
            // 
            this.StandartPage.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.StandartPage.Controls.Add(this.button46);
            this.StandartPage.Controls.Add(this.groupBox3);
            this.StandartPage.Controls.Add(this.label8);
            this.StandartPage.Controls.Add(this.label7);
            this.StandartPage.Controls.Add(this.label6);
            this.StandartPage.Controls.Add(this.label5);
            this.StandartPage.Controls.Add(this.trackBar1);
            this.StandartPage.Controls.Add(this.button8);
            this.StandartPage.Controls.Add(this.button7);
            this.StandartPage.Controls.Add(this.label4);
            this.StandartPage.Controls.Add(this.button6);
            this.StandartPage.Controls.Add(this.comboBox2);
            this.StandartPage.Controls.Add(this.button5);
            this.StandartPage.Controls.Add(this.button3);
            this.StandartPage.Controls.Add(this.label3);
            this.StandartPage.Controls.Add(this.comboBox1);
            this.StandartPage.Location = new System.Drawing.Point(4, 22);
            this.StandartPage.Name = "StandartPage";
            this.StandartPage.Size = new System.Drawing.Size(465, 403);
            this.StandartPage.TabIndex = 2;
            this.StandartPage.Text = "[Standart]";
            // 
            // button46
            // 
            this.button46.Location = new System.Drawing.Point(88, 377);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(192, 23);
            this.button46.TabIndex = 29;
            this.button46.Text = "Удалить все с такой текстурой";
            this.button46.UseVisualStyleBackColor = true;
            this.button46.Click += new System.EventHandler(this.button46_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.isShadowCaster_checkBox);
            this.groupBox3.Controls.Add(this.button45);
            this.groupBox3.Controls.Add(this.button43);
            this.groupBox3.Controls.Add(this.button42);
            this.groupBox3.Controls.Add(this.button40);
            this.groupBox3.Controls.Add(this.label108);
            this.groupBox3.Controls.Add(this.label106);
            this.groupBox3.Controls.Add(this.button25);
            this.groupBox3.Controls.Add(this.button33);
            this.groupBox3.Controls.Add(this.label100);
            this.groupBox3.Controls.Add(this.rotation_textBox);
            this.groupBox3.Controls.Add(this.label58);
            this.groupBox3.Controls.Add(this.textBox18);
            this.groupBox3.Controls.Add(this.label57);
            this.groupBox3.Controls.Add(this.textBox17);
            this.groupBox3.Controls.Add(this.button11);
            this.groupBox3.Controls.Add(this.label56);
            this.groupBox3.Controls.Add(this.label55);
            this.groupBox3.Controls.Add(this.button13);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.label54);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Controls.Add(this.button12);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Location = new System.Drawing.Point(9, 134);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(443, 237);
            this.groupBox3.TabIndex = 28;
            this.groupBox3.TabStop = false;
            // 
            // isShadowCaster_checkBox
            // 
            this.isShadowCaster_checkBox.AutoSize = true;
            this.isShadowCaster_checkBox.Location = new System.Drawing.Point(318, 152);
            this.isShadowCaster_checkBox.Name = "isShadowCaster_checkBox";
            this.isShadowCaster_checkBox.Size = new System.Drawing.Size(120, 17);
            this.isShadowCaster_checkBox.TabIndex = 43;
            this.isShadowCaster_checkBox.Text = "Отбрасывает тень";
            this.isShadowCaster_checkBox.UseVisualStyleBackColor = true;
            this.isShadowCaster_checkBox.CheckedChanged += new System.EventHandler(this.isShadowCaster_checkBox_CheckedChanged);
            // 
            // button45
            // 
            this.button45.Location = new System.Drawing.Point(368, 41);
            this.button45.Name = "button45";
            this.button45.Size = new System.Drawing.Size(32, 23);
            this.button45.TabIndex = 42;
            this.button45.Text = "-5";
            this.button45.UseVisualStyleBackColor = true;
            this.button45.Click += new System.EventHandler(this.button45_Click);
            // 
            // button43
            // 
            this.button43.Location = new System.Drawing.Point(368, 15);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(32, 23);
            this.button43.TabIndex = 41;
            this.button43.Text = "+5";
            this.button43.UseVisualStyleBackColor = true;
            this.button43.Click += new System.EventHandler(this.button43_Click);
            // 
            // button42
            // 
            this.button42.Location = new System.Drawing.Point(275, 59);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(37, 23);
            this.button42.TabIndex = 40;
            this.button42.Text = "-5";
            this.button42.UseVisualStyleBackColor = true;
            this.button42.Click += new System.EventHandler(this.button42_Click);
            // 
            // button40
            // 
            this.button40.Location = new System.Drawing.Point(232, 59);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(37, 23);
            this.button40.TabIndex = 39;
            this.button40.Text = "+5";
            this.button40.UseVisualStyleBackColor = true;
            this.button40.Click += new System.EventHandler(this.button40_Click);
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(315, 35);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(14, 13);
            this.label108.TabIndex = 38;
            this.label108.Text = "Y";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(222, 35);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(14, 13);
            this.label106.TabIndex = 37;
            this.label106.Text = "X";
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(335, 15);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(27, 23);
            this.button25.TabIndex = 35;
            this.button25.Text = "+";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.button25_Click);
            // 
            // button33
            // 
            this.button33.Location = new System.Drawing.Point(335, 41);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(27, 23);
            this.button33.TabIndex = 36;
            this.button33.Text = "-";
            this.button33.UseVisualStyleBackColor = true;
            this.button33.Click += new System.EventHandler(this.button33_Click);
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(60, 206);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(47, 13);
            this.label100.TabIndex = 34;
            this.label100.Text = "label100";
            // 
            // rotation_textBox
            // 
            this.rotation_textBox.Location = new System.Drawing.Point(121, 203);
            this.rotation_textBox.Name = "rotation_textBox";
            this.rotation_textBox.Size = new System.Drawing.Size(76, 20);
            this.rotation_textBox.TabIndex = 33;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(-1, 181);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(14, 13);
            this.label58.TabIndex = 32;
            this.label58.Text = "X";
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(19, 178);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(76, 20);
            this.textBox18.TabIndex = 31;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(101, 181);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(14, 13);
            this.label57.TabIndex = 30;
            this.label57.Text = "Y";
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(121, 177);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(76, 20);
            this.textBox17.TabIndex = 29;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(203, 177);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(99, 20);
            this.button11.TabIndex = 29;
            this.button11.Text = "Set";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(66, 130);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(41, 13);
            this.label56.TabIndex = 29;
            this.label56.Text = "label56";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(1, 130);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(43, 13);
            this.label55.TabIndex = 29;
            this.label55.Text = "Texture";
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(69, 76);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(153, 21);
            this.button13.TabIndex = 26;
            this.button13.Text = "button13";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(66, 156);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 13);
            this.label22.TabIndex = 17;
            this.label22.Text = "label22";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(66, 105);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(41, 13);
            this.label28.TabIndex = 25;
            this.label28.Text = "label28";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(4, 206);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(32, 13);
            this.label54.TabIndex = 16;
            this.label54.Text = "Угол";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(2, 156);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(44, 13);
            this.label21.TabIndex = 16;
            this.label21.Text = "Position";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(0, 105);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(36, 13);
            this.label27.TabIndex = 24;
            this.label27.Text = "Folder";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(8, 35);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(34, 13);
            this.label23.TabIndex = 18;
            this.label23.Text = "Scale";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(74, 35);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(41, 13);
            this.label24.TabIndex = 19;
            this.label24.Text = "label24";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(242, 30);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(27, 23);
            this.button1.TabIndex = 20;
            this.button1.Text = "+";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(275, 30);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(27, 23);
            this.button12.TabIndex = 21;
            this.button12.Text = "-";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(0, 80);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(40, 13);
            this.label25.TabIndex = 22;
            this.label25.Text = "Effects";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(75, 101);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "верх";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(427, 101);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "низ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(44, 74);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(19, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "***";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Слой";
            // 
            // trackBar1
            // 
            this.trackBar1.AutoSize = false;
            this.trackBar1.Location = new System.Drawing.Point(78, 65);
            this.trackBar1.Maximum = 100;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(384, 33);
            this.trackBar1.TabIndex = 11;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(286, 378);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(85, 22);
            this.button8.TabIndex = 10;
            this.button8.Text = "Удалить";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(404, 30);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(58, 23);
            this.button7.TabIndex = 9;
            this.button7.Text = "[E] / [G]";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(143, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Существующие аттрибуты:";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(323, 30);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 7;
            this.button6.Text = "Удалить";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(164, 32);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(155, 21);
            this.comboBox2.TabIndex = 6;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(323, 3);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 4;
            this.button5.Text = "Добавить";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(377, 377);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(85, 23);
            this.button3.TabIndex = 1;
            this.button3.Text = "Клонировать";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(147, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Недобавленные аттрибуты:";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(164, 5);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(155, 21);
            this.comboBox1.TabIndex = 3;
            // 
            // SelectPage
            // 
            this.SelectPage.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.SelectPage.Controls.Add(this.checkBox2);
            this.SelectPage.Controls.Add(this.checkBox1);
            this.SelectPage.Location = new System.Drawing.Point(4, 22);
            this.SelectPage.Name = "SelectPage";
            this.SelectPage.Padding = new System.Windows.Forms.Padding(3);
            this.SelectPage.Size = new System.Drawing.Size(465, 403);
            this.SelectPage.TabIndex = 0;
            this.SelectPage.Text = "Select";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(9, 29);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(220, 17);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "При выделении указывать 4 вершины";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(9, 6);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(225, 17);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "При выделении окрашивать в красный";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // DragPage
            // 
            this.DragPage.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.DragPage.Controls.Add(this.textBox43);
            this.DragPage.Controls.Add(this.button14);
            this.DragPage.Controls.Add(this.label191);
            this.DragPage.Controls.Add(this.label190);
            this.DragPage.Location = new System.Drawing.Point(4, 22);
            this.DragPage.Name = "DragPage";
            this.DragPage.Size = new System.Drawing.Size(465, 403);
            this.DragPage.TabIndex = 1;
            this.DragPage.Text = "Drag";
            // 
            // textBox43
            // 
            this.textBox43.Location = new System.Drawing.Point(291, 6);
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(59, 20);
            this.textBox43.TabIndex = 3;
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(356, 4);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(75, 23);
            this.button14.TabIndex = 2;
            this.button14.Text = "Set";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // label191
            // 
            this.label191.AutoSize = true;
            this.label191.Location = new System.Drawing.Point(232, 9);
            this.label191.Name = "label191";
            this.label191.Size = new System.Drawing.Size(47, 13);
            this.label191.TabIndex = 1;
            this.label191.Text = "label191";
            // 
            // label190
            // 
            this.label190.AutoSize = true;
            this.label190.Location = new System.Drawing.Point(3, 9);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(223, 13);
            this.label190.TabIndex = 0;
            this.label190.Text = "Шаг привязки (для всех объектов уровня):";
            // 
            // AnimationPage
            // 
            this.AnimationPage.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.AnimationPage.Controls.Add(this.button58);
            this.AnimationPage.Controls.Add(this.button57);
            this.AnimationPage.Controls.Add(this.button53);
            this.AnimationPage.Controls.Add(this.checkBox10);
            this.AnimationPage.Controls.Add(this.button26);
            this.AnimationPage.Controls.Add(this.groupBox14);
            this.AnimationPage.Controls.Add(this.comboBox_animations);
            this.AnimationPage.Controls.Add(this.label87);
            this.AnimationPage.Controls.Add(this.label86);
            this.AnimationPage.Controls.Add(this.label82);
            this.AnimationPage.Location = new System.Drawing.Point(4, 22);
            this.AnimationPage.Name = "AnimationPage";
            this.AnimationPage.Size = new System.Drawing.Size(465, 403);
            this.AnimationPage.TabIndex = 3;
            this.AnimationPage.Text = "Animation";
            // 
            // button58
            // 
            this.button58.Location = new System.Drawing.Point(394, 11);
            this.button58.Name = "button58";
            this.button58.Size = new System.Drawing.Size(54, 23);
            this.button58.TabIndex = 17;
            this.button58.Text = "load...";
            this.button58.UseVisualStyleBackColor = true;
            this.button58.Click += new System.EventHandler(this.button58_Click);
            // 
            // button57
            // 
            this.button57.Location = new System.Drawing.Point(394, 38);
            this.button57.Name = "button57";
            this.button57.Size = new System.Drawing.Size(54, 23);
            this.button57.TabIndex = 16;
            this.button57.Text = "save...";
            this.button57.UseVisualStyleBackColor = true;
            this.button57.Click += new System.EventHandler(this.button57_Click);
            // 
            // button53
            // 
            this.button53.Location = new System.Drawing.Point(353, 38);
            this.button53.Name = "button53";
            this.button53.Size = new System.Drawing.Size(40, 23);
            this.button53.TabIndex = 15;
            this.button53.Text = "-Del";
            this.button53.UseVisualStyleBackColor = true;
            this.button53.Click += new System.EventHandler(this.button53_Click);
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(16, 15);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(65, 17);
            this.checkBox10.TabIndex = 14;
            this.checkBox10.Text = "Enabled";
            this.checkBox10.UseVisualStyleBackColor = true;
            this.checkBox10.CheckedChanged += new System.EventHandler(this.checkBox10_CheckedChanged);
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(312, 38);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(40, 23);
            this.button26.TabIndex = 12;
            this.button26.Text = "+Add";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.button_add_animation_Click);
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.textBox42);
            this.groupBox14.Controls.Add(this.button56);
            this.groupBox14.Controls.Add(this.label187);
            this.groupBox14.Controls.Add(this.button55);
            this.groupBox14.Controls.Add(this.button50);
            this.groupBox14.Controls.Add(this.groupBox15);
            this.groupBox14.Controls.Add(this.button51);
            this.groupBox14.Controls.Add(this.button52);
            this.groupBox14.Controls.Add(this.textBox34);
            this.groupBox14.Controls.Add(this.comboBox_frames);
            this.groupBox14.Controls.Add(this.label173);
            this.groupBox14.Controls.Add(this.label177);
            this.groupBox14.Controls.Add(this.label176);
            this.groupBox14.Controls.Add(this.textBox32);
            this.groupBox14.Controls.Add(this.label175);
            this.groupBox14.Controls.Add(this.checkBox9);
            this.groupBox14.Controls.Add(this.label172);
            this.groupBox14.Controls.Add(this.label88);
            this.groupBox14.Controls.Add(this.textBox31);
            this.groupBox14.Controls.Add(this.label171);
            this.groupBox14.Location = new System.Drawing.Point(15, 67);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(446, 333);
            this.groupBox14.TabIndex = 11;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Настройки анимации";
            // 
            // textBox42
            // 
            this.textBox42.Location = new System.Drawing.Point(246, 133);
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(43, 20);
            this.textBox42.TabIndex = 38;
            // 
            // button56
            // 
            this.button56.Location = new System.Drawing.Point(295, 131);
            this.button56.Name = "button56";
            this.button56.Size = new System.Drawing.Size(75, 23);
            this.button56.TabIndex = 23;
            this.button56.Text = "Set";
            this.button56.UseVisualStyleBackColor = true;
            this.button56.Click += new System.EventHandler(this.button56_Click);
            // 
            // label187
            // 
            this.label187.AutoSize = true;
            this.label187.Location = new System.Drawing.Point(14, 136);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(224, 13);
            this.label187.TabIndex = 22;
            this.label187.Text = "Задержка для всех кадров одновременно:";
            // 
            // button55
            // 
            this.button55.Location = new System.Drawing.Point(301, 106);
            this.button55.Name = "button55";
            this.button55.Size = new System.Drawing.Size(84, 23);
            this.button55.TabIndex = 21;
            this.button55.Text = "+Add copy";
            this.button55.UseVisualStyleBackColor = true;
            this.button55.Click += new System.EventHandler(this.button55_Click);
            // 
            // button50
            // 
            this.button50.Location = new System.Drawing.Point(321, 50);
            this.button50.Name = "button50";
            this.button50.Size = new System.Drawing.Size(75, 23);
            this.button50.TabIndex = 20;
            this.button50.Text = "Change";
            this.button50.UseVisualStyleBackColor = true;
            this.button50.Click += new System.EventHandler(this.button50_Click);
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.button54);
            this.groupBox15.Controls.Add(this.label186);
            this.groupBox15.Controls.Add(this.textBox41);
            this.groupBox15.Controls.Add(this.textBox40);
            this.groupBox15.Controls.Add(this.textBox39);
            this.groupBox15.Controls.Add(this.label184);
            this.groupBox15.Controls.Add(this.label174);
            this.groupBox15.Controls.Add(this.button27);
            this.groupBox15.Controls.Add(this.textBox37);
            this.groupBox15.Controls.Add(this.button44);
            this.groupBox15.Controls.Add(this.label182);
            this.groupBox15.Controls.Add(this.label183);
            this.groupBox15.Controls.Add(this.textBox36);
            this.groupBox15.Controls.Add(this.label181);
            this.groupBox15.Controls.Add(this.textBox38);
            this.groupBox15.Controls.Add(this.label185);
            this.groupBox15.Controls.Add(this.label180);
            this.groupBox15.Controls.Add(this.comboBox_fromTexture);
            this.groupBox15.Controls.Add(this.label179);
            this.groupBox15.Controls.Add(this.textBox35);
            this.groupBox15.Controls.Add(this.label178);
            this.groupBox15.Location = new System.Drawing.Point(12, 163);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(427, 171);
            this.groupBox15.TabIndex = 19;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Настройки кадра";
            // 
            // button54
            // 
            this.button54.Location = new System.Drawing.Point(346, 50);
            this.button54.Name = "button54";
            this.button54.Size = new System.Drawing.Size(75, 23);
            this.button54.TabIndex = 37;
            this.button54.Text = "Применить";
            this.button54.UseVisualStyleBackColor = true;
            this.button54.Click += new System.EventHandler(this.button54_Click);
            // 
            // label186
            // 
            this.label186.AutoSize = true;
            this.label186.Location = new System.Drawing.Point(190, 122);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(47, 13);
            this.label186.TabIndex = 36;
            this.label186.Text = "label186";
            // 
            // textBox41
            // 
            this.textBox41.Location = new System.Drawing.Point(130, 119);
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(36, 20);
            this.textBox41.TabIndex = 35;
            this.textBox41.TextChanged += new System.EventHandler(this.textBox41_TextChanged);
            // 
            // textBox40
            // 
            this.textBox40.Location = new System.Drawing.Point(295, 146);
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new System.Drawing.Size(36, 20);
            this.textBox40.TabIndex = 34;
            this.textBox40.TextChanged += new System.EventHandler(this.textBox40_TextChanged);
            // 
            // textBox39
            // 
            this.textBox39.Location = new System.Drawing.Point(121, 145);
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(36, 20);
            this.textBox39.TabIndex = 33;
            this.textBox39.TextChanged += new System.EventHandler(this.textBox39_TextChanged);
            // 
            // label184
            // 
            this.label184.AutoSize = true;
            this.label184.Location = new System.Drawing.Point(177, 149);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(113, 13);
            this.label184.TabIndex = 27;
            this.label184.Text = "спрайтов по ширине:";
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Location = new System.Drawing.Point(8, 149);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(105, 13);
            this.label174.TabIndex = 26;
            this.label174.Text = "спрайтов по длине:";
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(134, 75);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(40, 23);
            this.button27.TabIndex = 22;
            this.button27.Text = "+Add";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.button27_Click);
            // 
            // textBox37
            // 
            this.textBox37.Location = new System.Drawing.Point(343, 102);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(29, 20);
            this.textBox37.TabIndex = 22;
            this.textBox37.Text = "*";
            // 
            // button44
            // 
            this.button44.Location = new System.Drawing.Point(180, 75);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(40, 23);
            this.button44.TabIndex = 21;
            this.button44.Text = "-Del";
            this.button44.UseVisualStyleBackColor = true;
            this.button44.Click += new System.EventHandler(this.button44_Click);
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.Location = new System.Drawing.Point(242, 105);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(79, 13);
            this.label182.TabIndex = 25;
            this.label182.Text = "или    позиция";
            // 
            // label183
            // 
            this.label183.AutoSize = true;
            this.label183.Location = new System.Drawing.Point(378, 105);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(14, 13);
            this.label183.TabIndex = 24;
            this.label183.Text = "Y";
            // 
            // textBox36
            // 
            this.textBox36.Location = new System.Drawing.Point(88, 119);
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(36, 20);
            this.textBox36.TabIndex = 24;
            this.textBox36.TextChanged += new System.EventHandler(this.textBox36_TextChanged);
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Location = new System.Drawing.Point(10, 122);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(70, 13);
            this.label181.TabIndex = 23;
            this.label181.Text = "Координата:";
            // 
            // textBox38
            // 
            this.textBox38.Location = new System.Drawing.Point(398, 102);
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(29, 20);
            this.textBox38.TabIndex = 21;
            this.textBox38.Text = "*";
            // 
            // label185
            // 
            this.label185.AutoSize = true;
            this.label185.Location = new System.Drawing.Point(323, 105);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(14, 13);
            this.label185.TabIndex = 23;
            this.label185.Text = "Х";
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Location = new System.Drawing.Point(10, 53);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(107, 13);
            this.label180.TabIndex = 22;
            this.label180.Text = "Из какой текстуры:";
            // 
            // comboBox_fromTexture
            // 
            this.comboBox_fromTexture.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_fromTexture.FormattingEnabled = true;
            this.comboBox_fromTexture.Location = new System.Drawing.Point(132, 50);
            this.comboBox_fromTexture.Name = "comboBox_fromTexture";
            this.comboBox_fromTexture.Size = new System.Drawing.Size(208, 21);
            this.comboBox_fromTexture.TabIndex = 20;
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Location = new System.Drawing.Point(129, 27);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(47, 13);
            this.label179.TabIndex = 21;
            this.label179.Text = "label179";
            // 
            // textBox35
            // 
            this.textBox35.Location = new System.Drawing.Point(182, 24);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(97, 20);
            this.textBox35.TabIndex = 20;
            this.textBox35.TextChanged += new System.EventHandler(this.textBox35_TextChanged);
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Location = new System.Drawing.Point(10, 27);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(106, 13);
            this.label178.TabIndex = 0;
            this.label178.Text = "Время показа (сек)";
            // 
            // button51
            // 
            this.button51.Location = new System.Drawing.Point(255, 106);
            this.button51.Name = "button51";
            this.button51.Size = new System.Drawing.Size(40, 23);
            this.button51.TabIndex = 16;
            this.button51.Text = "+Add";
            this.button51.UseVisualStyleBackColor = true;
            this.button51.Click += new System.EventHandler(this.button51_Click);
            // 
            // button52
            // 
            this.button52.Location = new System.Drawing.Point(394, 106);
            this.button52.Name = "button52";
            this.button52.Size = new System.Drawing.Size(40, 23);
            this.button52.TabIndex = 15;
            this.button52.Text = "-Del";
            this.button52.UseVisualStyleBackColor = true;
            this.button52.Click += new System.EventHandler(this.button52_Click);
            // 
            // textBox34
            // 
            this.textBox34.Location = new System.Drawing.Point(218, 52);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(97, 20);
            this.textBox34.TabIndex = 13;
            // 
            // comboBox_frames
            // 
            this.comboBox_frames.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_frames.FormattingEnabled = true;
            this.comboBox_frames.Location = new System.Drawing.Point(72, 79);
            this.comboBox_frames.Name = "comboBox_frames";
            this.comboBox_frames.Size = new System.Drawing.Size(362, 21);
            this.comboBox_frames.TabIndex = 14;
            this.comboBox_frames.SelectedIndexChanged += new System.EventHandler(this.comboBox_frames_SelectedIndexChanged);
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Location = new System.Drawing.Point(275, 28);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(47, 13);
            this.label173.TabIndex = 11;
            this.label173.Text = "label173";
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.Location = new System.Drawing.Point(14, 82);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(43, 13);
            this.label177.TabIndex = 13;
            this.label177.Text = "Кадры:";
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Location = new System.Drawing.Point(79, 55);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(47, 13);
            this.label176.TabIndex = 17;
            this.label176.Text = "label176";
            // 
            // textBox32
            // 
            this.textBox32.Location = new System.Drawing.Point(223, 25);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(29, 20);
            this.textBox32.TabIndex = 8;
            this.textBox32.TextChanged += new System.EventHandler(this.textBox32_TextChanged);
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Location = new System.Drawing.Point(13, 55);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(60, 13);
            this.label175.TabIndex = 16;
            this.label175.Text = "Название:";
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(359, 24);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(80, 17);
            this.checkBox9.TabIndex = 15;
            this.checkBox9.Text = "Зациклить";
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.CheckedChanged += new System.EventHandler(this.checkBox9_CheckedChanged);
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Location = new System.Drawing.Point(203, 28);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(14, 13);
            this.label172.TabIndex = 10;
            this.label172.Text = "Y";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(11, 28);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(126, 13);
            this.label88.TabIndex = 4;
            this.label88.Text = "Длина\\Ширина кадров:";
            // 
            // textBox31
            // 
            this.textBox31.Location = new System.Drawing.Point(168, 25);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(29, 20);
            this.textBox31.TabIndex = 7;
            this.textBox31.TextChanged += new System.EventHandler(this.textBox31_TextChanged);
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Location = new System.Drawing.Point(148, 28);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(14, 13);
            this.label171.TabIndex = 9;
            this.label171.Text = "Х";
            // 
            // comboBox_animations
            // 
            this.comboBox_animations.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_animations.FormattingEnabled = true;
            this.comboBox_animations.Location = new System.Drawing.Point(84, 40);
            this.comboBox_animations.Name = "comboBox_animations";
            this.comboBox_animations.Size = new System.Drawing.Size(222, 21);
            this.comboBox_animations.TabIndex = 5;
            this.comboBox_animations.SelectedIndexChanged += new System.EventHandler(this.comboBox_animations_SelectedIndexChanged);
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(12, 43);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(61, 13);
            this.label87.TabIndex = 3;
            this.label87.Text = "Анимация:";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(204, 15);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(41, 13);
            this.label86.TabIndex = 2;
            this.label86.Text = "label86";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(94, 15);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(90, 13);
            this.label82.TabIndex = 1;
            this.label82.Text = "Проигрывается:";
            // 
            // DrawPage
            // 
            this.DrawPage.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.DrawPage.Location = new System.Drawing.Point(4, 22);
            this.DrawPage.Name = "DrawPage";
            this.DrawPage.Size = new System.Drawing.Size(465, 403);
            this.DrawPage.TabIndex = 4;
            this.DrawPage.Text = "Draw";
            // 
            // BarrierPage
            // 
            this.BarrierPage.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.BarrierPage.Controls.Add(this.isInstantPlatform_checkBox);
            this.BarrierPage.Controls.Add(this.moveWaypoints_checkBox);
            this.BarrierPage.Controls.Add(this.speed_label);
            this.BarrierPage.Controls.Add(this.label29);
            this.BarrierPage.Controls.Add(this.setSpeed_textBox);
            this.BarrierPage.Controls.Add(this.isSensor_checkBox);
            this.BarrierPage.Controls.Add(this.friction_label);
            this.BarrierPage.Controls.Add(this.label107);
            this.BarrierPage.Controls.Add(this.setFriction_textBox);
            this.BarrierPage.Controls.Add(this.mass_label);
            this.BarrierPage.Controls.Add(this.label105);
            this.BarrierPage.Controls.Add(this.setMass_textBox);
            this.BarrierPage.Controls.Add(this.isCircular_checkBox);
            this.BarrierPage.Controls.Add(this.deleteWaypoint_button);
            this.BarrierPage.Controls.Add(this.addWaypoint_button);
            this.BarrierPage.Controls.Add(this.testMoving_checkBox);
            this.BarrierPage.Controls.Add(this.groupBox8);
            this.BarrierPage.Controls.Add(this.groupBox5);
            this.BarrierPage.Location = new System.Drawing.Point(4, 22);
            this.BarrierPage.Name = "BarrierPage";
            this.BarrierPage.Size = new System.Drawing.Size(465, 403);
            this.BarrierPage.TabIndex = 5;
            this.BarrierPage.Text = "Barrier";
            // 
            // isInstantPlatform_checkBox
            // 
            this.isInstantPlatform_checkBox.AutoSize = true;
            this.isInstantPlatform_checkBox.Location = new System.Drawing.Point(318, 256);
            this.isInstantPlatform_checkBox.Name = "isInstantPlatform_checkBox";
            this.isInstantPlatform_checkBox.Size = new System.Drawing.Size(141, 17);
            this.isInstantPlatform_checkBox.TabIndex = 28;
            this.isInstantPlatform_checkBox.Text = "Мгновенное движение";
            this.isInstantPlatform_checkBox.UseVisualStyleBackColor = true;
            this.isInstantPlatform_checkBox.CheckedChanged += new System.EventHandler(this.isInstantPlatform_checkBox_CheckedChanged);
            // 
            // moveWaypoints_checkBox
            // 
            this.moveWaypoints_checkBox.AutoSize = true;
            this.moveWaypoints_checkBox.Location = new System.Drawing.Point(318, 279);
            this.moveWaypoints_checkBox.Name = "moveWaypoints_checkBox";
            this.moveWaypoints_checkBox.Size = new System.Drawing.Size(140, 17);
            this.moveWaypoints_checkBox.TabIndex = 27;
            this.moveWaypoints_checkBox.Text = "двигать все Waypoints";
            this.moveWaypoints_checkBox.UseVisualStyleBackColor = true;
            this.moveWaypoints_checkBox.CheckedChanged += new System.EventHandler(this.moveWaypoints_checkBox_CheckedChanged);
            // 
            // speed_label
            // 
            this.speed_label.AutoSize = true;
            this.speed_label.Location = new System.Drawing.Point(254, 259);
            this.speed_label.Name = "speed_label";
            this.speed_label.Size = new System.Drawing.Size(55, 13);
            this.speed_label.TabIndex = 26;
            this.speed_label.Text = "Скорость";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(193, 259);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(55, 13);
            this.label29.TabIndex = 25;
            this.label29.Text = "Скорость";
            // 
            // setSpeed_textBox
            // 
            this.setSpeed_textBox.Location = new System.Drawing.Point(196, 278);
            this.setSpeed_textBox.Name = "setSpeed_textBox";
            this.setSpeed_textBox.Size = new System.Drawing.Size(112, 20);
            this.setSpeed_textBox.TabIndex = 24;
            this.setSpeed_textBox.TextChanged += new System.EventHandler(this.setSpeed_textBox_TextChanged);
            // 
            // isSensor_checkBox
            // 
            this.isSensor_checkBox.AutoSize = true;
            this.isSensor_checkBox.Location = new System.Drawing.Point(9, 217);
            this.isSensor_checkBox.Name = "isSensor_checkBox";
            this.isSensor_checkBox.Size = new System.Drawing.Size(163, 17);
            this.isSensor_checkBox.TabIndex = 22;
            this.isSensor_checkBox.Text = "не является препятствием";
            this.isSensor_checkBox.UseVisualStyleBackColor = true;
            this.isSensor_checkBox.CheckedChanged += new System.EventHandler(this.isSensor_checkBox_CheckedChanged);
            // 
            // friction_label
            // 
            this.friction_label.AutoSize = true;
            this.friction_label.Location = new System.Drawing.Point(244, 348);
            this.friction_label.Name = "friction_label";
            this.friction_label.Size = new System.Drawing.Size(42, 13);
            this.friction_label.TabIndex = 21;
            this.friction_label.Text = "трение";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(193, 348);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(44, 13);
            this.label107.TabIndex = 20;
            this.label107.Text = "Трение";
            // 
            // setFriction_textBox
            // 
            this.setFriction_textBox.Location = new System.Drawing.Point(196, 367);
            this.setFriction_textBox.Name = "setFriction_textBox";
            this.setFriction_textBox.Size = new System.Drawing.Size(112, 20);
            this.setFriction_textBox.TabIndex = 19;
            this.setFriction_textBox.TextChanged += new System.EventHandler(this.setFriction_textBox_TextChanged);
            // 
            // mass_label
            // 
            this.mass_label.AutoSize = true;
            this.mass_label.Location = new System.Drawing.Point(244, 301);
            this.mass_label.Name = "mass_label";
            this.mass_label.Size = new System.Drawing.Size(40, 13);
            this.mass_label.TabIndex = 17;
            this.mass_label.Text = "Масса";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(193, 301);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(40, 13);
            this.label105.TabIndex = 14;
            this.label105.Text = "Масса";
            // 
            // setMass_textBox
            // 
            this.setMass_textBox.Location = new System.Drawing.Point(196, 320);
            this.setMass_textBox.Name = "setMass_textBox";
            this.setMass_textBox.Size = new System.Drawing.Size(112, 20);
            this.setMass_textBox.TabIndex = 14;
            this.setMass_textBox.TextChanged += new System.EventHandler(this.setMass_textBox_TextChanged);
            // 
            // isCircular_checkBox
            // 
            this.isCircular_checkBox.AutoSize = true;
            this.isCircular_checkBox.Location = new System.Drawing.Point(318, 233);
            this.isCircular_checkBox.Name = "isCircular_checkBox";
            this.isCircular_checkBox.Size = new System.Drawing.Size(146, 17);
            this.isCircular_checkBox.TabIndex = 16;
            this.isCircular_checkBox.Text = "Циклическое движение";
            this.isCircular_checkBox.UseVisualStyleBackColor = true;
            this.isCircular_checkBox.CheckedChanged += new System.EventHandler(this.isCircular_checkBox_CheckedChanged);
            // 
            // deleteWaypoint_button
            // 
            this.deleteWaypoint_button.Location = new System.Drawing.Point(196, 233);
            this.deleteWaypoint_button.Name = "deleteWaypoint_button";
            this.deleteWaypoint_button.Size = new System.Drawing.Size(113, 23);
            this.deleteWaypoint_button.TabIndex = 15;
            this.deleteWaypoint_button.Text = "Удалить точку";
            this.deleteWaypoint_button.UseVisualStyleBackColor = true;
            this.deleteWaypoint_button.Click += new System.EventHandler(this.deleteWaypoint_button_Click);
            // 
            // addWaypoint_button
            // 
            this.addWaypoint_button.Location = new System.Drawing.Point(196, 204);
            this.addWaypoint_button.Name = "addWaypoint_button";
            this.addWaypoint_button.Size = new System.Drawing.Size(113, 23);
            this.addWaypoint_button.TabIndex = 14;
            this.addWaypoint_button.Text = "Добавить точку";
            this.addWaypoint_button.UseVisualStyleBackColor = true;
            this.addWaypoint_button.Click += new System.EventHandler(this.addWaypoint_button_Click);
            // 
            // testMoving_checkBox
            // 
            this.testMoving_checkBox.AutoSize = true;
            this.testMoving_checkBox.Location = new System.Drawing.Point(318, 208);
            this.testMoving_checkBox.Name = "testMoving_checkBox";
            this.testMoving_checkBox.Size = new System.Drawing.Size(106, 17);
            this.testMoving_checkBox.TabIndex = 6;
            this.testMoving_checkBox.Text = "Тест Движения";
            this.testMoving_checkBox.UseVisualStyleBackColor = true;
            this.testMoving_checkBox.CheckedChanged += new System.EventHandler(this.testMoving_checkBox_CheckedChanged);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label102);
            this.groupBox8.Controls.Add(this.label101);
            this.groupBox8.Controls.Add(this.dynamic_radioButton);
            this.groupBox8.Controls.Add(this.moving_radioButton);
            this.groupBox8.Controls.Add(this.staticBarrier_radioButton);
            this.groupBox8.Location = new System.Drawing.Point(9, 240);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(178, 116);
            this.groupBox8.TabIndex = 5;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Тип барьера";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(64, 64);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(58, 13);
            this.label102.TabIndex = 6;
            this.label102.Text = "(\"Ящики\")";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(44, 100);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(101, 13);
            this.label101.TabIndex = 5;
            this.label101.Text = "(двиг. платформы)";
            // 
            // dynamic_radioButton
            // 
            this.dynamic_radioButton.AutoSize = true;
            this.dynamic_radioButton.Location = new System.Drawing.Point(15, 43);
            this.dynamic_radioButton.Name = "dynamic_radioButton";
            this.dynamic_radioButton.Size = new System.Drawing.Size(101, 17);
            this.dynamic_radioButton.TabIndex = 3;
            this.dynamic_radioButton.TabStop = true;
            this.dynamic_radioButton.Text = "Динамический";
            this.dynamic_radioButton.UseVisualStyleBackColor = true;
            this.dynamic_radioButton.CheckedChanged += new System.EventHandler(this.dynamic_radioButton_CheckedChanged);
            // 
            // moving_radioButton
            // 
            this.moving_radioButton.AutoSize = true;
            this.moving_radioButton.Location = new System.Drawing.Point(15, 80);
            this.moving_radioButton.Name = "moving_radioButton";
            this.moving_radioButton.Size = new System.Drawing.Size(113, 17);
            this.moving_radioButton.TabIndex = 4;
            this.moving_radioButton.TabStop = true;
            this.moving_radioButton.Text = "Кинематический ";
            this.moving_radioButton.UseVisualStyleBackColor = true;
            this.moving_radioButton.CheckedChanged += new System.EventHandler(this.moving_radioButton_CheckedChanged);
            // 
            // staticBarrier_radioButton
            // 
            this.staticBarrier_radioButton.AutoSize = true;
            this.staticBarrier_radioButton.Location = new System.Drawing.Point(15, 20);
            this.staticBarrier_radioButton.Name = "staticBarrier_radioButton";
            this.staticBarrier_radioButton.Size = new System.Drawing.Size(97, 17);
            this.staticBarrier_radioButton.TabIndex = 2;
            this.staticBarrier_radioButton.TabStop = true;
            this.staticBarrier_radioButton.Text = "Неподвижный";
            this.staticBarrier_radioButton.UseVisualStyleBackColor = true;
            this.staticBarrier_radioButton.CheckedChanged += new System.EventHandler(this.staticBarrier_radioButton_CheckedChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.isCircleBody_checkBox);
            this.groupBox5.Controls.Add(this.label40);
            this.groupBox5.Controls.Add(this.textBox12);
            this.groupBox5.Controls.Add(this.label38);
            this.groupBox5.Controls.Add(this.button15);
            this.groupBox5.Controls.Add(this.label34);
            this.groupBox5.Controls.Add(this.label39);
            this.groupBox5.Controls.Add(this.textBox10);
            this.groupBox5.Controls.Add(this.label32);
            this.groupBox5.Controls.Add(this.label35);
            this.groupBox5.Controls.Add(this.textBox11);
            this.groupBox5.Controls.Add(this.label37);
            this.groupBox5.Controls.Add(this.label33);
            this.groupBox5.Controls.Add(this.textBox9);
            this.groupBox5.Controls.Add(this.label36);
            this.groupBox5.Location = new System.Drawing.Point(9, 17);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(444, 180);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Область коллизии";
            // 
            // isCircleBody_checkBox
            // 
            this.isCircleBody_checkBox.AutoSize = true;
            this.isCircleBody_checkBox.Location = new System.Drawing.Point(328, 19);
            this.isCircleBody_checkBox.Name = "isCircleBody_checkBox";
            this.isCircleBody_checkBox.Size = new System.Drawing.Size(108, 17);
            this.isCircleBody_checkBox.TabIndex = 29;
            this.isCircleBody_checkBox.Text = "Круглый барьер";
            this.isCircleBody_checkBox.UseVisualStyleBackColor = true;
            this.isCircleBody_checkBox.CheckedChanged += new System.EventHandler(this.isCircleBody_checkBox_CheckedChanged);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(6, 157);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(26, 13);
            this.label40.TabIndex = 13;
            this.label40.Text = "max";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(163, 55);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(100, 20);
            this.textBox12.TabIndex = 12;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(177, 16);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(41, 13);
            this.label38.TabIndex = 11;
            this.label38.Text = "label38";
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(193, 81);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(39, 23);
            this.button15.TabIndex = 2;
            this.button15.Text = "Set";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(355, 107);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(41, 13);
            this.label34.TabIndex = 5;
            this.label34.Text = "label34";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(177, 39);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(79, 13);
            this.label39.TabIndex = 10;
            this.label39.Text = "Отступ сверху";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(238, 81);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(100, 20);
            this.textBox10.TabIndex = 6;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 81);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(75, 13);
            this.label32.TabIndex = 0;
            this.label32.Text = "Отступ слева";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(355, 81);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(81, 13);
            this.label35.TabIndex = 4;
            this.label35.Text = "Отступ справа";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(163, 107);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(100, 20);
            this.textBox11.TabIndex = 9;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(177, 130);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(74, 13);
            this.label37.TabIndex = 7;
            this.label37.Text = "Отступ снизу";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(6, 107);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(41, 13);
            this.label33.TabIndex = 1;
            this.label33.Text = "label33";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(87, 81);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(100, 20);
            this.textBox9.TabIndex = 3;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(177, 157);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(41, 13);
            this.label36.TabIndex = 8;
            this.label36.Text = "label36";
            // 
            // MovementHeroPage
            // 
            this.MovementHeroPage.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.MovementHeroPage.Controls.Add(this.groupBox16);
            this.MovementHeroPage.Controls.Add(this.button47);
            this.MovementHeroPage.Controls.Add(this.button23);
            this.MovementHeroPage.Controls.Add(this.maxJumpSpeed_textBox);
            this.MovementHeroPage.Controls.Add(this.jumpStr_textBox);
            this.MovementHeroPage.Controls.Add(this.runSpeed_textBox);
            this.MovementHeroPage.Controls.Add(this.applyJumpParam_button);
            this.MovementHeroPage.Controls.Add(this.label52);
            this.MovementHeroPage.Controls.Add(this.label51);
            this.MovementHeroPage.Controls.Add(this.label49);
            this.MovementHeroPage.Controls.Add(this.label12);
            this.MovementHeroPage.Controls.Add(this.label11);
            this.MovementHeroPage.Controls.Add(this.label9);
            this.MovementHeroPage.Location = new System.Drawing.Point(4, 22);
            this.MovementHeroPage.Name = "MovementHeroPage";
            this.MovementHeroPage.Size = new System.Drawing.Size(465, 403);
            this.MovementHeroPage.TabIndex = 6;
            this.MovementHeroPage.Text = "MovementHero";
            // 
            // groupBox16
            // 
            this.groupBox16.Location = new System.Drawing.Point(15, 133);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(447, 267);
            this.groupBox16.TabIndex = 30;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Оружие";
            // 
            // button47
            // 
            this.button47.Location = new System.Drawing.Point(313, 29);
            this.button47.Name = "button47";
            this.button47.Size = new System.Drawing.Size(95, 23);
            this.button47.TabIndex = 29;
            this.button47.Text = "Jump sound...";
            this.button47.UseVisualStyleBackColor = true;
            this.button47.Click += new System.EventHandler(this.button47_Click);
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(313, 4);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(95, 23);
            this.button23.TabIndex = 25;
            this.button23.Text = "Run sound...";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // maxJumpSpeed_textBox
            // 
            this.maxJumpSpeed_textBox.Location = new System.Drawing.Point(206, 55);
            this.maxJumpSpeed_textBox.Name = "maxJumpSpeed_textBox";
            this.maxJumpSpeed_textBox.Size = new System.Drawing.Size(95, 20);
            this.maxJumpSpeed_textBox.TabIndex = 24;
            // 
            // jumpStr_textBox
            // 
            this.jumpStr_textBox.Location = new System.Drawing.Point(206, 31);
            this.jumpStr_textBox.Name = "jumpStr_textBox";
            this.jumpStr_textBox.Size = new System.Drawing.Size(95, 20);
            this.jumpStr_textBox.TabIndex = 23;
            // 
            // runSpeed_textBox
            // 
            this.runSpeed_textBox.Location = new System.Drawing.Point(206, 6);
            this.runSpeed_textBox.Name = "runSpeed_textBox";
            this.runSpeed_textBox.Size = new System.Drawing.Size(95, 20);
            this.runSpeed_textBox.TabIndex = 21;
            // 
            // applyJumpParam_button
            // 
            this.applyJumpParam_button.Location = new System.Drawing.Point(206, 81);
            this.applyJumpParam_button.Name = "applyJumpParam_button";
            this.applyJumpParam_button.Size = new System.Drawing.Size(95, 25);
            this.applyJumpParam_button.TabIndex = 20;
            this.applyJumpParam_button.Text = "Apply";
            this.applyJumpParam_button.UseVisualStyleBackColor = true;
            this.applyJumpParam_button.Click += new System.EventHandler(this.applyJumpParam_button_Click);
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(131, 58);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(41, 13);
            this.label52.TabIndex = 17;
            this.label52.Text = "label52";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(131, 36);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(41, 13);
            this.label51.TabIndex = 16;
            this.label51.Text = "label51";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(131, 13);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(41, 13);
            this.label49.TabIndex = 14;
            this.label49.Text = "label49";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 58);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "MaxJumpSpeed";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 36);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "Jump Strenght ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 13);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Run Speed";
            // 
            // RpgCharacterPage
            // 
            this.RpgCharacterPage.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.RpgCharacterPage.Controls.Add(this.textBox1);
            this.RpgCharacterPage.Controls.Add(this.label31);
            this.RpgCharacterPage.Controls.Add(this.label30);
            this.RpgCharacterPage.Controls.Add(this.textBox16);
            this.RpgCharacterPage.Controls.Add(this.label48);
            this.RpgCharacterPage.Controls.Add(this.textBox15);
            this.RpgCharacterPage.Controls.Add(this.label47);
            this.RpgCharacterPage.Controls.Add(this.textBox14);
            this.RpgCharacterPage.Controls.Add(this.label46);
            this.RpgCharacterPage.Controls.Add(this.label45);
            this.RpgCharacterPage.Controls.Add(this.label44);
            this.RpgCharacterPage.Controls.Add(this.label43);
            this.RpgCharacterPage.Controls.Add(this.textBox13);
            this.RpgCharacterPage.Controls.Add(this.label42);
            this.RpgCharacterPage.Controls.Add(this.label41);
            this.RpgCharacterPage.Location = new System.Drawing.Point(4, 22);
            this.RpgCharacterPage.Name = "RpgCharacterPage";
            this.RpgCharacterPage.Size = new System.Drawing.Size(465, 403);
            this.RpgCharacterPage.TabIndex = 7;
            this.RpgCharacterPage.Text = "RpgCharacter";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(88, 81);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 15;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(53, 81);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(41, 13);
            this.label31.TabIndex = 14;
            this.label31.Text = "label31";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(13, 81);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(33, 13);
            this.label30.TabIndex = 13;
            this.label30.Text = "Coins";
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(347, 46);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(100, 20);
            this.textBox16.TabIndex = 12;
            this.textBox16.TextChanged += new System.EventHandler(this.textBox16_TextChanged);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(300, 46);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(41, 13);
            this.label48.TabIndex = 11;
            this.label48.Text = "label48";
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(88, 39);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(100, 20);
            this.textBox15.TabIndex = 10;
            this.textBox15.TextChanged += new System.EventHandler(this.textBox15_TextChanged);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(42, 42);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(41, 13);
            this.label47.TabIndex = 9;
            this.label47.Text = "label47";
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(347, 17);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(100, 20);
            this.textBox14.TabIndex = 8;
            this.textBox14.TextChanged += new System.EventHandler(this.textBox14_TextChanged);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(300, 20);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(41, 13);
            this.label46.TabIndex = 7;
            this.label46.Text = "label46";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(248, 46);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(46, 13);
            this.label45.TabIndex = 6;
            this.label45.Text = "Max MP";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(13, 42);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(23, 13);
            this.label44.TabIndex = 5;
            this.label44.Text = "MP";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(249, 20);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(45, 13);
            this.label43.TabIndex = 4;
            this.label43.Text = "Max HP";
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(88, 13);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(100, 20);
            this.textBox13.TabIndex = 3;
            this.textBox13.TextChanged += new System.EventHandler(this.textBox13_TextChanged);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(41, 20);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(41, 13);
            this.label42.TabIndex = 2;
            this.label42.Text = "label42";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(13, 20);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(22, 13);
            this.label41.TabIndex = 1;
            this.label41.Text = "HP";
            // 
            // BonusPage
            // 
            this.BonusPage.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.BonusPage.Controls.Add(this.setBounceParam_button);
            this.BonusPage.Controls.Add(this.label14);
            this.BonusPage.Controls.Add(this.period_textBox);
            this.BonusPage.Controls.Add(this.label13);
            this.BonusPage.Controls.Add(this.amplitude_textBox);
            this.BonusPage.Controls.Add(this.bounceBonus_checkBox);
            this.BonusPage.Controls.Add(this.button19);
            this.BonusPage.Controls.Add(this.label63);
            this.BonusPage.Controls.Add(this.textBox20);
            this.BonusPage.Controls.Add(this.button18);
            this.BonusPage.Controls.Add(this.label62);
            this.BonusPage.Controls.Add(this.label61);
            this.BonusPage.Controls.Add(this.textBox19);
            this.BonusPage.Controls.Add(this.label60);
            this.BonusPage.Controls.Add(this.button17);
            this.BonusPage.Controls.Add(this.comboBox3);
            this.BonusPage.Controls.Add(this.label59);
            this.BonusPage.Location = new System.Drawing.Point(4, 22);
            this.BonusPage.Name = "BonusPage";
            this.BonusPage.Size = new System.Drawing.Size(465, 403);
            this.BonusPage.TabIndex = 8;
            this.BonusPage.Text = "Bonus";
            // 
            // setBounceParam_button
            // 
            this.setBounceParam_button.Location = new System.Drawing.Point(177, 110);
            this.setBounceParam_button.Name = "setBounceParam_button";
            this.setBounceParam_button.Size = new System.Drawing.Size(75, 23);
            this.setBounceParam_button.TabIndex = 18;
            this.setBounceParam_button.Text = "Set";
            this.setBounceParam_button.UseVisualStyleBackColor = true;
            this.setBounceParam_button.Click += new System.EventHandler(this.setBounceParam_button_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(16, 141);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 13);
            this.label14.TabIndex = 17;
            this.label14.Text = "Период";
            // 
            // period_textBox
            // 
            this.period_textBox.Location = new System.Drawing.Point(89, 138);
            this.period_textBox.Name = "period_textBox";
            this.period_textBox.Size = new System.Drawing.Size(73, 20);
            this.period_textBox.TabIndex = 16;
            this.period_textBox.Text = "10";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 115);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 13);
            this.label13.TabIndex = 15;
            this.label13.Text = "Амплитуда";
            // 
            // amplitude_textBox
            // 
            this.amplitude_textBox.Location = new System.Drawing.Point(89, 112);
            this.amplitude_textBox.Name = "amplitude_textBox";
            this.amplitude_textBox.Size = new System.Drawing.Size(73, 20);
            this.amplitude_textBox.TabIndex = 14;
            this.amplitude_textBox.Text = "15";
            // 
            // bounceBonus_checkBox
            // 
            this.bounceBonus_checkBox.AutoSize = true;
            this.bounceBonus_checkBox.Location = new System.Drawing.Point(19, 89);
            this.bounceBonus_checkBox.Name = "bounceBonus_checkBox";
            this.bounceBonus_checkBox.Size = new System.Drawing.Size(128, 17);
            this.bounceBonus_checkBox.TabIndex = 13;
            this.bounceBonus_checkBox.Text = "Прыгающие бонусы";
            this.bounceBonus_checkBox.UseVisualStyleBackColor = true;
            this.bounceBonus_checkBox.CheckedChanged += new System.EventHandler(this.bounceBonus_checkBox_CheckedChanged);
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(9, 357);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(147, 32);
            this.button19.TabIndex = 12;
            this.button19.Text = "Down";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(233, 337);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(232, 52);
            this.label63.TabIndex = 11;
            this.label63.Text = "Если до ближайшего барьера расстояние \r\nменьше указанного то объект \"падает\", \r\nп" +
    "ока не будет на необходимой дистанции.\r\nЕсли под ним ничего нет то он не двигает" +
    "ся";
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(162, 344);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(62, 20);
            this.textBox20.TabIndex = 10;
            this.textBox20.Text = "100";
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(9, 318);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(147, 32);
            this.button18.TabIndex = 7;
            this.button18.Text = "Top";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(86, 54);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(41, 13);
            this.label62.TabIndex = 6;
            this.label62.Text = "label62";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(86, 18);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(41, 13);
            this.label61.TabIndex = 5;
            this.label61.Text = "label61";
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(216, 47);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(73, 20);
            this.textBox19.TabIndex = 4;
            this.textBox19.TextChanged += new System.EventHandler(this.textBox19_TextChanged);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(6, 54);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(55, 13);
            this.label60.TabIndex = 3;
            this.label60.Text = "Значение";
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(374, 13);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(75, 23);
            this.button17.TabIndex = 2;
            this.button17.Text = "ОК";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // comboBox3
            // 
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(216, 15);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(152, 21);
            this.comboBox3.TabIndex = 1;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(6, 18);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(67, 13);
            this.label59.TabIndex = 0;
            this.label59.Text = "Тип бонуса:";
            // 
            // PortalPage
            // 
            this.PortalPage.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.PortalPage.Controls.Add(this.isInteractive_checkBox);
            this.PortalPage.Controls.Add(this.isInstant_checkBox);
            this.PortalPage.Controls.Add(this.label90);
            this.PortalPage.Controls.Add(this.exitTime_textBox);
            this.PortalPage.Controls.Add(this.label89);
            this.PortalPage.Controls.Add(this.transitionTime_textBox);
            this.PortalPage.Controls.Add(this.isExit_checkBox);
            this.PortalPage.Controls.Add(this.twoSides_checkBox);
            this.PortalPage.Location = new System.Drawing.Point(4, 22);
            this.PortalPage.Name = "PortalPage";
            this.PortalPage.Padding = new System.Windows.Forms.Padding(3);
            this.PortalPage.Size = new System.Drawing.Size(465, 403);
            this.PortalPage.TabIndex = 9;
            this.PortalPage.Text = "Portal";
            // 
            // isInteractive_checkBox
            // 
            this.isInteractive_checkBox.AutoSize = true;
            this.isInteractive_checkBox.Location = new System.Drawing.Point(45, 149);
            this.isInteractive_checkBox.Name = "isInteractive_checkBox";
            this.isInteractive_checkBox.Size = new System.Drawing.Size(152, 17);
            this.isInteractive_checkBox.TabIndex = 7;
            this.isInteractive_checkBox.Text = "Вход по нажатию кнопки";
            this.isInteractive_checkBox.UseVisualStyleBackColor = true;
            this.isInteractive_checkBox.CheckedChanged += new System.EventHandler(this.isInteractive_checkBox_CheckedChanged);
            // 
            // isInstant_checkBox
            // 
            this.isInstant_checkBox.AutoSize = true;
            this.isInstant_checkBox.Location = new System.Drawing.Point(45, 113);
            this.isInstant_checkBox.Name = "isInstant_checkBox";
            this.isInstant_checkBox.Size = new System.Drawing.Size(140, 30);
            this.isInstant_checkBox.TabIndex = 6;
            this.isInstant_checkBox.Text = "Мгновенное\r\nперемещение камеры";
            this.isInstant_checkBox.UseVisualStyleBackColor = true;
            this.isInstant_checkBox.CheckedChanged += new System.EventHandler(this.isInstant_checkBox_CheckedChanged);
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(191, 81);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(123, 13);
            this.label90.TabIndex = 5;
            this.label90.Text = "Простой после выхода";
            // 
            // exitTime_textBox
            // 
            this.exitTime_textBox.Location = new System.Drawing.Point(147, 78);
            this.exitTime_textBox.Name = "exitTime_textBox";
            this.exitTime_textBox.Size = new System.Drawing.Size(38, 20);
            this.exitTime_textBox.TabIndex = 4;
            this.exitTime_textBox.Text = "0";
            this.exitTime_textBox.TextChanged += new System.EventHandler(this.exitTime_textBox_TextChanged);
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(191, 47);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(105, 13);
            this.label89.TabIndex = 3;
            this.label89.Text = "Задержка на входе";
            // 
            // transitionTime_textBox
            // 
            this.transitionTime_textBox.Location = new System.Drawing.Point(147, 44);
            this.transitionTime_textBox.Name = "transitionTime_textBox";
            this.transitionTime_textBox.Size = new System.Drawing.Size(38, 20);
            this.transitionTime_textBox.TabIndex = 2;
            this.transitionTime_textBox.Text = "0";
            this.transitionTime_textBox.TextChanged += new System.EventHandler(this.transitionTime_textBox_TextChanged);
            // 
            // isExit_checkBox
            // 
            this.isExit_checkBox.AutoSize = true;
            this.isExit_checkBox.Enabled = false;
            this.isExit_checkBox.Location = new System.Drawing.Point(45, 77);
            this.isExit_checkBox.Name = "isExit_checkBox";
            this.isExit_checkBox.Size = new System.Drawing.Size(58, 17);
            this.isExit_checkBox.TabIndex = 1;
            this.isExit_checkBox.Text = "Выход";
            this.isExit_checkBox.UseVisualStyleBackColor = true;
            // 
            // twoSides_checkBox
            // 
            this.twoSides_checkBox.AutoSize = true;
            this.twoSides_checkBox.Location = new System.Drawing.Point(45, 43);
            this.twoSides_checkBox.Name = "twoSides_checkBox";
            this.twoSides_checkBox.Size = new System.Drawing.Size(96, 17);
            this.twoSides_checkBox.TabIndex = 0;
            this.twoSides_checkBox.Text = "Двустронний ";
            this.twoSides_checkBox.UseVisualStyleBackColor = true;
            this.twoSides_checkBox.CheckedChanged += new System.EventHandler(this.twoSides_checkBox_CheckedChanged);
            // 
            // ToolTipPage
            // 
            this.ToolTipPage.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ToolTipPage.Controls.Add(this.label99);
            this.ToolTipPage.Controls.Add(this.label98);
            this.ToolTipPage.Controls.Add(this.omega_textBox);
            this.ToolTipPage.Controls.Add(this.appearTime_textBox);
            this.ToolTipPage.Controls.Add(this.label97);
            this.ToolTipPage.Controls.Add(this.label93);
            this.ToolTipPage.Controls.Add(this.fontLoad_button);
            this.ToolTipPage.Controls.Add(this.bgLoad_button);
            this.ToolTipPage.Controls.Add(this.label96);
            this.ToolTipPage.Controls.Add(this.label95);
            this.ToolTipPage.Controls.Add(this.label94);
            this.ToolTipPage.Controls.Add(this.label92);
            this.ToolTipPage.Controls.Add(this.label91);
            this.ToolTipPage.Controls.Add(this.ttText_textBox);
            this.ToolTipPage.Controls.Add(this.isAlwaysShow_checkBox);
            this.ToolTipPage.Controls.Add(this.ttOffsetY_numericUpDown);
            this.ToolTipPage.Controls.Add(this.ttOffsetX_numericUpDown);
            this.ToolTipPage.Controls.Add(this.ttWidth_numericUpDown);
            this.ToolTipPage.Controls.Add(this.ttHeight_numericUpDown);
            this.ToolTipPage.Location = new System.Drawing.Point(4, 22);
            this.ToolTipPage.Name = "ToolTipPage";
            this.ToolTipPage.Padding = new System.Windows.Forms.Padding(3);
            this.ToolTipPage.Size = new System.Drawing.Size(465, 403);
            this.ToolTipPage.TabIndex = 10;
            this.ToolTipPage.Text = "ToolTip";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(376, 194);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(28, 13);
            this.label99.TabIndex = 19;
            this.label99.Text = "Font";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(360, 148);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(65, 13);
            this.label98.TabIndex = 18;
            this.label98.Text = "Background";
            // 
            // omega_textBox
            // 
            this.omega_textBox.Location = new System.Drawing.Point(315, 103);
            this.omega_textBox.Name = "omega_textBox";
            this.omega_textBox.Size = new System.Drawing.Size(100, 20);
            this.omega_textBox.TabIndex = 17;
            this.omega_textBox.TextChanged += new System.EventHandler(this.omega_textBox_TextChanged);
            // 
            // appearTime_textBox
            // 
            this.appearTime_textBox.Location = new System.Drawing.Point(315, 76);
            this.appearTime_textBox.Name = "appearTime_textBox";
            this.appearTime_textBox.Size = new System.Drawing.Size(100, 20);
            this.appearTime_textBox.TabIndex = 16;
            this.appearTime_textBox.TextChanged += new System.EventHandler(this.appearTime_textBox_TextChanged);
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(197, 103);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(111, 26);
            this.label97.TabIndex = 15;
            this.label97.Text = "Частота колебаний\r\nво время появления";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(211, 79);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(97, 13);
            this.label93.TabIndex = 14;
            this.label93.Text = "Время появления";
            // 
            // fontLoad_button
            // 
            this.fontLoad_button.Location = new System.Drawing.Point(327, 212);
            this.fontLoad_button.Name = "fontLoad_button";
            this.fontLoad_button.Size = new System.Drawing.Size(132, 23);
            this.fontLoad_button.TabIndex = 13;
            this.fontLoad_button.Text = "font";
            this.toolTip1.SetToolTip(this.fontLoad_button, "000999");
            this.fontLoad_button.UseVisualStyleBackColor = true;
            this.fontLoad_button.Click += new System.EventHandler(this.fontLoad_button_Click);
            // 
            // bgLoad_button
            // 
            this.bgLoad_button.Location = new System.Drawing.Point(327, 166);
            this.bgLoad_button.Name = "bgLoad_button";
            this.bgLoad_button.Size = new System.Drawing.Size(132, 23);
            this.bgLoad_button.TabIndex = 12;
            this.bgLoad_button.Text = "background";
            this.toolTip1.SetToolTip(this.bgLoad_button, "3333");
            this.bgLoad_button.UseVisualStyleBackColor = true;
            this.bgLoad_button.Click += new System.EventHandler(this.bgLoad_button_Click);
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(211, 47);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(86, 13);
            this.label96.TabIndex = 11;
            this.label96.Text = "Смещение по Y";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(77, 42);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(46, 13);
            this.label95.TabIndex = 10;
            this.label95.Text = "Ширина";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(211, 16);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(86, 13);
            this.label94.TabIndex = 9;
            this.label94.Text = "Смещение по X";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(77, 16);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(45, 13);
            this.label92.TabIndex = 7;
            this.label92.Text = "Высота";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(6, 135);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(79, 13);
            this.label91.TabIndex = 6;
            this.label91.Text = "Текст тултипа";
            // 
            // ttText_textBox
            // 
            this.ttText_textBox.Location = new System.Drawing.Point(6, 151);
            this.ttText_textBox.Multiline = true;
            this.ttText_textBox.Name = "ttText_textBox";
            this.ttText_textBox.Size = new System.Drawing.Size(315, 84);
            this.ttText_textBox.TabIndex = 5;
            this.ttText_textBox.Text = "Текст";
            this.ttText_textBox.TextChanged += new System.EventHandler(this.ttText_textBox_TextChanged);
            // 
            // isAlwaysShow_checkBox
            // 
            this.isAlwaysShow_checkBox.AutoSize = true;
            this.isAlwaysShow_checkBox.ForeColor = System.Drawing.Color.Black;
            this.isAlwaysShow_checkBox.Location = new System.Drawing.Point(9, 115);
            this.isAlwaysShow_checkBox.Name = "isAlwaysShow_checkBox";
            this.isAlwaysShow_checkBox.Size = new System.Drawing.Size(126, 17);
            this.isAlwaysShow_checkBox.TabIndex = 4;
            this.isAlwaysShow_checkBox.Text = "Всегда показывать";
            this.isAlwaysShow_checkBox.UseVisualStyleBackColor = true;
            this.isAlwaysShow_checkBox.CheckedChanged += new System.EventHandler(this.isAlwaysShow_checkBox_CheckedChanged);
            // 
            // ttOffsetY_numericUpDown
            // 
            this.ttOffsetY_numericUpDown.Location = new System.Drawing.Point(303, 40);
            this.ttOffsetY_numericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.ttOffsetY_numericUpDown.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.ttOffsetY_numericUpDown.Name = "ttOffsetY_numericUpDown";
            this.ttOffsetY_numericUpDown.Size = new System.Drawing.Size(60, 20);
            this.ttOffsetY_numericUpDown.TabIndex = 3;
            this.ttOffsetY_numericUpDown.ValueChanged += new System.EventHandler(this.ttOffsetY_numericUpDown_ValueChanged);
            // 
            // ttOffsetX_numericUpDown
            // 
            this.ttOffsetX_numericUpDown.Location = new System.Drawing.Point(303, 14);
            this.ttOffsetX_numericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.ttOffsetX_numericUpDown.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.ttOffsetX_numericUpDown.Name = "ttOffsetX_numericUpDown";
            this.ttOffsetX_numericUpDown.Size = new System.Drawing.Size(60, 20);
            this.ttOffsetX_numericUpDown.TabIndex = 2;
            this.ttOffsetX_numericUpDown.ValueChanged += new System.EventHandler(this.ttOffsetX_numericUpDown_ValueChanged);
            // 
            // ttWidth_numericUpDown
            // 
            this.ttWidth_numericUpDown.Location = new System.Drawing.Point(9, 40);
            this.ttWidth_numericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.ttWidth_numericUpDown.Name = "ttWidth_numericUpDown";
            this.ttWidth_numericUpDown.Size = new System.Drawing.Size(60, 20);
            this.ttWidth_numericUpDown.TabIndex = 1;
            this.ttWidth_numericUpDown.ValueChanged += new System.EventHandler(this.ttWidth_numericUpDown_ValueChanged);
            // 
            // ttHeight_numericUpDown
            // 
            this.ttHeight_numericUpDown.Location = new System.Drawing.Point(9, 14);
            this.ttHeight_numericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.ttHeight_numericUpDown.Name = "ttHeight_numericUpDown";
            this.ttHeight_numericUpDown.Size = new System.Drawing.Size(60, 20);
            this.ttHeight_numericUpDown.TabIndex = 0;
            this.ttHeight_numericUpDown.ValueChanged += new System.EventHandler(this.ttHeight_numericUpDown_ValueChanged);
            // 
            // DeathZonePage
            // 
            this.DeathZonePage.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.DeathZonePage.Controls.Add(this.label104);
            this.DeathZonePage.Controls.Add(this.radioButton2);
            this.DeathZonePage.Controls.Add(this.radioButton1);
            this.DeathZonePage.Controls.Add(this.textBox3);
            this.DeathZonePage.Controls.Add(this.label103);
            this.DeathZonePage.Controls.Add(this.button32);
            this.DeathZonePage.Controls.Add(this.checkBox6);
            this.DeathZonePage.Location = new System.Drawing.Point(4, 22);
            this.DeathZonePage.Name = "DeathZonePage";
            this.DeathZonePage.Size = new System.Drawing.Size(465, 403);
            this.DeathZonePage.TabIndex = 11;
            this.DeathZonePage.Text = "DeathZone";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(69, 46);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(11, 13);
            this.label104.TabIndex = 6;
            this.label104.Text = "*";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(234, 55);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(98, 17);
            this.radioButton2.TabIndex = 5;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "% max hp \\ sec";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(234, 32);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(65, 17);
            this.radioButton1.TabIndex = 4;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "hp \\ sec";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(128, 43);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 3;
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(6, 47);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(57, 13);
            this.label103.TabIndex = 2;
            this.label103.Text = "Отнимает";
            // 
            // button32
            // 
            this.button32.Location = new System.Drawing.Point(352, 41);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(75, 23);
            this.button32.TabIndex = 1;
            this.button32.Text = "Set";
            this.button32.UseVisualStyleBackColor = true;
            this.button32.Click += new System.EventHandler(this.button32_Click);
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(9, 17);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(139, 17);
            this.checkBox6.TabIndex = 0;
            this.checkBox6.Text = "Моментально убивает";
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.CheckedChanged += new System.EventHandler(this.checkBox6_CheckedChanged);
            // 
            // DialogPage
            // 
            this.DialogPage.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.DialogPage.Controls.Add(this.setActorTexture_button);
            this.DialogPage.Controls.Add(this.deleteDialogScreen_button);
            this.DialogPage.Controls.Add(this.deleteAnswer_button);
            this.DialogPage.Controls.Add(this.label158);
            this.DialogPage.Controls.Add(this.dialogText_textBox);
            this.DialogPage.Controls.Add(this.newAnswer_button);
            this.DialogPage.Controls.Add(this.screenId_label);
            this.DialogPage.Controls.Add(this.label156);
            this.DialogPage.Controls.Add(this.addDialogScreen_button);
            this.DialogPage.Controls.Add(this.label154);
            this.DialogPage.Controls.Add(this.label133);
            this.DialogPage.Controls.Add(this.nextDialogScreen_comboBox);
            this.DialogPage.Controls.Add(this.answers_listBox);
            this.DialogPage.Controls.Add(this.label132);
            this.DialogPage.Controls.Add(this.dialogScreenList_comboBox);
            this.DialogPage.Controls.Add(this.currentAnswerText_textBox);
            this.DialogPage.Location = new System.Drawing.Point(4, 22);
            this.DialogPage.Name = "DialogPage";
            this.DialogPage.Size = new System.Drawing.Size(465, 403);
            this.DialogPage.TabIndex = 12;
            this.DialogPage.Text = "Dialog";
            this.DialogPage.Enter += new System.EventHandler(this.DialogPage_Enter);
            this.DialogPage.Validated += new System.EventHandler(this.DialogPage_Enter);
            // 
            // setActorTexture_button
            // 
            this.setActorTexture_button.Location = new System.Drawing.Point(324, 80);
            this.setActorTexture_button.Name = "setActorTexture_button";
            this.setActorTexture_button.Size = new System.Drawing.Size(98, 23);
            this.setActorTexture_button.TabIndex = 16;
            this.setActorTexture_button.Text = "Персонаж";
            this.setActorTexture_button.UseVisualStyleBackColor = true;
            this.setActorTexture_button.Click += new System.EventHandler(this.setActorTexture_button_Click);
            // 
            // deleteDialogScreen_button
            // 
            this.deleteDialogScreen_button.Location = new System.Drawing.Point(247, 43);
            this.deleteDialogScreen_button.Name = "deleteDialogScreen_button";
            this.deleteDialogScreen_button.Size = new System.Drawing.Size(98, 23);
            this.deleteDialogScreen_button.TabIndex = 15;
            this.deleteDialogScreen_button.Text = "Удалить экран";
            this.deleteDialogScreen_button.UseVisualStyleBackColor = true;
            this.deleteDialogScreen_button.Click += new System.EventHandler(this.deleteDialogScreen_button_Click);
            // 
            // deleteAnswer_button
            // 
            this.deleteAnswer_button.Location = new System.Drawing.Point(270, 211);
            this.deleteAnswer_button.Name = "deleteAnswer_button";
            this.deleteAnswer_button.Size = new System.Drawing.Size(98, 23);
            this.deleteAnswer_button.TabIndex = 14;
            this.deleteAnswer_button.Text = "Удалить";
            this.deleteAnswer_button.UseVisualStyleBackColor = true;
            this.deleteAnswer_button.Click += new System.EventHandler(this.deleteAnswer_button_Click);
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Location = new System.Drawing.Point(22, 64);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(81, 13);
            this.label158.TabIndex = 13;
            this.label158.Text = "Текст диалога";
            // 
            // dialogText_textBox
            // 
            this.dialogText_textBox.Location = new System.Drawing.Point(25, 80);
            this.dialogText_textBox.Multiline = true;
            this.dialogText_textBox.Name = "dialogText_textBox";
            this.dialogText_textBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dialogText_textBox.Size = new System.Drawing.Size(290, 81);
            this.dialogText_textBox.TabIndex = 12;
            this.dialogText_textBox.TextChanged += new System.EventHandler(this.dialogText_textBox_TextChanged);
            // 
            // newAnswer_button
            // 
            this.newAnswer_button.Location = new System.Drawing.Point(270, 182);
            this.newAnswer_button.Name = "newAnswer_button";
            this.newAnswer_button.Size = new System.Drawing.Size(98, 23);
            this.newAnswer_button.TabIndex = 11;
            this.newAnswer_button.Text = "Новый ответ";
            this.newAnswer_button.UseVisualStyleBackColor = true;
            this.newAnswer_button.Click += new System.EventHandler(this.newAnswer_button_Click);
            // 
            // screenId_label
            // 
            this.screenId_label.AutoSize = true;
            this.screenId_label.Location = new System.Drawing.Point(428, 17);
            this.screenId_label.Name = "screenId_label";
            this.screenId_label.Size = new System.Drawing.Size(19, 13);
            this.screenId_label.TabIndex = 10;
            this.screenId_label.Text = "00";
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Location = new System.Drawing.Point(365, 17);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(57, 13);
            this.label156.TabIndex = 9;
            this.label156.Text = "ID экрана";
            // 
            // addDialogScreen_button
            // 
            this.addDialogScreen_button.Location = new System.Drawing.Point(247, 14);
            this.addDialogScreen_button.Name = "addDialogScreen_button";
            this.addDialogScreen_button.Size = new System.Drawing.Size(98, 23);
            this.addDialogScreen_button.TabIndex = 7;
            this.addDialogScreen_button.Text = "Новый экран";
            this.addDialogScreen_button.UseVisualStyleBackColor = true;
            this.addDialogScreen_button.Click += new System.EventHandler(this.addDialogScreen_button_Click);
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Location = new System.Drawing.Point(22, 264);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(88, 13);
            this.label154.TabIndex = 6;
            this.label154.Text = "Ответ подробно";
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(87, 380);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(59, 13);
            this.label133.TabIndex = 5;
            this.label133.Text = "Переход к";
            // 
            // nextDialogScreen_comboBox
            // 
            this.nextDialogScreen_comboBox.FormattingEnabled = true;
            this.nextDialogScreen_comboBox.Location = new System.Drawing.Point(155, 377);
            this.nextDialogScreen_comboBox.Name = "nextDialogScreen_comboBox";
            this.nextDialogScreen_comboBox.Size = new System.Drawing.Size(121, 21);
            this.nextDialogScreen_comboBox.TabIndex = 4;
            this.nextDialogScreen_comboBox.SelectedIndexChanged += new System.EventHandler(this.nextDialogScreen_comboBox_SelectedIndexChanged);
            // 
            // answers_listBox
            // 
            this.answers_listBox.FormattingEnabled = true;
            this.answers_listBox.Items.AddRange(new object[] {
            "Первый ответ",
            "Второй ответ"});
            this.answers_listBox.Location = new System.Drawing.Point(25, 182);
            this.answers_listBox.Name = "answers_listBox";
            this.answers_listBox.Size = new System.Drawing.Size(239, 69);
            this.answers_listBox.TabIndex = 3;
            this.answers_listBox.SelectedIndexChanged += new System.EventHandler(this.answers_listBox_SelectedIndexChanged);
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(152, 17);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(89, 13);
            this.label132.TabIndex = 2;
            this.label132.Text = "Список экранов";
            // 
            // dialogScreenList_comboBox
            // 
            this.dialogScreenList_comboBox.FormattingEnabled = true;
            this.dialogScreenList_comboBox.Location = new System.Drawing.Point(25, 14);
            this.dialogScreenList_comboBox.Name = "dialogScreenList_comboBox";
            this.dialogScreenList_comboBox.Size = new System.Drawing.Size(121, 21);
            this.dialogScreenList_comboBox.TabIndex = 1;
            this.dialogScreenList_comboBox.SelectedIndexChanged += new System.EventHandler(this.dialogScreenList_comboBox_SelectedIndexChanged);
            this.dialogScreenList_comboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dialogScreenList_comboBox_KeyDown);
            // 
            // currentAnswerText_textBox
            // 
            this.currentAnswerText_textBox.Location = new System.Drawing.Point(25, 290);
            this.currentAnswerText_textBox.Multiline = true;
            this.currentAnswerText_textBox.Name = "currentAnswerText_textBox";
            this.currentAnswerText_textBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.currentAnswerText_textBox.Size = new System.Drawing.Size(290, 81);
            this.currentAnswerText_textBox.TabIndex = 0;
            this.currentAnswerText_textBox.TextChanged += new System.EventHandler(this.currentAnswerText_textBox_TextChanged);
            // 
            // LightPage
            // 
            this.LightPage.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.LightPage.Controls.Add(this.gammaLight_numericUpDown);
            this.LightPage.Controls.Add(this.label155);
            this.LightPage.Controls.Add(this.directionAngle_numericUpDown);
            this.LightPage.Controls.Add(this.innerAngle_numericUpDown);
            this.LightPage.Controls.Add(this.outerAngle_numericUpDown);
            this.LightPage.Controls.Add(this.lightColor_button);
            this.LightPage.Controls.Add(this.label142);
            this.LightPage.Controls.Add(this.label141);
            this.LightPage.Controls.Add(this.label140);
            this.LightPage.Controls.Add(this.label139);
            this.LightPage.Controls.Add(this.label138);
            this.LightPage.Controls.Add(this.radius_textBox);
            this.LightPage.Controls.Add(this.label137);
            this.LightPage.Controls.Add(this.label135);
            this.LightPage.Controls.Add(this.label134);
            this.LightPage.Controls.Add(this.lightOffsetY_textBox);
            this.LightPage.Controls.Add(this.lightOffsetX_textBox);
            this.LightPage.Controls.Add(this.lights_comboBox);
            this.LightPage.Location = new System.Drawing.Point(4, 22);
            this.LightPage.Name = "LightPage";
            this.LightPage.Padding = new System.Windows.Forms.Padding(3);
            this.LightPage.Size = new System.Drawing.Size(465, 403);
            this.LightPage.TabIndex = 13;
            this.LightPage.Text = "Light";
            // 
            // gammaLight_numericUpDown
            // 
            this.gammaLight_numericUpDown.Location = new System.Drawing.Point(119, 103);
            this.gammaLight_numericUpDown.Maximum = new decimal(new int[] {
            3600,
            0,
            0,
            0});
            this.gammaLight_numericUpDown.Minimum = new decimal(new int[] {
            3600,
            0,
            0,
            -2147483648});
            this.gammaLight_numericUpDown.Name = "gammaLight_numericUpDown";
            this.gammaLight_numericUpDown.Size = new System.Drawing.Size(57, 20);
            this.gammaLight_numericUpDown.TabIndex = 21;
            this.gammaLight_numericUpDown.ValueChanged += new System.EventHandler(this.gammaLight_numericUpDown_ValueChanged);
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Location = new System.Drawing.Point(123, 87);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(50, 13);
            this.label155.TabIndex = 20;
            this.label155.Text = "Яркость";
            // 
            // directionAngle_numericUpDown
            // 
            this.directionAngle_numericUpDown.Location = new System.Drawing.Point(11, 144);
            this.directionAngle_numericUpDown.Maximum = new decimal(new int[] {
            3600,
            0,
            0,
            0});
            this.directionAngle_numericUpDown.Minimum = new decimal(new int[] {
            3600,
            0,
            0,
            -2147483648});
            this.directionAngle_numericUpDown.Name = "directionAngle_numericUpDown";
            this.directionAngle_numericUpDown.Size = new System.Drawing.Size(57, 20);
            this.directionAngle_numericUpDown.TabIndex = 18;
            this.directionAngle_numericUpDown.ValueChanged += new System.EventHandler(this.directionAngle_numericUpDown_ValueChanged);
            // 
            // innerAngle_numericUpDown
            // 
            this.innerAngle_numericUpDown.Location = new System.Drawing.Point(268, 91);
            this.innerAngle_numericUpDown.Maximum = new decimal(new int[] {
            3600,
            0,
            0,
            0});
            this.innerAngle_numericUpDown.Minimum = new decimal(new int[] {
            3600,
            0,
            0,
            -2147483648});
            this.innerAngle_numericUpDown.Name = "innerAngle_numericUpDown";
            this.innerAngle_numericUpDown.Size = new System.Drawing.Size(57, 20);
            this.innerAngle_numericUpDown.TabIndex = 17;
            this.innerAngle_numericUpDown.ValueChanged += new System.EventHandler(this.innerAngle_numericUpDown_ValueChanged);
            // 
            // outerAngle_numericUpDown
            // 
            this.outerAngle_numericUpDown.Location = new System.Drawing.Point(268, 60);
            this.outerAngle_numericUpDown.Maximum = new decimal(new int[] {
            3600,
            0,
            0,
            0});
            this.outerAngle_numericUpDown.Minimum = new decimal(new int[] {
            3600,
            0,
            0,
            -2147483648});
            this.outerAngle_numericUpDown.Name = "outerAngle_numericUpDown";
            this.outerAngle_numericUpDown.Size = new System.Drawing.Size(57, 20);
            this.outerAngle_numericUpDown.TabIndex = 16;
            this.outerAngle_numericUpDown.ValueChanged += new System.EventHandler(this.outerAngle_numericUpDown_ValueChanged);
            // 
            // lightColor_button
            // 
            this.lightColor_button.Location = new System.Drawing.Point(6, 185);
            this.lightColor_button.Name = "lightColor_button";
            this.lightColor_button.Size = new System.Drawing.Size(75, 23);
            this.lightColor_button.TabIndex = 15;
            this.lightColor_button.Text = "Цвет";
            this.lightColor_button.UseVisualStyleBackColor = true;
            this.lightColor_button.Click += new System.EventHandler(this.lightColor_button_Click);
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(74, 146);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(101, 13);
            this.label142.TabIndex = 14;
            this.label142.Text = "Угол направления";
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Location = new System.Drawing.Point(200, 91);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(66, 13);
            this.label141.TabIndex = 12;
            this.label141.Text = "Внутренний";
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(200, 62);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(52, 13);
            this.label140.TabIndex = 10;
            this.label140.Text = "Внешний";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(210, 40);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(104, 13);
            this.label139.TabIndex = 9;
            this.label139.Text = "Углы конуса света";
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(123, 40);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(43, 13);
            this.label138.TabIndex = 7;
            this.label138.Text = "Радиус";
            // 
            // radius_textBox
            // 
            this.radius_textBox.Location = new System.Drawing.Point(119, 59);
            this.radius_textBox.Name = "radius_textBox";
            this.radius_textBox.Size = new System.Drawing.Size(56, 20);
            this.radius_textBox.TabIndex = 6;
            this.radius_textBox.TextChanged += new System.EventHandler(this.radius_textBox_TextChanged);
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(58, 88);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(14, 13);
            this.label137.TabIndex = 5;
            this.label137.Text = "Y";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(58, 62);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(14, 13);
            this.label135.TabIndex = 4;
            this.label135.Text = "X";
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(6, 30);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(87, 26);
            this.label134.TabIndex = 3;
            this.label134.Text = "Смещение отн.\r\nцентра объекта";
            // 
            // lightOffsetY_textBox
            // 
            this.lightOffsetY_textBox.Location = new System.Drawing.Point(9, 85);
            this.lightOffsetY_textBox.Name = "lightOffsetY_textBox";
            this.lightOffsetY_textBox.Size = new System.Drawing.Size(38, 20);
            this.lightOffsetY_textBox.TabIndex = 2;
            this.lightOffsetY_textBox.TextChanged += new System.EventHandler(this.lightOffsetY_textBox_TextChanged);
            // 
            // lightOffsetX_textBox
            // 
            this.lightOffsetX_textBox.Location = new System.Drawing.Point(9, 59);
            this.lightOffsetX_textBox.Name = "lightOffsetX_textBox";
            this.lightOffsetX_textBox.Size = new System.Drawing.Size(38, 20);
            this.lightOffsetX_textBox.TabIndex = 1;
            this.lightOffsetX_textBox.TextChanged += new System.EventHandler(this.lightOffsetX_textBox_TextChanged);
            // 
            // lights_comboBox
            // 
            this.lights_comboBox.FormattingEnabled = true;
            this.lights_comboBox.Location = new System.Drawing.Point(6, 6);
            this.lights_comboBox.Name = "lights_comboBox";
            this.lights_comboBox.Size = new System.Drawing.Size(121, 21);
            this.lights_comboBox.TabIndex = 0;
            // 
            // TriggerPage
            // 
            this.TriggerPage.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.TriggerPage.Controls.Add(this.groupBox13);
            this.TriggerPage.Controls.Add(this.label164);
            this.TriggerPage.Controls.Add(this.comboBox4);
            this.TriggerPage.Controls.Add(this.button41);
            this.TriggerPage.Controls.Add(this.groupBox12);
            this.TriggerPage.Controls.Add(this.groupBox11);
            this.TriggerPage.Controls.Add(this.label160);
            this.TriggerPage.Controls.Add(this.label159);
            this.TriggerPage.Controls.Add(this.textBox29);
            this.TriggerPage.Location = new System.Drawing.Point(4, 22);
            this.TriggerPage.Name = "TriggerPage";
            this.TriggerPage.Size = new System.Drawing.Size(465, 403);
            this.TriggerPage.TabIndex = 14;
            this.TriggerPage.Text = "Trigger";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.comboBox_fields_settings);
            this.groupBox13.Controls.Add(this.label168);
            this.groupBox13.Controls.Add(this.public_param_b_1);
            this.groupBox13.Controls.Add(this.label167);
            this.groupBox13.Controls.Add(this.label166);
            this.groupBox13.Controls.Add(this.label165);
            this.groupBox13.Controls.Add(this.label157);
            this.groupBox13.Location = new System.Drawing.Point(8, 237);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(449, 154);
            this.groupBox13.TabIndex = 24;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Настройки скрипта";
            // 
            // comboBox_fields_settings
            // 
            this.comboBox_fields_settings.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_fields_settings.FormattingEnabled = true;
            this.comboBox_fields_settings.Location = new System.Drawing.Point(80, 62);
            this.comboBox_fields_settings.Name = "comboBox_fields_settings";
            this.comboBox_fields_settings.Size = new System.Drawing.Size(242, 21);
            this.comboBox_fields_settings.TabIndex = 6;
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Location = new System.Drawing.Point(6, 65);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(61, 13);
            this.label168.TabIndex = 5;
            this.label168.Text = "Параметр:";
            // 
            // public_param_b_1
            // 
            this.public_param_b_1.Location = new System.Drawing.Point(328, 60);
            this.public_param_b_1.Name = "public_param_b_1";
            this.public_param_b_1.Size = new System.Drawing.Size(115, 23);
            this.public_param_b_1.TabIndex = 4;
            this.public_param_b_1.Text = "Настроить";
            this.public_param_b_1.UseVisualStyleBackColor = true;
            this.public_param_b_1.Click += new System.EventHandler(this.public_param_b_1_Click);
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Location = new System.Drawing.Point(113, 20);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(11, 13);
            this.label167.TabIndex = 3;
            this.label167.Text = "*";
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Location = new System.Drawing.Point(6, 20);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(104, 13);
            this.label166.TabIndex = 2;
            this.label166.Text = "Название скрипта:";
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Location = new System.Drawing.Point(81, 43);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(11, 13);
            this.label165.TabIndex = 1;
            this.label165.Text = "*";
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Location = new System.Drawing.Point(6, 43);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(69, 13);
            this.label157.TabIndex = 0;
            this.label157.Text = "Параметры:";
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Location = new System.Drawing.Point(7, 213);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(43, 13);
            this.label164.TabIndex = 23;
            this.label164.Text = "Скрипт";
            // 
            // comboBox4
            // 
            this.comboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(56, 210);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(333, 21);
            this.comboBox4.TabIndex = 22;
            // 
            // button41
            // 
            this.button41.Location = new System.Drawing.Point(394, 208);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(66, 23);
            this.button41.TabIndex = 21;
            this.button41.Text = "Apply";
            this.button41.UseVisualStyleBackColor = true;
            this.button41.Click += new System.EventHandler(this.button41_Click_1);
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.label152);
            this.groupBox12.Controls.Add(this.radioButton_loop_Unlimited);
            this.groupBox12.Controls.Add(this.label163);
            this.groupBox12.Controls.Add(this.radioButton_loop_Once);
            this.groupBox12.Controls.Add(this.label162);
            this.groupBox12.Controls.Add(this.radioButton_loop_useCD);
            this.groupBox12.Controls.Add(this.button39);
            this.groupBox12.Controls.Add(this.textBox30);
            this.groupBox12.Controls.Add(this.label161);
            this.groupBox12.Location = new System.Drawing.Point(8, 90);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(455, 87);
            this.groupBox12.TabIndex = 20;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "LoopType";
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Location = new System.Drawing.Point(6, 17);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(59, 13);
            this.label152.TabIndex = 5;
            this.label152.Text = "Повторов:";
            // 
            // radioButton_loop_Unlimited
            // 
            this.radioButton_loop_Unlimited.AutoSize = true;
            this.radioButton_loop_Unlimited.Location = new System.Drawing.Point(80, 15);
            this.radioButton_loop_Unlimited.Name = "radioButton_loop_Unlimited";
            this.radioButton_loop_Unlimited.Size = new System.Drawing.Size(84, 17);
            this.radioButton_loop_Unlimited.TabIndex = 6;
            this.radioButton_loop_Unlimited.TabStop = true;
            this.radioButton_loop_Unlimited.Text = "бесконечно";
            this.radioButton_loop_Unlimited.UseVisualStyleBackColor = true;
            this.radioButton_loop_Unlimited.CheckedChanged += new System.EventHandler(this.radioButton_loop_Unlimited_CheckedChanged);
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Location = new System.Drawing.Point(92, 61);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(11, 13);
            this.label163.TabIndex = 18;
            this.label163.Text = "*";
            // 
            // radioButton_loop_Once
            // 
            this.radioButton_loop_Once.AutoSize = true;
            this.radioButton_loop_Once.Location = new System.Drawing.Point(170, 15);
            this.radioButton_loop_Once.Name = "radioButton_loop_Once";
            this.radioButton_loop_Once.Size = new System.Drawing.Size(70, 17);
            this.radioButton_loop_Once.TabIndex = 7;
            this.radioButton_loop_Once.TabStop = true;
            this.radioButton_loop_Once.Text = "один раз";
            this.radioButton_loop_Once.UseVisualStyleBackColor = true;
            this.radioButton_loop_Once.CheckedChanged += new System.EventHandler(this.radioButton_loop_Once_CheckedChanged);
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Location = new System.Drawing.Point(12, 61);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(74, 13);
            this.label162.TabIndex = 17;
            this.label162.Text = "CoolDownLeft";
            // 
            // radioButton_loop_useCD
            // 
            this.radioButton_loop_useCD.AutoSize = true;
            this.radioButton_loop_useCD.Location = new System.Drawing.Point(246, 15);
            this.radioButton_loop_useCD.Name = "radioButton_loop_useCD";
            this.radioButton_loop_useCD.Size = new System.Drawing.Size(103, 17);
            this.radioButton_loop_useCD.TabIndex = 13;
            this.radioButton_loop_useCD.TabStop = true;
            this.radioButton_loop_useCD.Text = "с откатом (сек)";
            this.radioButton_loop_useCD.UseVisualStyleBackColor = true;
            this.radioButton_loop_useCD.CheckedChanged += new System.EventHandler(this.radioButton_loop_useCD_CheckedChanged);
            // 
            // button39
            // 
            this.button39.Location = new System.Drawing.Point(169, 56);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(70, 23);
            this.button39.TabIndex = 16;
            this.button39.Text = "ResetCd";
            this.button39.UseVisualStyleBackColor = true;
            this.button39.Click += new System.EventHandler(this.button39_Click);
            // 
            // textBox30
            // 
            this.textBox30.Location = new System.Drawing.Point(412, 14);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(37, 20);
            this.textBox30.TabIndex = 14;
            this.textBox30.TextChanged += new System.EventHandler(this.textBox30_TextChanged);
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Location = new System.Drawing.Point(349, 17);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(11, 13);
            this.label161.TabIndex = 15;
            this.label161.Text = "*";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label153);
            this.groupBox11.Controls.Add(this.radioButton_OnCollisionStart);
            this.groupBox11.Controls.Add(this.radioButton_OnBeing);
            this.groupBox11.Controls.Add(this.radioButton_OnLeaving);
            this.groupBox11.Controls.Add(this.radioButton_Always);
            this.groupBox11.Location = new System.Drawing.Point(8, 3);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(454, 81);
            this.groupBox11.TabIndex = 19;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "WorkCondition";
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Location = new System.Drawing.Point(12, 27);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(130, 13);
            this.label153.TabIndex = 1;
            this.label153.Text = "Условие срабатывания:";
            // 
            // radioButton_OnCollisionStart
            // 
            this.radioButton_OnCollisionStart.AutoSize = true;
            this.radioButton_OnCollisionStart.Location = new System.Drawing.Point(181, 25);
            this.radioButton_OnCollisionStart.Name = "radioButton_OnCollisionStart";
            this.radioButton_OnCollisionStart.Size = new System.Drawing.Size(122, 17);
            this.radioButton_OnCollisionStart.TabIndex = 2;
            this.radioButton_OnCollisionStart.TabStop = true;
            this.radioButton_OnCollisionStart.Text = "на входе в область";
            this.radioButton_OnCollisionStart.UseVisualStyleBackColor = true;
            this.radioButton_OnCollisionStart.CheckedChanged += new System.EventHandler(this.radioButton_OnCollisionStart_CheckedChanged);
            // 
            // radioButton_OnBeing
            // 
            this.radioButton_OnBeing.AutoSize = true;
            this.radioButton_OnBeing.Location = new System.Drawing.Point(181, 48);
            this.radioButton_OnBeing.Name = "radioButton_OnBeing";
            this.radioButton_OnBeing.Size = new System.Drawing.Size(160, 17);
            this.radioButton_OnBeing.TabIndex = 3;
            this.radioButton_OnBeing.TabStop = true;
            this.radioButton_OnBeing.Text = "при нахождении в области";
            this.radioButton_OnBeing.UseVisualStyleBackColor = true;
            this.radioButton_OnBeing.CheckedChanged += new System.EventHandler(this.radioButton_OnBeing_CheckedChanged);
            // 
            // radioButton_OnLeaving
            // 
            this.radioButton_OnLeaving.AutoSize = true;
            this.radioButton_OnLeaving.Location = new System.Drawing.Point(309, 27);
            this.radioButton_OnLeaving.Name = "radioButton_OnLeaving";
            this.radioButton_OnLeaving.Size = new System.Drawing.Size(136, 17);
            this.radioButton_OnLeaving.TabIndex = 4;
            this.radioButton_OnLeaving.TabStop = true;
            this.radioButton_OnLeaving.Text = "на выходе из области";
            this.radioButton_OnLeaving.UseVisualStyleBackColor = true;
            this.radioButton_OnLeaving.CheckedChanged += new System.EventHandler(this.radioButton_OnLeaving_CheckedChanged);
            // 
            // radioButton_Always
            // 
            this.radioButton_Always.AutoSize = true;
            this.radioButton_Always.Location = new System.Drawing.Point(347, 50);
            this.radioButton_Always.Name = "radioButton_Always";
            this.radioButton_Always.Size = new System.Drawing.Size(60, 17);
            this.radioButton_Always.TabIndex = 12;
            this.radioButton_Always.TabStop = true;
            this.radioButton_Always.Text = "всегда";
            this.radioButton_Always.UseVisualStyleBackColor = true;
            this.radioButton_Always.CheckedChanged += new System.EventHandler(this.radioButton_Always_CheckedChanged);
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Location = new System.Drawing.Point(84, 180);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(11, 13);
            this.label160.TabIndex = 11;
            this.label160.Text = "*";
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Location = new System.Drawing.Point(14, 180);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(43, 13);
            this.label159.TabIndex = 10;
            this.label159.Text = "Радиус";
            // 
            // textBox29
            // 
            this.textBox29.Location = new System.Drawing.Point(147, 177);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(100, 20);
            this.textBox29.TabIndex = 9;
            this.textBox29.TextChanged += new System.EventHandler(this.textBox29_TextChanged_1);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(308, 455);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(85, 23);
            this.button4.TabIndex = 2;
            this.button4.Text = "Удалить";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Выделен:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(15, 548);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 0;
            this.button2.Text = "Создать";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Main_SpecialObjects
            // 
            this.Main_SpecialObjects.Controls.Add(this.label69);
            this.Main_SpecialObjects.Controls.Add(this.label68);
            this.Main_SpecialObjects.Controls.Add(this.groupBox7);
            this.Main_SpecialObjects.Controls.Add(this.SpecialObjectsTabControl);
            this.Main_SpecialObjects.Location = new System.Drawing.Point(4, 22);
            this.Main_SpecialObjects.Name = "Main_SpecialObjects";
            this.Main_SpecialObjects.Size = new System.Drawing.Size(493, 582);
            this.Main_SpecialObjects.TabIndex = 3;
            this.Main_SpecialObjects.Text = "Special Objects";
            this.Main_SpecialObjects.UseVisualStyleBackColor = true;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(155, 88);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(11, 13);
            this.label69.TabIndex = 6;
            this.label69.Text = "*";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(5, 88);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(144, 13);
            this.label68.TabIndex = 5;
            this.label68.Text = "Тип выделенного объекта:";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.button34);
            this.groupBox7.Controls.Add(this.button31);
            this.groupBox7.Controls.Add(this.button30);
            this.groupBox7.Controls.Add(this.button24);
            this.groupBox7.Location = new System.Drawing.Point(7, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(473, 82);
            this.groupBox7.TabIndex = 4;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Добавление особых объектов";
            // 
            // button34
            // 
            this.button34.Location = new System.Drawing.Point(6, 48);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(106, 23);
            this.button34.TabIndex = 9;
            this.button34.Text = "Enemy";
            this.button34.UseVisualStyleBackColor = true;
            this.button34.Click += new System.EventHandler(this.button34_Click);
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(392, 48);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(75, 23);
            this.button31.TabIndex = 8;
            this.button31.Text = "Finish";
            this.button31.UseVisualStyleBackColor = true;
            this.button31.Click += new System.EventHandler(this.button31_Click);
            // 
            // button30
            // 
            this.button30.Location = new System.Drawing.Point(392, 19);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(75, 23);
            this.button30.TabIndex = 7;
            this.button30.Text = "Start";
            this.button30.UseVisualStyleBackColor = true;
            this.button30.Click += new System.EventHandler(this.button30_Click);
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(6, 19);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(106, 23);
            this.button24.TabIndex = 2;
            this.button24.Text = "Система частиц";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // SpecialObjectsTabControl
            // 
            this.SpecialObjectsTabControl.Controls.Add(this.CreateEmitterPage);
            this.SpecialObjectsTabControl.Controls.Add(this.EmitterPage);
            this.SpecialObjectsTabControl.Controls.Add(this.EnemySettingsPage);
            this.SpecialObjectsTabControl.Controls.Add(this.CreateEnemyPage);
            this.SpecialObjectsTabControl.Controls.Add(this.weatherPage);
            this.SpecialObjectsTabControl.Location = new System.Drawing.Point(7, 104);
            this.SpecialObjectsTabControl.Name = "SpecialObjectsTabControl";
            this.SpecialObjectsTabControl.SelectedIndex = 0;
            this.SpecialObjectsTabControl.Size = new System.Drawing.Size(473, 456);
            this.SpecialObjectsTabControl.TabIndex = 1;
            // 
            // CreateEmitterPage
            // 
            this.CreateEmitterPage.Controls.Add(this.CreateCircleEmitterButton);
            this.CreateEmitterPage.Controls.Add(this.CreateLineEmitterButton);
            this.CreateEmitterPage.Controls.Add(this.CreateDotEmitterButton);
            this.CreateEmitterPage.Location = new System.Drawing.Point(4, 22);
            this.CreateEmitterPage.Name = "CreateEmitterPage";
            this.CreateEmitterPage.Padding = new System.Windows.Forms.Padding(3);
            this.CreateEmitterPage.Size = new System.Drawing.Size(465, 430);
            this.CreateEmitterPage.TabIndex = 3;
            this.CreateEmitterPage.Text = "Создание эмиттера";
            this.CreateEmitterPage.UseVisualStyleBackColor = true;
            // 
            // CreateCircleEmitterButton
            // 
            this.CreateCircleEmitterButton.Location = new System.Drawing.Point(19, 94);
            this.CreateCircleEmitterButton.Name = "CreateCircleEmitterButton";
            this.CreateCircleEmitterButton.Size = new System.Drawing.Size(124, 23);
            this.CreateCircleEmitterButton.TabIndex = 2;
            this.CreateCircleEmitterButton.Text = "Арочный эмиттер";
            this.CreateCircleEmitterButton.UseVisualStyleBackColor = true;
            this.CreateCircleEmitterButton.Click += new System.EventHandler(this.CreateCircleEmitterButton_Click);
            // 
            // CreateLineEmitterButton
            // 
            this.CreateLineEmitterButton.Location = new System.Drawing.Point(19, 55);
            this.CreateLineEmitterButton.Name = "CreateLineEmitterButton";
            this.CreateLineEmitterButton.Size = new System.Drawing.Size(124, 23);
            this.CreateLineEmitterButton.TabIndex = 1;
            this.CreateLineEmitterButton.Text = "Линейный эмиттер";
            this.CreateLineEmitterButton.UseVisualStyleBackColor = true;
            this.CreateLineEmitterButton.Click += new System.EventHandler(this.CreateLineEmitterButton_Click);
            // 
            // CreateDotEmitterButton
            // 
            this.CreateDotEmitterButton.Location = new System.Drawing.Point(19, 15);
            this.CreateDotEmitterButton.Name = "CreateDotEmitterButton";
            this.CreateDotEmitterButton.Size = new System.Drawing.Size(124, 23);
            this.CreateDotEmitterButton.TabIndex = 0;
            this.CreateDotEmitterButton.Text = "Точечный эмиттер";
            this.CreateDotEmitterButton.UseVisualStyleBackColor = true;
            this.CreateDotEmitterButton.Click += new System.EventHandler(this.CreateDotEmitterButton_Click);
            // 
            // EmitterPage
            // 
            this.EmitterPage.Controls.Add(this.label169);
            this.EmitterPage.Controls.Add(this.label170);
            this.EmitterPage.Controls.Add(this.energyLoss_numericUpDown);
            this.EmitterPage.Controls.Add(this.isSolid_checkBox);
            this.EmitterPage.Controls.Add(this.mass_textBox);
            this.EmitterPage.Controls.Add(this.label143);
            this.EmitterPage.Controls.Add(this.isInteractveEmitter_checkBox);
            this.EmitterPage.Controls.Add(this.normalDistr_checkBox);
            this.EmitterPage.Controls.Add(this.isBackLayer_checkBox);
            this.EmitterPage.Controls.Add(this.Color_button);
            this.EmitterPage.Controls.Add(this.label85);
            this.EmitterPage.Controls.Add(this.label84);
            this.EmitterPage.Controls.Add(this.label83);
            this.EmitterPage.Controls.Add(this.tMaxAlpha_textBox);
            this.EmitterPage.Controls.Add(this.sizeVel_textBox);
            this.EmitterPage.Controls.Add(this.label76);
            this.EmitterPage.Controls.Add(this.label79);
            this.EmitterPage.Controls.Add(this.bothWays_checkBox);
            this.EmitterPage.Controls.Add(this.label193);
            this.EmitterPage.Controls.Add(this.label75);
            this.EmitterPage.Controls.Add(this.label80);
            this.EmitterPage.Controls.Add(this.label77);
            this.EmitterPage.Controls.Add(this.label81);
            this.EmitterPage.Controls.Add(this.label78);
            this.EmitterPage.Controls.Add(this.ttl_textBox);
            this.EmitterPage.Controls.Add(this.label192);
            this.EmitterPage.Controls.Add(this.label74);
            this.EmitterPage.Controls.Add(this.emittrtomega_textBox);
            this.EmitterPage.Controls.Add(this.angleVel_textBox);
            this.EmitterPage.Controls.Add(this.alphaVel_textBox);
            this.EmitterPage.Controls.Add(this.width_textBox);
            this.EmitterPage.Controls.Add(this.height_textBox);
            this.EmitterPage.Controls.Add(this.label72);
            this.EmitterPage.Controls.Add(this.label73);
            this.EmitterPage.Controls.Add(this.width_label75);
            this.EmitterPage.Controls.Add(this.height_label76);
            this.EmitterPage.Controls.Add(this.label71);
            this.EmitterPage.Controls.Add(this.label70);
            this.EmitterPage.Controls.Add(this.maxSpeed_textBox);
            this.EmitterPage.Controls.Add(this.minSpeed_textBox);
            this.EmitterPage.Controls.Add(this.maxSize_textBox);
            this.EmitterPage.Controls.Add(this.minSize_textBox);
            this.EmitterPage.Controls.Add(this.label70_position);
            this.EmitterPage.Controls.Add(this.label70_secondaryAngle);
            this.EmitterPage.Controls.Add(this.label71_mainAngle);
            this.EmitterPage.Controls.Add(this.secondaryAngle_numericUpDown);
            this.EmitterPage.Controls.Add(this.startAngle_numericUpDown);
            this.EmitterPage.Controls.Add(this.mainAngle_numericUpDown);
            this.EmitterPage.Controls.Add(this.label70_maxSize);
            this.EmitterPage.Controls.Add(this.label71_minSize);
            this.EmitterPage.Controls.Add(this.label72_startSize);
            this.EmitterPage.Controls.Add(this.label72_maxSpeed);
            this.EmitterPage.Controls.Add(this.label71_minSpeed);
            this.EmitterPage.Controls.Add(this.label70_startSpeed);
            this.EmitterPage.Controls.Add(this.label70_pps);
            this.EmitterPage.Controls.Add(this.Pps_numericUpDown);
            this.EmitterPage.Controls.Add(this.alphablend_checkBox);
            this.EmitterPage.Location = new System.Drawing.Point(4, 22);
            this.EmitterPage.Name = "EmitterPage";
            this.EmitterPage.Padding = new System.Windows.Forms.Padding(3);
            this.EmitterPage.Size = new System.Drawing.Size(465, 430);
            this.EmitterPage.TabIndex = 0;
            this.EmitterPage.Text = "Свойства эмиттера";
            this.EmitterPage.UseVisualStyleBackColor = true;
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Location = new System.Drawing.Point(216, 372);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(15, 13);
            this.label169.TabIndex = 62;
            this.label169.Text = "%";
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Location = new System.Drawing.Point(21, 370);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(97, 26);
            this.label170.TabIndex = 61;
            this.label170.Text = "Потеря скорости \r\nпри ударе";
            // 
            // energyLoss_numericUpDown
            // 
            this.energyLoss_numericUpDown.Location = new System.Drawing.Point(149, 370);
            this.energyLoss_numericUpDown.Name = "energyLoss_numericUpDown";
            this.energyLoss_numericUpDown.Size = new System.Drawing.Size(60, 20);
            this.energyLoss_numericUpDown.TabIndex = 60;
            this.energyLoss_numericUpDown.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.energyLoss_numericUpDown.ValueChanged += new System.EventHandler(this.energyLoss_numericUpDown_ValueChanged);
            // 
            // isSolid_checkBox
            // 
            this.isSolid_checkBox.AutoSize = true;
            this.isSolid_checkBox.Location = new System.Drawing.Point(217, 51);
            this.isSolid_checkBox.Name = "isSolid_checkBox";
            this.isSolid_checkBox.Size = new System.Drawing.Size(84, 30);
            this.isSolid_checkBox.TabIndex = 59;
            this.isSolid_checkBox.Text = "Коллизии с\r\nбарьерами";
            this.isSolid_checkBox.UseVisualStyleBackColor = true;
            this.isSolid_checkBox.CheckedChanged += new System.EventHandler(this.isSolid_checkBox_CheckedChanged);
            // 
            // mass_textBox
            // 
            this.mass_textBox.Location = new System.Drawing.Point(149, 340);
            this.mass_textBox.Name = "mass_textBox";
            this.mass_textBox.Size = new System.Drawing.Size(60, 20);
            this.mass_textBox.TabIndex = 58;
            this.mass_textBox.TextChanged += new System.EventHandler(this.mass_textBox_TextChanged);
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Location = new System.Drawing.Point(25, 343);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(40, 13);
            this.label143.TabIndex = 57;
            this.label143.Text = "Масса";
            // 
            // isInteractveEmitter_checkBox
            // 
            this.isInteractveEmitter_checkBox.AutoSize = true;
            this.isInteractveEmitter_checkBox.Location = new System.Drawing.Point(217, 6);
            this.isInteractveEmitter_checkBox.Name = "isInteractveEmitter_checkBox";
            this.isInteractveEmitter_checkBox.Size = new System.Drawing.Size(76, 43);
            this.isInteractveEmitter_checkBox.TabIndex = 55;
            this.isInteractveEmitter_checkBox.Text = "Игрок\r\nвлияет на\r\nчастицы";
            this.isInteractveEmitter_checkBox.UseVisualStyleBackColor = true;
            this.isInteractveEmitter_checkBox.CheckedChanged += new System.EventHandler(this.isInteractveEmitter_checkBox_CheckedChanged);
            // 
            // normalDistr_checkBox
            // 
            this.normalDistr_checkBox.AutoSize = true;
            this.normalDistr_checkBox.Location = new System.Drawing.Point(307, 82);
            this.normalDistr_checkBox.Name = "normalDistr_checkBox";
            this.normalDistr_checkBox.Size = new System.Drawing.Size(151, 17);
            this.normalDistr_checkBox.TabIndex = 54;
            this.normalDistr_checkBox.Text = "Меньше частиц на краях";
            this.normalDistr_checkBox.UseVisualStyleBackColor = true;
            this.normalDistr_checkBox.CheckedChanged += new System.EventHandler(this.normalDistr_checkBox_CheckedChanged);
            // 
            // isBackLayer_checkBox
            // 
            this.isBackLayer_checkBox.AutoSize = true;
            this.isBackLayer_checkBox.Location = new System.Drawing.Point(307, 12);
            this.isBackLayer_checkBox.Name = "isBackLayer_checkBox";
            this.isBackLayer_checkBox.Size = new System.Drawing.Size(90, 17);
            this.isBackLayer_checkBox.TabIndex = 53;
            this.isBackLayer_checkBox.Text = "Задний слой";
            this.isBackLayer_checkBox.UseVisualStyleBackColor = true;
            this.isBackLayer_checkBox.CheckedChanged += new System.EventHandler(this.isBackLayer_checkBox_CheckedChanged);
            // 
            // Color_button
            // 
            this.Color_button.Location = new System.Drawing.Point(131, 41);
            this.Color_button.Name = "Color_button";
            this.Color_button.Size = new System.Drawing.Size(75, 23);
            this.Color_button.TabIndex = 50;
            this.Color_button.Text = "Цвет";
            this.Color_button.UseVisualStyleBackColor = true;
            this.Color_button.Click += new System.EventHandler(this.Color_button_Click);
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(416, 374);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(42, 13);
            this.label85.TabIndex = 49;
            this.label85.Text = "секунд";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(252, 371);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(97, 13);
            this.label84.TabIndex = 48;
            this.label84.Text = "Время появления";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(252, 327);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(70, 26);
            this.label83.TabIndex = 46;
            this.label83.Text = "Скорость\r\nуменьшения";
            // 
            // tMaxAlpha_textBox
            // 
            this.tMaxAlpha_textBox.Location = new System.Drawing.Point(350, 367);
            this.tMaxAlpha_textBox.Name = "tMaxAlpha_textBox";
            this.tMaxAlpha_textBox.Size = new System.Drawing.Size(60, 20);
            this.tMaxAlpha_textBox.TabIndex = 45;
            this.tMaxAlpha_textBox.TextChanged += new System.EventHandler(this.tMaxAlpha_textBox_TextChanged);
            // 
            // sizeVel_textBox
            // 
            this.sizeVel_textBox.Location = new System.Drawing.Point(350, 328);
            this.sizeVel_textBox.Name = "sizeVel_textBox";
            this.sizeVel_textBox.Size = new System.Drawing.Size(60, 20);
            this.sizeVel_textBox.TabIndex = 45;
            this.sizeVel_textBox.TextChanged += new System.EventHandler(this.sizeVel_textBox_TextChanged);
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(411, 286);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(30, 13);
            this.label76.TabIndex = 44;
            this.label76.Text = "град";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(250, 286);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(89, 13);
            this.label79.TabIndex = 43;
            this.label79.Text = "Начальный угол";
            // 
            // bothWays_checkBox
            // 
            this.bothWays_checkBox.AutoSize = true;
            this.bothWays_checkBox.Location = new System.Drawing.Point(307, 35);
            this.bothWays_checkBox.Name = "bothWays_checkBox";
            this.bothWays_checkBox.Size = new System.Drawing.Size(152, 17);
            this.bothWays_checkBox.TabIndex = 41;
            this.bothWays_checkBox.Text = "Излучать в обе стороны.";
            this.bothWays_checkBox.UseVisualStyleBackColor = true;
            this.bothWays_checkBox.CheckedChanged += new System.EventHandler(this.bothWays_checkBox_CheckedChanged);
            // 
            // label193
            // 
            this.label193.AutoSize = true;
            this.label193.Location = new System.Drawing.Point(411, 237);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(53, 13);
            this.label193.TabIndex = 40;
            this.label193.Text = "град/сек";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(411, 197);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(53, 13);
            this.label75.TabIndex = 40;
            this.label75.Text = "град/сек";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(411, 162);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(42, 13);
            this.label80.TabIndex = 40;
            this.label80.Text = "секунд";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(411, 115);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(42, 13);
            this.label77.TabIndex = 40;
            this.label77.Text = "секунд";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(252, 108);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(92, 26);
            this.label81.TabIndex = 39;
            this.label81.Text = "Время \r\nдо прозрачности";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(250, 162);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(75, 13);
            this.label78.TabIndex = 39;
            this.label78.Text = "Время жизни";
            // 
            // ttl_textBox
            // 
            this.ttl_textBox.Location = new System.Drawing.Point(350, 155);
            this.ttl_textBox.Name = "ttl_textBox";
            this.ttl_textBox.Size = new System.Drawing.Size(60, 20);
            this.ttl_textBox.TabIndex = 38;
            this.ttl_textBox.TextChanged += new System.EventHandler(this.ttl_textBox_TextChanged);
            // 
            // label192
            // 
            this.label192.AutoSize = true;
            this.label192.Location = new System.Drawing.Point(250, 227);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(54, 26);
            this.label192.TabIndex = 39;
            this.label192.Text = "Угловая\r\nскорость";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(230, 190);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(109, 26);
            this.label74.TabIndex = 39;
            this.label74.Text = "Скорость вращения\r\nвокруг своей оси";
            // 
            // emittrtomega_textBox
            // 
            this.emittrtomega_textBox.Location = new System.Drawing.Point(350, 230);
            this.emittrtomega_textBox.Name = "emittrtomega_textBox";
            this.emittrtomega_textBox.Size = new System.Drawing.Size(60, 20);
            this.emittrtomega_textBox.TabIndex = 38;
            this.emittrtomega_textBox.TextChanged += new System.EventHandler(this.emittrtomega_textBox_TextChanged);
            // 
            // angleVel_textBox
            // 
            this.angleVel_textBox.Location = new System.Drawing.Point(350, 190);
            this.angleVel_textBox.Name = "angleVel_textBox";
            this.angleVel_textBox.Size = new System.Drawing.Size(60, 20);
            this.angleVel_textBox.TabIndex = 38;
            this.angleVel_textBox.TextChanged += new System.EventHandler(this.angleVel_textBox_TextChanged);
            // 
            // alphaVel_textBox
            // 
            this.alphaVel_textBox.Location = new System.Drawing.Point(350, 108);
            this.alphaVel_textBox.Name = "alphaVel_textBox";
            this.alphaVel_textBox.Size = new System.Drawing.Size(60, 20);
            this.alphaVel_textBox.TabIndex = 38;
            this.alphaVel_textBox.TextChanged += new System.EventHandler(this.alphaVel_textBox_TextChanged);
            // 
            // width_textBox
            // 
            this.width_textBox.Location = new System.Drawing.Point(149, 314);
            this.width_textBox.Name = "width_textBox";
            this.width_textBox.Size = new System.Drawing.Size(60, 20);
            this.width_textBox.TabIndex = 37;
            this.width_textBox.TextChanged += new System.EventHandler(this.width_textBox_TextChanged);
            // 
            // height_textBox
            // 
            this.height_textBox.Location = new System.Drawing.Point(149, 288);
            this.height_textBox.Name = "height_textBox";
            this.height_textBox.Size = new System.Drawing.Size(60, 20);
            this.height_textBox.TabIndex = 36;
            this.height_textBox.TextChanged += new System.EventHandler(this.height_textBox_TextChanged);
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(216, 317);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(35, 13);
            this.label72.TabIndex = 35;
            this.label72.Text = "точек";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(216, 291);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(35, 13);
            this.label73.TabIndex = 34;
            this.label73.Text = "точек";
            // 
            // width_label75
            // 
            this.width_label75.AutoSize = true;
            this.width_label75.Location = new System.Drawing.Point(25, 317);
            this.width_label75.Name = "width_label75";
            this.width_label75.Size = new System.Drawing.Size(46, 13);
            this.width_label75.TabIndex = 32;
            this.width_label75.Text = "Ширина";
            // 
            // height_label76
            // 
            this.height_label76.AutoSize = true;
            this.height_label76.Location = new System.Drawing.Point(26, 291);
            this.height_label76.Name = "height_label76";
            this.height_label76.Size = new System.Drawing.Size(45, 13);
            this.height_label76.TabIndex = 31;
            this.height_label76.Text = "Высота";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(216, 259);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(30, 13);
            this.label71.TabIndex = 28;
            this.label71.Text = "град";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(216, 233);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(30, 13);
            this.label70.TabIndex = 27;
            this.label70.Text = "град";
            // 
            // maxSpeed_textBox
            // 
            this.maxSpeed_textBox.Location = new System.Drawing.Point(145, 108);
            this.maxSpeed_textBox.Name = "maxSpeed_textBox";
            this.maxSpeed_textBox.Size = new System.Drawing.Size(48, 20);
            this.maxSpeed_textBox.TabIndex = 25;
            this.maxSpeed_textBox.TextChanged += new System.EventHandler(this.maxSpeed_textBox_TextChanged);
            // 
            // minSpeed_textBox
            // 
            this.minSpeed_textBox.Location = new System.Drawing.Point(145, 82);
            this.minSpeed_textBox.Name = "minSpeed_textBox";
            this.minSpeed_textBox.Size = new System.Drawing.Size(48, 20);
            this.minSpeed_textBox.TabIndex = 24;
            this.minSpeed_textBox.TextChanged += new System.EventHandler(this.minSpeed_textBox_TextChanged);
            // 
            // maxSize_textBox
            // 
            this.maxSize_textBox.Location = new System.Drawing.Point(145, 187);
            this.maxSize_textBox.Name = "maxSize_textBox";
            this.maxSize_textBox.Size = new System.Drawing.Size(48, 20);
            this.maxSize_textBox.TabIndex = 19;
            this.maxSize_textBox.TextChanged += new System.EventHandler(this.maxSize_textBox_TextChanged);
            // 
            // minSize_textBox
            // 
            this.minSize_textBox.Location = new System.Drawing.Point(145, 161);
            this.minSize_textBox.Name = "minSize_textBox";
            this.minSize_textBox.Size = new System.Drawing.Size(48, 20);
            this.minSize_textBox.TabIndex = 18;
            this.minSize_textBox.TextChanged += new System.EventHandler(this.minSize_textBox_TextChanged);
            // 
            // label70_position
            // 
            this.label70_position.AutoSize = true;
            this.label70_position.Location = new System.Drawing.Point(6, 215);
            this.label70_position.Name = "label70_position";
            this.label70_position.Size = new System.Drawing.Size(65, 13);
            this.label70_position.TabIndex = 17;
            this.label70_position.Text = "Положение";
            // 
            // label70_secondaryAngle
            // 
            this.label70_secondaryAngle.AutoSize = true;
            this.label70_secondaryAngle.Location = new System.Drawing.Point(26, 264);
            this.label70_secondaryAngle.Name = "label70_secondaryAngle";
            this.label70_secondaryAngle.Size = new System.Drawing.Size(117, 13);
            this.label70_secondaryAngle.TabIndex = 16;
            this.label70_secondaryAngle.Text = "Второстепенный угол";
            // 
            // label71_mainAngle
            // 
            this.label71_mainAngle.AutoSize = true;
            this.label71_mainAngle.Location = new System.Drawing.Point(26, 237);
            this.label71_mainAngle.Name = "label71_mainAngle";
            this.label71_mainAngle.Size = new System.Drawing.Size(82, 13);
            this.label71_mainAngle.TabIndex = 15;
            this.label71_mainAngle.Text = "Основной угол";
            // 
            // secondaryAngle_numericUpDown
            // 
            this.secondaryAngle_numericUpDown.Location = new System.Drawing.Point(149, 257);
            this.secondaryAngle_numericUpDown.Maximum = new decimal(new int[] {
            3600,
            0,
            0,
            0});
            this.secondaryAngle_numericUpDown.Minimum = new decimal(new int[] {
            3600,
            0,
            0,
            -2147483648});
            this.secondaryAngle_numericUpDown.Name = "secondaryAngle_numericUpDown";
            this.secondaryAngle_numericUpDown.Size = new System.Drawing.Size(60, 20);
            this.secondaryAngle_numericUpDown.TabIndex = 14;
            this.secondaryAngle_numericUpDown.ValueChanged += new System.EventHandler(this.secondaryAngle_numericUpDown_ValueChanged);
            // 
            // startAngle_numericUpDown
            // 
            this.startAngle_numericUpDown.Location = new System.Drawing.Point(350, 284);
            this.startAngle_numericUpDown.Maximum = new decimal(new int[] {
            3600,
            0,
            0,
            0});
            this.startAngle_numericUpDown.Minimum = new decimal(new int[] {
            3600,
            0,
            0,
            -2147483648});
            this.startAngle_numericUpDown.Name = "startAngle_numericUpDown";
            this.startAngle_numericUpDown.Size = new System.Drawing.Size(60, 20);
            this.startAngle_numericUpDown.TabIndex = 13;
            this.startAngle_numericUpDown.ValueChanged += new System.EventHandler(this.startAngle_numericUpDown_ValueChanged);
            // 
            // mainAngle_numericUpDown
            // 
            this.mainAngle_numericUpDown.Location = new System.Drawing.Point(149, 227);
            this.mainAngle_numericUpDown.Maximum = new decimal(new int[] {
            3600,
            0,
            0,
            0});
            this.mainAngle_numericUpDown.Minimum = new decimal(new int[] {
            3600,
            0,
            0,
            -2147483648});
            this.mainAngle_numericUpDown.Name = "mainAngle_numericUpDown";
            this.mainAngle_numericUpDown.Size = new System.Drawing.Size(60, 20);
            this.mainAngle_numericUpDown.TabIndex = 13;
            this.mainAngle_numericUpDown.ValueChanged += new System.EventHandler(this.mainAngle_numericUpDown_ValueChanged);
            // 
            // label70_maxSize
            // 
            this.label70_maxSize.AutoSize = true;
            this.label70_maxSize.Location = new System.Drawing.Point(47, 187);
            this.label70_maxSize.Name = "label70_maxSize";
            this.label70_maxSize.Size = new System.Drawing.Size(61, 13);
            this.label70_maxSize.TabIndex = 12;
            this.label70_maxSize.Text = "Максимум";
            // 
            // label71_minSize
            // 
            this.label71_minSize.AutoSize = true;
            this.label71_minSize.Location = new System.Drawing.Point(47, 164);
            this.label71_minSize.Name = "label71_minSize";
            this.label71_minSize.Size = new System.Drawing.Size(55, 13);
            this.label71_minSize.TabIndex = 11;
            this.label71_minSize.Text = "Минимум";
            // 
            // label72_startSize
            // 
            this.label72_startSize.AutoSize = true;
            this.label72_startSize.Location = new System.Drawing.Point(6, 141);
            this.label72_startSize.Name = "label72_startSize";
            this.label72_startSize.Size = new System.Drawing.Size(105, 13);
            this.label72_startSize.TabIndex = 10;
            this.label72_startSize.Text = "Начальный размер";
            // 
            // label72_maxSpeed
            // 
            this.label72_maxSpeed.AutoSize = true;
            this.label72_maxSpeed.Location = new System.Drawing.Point(47, 108);
            this.label72_maxSpeed.Name = "label72_maxSpeed";
            this.label72_maxSpeed.Size = new System.Drawing.Size(61, 13);
            this.label72_maxSpeed.TabIndex = 7;
            this.label72_maxSpeed.Text = "Максимум";
            // 
            // label71_minSpeed
            // 
            this.label71_minSpeed.AutoSize = true;
            this.label71_minSpeed.Location = new System.Drawing.Point(47, 82);
            this.label71_minSpeed.Name = "label71_minSpeed";
            this.label71_minSpeed.Size = new System.Drawing.Size(55, 13);
            this.label71_minSpeed.TabIndex = 6;
            this.label71_minSpeed.Text = "Минимум";
            // 
            // label70_startSpeed
            // 
            this.label70_startSpeed.AutoSize = true;
            this.label70_startSpeed.Location = new System.Drawing.Point(6, 59);
            this.label70_startSpeed.Name = "label70_startSpeed";
            this.label70_startSpeed.Size = new System.Drawing.Size(112, 13);
            this.label70_startSpeed.TabIndex = 5;
            this.label70_startSpeed.Text = "Начальная скорость";
            // 
            // label70_pps
            // 
            this.label70_pps.AutoSize = true;
            this.label70_pps.Location = new System.Drawing.Point(6, 17);
            this.label70_pps.Name = "label70_pps";
            this.label70_pps.Size = new System.Drawing.Size(96, 13);
            this.label70_pps.TabIndex = 2;
            this.label70_pps.Text = "Частиц в секунду";
            // 
            // Pps_numericUpDown
            // 
            this.Pps_numericUpDown.Location = new System.Drawing.Point(131, 15);
            this.Pps_numericUpDown.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.Pps_numericUpDown.Name = "Pps_numericUpDown";
            this.Pps_numericUpDown.Size = new System.Drawing.Size(60, 20);
            this.Pps_numericUpDown.TabIndex = 1;
            this.Pps_numericUpDown.ValueChanged += new System.EventHandler(this.Pps_numericUpDown_ValueChanged);
            // 
            // alphablend_checkBox
            // 
            this.alphablend_checkBox.AutoSize = true;
            this.alphablend_checkBox.Location = new System.Drawing.Point(307, 59);
            this.alphablend_checkBox.Name = "alphablend_checkBox";
            this.alphablend_checkBox.Size = new System.Drawing.Size(83, 17);
            this.alphablend_checkBox.TabIndex = 0;
            this.alphablend_checkBox.Text = "Alpha Blend";
            this.alphablend_checkBox.UseVisualStyleBackColor = true;
            this.alphablend_checkBox.CheckedChanged += new System.EventHandler(this.alphablend_checkBox_CheckedChanged);
            // 
            // EnemySettingsPage
            // 
            this.EnemySettingsPage.Controls.Add(this.groupBox10);
            this.EnemySettingsPage.Controls.Add(this.kamikaze_groupBox);
            this.EnemySettingsPage.Controls.Add(this.groupBox9);
            this.EnemySettingsPage.Location = new System.Drawing.Point(4, 22);
            this.EnemySettingsPage.Name = "EnemySettingsPage";
            this.EnemySettingsPage.Size = new System.Drawing.Size(465, 430);
            this.EnemySettingsPage.TabIndex = 4;
            this.EnemySettingsPage.Text = "EnemySettins";
            this.EnemySettingsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.textBox28);
            this.groupBox10.Controls.Add(this.textBox27);
            this.groupBox10.Controls.Add(this.textBox26);
            this.groupBox10.Controls.Add(this.button37);
            this.groupBox10.Controls.Add(this.label131);
            this.groupBox10.Controls.Add(this.label130);
            this.groupBox10.Controls.Add(this.label129);
            this.groupBox10.Controls.Add(this.label128);
            this.groupBox10.Controls.Add(this.label127);
            this.groupBox10.Controls.Add(this.label126);
            this.groupBox10.Location = new System.Drawing.Point(3, 151);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(460, 66);
            this.groupBox10.TabIndex = 2;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Movable (настройки типа, который подвижен, не летает)";
            // 
            // textBox28
            // 
            this.textBox28.Location = new System.Drawing.Point(187, 36);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(43, 20);
            this.textBox28.TabIndex = 27;
            // 
            // textBox27
            // 
            this.textBox27.Location = new System.Drawing.Point(411, 13);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(43, 20);
            this.textBox27.TabIndex = 26;
            // 
            // textBox26
            // 
            this.textBox26.Location = new System.Drawing.Point(203, 13);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(43, 20);
            this.textBox26.TabIndex = 25;
            // 
            // button37
            // 
            this.button37.Location = new System.Drawing.Point(411, 39);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(43, 23);
            this.button37.TabIndex = 6;
            this.button37.Text = "SET";
            this.button37.UseVisualStyleBackColor = true;
            this.button37.Click += new System.EventHandler(this.button37_Click);
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(124, 39);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(47, 13);
            this.label131.TabIndex = 5;
            this.label131.Text = "label131";
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(349, 16);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(47, 13);
            this.label130.TabIndex = 4;
            this.label130.Text = "label130";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(150, 16);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(47, 13);
            this.label129.TabIndex = 3;
            this.label129.Text = "label129";
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(8, 39);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(98, 13);
            this.label128.TabIndex = 2;
            this.label128.Text = "Скорость прыжка";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(264, 16);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(75, 13);
            this.label127.TabIndex = 1;
            this.label127.Text = "Сила прыжка";
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(6, 16);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(132, 13);
            this.label126.TabIndex = 0;
            this.label126.Text = "Скорость передвижения";
            // 
            // kamikaze_groupBox
            // 
            this.kamikaze_groupBox.Location = new System.Drawing.Point(3, 279);
            this.kamikaze_groupBox.Name = "kamikaze_groupBox";
            this.kamikaze_groupBox.Size = new System.Drawing.Size(459, 100);
            this.kamikaze_groupBox.TabIndex = 1;
            this.kamikaze_groupBox.TabStop = false;
            this.kamikaze_groupBox.Text = "kamikaze";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label188);
            this.groupBox9.Controls.Add(this.textBox2);
            this.groupBox9.Controls.Add(this.label189);
            this.groupBox9.Controls.Add(this.label125);
            this.groupBox9.Controls.Add(this.label124);
            this.groupBox9.Controls.Add(this.label123);
            this.groupBox9.Controls.Add(this.label122);
            this.groupBox9.Controls.Add(this.label121);
            this.groupBox9.Controls.Add(this.label120);
            this.groupBox9.Controls.Add(this.label119);
            this.groupBox9.Controls.Add(this.label118);
            this.groupBox9.Controls.Add(this.button36);
            this.groupBox9.Controls.Add(this.textBox25);
            this.groupBox9.Controls.Add(this.textBox24);
            this.groupBox9.Controls.Add(this.textBox23);
            this.groupBox9.Controls.Add(this.textBox8);
            this.groupBox9.Controls.Add(this.textBox7);
            this.groupBox9.Controls.Add(this.textBox6);
            this.groupBox9.Controls.Add(this.textBox5);
            this.groupBox9.Controls.Add(this.textBox4);
            this.groupBox9.Controls.Add(this.label117);
            this.groupBox9.Controls.Add(this.label116);
            this.groupBox9.Controls.Add(this.label115);
            this.groupBox9.Controls.Add(this.label114);
            this.groupBox9.Controls.Add(this.label113);
            this.groupBox9.Controls.Add(this.label112);
            this.groupBox9.Controls.Add(this.label111);
            this.groupBox9.Controls.Add(this.label110);
            this.groupBox9.Location = new System.Drawing.Point(3, 3);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(459, 116);
            this.groupBox9.TabIndex = 0;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Общие настройки:  (Enemy class)";
            // 
            // label188
            // 
            this.label188.AutoSize = true;
            this.label188.Location = new System.Drawing.Point(389, 87);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(47, 13);
            this.label188.TabIndex = 27;
            this.label188.Text = "label188";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(429, 84);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(28, 20);
            this.textBox2.TabIndex = 26;
            // 
            // label189
            // 
            this.label189.AutoSize = true;
            this.label189.Location = new System.Drawing.Point(254, 87);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(105, 13);
            this.label189.TabIndex = 25;
            this.label189.Text = "Дистанция отмены";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(150, 87);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(47, 13);
            this.label125.TabIndex = 24;
            this.label125.Text = "label125";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(390, 67);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(47, 13);
            this.label124.TabIndex = 23;
            this.label124.Text = "label124";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(389, 42);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(47, 13);
            this.label123.TabIndex = 22;
            this.label123.Text = "label123";
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(303, 67);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(47, 13);
            this.label122.TabIndex = 21;
            this.label122.Text = "label122";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(302, 42);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(47, 13);
            this.label121.TabIndex = 20;
            this.label121.Text = "label121";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(45, 67);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(47, 13);
            this.label120.TabIndex = 19;
            this.label120.Text = "label120";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(107, 41);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(47, 13);
            this.label119.TabIndex = 18;
            this.label119.Text = "label119";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(141, 16);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(47, 13);
            this.label118.TabIndex = 17;
            this.label118.Text = "label118";
            // 
            // button36
            // 
            this.button36.Location = new System.Drawing.Point(411, 9);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(43, 23);
            this.button36.TabIndex = 16;
            this.button36.Text = "SET";
            this.button36.UseVisualStyleBackColor = true;
            this.button36.Click += new System.EventHandler(this.button36_Click);
            // 
            // textBox25
            // 
            this.textBox25.Location = new System.Drawing.Point(429, 64);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(29, 20);
            this.textBox25.TabIndex = 15;
            // 
            // textBox24
            // 
            this.textBox24.Location = new System.Drawing.Point(429, 38);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(29, 20);
            this.textBox24.TabIndex = 14;
            // 
            // textBox23
            // 
            this.textBox23.Location = new System.Drawing.Point(319, 64);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(26, 20);
            this.textBox23.TabIndex = 13;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(319, 38);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(26, 20);
            this.textBox8.TabIndex = 12;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(182, 84);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(28, 20);
            this.textBox7.TabIndex = 11;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(110, 64);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(43, 20);
            this.textBox6.TabIndex = 10;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(161, 39);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(43, 20);
            this.textBox5.TabIndex = 9;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(192, 13);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(43, 20);
            this.textBox4.TabIndex = 8;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(361, 67);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(23, 13);
            this.label117.TabIndex = 7;
            this.label117.Text = "MP";
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(361, 41);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(22, 13);
            this.label116.TabIndex = 6;
            this.label116.Text = "HP";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(254, 67);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(43, 13);
            this.label115.TabIndex = 5;
            this.label115.Text = "MaxMP";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(254, 41);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(42, 13);
            this.label114.TabIndex = 4;
            this.label114.Text = "MaxHP";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(7, 87);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(137, 13);
            this.label113.TabIndex = 3;
            this.label113.Text = "Дистанция реагирования";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(6, 67);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(33, 13);
            this.label112.TabIndex = 2;
            this.label112.Text = "Урон";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(6, 41);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(75, 13);
            this.label111.TabIndex = 1;
            this.label111.Text = "Радиус атаки";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(6, 16);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(129, 13);
            this.label110.TabIndex = 0;
            this.label110.Text = "Задержка перед атакой";
            // 
            // CreateEnemyPage
            // 
            this.CreateEnemyPage.Controls.Add(this.button35);
            this.CreateEnemyPage.Location = new System.Drawing.Point(4, 22);
            this.CreateEnemyPage.Name = "CreateEnemyPage";
            this.CreateEnemyPage.Size = new System.Drawing.Size(465, 430);
            this.CreateEnemyPage.TabIndex = 5;
            this.CreateEnemyPage.Text = "CreateEnemy";
            this.CreateEnemyPage.UseVisualStyleBackColor = true;
            // 
            // button35
            // 
            this.button35.Location = new System.Drawing.Point(15, 17);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(106, 23);
            this.button35.TabIndex = 0;
            this.button35.Text = "kamikaze";
            this.button35.UseVisualStyleBackColor = true;
            this.button35.Click += new System.EventHandler(this.button35_Click);
            // 
            // weatherPage
            // 
            this.weatherPage.Controls.Add(this.groupBox17);
            this.weatherPage.Location = new System.Drawing.Point(4, 22);
            this.weatherPage.Name = "weatherPage";
            this.weatherPage.Padding = new System.Windows.Forms.Padding(3);
            this.weatherPage.Size = new System.Drawing.Size(465, 430);
            this.weatherPage.TabIndex = 6;
            this.weatherPage.Text = "Погода";
            this.weatherPage.UseVisualStyleBackColor = true;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.radioButton6);
            this.groupBox17.Controls.Add(this.weather_Rain);
            this.groupBox17.Controls.Add(this.weather_Snow);
            this.groupBox17.Controls.Add(this.noWeather);
            this.groupBox17.Location = new System.Drawing.Point(17, 17);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(160, 218);
            this.groupBox17.TabIndex = 0;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Тип";
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(17, 168);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(139, 17);
            this.radioButton6.TabIndex = 3;
            this.radioButton6.TabStop = true;
            this.radioButton6.Text = "Пришествие Селестии";
            this.radioButton6.UseVisualStyleBackColor = true;
            // 
            // weather_Rain
            // 
            this.weather_Rain.AutoSize = true;
            this.weather_Rain.Location = new System.Drawing.Point(17, 43);
            this.weather_Rain.Name = "weather_Rain";
            this.weather_Rain.Size = new System.Drawing.Size(60, 17);
            this.weather_Rain.TabIndex = 2;
            this.weather_Rain.TabStop = true;
            this.weather_Rain.Text = "Дождь";
            this.weather_Rain.UseVisualStyleBackColor = true;
            // 
            // weather_Snow
            // 
            this.weather_Snow.AutoSize = true;
            this.weather_Snow.Location = new System.Drawing.Point(17, 66);
            this.weather_Snow.Name = "weather_Snow";
            this.weather_Snow.Size = new System.Drawing.Size(49, 17);
            this.weather_Snow.TabIndex = 1;
            this.weather_Snow.TabStop = true;
            this.weather_Snow.Text = "Снег";
            this.weather_Snow.UseVisualStyleBackColor = true;
            // 
            // noWeather
            // 
            this.noWeather.AutoSize = true;
            this.noWeather.Location = new System.Drawing.Point(17, 20);
            this.noWeather.Name = "noWeather";
            this.noWeather.Size = new System.Drawing.Size(89, 17);
            this.noWeather.TabIndex = 0;
            this.noWeather.TabStop = true;
            this.noWeather.Text = "Без осадков";
            this.noWeather.UseVisualStyleBackColor = true;
            // 
            // Main_library
            // 
            this.Main_library.Location = new System.Drawing.Point(4, 22);
            this.Main_library.Name = "Main_library";
            this.Main_library.Size = new System.Drawing.Size(493, 582);
            this.Main_library.TabIndex = 2;
            this.Main_library.Text = "Library";
            this.Main_library.UseVisualStyleBackColor = true;
            // 
            // Main_Events
            // 
            this.Main_Events.Controls.Add(this.button22);
            this.Main_Events.Controls.Add(this.label149);
            this.Main_Events.Controls.Add(this.label148);
            this.Main_Events.Controls.Add(this.label147);
            this.Main_Events.Controls.Add(this.label146);
            this.Main_Events.Controls.Add(this.textBox22);
            this.Main_Events.Controls.Add(this.textBox21);
            this.Main_Events.Controls.Add(this.label144);
            this.Main_Events.Location = new System.Drawing.Point(4, 22);
            this.Main_Events.Name = "Main_Events";
            this.Main_Events.Size = new System.Drawing.Size(493, 582);
            this.Main_Events.TabIndex = 4;
            this.Main_Events.Text = "EventsPage";
            this.Main_Events.UseVisualStyleBackColor = true;
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(7, 556);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(75, 23);
            this.button22.TabIndex = 7;
            this.button22.Text = "UnDO";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Location = new System.Drawing.Point(75, 528);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(11, 13);
            this.label149.TabIndex = 6;
            this.label149.Text = "*";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Location = new System.Drawing.Point(332, 532);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(43, 13);
            this.label148.TabIndex = 5;
            this.label148.Text = "Events:";
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Location = new System.Drawing.Point(381, 532);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(11, 13);
            this.label147.TabIndex = 4;
            this.label147.Text = "*";
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(4, 528);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(65, 13);
            this.label146.TabIndex = 3;
            this.label146.Text = "Max events:";
            // 
            // textBox22
            // 
            this.textBox22.Location = new System.Drawing.Point(7, 40);
            this.textBox22.Multiline = true;
            this.textBox22.Name = "textBox22";
            this.textBox22.ReadOnly = true;
            this.textBox22.Size = new System.Drawing.Size(473, 485);
            this.textBox22.TabIndex = 2;
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(92, 525);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(100, 20);
            this.textBox21.TabIndex = 1;
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Location = new System.Drawing.Point(4, 10);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(73, 13);
            this.label144.TabIndex = 0;
            this.label144.Text = "Added events";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // colorDialog1
            // 
            this.colorDialog1.AnyColor = true;
            // 
            // button48
            // 
            this.button48.Location = new System.Drawing.Point(12, 608);
            this.button48.Name = "button48";
            this.button48.Size = new System.Drawing.Size(75, 23);
            this.button48.TabIndex = 1;
            this.button48.Text = "Пауза";
            this.button48.UseVisualStyleBackColor = true;
            this.button48.Click += new System.EventHandler(this.button48_Click);
            // 
            // button49
            // 
            this.button49.Location = new System.Drawing.Point(414, 608);
            this.button49.Name = "button49";
            this.button49.Size = new System.Drawing.Size(75, 23);
            this.button49.TabIndex = 2;
            this.button49.Text = "Карта";
            this.button49.UseVisualStyleBackColor = true;
            this.button49.Click += new System.EventHandler(this.button49_Click);
            // 
            // EditorMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(497, 633);
            this.ControlBox = false;
            this.Controls.Add(this.button49);
            this.Controls.Add(this.button48);
            this.Controls.Add(this.MainTabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EditorMenu";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "EditorMenu";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EditorMenu_FormClosing);
            this.Load += new System.EventHandler(this.EditorMenu_Load);
            this.LocationChanged += new System.EventHandler(this.EditorMenu_LocationChanged);
            this.MainTabControl.ResumeLayout(false);
            this.Main_LevelPage.ResumeLayout(false);
            this.Main_LevelPage.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.Main_ObjectPage.ResumeLayout(false);
            this.Main_ObjectPage.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.AttributestabControl.ResumeLayout(false);
            this.StandartPage.ResumeLayout(false);
            this.StandartPage.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.SelectPage.ResumeLayout(false);
            this.SelectPage.PerformLayout();
            this.DragPage.ResumeLayout(false);
            this.DragPage.PerformLayout();
            this.AnimationPage.ResumeLayout(false);
            this.AnimationPage.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.BarrierPage.ResumeLayout(false);
            this.BarrierPage.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.MovementHeroPage.ResumeLayout(false);
            this.MovementHeroPage.PerformLayout();
            this.RpgCharacterPage.ResumeLayout(false);
            this.RpgCharacterPage.PerformLayout();
            this.BonusPage.ResumeLayout(false);
            this.BonusPage.PerformLayout();
            this.PortalPage.ResumeLayout(false);
            this.PortalPage.PerformLayout();
            this.ToolTipPage.ResumeLayout(false);
            this.ToolTipPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ttOffsetY_numericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ttOffsetX_numericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ttWidth_numericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ttHeight_numericUpDown)).EndInit();
            this.DeathZonePage.ResumeLayout(false);
            this.DeathZonePage.PerformLayout();
            this.DialogPage.ResumeLayout(false);
            this.DialogPage.PerformLayout();
            this.LightPage.ResumeLayout(false);
            this.LightPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gammaLight_numericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.directionAngle_numericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.innerAngle_numericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.outerAngle_numericUpDown)).EndInit();
            this.TriggerPage.ResumeLayout(false);
            this.TriggerPage.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.Main_SpecialObjects.ResumeLayout(false);
            this.Main_SpecialObjects.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.SpecialObjectsTabControl.ResumeLayout(false);
            this.CreateEmitterPage.ResumeLayout(false);
            this.EmitterPage.ResumeLayout(false);
            this.EmitterPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.energyLoss_numericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondaryAngle_numericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startAngle_numericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainAngle_numericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pps_numericUpDown)).EndInit();
            this.EnemySettingsPage.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.CreateEnemyPage.ResumeLayout(false);
            this.weatherPage.ResumeLayout(false);
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.Main_Events.ResumeLayout(false);
            this.Main_Events.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl MainTabControl;
        private System.Windows.Forms.TabPage Main_ObjectPage;
        private System.Windows.Forms.TabPage Main_LevelPage;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TabControl AttributestabControl;
        private System.Windows.Forms.TabPage SelectPage;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TabPage DragPage;
        private System.Windows.Forms.TabPage StandartPage;
        private System.Windows.Forms.TabPage AnimationPage;
        private System.Windows.Forms.TabPage DrawPage;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabPage BarrierPage;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.TabPage RpgCharacterPage;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TabPage BonusPage;
        private System.Windows.Forms.TabPage MovementHeroPage;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox maxJumpSpeed_textBox;
        private System.Windows.Forms.TextBox jumpStr_textBox;
        private System.Windows.Forms.TextBox runSpeed_textBox;
        private System.Windows.Forms.Button applyJumpParam_button;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.TextBox brightness_textbox;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TabPage Main_library;
        private System.Windows.Forms.TabPage Main_SpecialObjects;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.TabControl SpecialObjectsTabControl;
        private System.Windows.Forms.TabPage EmitterPage;
        private System.Windows.Forms.TabPage CreateEmitterPage;
        private System.Windows.Forms.Button CreateDotEmitterButton;
        private System.Windows.Forms.CheckBox alphablend_checkBox;
        private System.Windows.Forms.Label label70_position;
        private System.Windows.Forms.Label label70_secondaryAngle;
        private System.Windows.Forms.Label label71_mainAngle;
        private System.Windows.Forms.NumericUpDown secondaryAngle_numericUpDown;
        private System.Windows.Forms.NumericUpDown mainAngle_numericUpDown;
        private System.Windows.Forms.Label label70_maxSize;
        private System.Windows.Forms.Label label71_minSize;
        private System.Windows.Forms.Label label72_startSize;
        private System.Windows.Forms.Label label72_maxSpeed;
        private System.Windows.Forms.Label label71_minSpeed;
        private System.Windows.Forms.Label label70_startSpeed;
        private System.Windows.Forms.Label label70_pps;
        private System.Windows.Forms.NumericUpDown Pps_numericUpDown;
        private System.Windows.Forms.TextBox maxSize_textBox;
        private System.Windows.Forms.TextBox minSize_textBox;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.TextBox maxSpeed_textBox;
        private System.Windows.Forms.TextBox minSpeed_textBox;
        private System.Windows.Forms.Button CreateLineEmitterButton;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label width_label75;
        private System.Windows.Forms.Label height_label76;
        private System.Windows.Forms.TextBox width_textBox;
        private System.Windows.Forms.TextBox height_textBox;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.TextBox alphaVel_textBox;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Button CreateCircleEmitterButton;
        private System.Windows.Forms.CheckBox bothWays_checkBox;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.TextBox ttl_textBox;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.TextBox angleVel_textBox;
        private System.Windows.Forms.NumericUpDown startAngle_numericUpDown;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.TextBox sizeVel_textBox;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.TextBox tMaxAlpha_textBox;
        private System.Windows.Forms.Button Color_button;
        private ColorDialogEx colorDialog1;
        private System.Windows.Forms.TabPage PortalPage;
        private System.Windows.Forms.CheckBox isExit_checkBox;
        private System.Windows.Forms.CheckBox twoSides_checkBox;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.TextBox exitTime_textBox;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.TextBox transitionTime_textBox;
        private System.Windows.Forms.CheckBox isInstant_checkBox;
        private System.Windows.Forms.CheckBox isInteractive_checkBox;
        private System.Windows.Forms.TabPage ToolTipPage;
        private System.Windows.Forms.NumericUpDown ttOffsetY_numericUpDown;
        private System.Windows.Forms.NumericUpDown ttOffsetX_numericUpDown;
        private System.Windows.Forms.NumericUpDown ttWidth_numericUpDown;
        private System.Windows.Forms.NumericUpDown ttHeight_numericUpDown;
        private System.Windows.Forms.CheckBox isAlwaysShow_checkBox;
        private System.Windows.Forms.TextBox ttText_textBox;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Button fontLoad_button;
        private System.Windows.Forms.Button bgLoad_button;
        private System.Windows.Forms.TextBox omega_textBox;
        private System.Windows.Forms.TextBox appearTime_textBox;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.RadioButton dynamic_radioButton;
        private System.Windows.Forms.RadioButton moving_radioButton;
        private System.Windows.Forms.RadioButton staticBarrier_radioButton;
        private System.Windows.Forms.Button setBounceParam_button;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox period_textBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox amplitude_textBox;
        private System.Windows.Forms.CheckBox bounceBonus_checkBox;
        private System.Windows.Forms.CheckBox testMoving_checkBox;
        private System.Windows.Forms.Button addWaypoint_button;
        private System.Windows.Forms.Button deleteWaypoint_button;
        private System.Windows.Forms.CheckBox isCircular_checkBox;
        private System.Windows.Forms.CheckBox isBackLayer_checkBox;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.TextBox rotation_textBox;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.TabPage DeathZonePage;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.TextBox setMass_textBox;
        private System.Windows.Forms.Label mass_label;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label friction_label;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.TextBox setFriction_textBox;
        private System.Windows.Forms.CheckBox normalDistr_checkBox;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.TabPage EnemySettingsPage;
        private System.Windows.Forms.TabPage CreateEnemyPage;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Label physBody_label;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.TextBox gravity_textBox;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox isInteractveEmitter_checkBox;
        private System.Windows.Forms.TabPage DialogPage;
        private System.Windows.Forms.GroupBox kamikaze_groupBox;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.CheckBox DrawAllBody_checkBox;
        private System.Windows.Forms.TextBox currentAnswerText_textBox;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.CheckBox isSensor_checkBox;
        private System.Windows.Forms.Label speed_label;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox setSpeed_textBox;
        private System.Windows.Forms.CheckBox moveWaypoints_checkBox;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.CheckBox isInstantPlatform_checkBox;
        private System.Windows.Forms.ComboBox backgroundList_comboBox;
        private System.Windows.Forms.TextBox parallax_textBox;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.TextBox positionY_textBox;
        private System.Windows.Forms.TextBox positionX_textBox;
        private System.Windows.Forms.TextBox layer_textBox;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.CheckBox tileVertical_checkBox;
        private System.Windows.Forms.CheckBox tileHorizontal_checkbox;
        private System.Windows.Forms.CheckBox isShadowCaster_checkBox;
        private System.Windows.Forms.TabPage LightPage;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.TextBox radius_textBox;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.TextBox lightOffsetY_textBox;
        private System.Windows.Forms.TextBox lightOffsetX_textBox;
        private System.Windows.Forms.ComboBox lights_comboBox;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Button lightColor_button;
        private System.Windows.Forms.NumericUpDown directionAngle_numericUpDown;
        private System.Windows.Forms.NumericUpDown innerAngle_numericUpDown;
        private System.Windows.Forms.NumericUpDown outerAngle_numericUpDown;
        private System.Windows.Forms.CheckBox isSky_checkBox;
        private System.Windows.Forms.TextBox mass_textBox;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.CheckBox isSolid_checkBox;
        private System.Windows.Forms.Label currentBGSize_label;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.TabPage Main_Events;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Button backgroundColor;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button47;
        private System.Windows.Forms.Button button48;
        private System.Windows.Forms.Button button49;
        private System.Windows.Forms.ListBox answers_listBox;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.ComboBox dialogScreenList_comboBox;
        private System.Windows.Forms.ComboBox nextDialogScreen_comboBox;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.NumericUpDown gammaLight_numericUpDown;
        private System.Windows.Forms.TabPage TriggerPage;
        private System.Windows.Forms.Button addDialogScreen_button;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.Label screenId_label;
        private System.Windows.Forms.Button newAnswer_button;
        private System.Windows.Forms.RadioButton radioButton_OnLeaving;
        private System.Windows.Forms.RadioButton radioButton_OnBeing;
        private System.Windows.Forms.RadioButton radioButton_OnCollisionStart;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.RadioButton radioButton_loop_Once;
        private System.Windows.Forms.RadioButton radioButton_loop_Unlimited;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.TextBox dialogText_textBox;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.Button deleteDialogScreen_button;
        private System.Windows.Forms.Button deleteAnswer_button;
        private System.Windows.Forms.Button deleteFarObject_button;
        private System.Windows.Forms.CheckBox isCircleBody_checkBox;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.RadioButton radioButton_Always;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.RadioButton radioButton_loop_useCD;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.Label label162;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Button setActorTexture_button;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.Label label165;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.Label label166;
        private System.Windows.Forms.ComboBox comboBox_fields_settings;
        private System.Windows.Forms.Label label168;
        private System.Windows.Forms.Button public_param_b_1;
        private System.Windows.Forms.Label label169;
        private System.Windows.Forms.Label label170;
        private System.Windows.Forms.NumericUpDown energyLoss_numericUpDown;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Label label173;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.Label label172;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label171;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.ComboBox comboBox_animations;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.Label label176;
        private System.Windows.Forms.Label label175;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.Button button51;
        private System.Windows.Forms.Button button52;
        private System.Windows.Forms.ComboBox comboBox_frames;
        private System.Windows.Forms.Label label177;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.Label label182;
        private System.Windows.Forms.Label label183;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.Label label181;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.Label label185;
        private System.Windows.Forms.Label label180;
        private System.Windows.Forms.ComboBox comboBox_fromTexture;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.Button button53;
        private System.Windows.Forms.Button button50;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.Label label184;
        private System.Windows.Forms.Label label174;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.Label label186;
        private System.Windows.Forms.Button button54;
        private System.Windows.Forms.Button button55;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.Button button56;
        private System.Windows.Forms.Label label187;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button57;
        private System.Windows.Forms.Button button58;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label188;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label189;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Label label191;
        private System.Windows.Forms.Label label190;
        private System.Windows.Forms.TabPage weatherPage;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton weather_Rain;
        private System.Windows.Forms.RadioButton weather_Snow;
        private System.Windows.Forms.RadioButton noWeather;
        private System.Windows.Forms.Label label193;
        private System.Windows.Forms.Label label192;
        private System.Windows.Forms.TextBox emittrtomega_textBox;
    }
}
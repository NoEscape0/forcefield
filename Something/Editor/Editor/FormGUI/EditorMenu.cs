﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using STORAGE;
using GAME;
using ENGINE.Logic;
using ENGINE.Logic.ObjAttributes;
using ENGINE.Logic.Enemy;
using Editor.Managers.MenuScreens;
using System.Reflection;
using Microsoft.Xna.Framework;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


namespace Editor.FormGUI
{
    public partial class EditorMenu : Form
    {
        public GameObject SelectedOBJ
        {
            get
            {
                return (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");
            }
        }
        void CheckIfNewGameObject()
        {
            if (was != SelectedOBJ)
            {
                RefreshAllOnce();
            }

            was = SelectedOBJ;
        }
        private GameObject was;

        //pause
        private void button48_Click(object sender, EventArgs e)
        {
            GameManager.SetMenu();
            Editor.Managers.EDrawManager.ActiveMenu = new EPauseM(Editor.Managers.EDrawManager.ActiveMenu);
        }

        //map
        private void button49_Click(object sender, EventArgs e)
        {
            GameManager.SetMap();
        }

        #region позиция формы, ее закрытие и перемещение
        public EditorMenu()
        {
            InitializeComponent();
        }

        private void EditorMenu_Load(object sender, EventArgs e)
        {
            groupBox1.Enabled = false;
            AttributestabControl.TabPages.Clear();
            SpecialObjectsTabControl.TabPages.Clear();
            AttributestabControl.TabPages.Add(StandartPage);
        }

        private void EditorMenu_LocationChanged(object sender, EventArgs e)
        {
            this.Location = location;
            Depository.EditorFormRect = new Microsoft.Xna.Framework.Rectangle(location.X, location.Y, this.Width, this.Height);
        }

        private void EditorMenu_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
            }
        }

        public bool hided = false;
        public System.Drawing.Point location
        {
            get
            {
                if (hided)
                {
                    return new System.Drawing.Point((int)(Depository.ScreenResolution.X + 25), 0);
                }
                else
                {
                    return new System.Drawing.Point((int)(Depository.ScreenResolution.X - this.Width), 0);
                }
            }
        }

        #endregion

        #region автообновление параметров (timer1)
        GameObject lastSelected;
        string typewas = "";
        bool refreshBonus = true;
        bool setParameters = false;
        DialogScreen currentDialogScreen;
        int currentAnswer = 0;

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                CheckIfNewGameObject();

                GameObject SelectedOBJ = null;
                try
                { SelectedOBJ = this.SelectedOBJ; }
                catch { };


                #region object
                if (MainTabControl.SelectedTab == Main_ObjectPage)
                {
                    if (SelectedOBJ != null)
                    {
                        lastSelected = SelectedOBJ;


                        if (AttributestabControl.SelectedTab != SelectPage)
                        {
                            checkBox1.Checked = (SelectedOBJ.data.ReadData("SelectParams") as SelectParams).Selectors_red;
                            checkBox2.Checked = (SelectedOBJ.data.ReadData("SelectParams") as SelectParams).Selectors_points;
                        }

                        if (AttributestabControl.SelectedTab == StandartPage)
                        {
                            if (SelectedOBJ.GetType() == typeof(ENGINE.Logic.Zones.StartFinishZone))
                                button5.Enabled = button6.Enabled = false;
                            else button5.Enabled = button6.Enabled = true;

                            label22.Text = string.Format("X {0}   Y {1}", Math.Round(SelectedOBJ.Gtexture.Position.X), Math.Round(SelectedOBJ.Gtexture.Position.Y));
                            label24.Text = SelectedOBJ.Gtexture.Scale.ToString();
                            label56.Text = "Width: " + SelectedOBJ.Gtexture.TextureResolution.width + " Height: " + SelectedOBJ.Gtexture.TextureResolution.height;
                            button13.Text = SelectedOBJ.Gtexture.Effects.ToString();

                            label28.Text = SelectedOBJ.Gtexture.Folder;
                            label100.Text = (SelectedOBJ.rotation * 180 / Math.PI).ToString("0.0");
                            isShadowCaster_checkBox.Checked = lastSelected.isShadowCaster;

                            label6.Text = Math.Round(SelectedOBJ.Gtexture.Layer, 2).ToString();
                            trackBar1.Value = (int)(SelectedOBJ.Gtexture.Layer * 100);
                        }

                        if (AttributestabControl.SelectedTab == BarrierPage)
                        {
                            ABarrier attr = (SelectedOBJ.data.ReadData("ABarrier") as ABarrier);
                            label33.Text = attr.MarginLeft.ToString();
                            label34.Text = attr.MarginRight.ToString();
                            label36.Text = attr.MarginDown.ToString();
                            label38.Text = attr.MarginTop.ToString();
                            label40.Text = String.Format("Максимальный отступ X: {0}    Y {1}", SelectedOBJ.Gtexture.TextureResolution.width, SelectedOBJ.Gtexture.TextureResolution.height);
                            if (SelectedOBJ.body != null)
                            {
                                staticBarrier_radioButton.Checked = SelectedOBJ.body.BodyType == FarseerPhysics.Dynamics.BodyType.Static;
                                dynamic_radioButton.Checked = SelectedOBJ.body.BodyType == FarseerPhysics.Dynamics.BodyType.Dynamic;
                                moving_radioButton.Checked = SelectedOBJ.body.BodyType == FarseerPhysics.Dynamics.BodyType.Kinematic;
                                mass_label.Text = SelectedOBJ.body.Mass.ToString();
                                friction_label.Text = SelectedOBJ.body.Friction.ToString();
                                isSensor_checkBox.Checked = SelectedOBJ.body.FixtureList[0].IsSensor;
                                isCircleBody_checkBox.Checked = SelectedOBJ.isCircleBody;
                            }
                            testMoving_checkBox.Checked = attr.testMoving;
                            isCircular_checkBox.Checked = attr.isCircular;
                            speed_label.Text = attr.speed.ToString();
                            moveWaypoints_checkBox.Checked = attr.moveWaypoints;
                            isInstantPlatform_checkBox.Checked = attr.isInstant;
                        }

                        if (AttributestabControl.SelectedTab == RpgCharacterPage)
                        {
                            label42.Text = (SelectedOBJ.data.ReadData("ARpgCharacterParams") as ARpgCharacter.CharacterParams).HP.ToString();
                            label46.Text = (SelectedOBJ.data.ReadData("ARpgCharacterParams") as ARpgCharacter.CharacterParams).MaxHP.ToString();

                            label47.Text = (SelectedOBJ.data.ReadData("ARpgCharacterParams") as ARpgCharacter.CharacterParams).MP.ToString();
                            label48.Text = (SelectedOBJ.data.ReadData("ARpgCharacterParams") as ARpgCharacter.CharacterParams).MaxMp.ToString();

                            label31.Text = (SelectedOBJ.data.ReadData("ARpgCharacterParams") as ARpgCharacter.CharacterParams).Coins.ToString();
                        }

                        if (AttributestabControl.SelectedTab == MovementHeroPage)
                        {
                            GameObject hero = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");
                            ENGINE.Logic.ObjAttributes.MoveController controller = (hero.data.ReadData("MController") as ENGINE.Logic.ObjAttributes.MoveController);
                            label49.Text = controller.RunSpeed.ToString();
                            label51.Text = controller.jumpStrength.ToString();
                            label52.Text = controller.maxJumpSpeed.ToString();
                        }

                        if (AttributestabControl.SelectedTab == DragPage)
                        {
                            label191.Text = Config.Snap_to_grid.ToString();
                        }


                        if (AttributestabControl.SelectedTab == BonusPage)
                        {
                            if (comboBox3.Items.Count == 0)
                            {
                                for (int i = 0; i < Enum.GetNames(typeof(ENGINE.Logic.Bonus.BonusType)).Length; ++i)
                                {
                                    string current_enum_name = Enum.GetName((typeof(ENGINE.Logic.Bonus.BonusType)), i).ToString(); // порядковый аттрибут
                                    comboBox3.Items.Add(current_enum_name);
                                }
                            }

                            label61.Text = Enum.GetName(typeof(ENGINE.Logic.Bonus.BonusType),
                                (SelectedOBJ.data.ReadData("BonusStandart") as ENGINE.Logic.Bonus.BonusStandart).type);

                            label62.Text = (SelectedOBJ.data.ReadData("BonusStandart") as ENGINE.Logic.Bonus.BonusStandart).value.ToString();
                            if (refreshBonus)
                            {
                                period_textBox.Text = ABonus.Period.ToString();
                                amplitude_textBox.Text = ABonus.amplitude.ToString();
                                bounceBonus_checkBox.Checked = ABonus.isBounced;
                                refreshBonus = false;
                            }
                        }



                        //==========================================================
                        if (AttributestabControl.SelectedTab == AnimationPage)
                        {


                            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);

                            checkBox10.Checked = anims.AnimationEnabled ? true : false;


                            if (anims.IsPlaying & anims.current_animation != null)
                            {
                                int framenow = 0;
                                for (int i = 0; i < anims.current_animation.Frames.Count; ++i)
                                {
                                    if (anims.current_animation.current_frame == anims.current_animation.Frames[i]) framenow = i;
                                }
                                label86.Text = "animation:" + anims.current_animation.Name +
                                    "[" + framenow.ToString() + @"\" + anims.current_animation.Frames.Count.ToString() + "]";
                            }
                            else label86.Text = "--";


                            if (anims.current_animation != null)
                            {
                                label173.Text = "w: " + anims.current_animation.FrameResolution.width.ToString() + "   h:" +
                                    anims.current_animation.FrameResolution.height.ToString();
                                label176.Text = anims.current_animation.Name;
                                checkBox9.Checked = anims.current_animation.looped ? true : false;
                                groupBox14.Enabled = true;


                                if (anims.current_animation.current_frame != null)
                                {
                                    label179.Text = anims.current_animation.current_frame.delay.ToString();
                                    label186.Text = "x: " + anims.current_animation.current_frame.Xcoord.ToString() + "     y:" +
                                        anims.current_animation.current_frame.Ycoord.ToString();

                                    groupBox15.Enabled = true;
                                }
                                else groupBox15.Enabled = false;
                            }
                            else
                            {
                                label173.Text = "--";
                                label176.Text = "--";
                                checkBox9.Checked = false;
                                groupBox14.Enabled = false;

                                groupBox15.Enabled = false;
                            }

                            if (!textBox31.Focused) textBox31.Text = "";
                            if (!textBox32.Focused) textBox32.Text = "";



                        }


                        if (AttributestabControl.SelectedTab == PortalPage)
                        {
                            twoSides_checkBox.Checked = (SelectedOBJ.data.ReadData("APortal") as APortal).TwoSided;
                            isExit_checkBox.Checked = (SelectedOBJ.data.ReadData("APortal") as APortal).isExit;
                            isInstant_checkBox.Checked = (SelectedOBJ.data.ReadData("APortal") as APortal).isInstant;
                            isInteractive_checkBox.Checked = (SelectedOBJ.data.ReadData("APortal") as APortal).IsInteractive;
                            /*if (transitionTime_textBox.Text=="0")*/
                            transitionTime_textBox.Text = (SelectedOBJ.data.ReadData("APortal") as APortal).transitionTime.ToString("0.00");
                            /*if (exitTime_textBox.Text == "0")*/
                            exitTime_textBox.Text = (SelectedOBJ.data.ReadData("APortal") as APortal).exitTime.ToString("0.00");

                        }

                        if (AttributestabControl.SelectedTab == ToolTipPage)
                        {
                            ATooltip at = (SelectedOBJ.data.ReadData("ATooltip") as ATooltip);
                            ttWidth_numericUpDown.Value = (int)at.Width;
                            ttHeight_numericUpDown.Value = (int)at.Height;
                            ttOffsetX_numericUpDown.Value = (int)at.offset.X;
                            ttOffsetY_numericUpDown.Value = (int)at.offset.Y;
                            isAlwaysShow_checkBox.Checked = at.isAlwaysShow;
                            if (ttText_textBox.Text != at.text) ttText_textBox.Text = at.text; // не Text
                            bgLoad_button.Text = at.BGName;
                            fontLoad_button.Text = at.FontName;
                            toolTip1.SetToolTip(bgLoad_button, at.BGName);
                            toolTip1.SetToolTip(fontLoad_button, at.FontName);
                            appearTime_textBox.Text = at.appearTime.ToString("0.00");
                            omega_textBox.Text = at.omega.ToString("0.00");
                        }

                        if (AttributestabControl.SelectedTab == DeathZonePage)
                        {
                            checkBox6.Checked = (SelectedOBJ.data.ReadData("ADeathZone") as ADeathZone).InstantDeath;
                            label104.Text = (SelectedOBJ.data.ReadData("ADeathZone") as ADeathZone).DecreaseValue.ToString();

                            if ((SelectedOBJ.data.ReadData("ADeathZone") as ADeathZone).AbsoluteValue == true) radioButton1.Checked = true; else radioButton1.Checked = false;
                        }

                        if (AttributestabControl.SelectedTab == DialogPage)
                        {
                            ADialog dialog = (SelectedOBJ.data.ReadData("ADialog") as ADialog);

                        }

                        if (AttributestabControl.SelectedTab == LightPage)
                        {
                            ENGINE.Logic.Lights.Light light = (SelectedOBJ.data.ReadData("ALight") as ALight).light;
                            lightOffsetX_textBox.Text = light.offset.X.ToString();
                            lightOffsetY_textBox.Text = light.offset.Y.ToString();
                            radius_textBox.Text = light.LightRadius.ToString();
                            outerAngle_numericUpDown.Value = (int)Math.Round(light.outerAngle * 180 / Math.PI);
                            innerAngle_numericUpDown.Value = (int)Math.Round(light.innerAngle * 180 / Math.PI);
                            directionAngle_numericUpDown.Value = (int)Math.Round(Math.Atan2(light.direction.Y, light.direction.X) * 180 / Math.PI);
                            lightColor_button.BackColor = System.Drawing.Color.FromArgb(light.color.R, light.color.G, light.color.B);
                            gammaLight_numericUpDown.Value = (int)(light.gamma * 100);
                        }


                        if (AttributestabControl.SelectedTab == TriggerPage)
                        {
                            RefreshTriggerPage();
                        }
                    }
                }
                #endregion

                #region world
                if (MainTabControl.SelectedTab == Main_LevelPage)
                {

                    label16.Text = string.Format("X {0} : Y {1}", Math.Round(GameManager.LvlController.camera.CameraPosition.X), Math.Round(GameManager.LvlController.camera.CameraPosition.Y));
                    label18.Text = Convert.ToString(Math.Round(GameManager.LvlController.camera.Zoom, 2));
                    label20.Text = GameManager.LvlController.ObjectArray.Count.ToString();
                    physBody_label.Text = GameManager.LvlController.PhysicWorld.BodyList.Count.ToString();

                    label50.Text = GAME.GameManager.LvlController.Gravity.Y.ToString();

                    checkBox3.Checked = Config.show_collision_rectangles;
                    DrawAllBody_checkBox.Checked = Editor.Managers.EDrawManager.isDrawAllBody;
                    //bg
                    checkBox4.Checked = GameManager.LvlController.Background.Show_in_game;
                    checkBox5.Checked = GameManager.LvlController.Background.Show_in_editor;

                    label53.Text = "'" + LevelController.ThisLevelFolder.Replace(AppDomain.CurrentDomain.BaseDirectory, "") + "'";


                    backgroundColor.Text = GameManager.LvlController.BackgroundFillColor.ToString();

                    if (!textBox33.Focused) textBox33.Text = GameManager.LvlController.AmbientLight.ToString();

                    checkBox7.Checked = GameManager.LvlController.EnableSound_E;
                    checkBox8.Checked = GameManager.LvlController.EnableSound_G;
                }


                #endregion

                #region Special Objects
                if (MainTabControl.SelectedTab == Main_SpecialObjects)
                {
                    if (SelectedOBJ != null)
                    {
                        lastSelected = SelectedOBJ;

                        if (SelectedOBJ.GetType().ToString() != typewas)
                        {
                            string currentobjType = SelectedOBJ.GetType().ToString();
                            currentobjType = currentobjType.Substring(currentobjType.LastIndexOf(".") + 1);
                            label69.Text = currentobjType;
                            SpecialObjectsTabControl.TabPages.Clear();


                            //emitters
                            if (SelectedOBJ.GetType().IsSubclassOf(typeof(ENGINE.Logic.Particles.BaseEmitter)))
                            {
                                SpecialObjectsTabControl.TabPages.Clear();
                                ENGINE.Logic.Particles.BaseEmitter emitter = (ENGINE.Logic.Particles.BaseEmitter)SelectedOBJ;
                                SpecialObjectsTabControl.TabPages.Add(EmitterPage);
                                SpecialObjectsTabControl.TabPages.Add(CreateEmitterPage);
                                alphablend_checkBox.Checked = emitter.alphablend;
                                Pps_numericUpDown.Value = (int)Math.Round(emitter.Pps);
                                minSpeed_textBox.Text = emitter.minSpeed.ToString(".000");
                                maxSpeed_textBox.Text = emitter.maxSpeed.ToString(".000");

                                minSize_textBox.Text = emitter.minSize.ToString(".000"); ;
                                maxSize_textBox.Text = emitter.maxSize.ToString(".000"); ;
                                width_textBox.Text = emitter.width.ToString();
                                height_textBox.Text = emitter.height.ToString();
                                alphaVel_textBox.Text = (255 / emitter.alphaVel).ToString();
                                angleVel_textBox.Text = (emitter.angleVelocity * 180 / Math.PI).ToString("00.00");
                                emittrtomega_textBox.Text = (emitter.omega * 180 / Math.PI).ToString("00.00");
                                startAngle_numericUpDown.Value = (int)(emitter.startAngle * 180 / Math.PI);
                                mainAngle_numericUpDown.Value = (int)(emitter.angleMain * 180 / Math.PI);
                                secondaryAngle_numericUpDown.Value = (int)(emitter.angleSecondary * 360 / Math.PI);
                                sizeVel_textBox.Text = emitter.sizeVel.ToString();
                                bothWays_checkBox.Text = "Излучать в обе стороны.";
                                height_label76.Text = "Высота";
                                width_label75.Text = "Ширина";
                                tMaxAlpha_textBox.Text = emitter.tMaxAlpha.ToString();
                                Color_button.BackColor = System.Drawing.Color.FromArgb(emitter.color.R, emitter.color.G, emitter.color.B);
                                isBackLayer_checkBox.Checked = emitter.isBackLayer;
                                normalDistr_checkBox.Checked = emitter.isNormalDistribution;
                                isInteractveEmitter_checkBox.Checked = emitter.isInteractive;
                                mass_textBox.Text = emitter.Mass.ToString();
                                isSolid_checkBox.Checked = emitter.isSolid;
                                energyLoss_numericUpDown.Value = (decimal)emitter.EnergyLoss;


                                if (emitter.GetType() == typeof(ENGINE.Logic.Particles.CircleEmitter))
                                {
                                    bothWays_checkBox.Text = "Излучать внутрь.";
                                    height_label76.Text = "Внешний радиус";
                                    width_label75.Text = "Внутренний радиус";
                                }
                                bothWays_checkBox.Checked = emitter.emitBothWay;
                                ttl_textBox.Text = emitter.ttl.ToString();
                            }

                            //enemy settings
                            if (SelectedOBJ.GetType().IsSubclassOf(typeof(Enemy)))
                            {
                                //adding tab pages
                                SpecialObjectsTabControl.TabPages.Clear();
                                SpecialObjectsTabControl.TabPages.Add(EnemySettingsPage);
                                SpecialObjectsTabControl.TabPages.Add(CreateEnemyPage);

                                //common enemy settings
                                ENGINE.Logic.Enemy.Enemy E = (SelectedOBJ as Enemy);

                                label118.Text = E.attacDelay.ToString();
                                label119.Text = E.AttacRange.ToString();
                                label120.Text = E.Damage.ToString();
                                label121.Text = E.MaxHP.ToString();
                                label122.Text = E.MaxMP.ToString();
                                label123.Text = E.HP.ToString();
                                label124.Text = E.MP.ToString();
                                label125.Text = E.ActivationDist.ToString();

                                //наземный тип
                                if (SelectedOBJ.GetType().IsSubclassOf(typeof(Movable_E)))
                                {
                                    //refresh
                                    ENGINE.Logic.Enemy.Movable_E M = (SelectedOBJ as ENGINE.Logic.Enemy.Movable_E);
                                    label129.Text = M.RunSpeed.ToString();
                                    label130.Text = M.JumpStrength.ToString();
                                    label131.Text = M.maxJumpSpeed.ToString();
                                }
                            }


                            typewas = SelectedOBJ.GetType().ToString();
                        }
                    }
                    else
                    {
                        SpecialObjectsTabControl.TabPages.Remove(EnemySettingsPage);
                        SpecialObjectsTabControl.TabPages.Remove(EmitterPage);

                        label69.Text = "[Объект не выделен]";
                        typewas = "";
                    }
                }
                #endregion

                #region размер окна
                if (MainTabControl.SelectedTab == Main_library)
                {
                    this.Height = 40;
                    Library_page.Show();
                }
                else
                {
                    this.Height = 657;
                    Library_page.Hide();
                }
                #endregion

                #region Events
                if (MainTabControl.SelectedTab == Main_Events)
                {
                    label147.Text = EditorEventHandler.events.Count.ToString();
                    label149.Text = EditorEventHandler.MaxEvents.ToString();


                    textBox22.Text = "";

                    for (int i = 0; i < EditorEventHandler.events.Count; ++i)
                    {
                        textBox22.Text += EditorEventHandler.events[i].name + "\r\n";
                    }
                }
                #endregion
            }
        }
        #endregion

        #region Object : Standart
        private void Reset() //сброс к дефолту вкладок
        {
            groupBox1.Enabled = false;
            label2.Text = "Нет";
            AttributestabControl.TabPages.Clear();
            comboBox1.Items.Clear();
            button6.Enabled = true;

            label6.Text = "";
            SpecialObjectsTabControl.TabPages.Remove(EmitterPage);
        }

        private void RefreshAllOnce()
        {
            Reset();
            RefreshTabs();
            RefreshComboBoxes();
            RefreshAnimationPage();
        }

        private void RefreshTabs() //когда мы выбираем новый объект или сбрасываем выделение
        {
            groupBox1.Enabled = true;
            label2.Text = "Да";

            GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");
            //в зависимости от аттрибутов добавить вкладки
            if (!AttributestabControl.TabPages.Contains(StandartPage) & SelectedOBJ != null)
                AttributestabControl.TabPages.Add(StandartPage);
            AddAttrTab(Functions.Attr.Drag, DragPage);
            AddAttrTab(Functions.Attr.Select, SelectPage);
            AddAttrTab(Functions.Attr.Draw, DrawPage);
            AddAttrTab(Functions.Attr.Barrier, BarrierPage);
            AddAttrTab(Functions.Attr.MovementHero, MovementHeroPage);
            AddAttrTab(Functions.Attr.RpgCharacter, RpgCharacterPage);
            AddAttrTab(Functions.Attr.Bonus, BonusPage);
            AddAttrTab(Functions.Attr.Animation, AnimationPage);
            AddAttrTab(Functions.Attr.Portal, PortalPage);
            AddAttrTab(Functions.Attr.Tooltip, ToolTipPage);
            AddAttrTab(Functions.Attr.DeathZone, DeathZonePage);
            AddAttrTab(Functions.Attr.Dialog, DialogPage);
            AddAttrTab(Functions.Attr.Light, LightPage);
            AddAttrTab(Functions.Attr.Trigger, TriggerPage);

        }

        private void AddAttrTab(Functions.Attr attribute, TabPage page)
        {
            if (Functions.HaveAttribute(attribute, SelectedOBJ) & !AttributestabControl.TabPages.Contains(page))
            {
                List<Functions.Attr> edOnlyAttr = (List<Functions.Attr>)SelectedOBJ.data.ReadData("editorOnly_attr"); //вызываем массив для изменения аттрибутов

                if (edOnlyAttr.Contains(attribute)) page.Text = page.Name.Replace("Page", " [E]");
                else page.Text = page.Name.Replace("Page", "");

                AttributestabControl.TabPages.Add(page);
            }
        }

        private void RefreshComboBoxes() // вызывать при выделении нового объекта и добавлении аттрибута -- refreshTabs
        {
            if (SelectedOBJ == null) return;

            comboBox1.Items.Clear();
            comboBox2.Items.Clear();

            for (int i = 0; i < Enum.GetNames(typeof(Functions.Attr)).Length; ++i)
            {
                string current_enum_name = Enum.GetName((typeof(Functions.Attr)), i).ToString(); // порядковый аттрибут

                bool have_this_attr = false;
                for (int i2 = 0; i2 < SelectedOBJ.AddedAttributes.Count; ++i2)
                {
                    if (SelectedOBJ.AddedAttributes[i2].ToString() == current_enum_name)
                    {
                        have_this_attr = true;
                        break;
                    }
                }

                Functions.Attr a = (Functions.Attr)i;
                if (!have_this_attr)
                {
                    comboBox1.Items.Add(a.ToString());
                }
                else
                {
                    comboBox2.Items.Add(a.ToString());
                }
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");
            SelectedOBJ.Gtexture.Layer = (float)trackBar1.Value / 100.0f;
            label6.Text = Math.Round(SelectedOBJ.Gtexture.Layer, 2).ToString();
            GameManager.LvlController.SortByLayer();
        }

        private void button5_Click(object sender, EventArgs e) //add attr
        {
            if (comboBox1.SelectedItem != null)
            {
                for (int i = 0; i < Enum.GetNames(typeof(Functions.Attr)).Length; ++i)
                {
                    Functions.Attr current = (Functions.Attr)i;

                    if (current.ToString() == comboBox1.SelectedItem.ToString())
                    {
                        GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");
                        Functions.AddAttribute(current, SelectedOBJ);
                        break;
                    }
                }

                RefreshAllOnce();
            }
        }

        private void button6_Click(object sender, EventArgs e)  //del attr
        {
            if (comboBox2.SelectedItem != null)
            {
                for (int i = 0; i < Enum.GetNames(typeof(Functions.Attr)).Length; ++i) //перечисление всех аттрибутов
                {
                    Functions.Attr current = (Functions.Attr)i;

                    if (current.ToString() == comboBox2.SelectedItem.ToString())
                    {
                        GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");
                        Functions.DeleteAttribute(current, SelectedOBJ);
                        if (SelectedOBJ.body != null)
                        {
                            GAME.GameManager.LvlController.PhysicWorld.RemoveBody(SelectedOBJ.body);
                            SelectedOBJ.body = null;
                        }
                        if (current == Functions.Attr.Light)
                        {
                            lastSelected.HasLight = false;
                        }
                        break;
                    }
                }

                RefreshAllOnce();
            }
        }

        private void button7_Click(object sender, EventArgs e) // editor \ game changer 
        {
            if (comboBox2.SelectedItem != null)
            {
                for (int i = 0; i < Enum.GetNames(typeof(Functions.Attr)).Length; ++i) //перечисление всех аттрибутов
                {
                    Functions.Attr current = (Functions.Attr)i;

                    if (current.ToString() == comboBox2.SelectedItem.ToString())
                    {
                        GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");

                        List<Functions.Attr> edOnlyArrt = (List<Functions.Attr>)SelectedOBJ.data.ReadData("editorOnly_attr"); //вызываем массив для изменения аттрибутов


                        if (!edOnlyArrt.Contains(current)) //добавляем в массив edOnly
                        {
                            edOnlyArrt.Add(current);
                        }
                        else //удаляем из массива
                        {
                            edOnlyArrt.Remove(current);
                        }
                    }
                }


                RefreshAllOnce();
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e) //combobox удаления поменялся
        {
            if (comboBox2.SelectedItem != null)
            {
                for (int i = 0; i < Enum.GetNames(typeof(Functions.Attr)).Length; ++i) //перечисление всех аттрибутов
                {
                    Functions.Attr current = (Functions.Attr)i;

                    if (current.ToString() == comboBox2.SelectedItem.ToString())
                    {
                        if (current == Functions.Attr.Draw | current == Functions.Attr.Drag | current == Functions.Attr.Select)
                            button6.Enabled = false;
                        else button6.Enabled = true;


                        break;
                    }
                }
            }
        }

        private void button3_Click(object sender, EventArgs e) //clone
        {
            EditorEventHandler.AddEvent("clone");


            object SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");
            ((SelectedOBJ as GameObject).data.ReadData("SelectParams") as SelectParams).DeSelect(SelectedOBJ as GameObject);

            object clone = Functions.DeepClone(SelectedOBJ);

            (clone as GameObject).Gtexture.Position.X += 50;
            (clone as GameObject).Gtexture.Position.Y += 50;
            if ((clone as GameObject).Gtexture.Layer > 0.01f) (clone as GameObject).Gtexture.Layer -= 0.01f;


            ((clone as GameObject).data.ReadData("SelectParams") as SelectParams).Select(clone as GameObject);

            GameManager.LvlController.ObjectArray.Add(clone as GameObject);

            GameManager.LvlController.SortByLayer();
            if (Functions.HaveAttribute(Functions.Attr.Bonus, (clone as GameObject)))
                ((clone as GameObject).data.ReadData("ABonus") as ABonus).originalPosition = (clone as GameObject).Gtexture.Position;
            if ((clone as ENGINE.Logic.Particles.BaseEmitter) != null)
                (clone as ENGINE.Logic.Particles.BaseEmitter).engine = (SelectedOBJ as ENGINE.Logic.Particles.BaseEmitter).engine;
        }

        private void button8_Click(object sender, EventArgs e) //delete object
        {
            try
            {
                EditorEventHandler.AddEvent("delete obj");

                GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");

                if (Functions.HaveAttribute(Functions.Attr.Portal, SelectedOBJ)) { GameManager.LvlController.ObjectArray.Remove((SelectedOBJ.data.ReadData("APortal") as APortal).dest); }

                GameManager.LvlController.ObjectArray.Remove(SelectedOBJ);
                if (SelectedOBJ.body != null) GameManager.LvlController.PhysicWorld.RemoveBody(SelectedOBJ.body);
                GameManager.LvlController.CommonData.WriteDataSimple("SelectedOBJ", null);
                GameManager.LvlController.RefindHero();
            }
            catch { };
        }

        private void button2_Click(object sender, EventArgs e) //create
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Выберете файл текстуры xnb";
            dialog.Filter = "XNB images (*.xnb)|*.xnb";
            dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + @"Content\Textures\";

            DialogResult res = dialog.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {
                string file = dialog.FileName;

                file = file.Replace(".xnb", "");
                file = file.Replace(AppDomain.CurrentDomain.BaseDirectory, "");
                file = file.Substring(8);


                EditorEventHandler.AddEvent("adding new GameObject  '" + file + "'");

                GameObject ob = new GameObject(file);
                ob.Gtexture.Position = GameManager.LvlController.camera.CameraPosition;
                ob.Gtexture.Layer = 0.80f;

                GameManager.LvlController.ObjectArray.Add(ob);
                GameManager.LvlController.SortByLayer();

                for (int i = 0; i < GameManager.LvlController.ObjectArray.Count; ++i)
                {
                    if (GameManager.LvlController.ObjectArray[i].GetType() == typeof(GameObject))
                        (GameManager.LvlController.ObjectArray[i].data.ReadData("SelectParams") as SelectParams).DeSelect(GameManager.LvlController.ObjectArray[i]);
                }

                (ob.data.ReadData("SelectParams") as SelectParams).Select(ob);
            }
        }

        private void deleteFarObject_button_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < GAME.GameManager.LvlController.ObjectArray.Count; i++)
            {
                GameObject go = GAME.GameManager.LvlController.ObjectArray[i];
                FarseerPhysics.Dynamics.Body MainBody = go.body;
                if (MainBody == null) continue;
                if (MainBody.Position.Y > ConvertUnits.ToSimUnits(100000))
                {
                    GAME.GameManager.LvlController.PhysicWorld.RemoveBody(MainBody);
                    go.body = null;
                    GAME.GameManager.LvlController.ObjectArray.Remove(go);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e) //scale x ++
        {
            GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");

            SelectedOBJ.Gtexture.Scale.X += 0.1f;
            if (SelectedOBJ.body != null)
            {
                GAME.GameManager.LvlController.PhysicWorld.RemoveBody(SelectedOBJ.body);
                SelectedOBJ.body = null;
            }
        }

        private void button12_Click(object sender, EventArgs e) //scale x --
        {
            GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");

            if (SelectedOBJ.Gtexture.Scale.X > 0.1f) SelectedOBJ.Gtexture.Scale.X -= 0.1f;

            if (SelectedOBJ.body != null)
            {
                GAME.GameManager.LvlController.PhysicWorld.RemoveBody(SelectedOBJ.body);
                SelectedOBJ.body = null;
            }
        }

        //sc y ++
        private void button25_Click(object sender, EventArgs e)
        {
            GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");

            SelectedOBJ.Gtexture.Scale.Y += 0.1f;
            if (SelectedOBJ.body != null)
            {
                GAME.GameManager.LvlController.PhysicWorld.RemoveBody(SelectedOBJ.body);
                SelectedOBJ.body = null;
            }
        }
        //sc y --
        private void button33_Click(object sender, EventArgs e)
        {
            GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");

            if (SelectedOBJ.Gtexture.Scale.Y > 0.1f) SelectedOBJ.Gtexture.Scale.Y -= 0.1f;

            if (SelectedOBJ.body != null)
            {
                GAME.GameManager.LvlController.PhysicWorld.RemoveBody(SelectedOBJ.body);
                SelectedOBJ.body = null;
            }
        }



        //scale x +5
        private void button40_Click(object sender, EventArgs e)
        {
            GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");

            SelectedOBJ.Gtexture.Scale.X += 5.0f;
            if (SelectedOBJ.body != null)
            {
                GAME.GameManager.LvlController.PhysicWorld.RemoveBody(SelectedOBJ.body);
                SelectedOBJ.body = null;
            }
        }

        //scale x -5
        private void button42_Click(object sender, EventArgs e)
        {
            GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");

            if (SelectedOBJ.Gtexture.Scale.X > 5) SelectedOBJ.Gtexture.Scale.X -= 5;

            if (SelectedOBJ.body != null)
            {
                GAME.GameManager.LvlController.PhysicWorld.RemoveBody(SelectedOBJ.body);
                SelectedOBJ.body = null;
            }
        }

        //scale y +5
        private void button43_Click(object sender, EventArgs e)
        {
            GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");

            SelectedOBJ.Gtexture.Scale.Y += 5;
            if (SelectedOBJ.body != null)
            {
                GAME.GameManager.LvlController.PhysicWorld.RemoveBody(SelectedOBJ.body);
                SelectedOBJ.body = null;
            }
        }

        //scale y -5
        private void button45_Click(object sender, EventArgs e)
        {
            GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");

            if (SelectedOBJ.Gtexture.Scale.Y > 5f) SelectedOBJ.Gtexture.Scale.Y -= 5f;

            if (SelectedOBJ.body != null)
            {
                GAME.GameManager.LvlController.PhysicWorld.RemoveBody(SelectedOBJ.body);
                SelectedOBJ.body = null;
            }
        }

        private void button13_Click(object sender, EventArgs e) //effects changer
        {
            GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");


            switch (SelectedOBJ.Gtexture.Effects)
            {
                case Microsoft.Xna.Framework.Graphics.SpriteEffects.FlipHorizontally:
                    {
                        SelectedOBJ.Gtexture.Effects = Microsoft.Xna.Framework.Graphics.SpriteEffects.FlipVertically;
                        break;
                    }
                case Microsoft.Xna.Framework.Graphics.SpriteEffects.FlipVertically:
                    {
                        SelectedOBJ.Gtexture.Effects = Microsoft.Xna.Framework.Graphics.SpriteEffects.None;
                        break;
                    }

                case Microsoft.Xna.Framework.Graphics.SpriteEffects.None:
                    {
                        SelectedOBJ.Gtexture.Effects = Microsoft.Xna.Framework.Graphics.SpriteEffects.FlipHorizontally;
                        break;
                    }

            }
        }

        private void button11_Click(object sender, EventArgs e) // edit posistion
        {
            GameObject hero = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");

            try
            {
                hero.Gtexture.Position.X = Convert.ToSingle(textBox18.Text);
            }
            catch { }

            try
            {
                hero.Gtexture.Position.Y = Convert.ToSingle(textBox17.Text);
            }
            catch { }
            float f = 0;
            bool success = float.TryParse(rotation_textBox.Text, out f);
            if (success) hero.rotation = (float)Math.PI * f / 180;

            textBox18.Text = "";
            textBox17.Text = "";
            if (Functions.HaveAttribute(Functions.Attr.Bonus, hero))
                (hero.data.ReadData("ABonus") as ABonus).originalPosition = hero.Gtexture.Position;
        }

        private void isShadowCaster_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            lastSelected.isShadowCaster = isShadowCaster_checkBox.Checked;
        }


        private void button46_Click(object sender, EventArgs e)
        {
            try
            {
                EditorEventHandler.AddEvent("delete (simular textures)");

                string folder = (GameManager.LvlController.CommonData.ReadData("SelectedOBJ") as GameObject).Gtexture.Folder;

                if (Functions.HaveAttribute(Functions.Attr.Portal, (GameManager.LvlController.CommonData.ReadData("SelectedOBJ") as GameObject)))
                {
                    MessageBox.Show("Порталы не удаляет");
                    return;
                }

                List<GameObject> toRemove = new List<GameObject>();

                for (int i = 0; i < GameManager.LvlController.ObjectArray.Count; ++i)
                {
                    if (GameManager.LvlController.ObjectArray[i].Gtexture.Folder == folder)
                    {
                        if (GameManager.LvlController.ObjectArray[i].body != null) GameManager.LvlController.PhysicWorld.RemoveBody(GameManager.LvlController.ObjectArray[i].body);

                        toRemove.Add(GameManager.LvlController.ObjectArray[i]);
                    }

                }

                foreach (GameObject o in toRemove) GameManager.LvlController.ObjectArray.Remove(o);

                GameManager.LvlController.RefindHero();
                GameManager.LvlController.CommonData.WriteDataSimple("SelectedOBJ", null);
            }
            catch { };
        }

        #endregion

        #region Object : Select
        private void checkBox1_CheckedChanged(object sender, EventArgs e) //Object : Select :: red
        {
            GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");

            (SelectedOBJ.data.ReadData("SelectParams") as SelectParams).Selectors_red = checkBox1.Checked;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e) // Select :: points
        {
            GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");

            (SelectedOBJ.data.ReadData("SelectParams") as SelectParams).Selectors_points = checkBox2.Checked;
        }
        #endregion

        #region Object : Animaton

        void RefreshAnimationPage()
        {
            if (!Functions.HaveAttribute(Functions.Attr.Animation, SelectedOBJ)) return;
            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);


            comboBox_animations.Items.Clear();
            for (int i = 0; i < anims.AnimationsArray.Count; ++i)
                comboBox_animations.Items.Add(i.ToString() + " ____" + anims.AnimationsArray[i].Name);

            for (int i = 0; i < anims.AnimationsArray.Count; ++i)
            {
                if (anims.AnimationsArray[i] == anims.current_animation)
                {
                    comboBox_animations.SelectedIndex = i;
                }
            }

            FramesRefresh();
        }

        void FramesRefresh()
        {
            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);


            //frames
            if (anims.current_animation != null)
            {
                comboBox_frames.Items.Clear();
                for (int i = 0; i < anims.current_animation.Frames.Count; ++i)
                {
                    string txt = "";
                    try
                    {
                        txt = anims.current_animation.Frames[i].TextureF.Substring(anims.current_animation.Frames[i].TextureF.LastIndexOf(@"\"));
                    }
                    catch { txt = anims.current_animation.Frames[i].TextureF; };


                    comboBox_frames.Items.Add(i.ToString() + "       x:" + anims.current_animation.Frames[i].Xcoord.ToString()
                        + "  y:  " + anims.current_animation.Frames[i].Ycoord.ToString() + "   -" + txt);
                }

                if (comboBox_frames.Items.Count > 0)
                {
                    comboBox_frames.SelectedIndex = 0;
                    anims.current_animation.current_frame = anims.current_animation.Frames[0];
                }

                if (anims.current_animation.current_frame != null)
                    FromWhichTextureRefresh();
            }
        }


        private void button57_Click(object sender, EventArgs e)
        {
            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);

            if (anims.current_animation == null) return;

            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Title = "Сохранить анимацию как...";
            dialog.Filter = "animation (*.anim)|*.anim";
            dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + "Levels";

            dialog.FileName = anims.current_animation.Name;
            DialogResult res = dialog.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {
                string file = dialog.FileName;

                // Functions.d

                System.IO.Directory.SetCurrentDirectory(System.Windows.Forms.Application.StartupPath);
                FileStream fstream = File.Open(file, FileMode.Create);
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(fstream, anims.current_animation);
                fstream.Close();
            }

        }
        private void button58_Click(object sender, EventArgs e)
        {
            SingleAnimation animation = null;
            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);


            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Загрузить анимацию...";
            dialog.Filter = "animation (*.anim)|*.anim";
            dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + "Levels";

            DialogResult res = dialog.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {
                string file = dialog.FileName;


                try
                {
                    FileStream fstream = File.Open(file, FileMode.Open);
                    BinaryFormatter binaryFormatter = new BinaryFormatter();
                    animation = (SingleAnimation)binaryFormatter.Deserialize(fstream);
                    fstream.Close();
                    anims.AnimationsArray.Add(animation);



                    //в advanced textures подгружаем текстуры

                    foreach (SingleFrame f in animation.Frames)
                    {
                        bool contains = false;
                        for (int i = 0; i < anims.AdvancedTextures.Count; ++i)
                        {
                            if (anims.AdvancedTextures[i].Folder == f.TextureF)
                            {
                                contains = true;
                                break;
                            }
                        }


                        if (!contains)
                        {
                            anims.AdvancedTextures.Add(new ENGINE.Graphics.GameTexture(true, f.TextureF));
                        }

                    }

                    RefreshAnimationPage();
                }
                catch
                { }
            }
        }

        void FromWhichTextureRefresh()
        {
            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);


            comboBox_fromTexture.Items.Clear();

            comboBox_fromTexture.Items.Add("0     " + "default");

            for (int i = 0; i < anims.AdvancedTextures.Count; ++i)
                comboBox_fromTexture.Items.Add((i + 1).ToString() + "     " + anims.AdvancedTextures[i].Folder.Substring(anims.AdvancedTextures[i].Folder.LastIndexOf(@"\")));
        }



        private void button_add_animation_Click(object sender, EventArgs e)
        {
            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);

            SingleAnimation sa = new SingleAnimation(SelectedOBJ.Gtexture.TextureResolution);
            sa.Name = "new animation";

            anims.AnimationsArray.Add(sa);
            RefreshAnimationPage();
            comboBox_animations.SelectedIndex = comboBox_animations.Items.Count - 1;
        }

        private void textBox31_TextChanged(object sender, EventArgs e)
        {
            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);

            if (anims.current_animation != null)
            {
                int i = 0;
                if (int.TryParse(textBox31.Text, out i)) anims.current_animation.FrameResolution.width = i;
            }
        }

        private void textBox32_TextChanged(object sender, EventArgs e)
        {
            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);

            if (anims.current_animation != null)
            {
                int i = 0;
                if (int.TryParse(textBox32.Text, out i)) anims.current_animation.FrameResolution.height = i;
            }
        }

        private void checkBox9_CheckedChanged(object sender, EventArgs e)
        {
            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);

            if (anims.current_animation != null)
            {
                anims.current_animation.looped = checkBox9.Checked;
            }
        }

        private void comboBox_animations_SelectedIndexChanged(object sender, EventArgs e)
        {
            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);

            anims.current_animation = anims.AnimationsArray[comboBox_animations.SelectedIndex];
            FramesRefresh();
        }

        private void checkBox10_CheckedChanged(object sender, EventArgs e)
        {
            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);
            anims.AnimationEnabled = checkBox10.Checked;
        }

        private void button50_Click(object sender, EventArgs e)
        {
            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);

            if (anims.current_animation != null)
            {
                anims.current_animation.Name = textBox34.Text;
                textBox34.Text = "";
                RefreshAnimationPage();
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);
            anims.PlayAnimation(comboBox_animations.SelectedIndex);
            RefreshAnimationPage();
        }

        private void button53_Click(object sender, EventArgs e)
        {
            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);
            anims.AnimationsArray.RemoveAt(comboBox_animations.SelectedIndex);



            RefreshAnimationPage();

            if (anims.AnimationsArray.Count >= 1)
            {
                anims.current_animation = anims.AnimationsArray[0];
                comboBox_animations.SelectedIndex = 0;
            }
            else anims.current_animation = null;
        }

        //add frame
        private void button51_Click(object sender, EventArgs e)
        {
            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);

            if (anims.current_animation == null) return;

            SingleFrame sf = new SingleFrame();
            sf.TextureF = "default";
            anims.current_animation.Frames.Add(sf);

            anims.current_animation.current_frame = anims.current_animation.Frames[anims.current_animation.Frames.Count - 1];

            RefreshAnimationPage();
            comboBox_frames.SelectedIndex = comboBox_frames.Items.Count - 1;
        }

        //del frame
        private void button52_Click(object sender, EventArgs e)
        {
            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);

            if (anims.current_animation == null) return;
            if (anims.current_animation.Frames.Count <= 0) return;

            if (comboBox_frames.SelectedIndex >= anims.current_animation.Frames.Count) return;


            anims.current_animation.Frames.RemoveAt(comboBox_frames.SelectedIndex);
            if (anims.current_animation.Frames.Count > 0) anims.current_animation.current_frame = anims.current_animation.Frames[0];
            else anims.current_animation.current_frame = null;

            int was = comboBox_frames.SelectedIndex;

            RefreshAnimationPage();

            if (was - 1 < 0) was = 1;

            if (anims.current_animation.Frames.Count > 0) comboBox_frames.SelectedIndex = was - 1;
        }

        private void textBox35_TextChanged(object sender, EventArgs e)
        {
            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);

            if (anims.current_animation.current_frame != null)
            {
                double k = 0;

                if (double.TryParse(textBox35.Text, out k))
                {
                    anims.current_animation.current_frame.delay = k;
                    label179.Text = anims.current_animation.current_frame.delay.ToString();
                }
            }
        }

        private void comboBox_frames_SelectedIndexChanged(object sender, EventArgs e)
        {
            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);

            if (comboBox_frames.SelectedIndex > anims.current_animation.Frames.Count - 1) return;

            anims.current_animation.current_frame = anims.current_animation.Frames[comboBox_frames.SelectedIndex];
            textBox35.Text = "";




            ENGINE.Graphics.GameTexture gt = null;

            foreach (ENGINE.Graphics.GameTexture ttt in anims.AdvancedTextures)
            {
                if (ttt.Folder == anims.current_animation.current_frame.TextureF)
                {
                    gt = ttt;
                    break;
                }
            }
            if (gt == null) gt = SelectedOBJ.Gtexture;


            int w = (gt.TextureResolution.width / anims.current_animation.FrameResolution.width);
            int h = (gt.TextureResolution.height / anims.current_animation.FrameResolution.height);


            textBox39.Text = w.ToString();
            textBox40.Text = h.ToString();

            //RefreshAnimationPage();
        }


        //dell texture
        private void button44_Click(object sender, EventArgs e)
        {
            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);

            if (comboBox_fromTexture.SelectedIndex != 0)
            {
                anims.AdvancedTextures.RemoveAt(comboBox_fromTexture.SelectedIndex - 1);

                FromWhichTextureRefresh();
                comboBox_fromTexture.SelectedIndex = 0;
            }
        }

        private void button27_Click(object sender, EventArgs e)
        {
            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);

            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Выберете файл текстуры xnb";
            dialog.Filter = "XNB images (*.xnb)|*.xnb";
            dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + @"Content\Textures\";

            DialogResult res = dialog.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {
                string file = dialog.FileName;

                file = file.Replace(".xnb", "");
                file = file.Replace(AppDomain.CurrentDomain.BaseDirectory, "");
                file = file.Substring(8);


                anims.AdvancedTextures.Add(new ENGINE.Graphics.GameTexture(true, file));


                FromWhichTextureRefresh();
                comboBox_fromTexture.SelectedIndex = comboBox_fromTexture.Items.Count - 1;
            }




        }

        int sp_w = 0;
        int sp_h = 0;

        //спрайтов по длине
        private void textBox39_TextChanged(object sender, EventArgs e)
        {
            int.TryParse(textBox39.Text, out sp_w);
        }

        private void textBox40_TextChanged(object sender, EventArgs e)
        {
            int.TryParse(textBox40.Text, out sp_h);
        }

        private void textBox36_TextChanged(object sender, EventArgs e)
        {
            int w = 0;
            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);

            int.TryParse(textBox36.Text, out w);

            if (w > -1 & w < sp_w) anims.current_animation.current_frame.Xcoord =
                w * anims.current_animation.FrameResolution.width;
        }

        private void textBox41_TextChanged(object sender, EventArgs e)
        {
            int h = 0;
            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);

            int.TryParse(textBox41.Text, out h);

            if (h > -1 & h < sp_h) anims.current_animation.current_frame.Ycoord =
                h * anims.current_animation.FrameResolution.height;
        }

        private void button54_Click(object sender, EventArgs e)
        {
            int s = comboBox_frames.SelectedIndex;
            if (comboBox_fromTexture.SelectedIndex == -1) return;
            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);
            if (comboBox_fromTexture.SelectedIndex == 0) anims.current_animation.current_frame.TextureF = "default";
            else anims.current_animation.current_frame.TextureF = anims.AdvancedTextures[comboBox_fromTexture.SelectedIndex - 1].Folder;

            FramesRefresh();

            comboBox_frames.SelectedIndex = s;
        }

        private void button55_Click(object sender, EventArgs e)
        {
            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);

            if (anims.current_animation == null) return;

            SingleFrame sf = new SingleFrame();

            sf = anims.current_animation.Frames[comboBox_frames.SelectedIndex].Clone();


            anims.current_animation.Frames.Add(sf);

            anims.current_animation.current_frame = anims.current_animation.Frames[anims.current_animation.Frames.Count - 1];

            RefreshAnimationPage();
            comboBox_frames.SelectedIndex = comboBox_frames.Items.Count - 1;
        }



        private void button56_Click(object sender, EventArgs e)
        {
            Animations anims = (SelectedOBJ.data.ReadData("ANIMATION") as Animations);
            if (anims.current_animation == null) return;

            double k = 0;

            if (double.TryParse(textBox42.Text, out k))
            {
                for (int i = 0; i < anims.current_animation.Frames.Count; ++i)
                {
                    anims.current_animation.Frames[i].delay = k;
                }
            }


            FramesRefresh();
        }
        #endregion

        #region Object : Barrier
        private void button15_Click(object sender, EventArgs e) //barrier - отступ
        {
            int l = -1;
            int r = -1;
            int t = -1;
            int d = -1;

            GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");

            try { l = Convert.ToInt32(textBox9.Text); }
            catch { };

            try { r = Convert.ToInt32(textBox10.Text); }
            catch { };

            try { t = Convert.ToInt32(textBox12.Text); }
            catch { };

            try { d = Convert.ToInt32(textBox11.Text); }
            catch { };


            if (l >= 0 & l < SelectedOBJ.Gtexture.TextureResolution.width) (SelectedOBJ.data.ReadData("ABarrier") as ABarrier).MarginLeft = l;
            if (r >= 0 & r < SelectedOBJ.Gtexture.TextureResolution.width) (SelectedOBJ.data.ReadData("ABarrier") as ABarrier).MarginRight = r;
            if (t >= 0 & t < SelectedOBJ.Gtexture.TextureResolution.height) (SelectedOBJ.data.ReadData("ABarrier") as ABarrier).MarginTop = t;
            if (d >= 0 & d < SelectedOBJ.Gtexture.TextureResolution.height) (SelectedOBJ.data.ReadData("ABarrier") as ABarrier).MarginDown = d;
            GAME.GameManager.LvlController.PhysicWorld.RemoveBody(SelectedOBJ.body);
            SelectedOBJ.body = null;
        }

        private void staticBarrier_radioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (!staticBarrier_radioButton.Checked) return;
            GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");
            ABarrier attr = (SelectedOBJ.data.ReadData("ABarrier") as ABarrier);
            SelectedOBJ.DeAttach();
            SelectedOBJ.DeAttachAll();
            SelectedOBJ.body.IsStatic = true;
            SelectedOBJ.body.ResetMassData();
            SelectedOBJ.body.ResetDynamics();
            SelectedOBJ.body.Friction = 0.2f;
            SelectedOBJ.body.FixedRotation = false;
            attr.testMoving = false;
            attr.bodyType = FarseerPhysics.Dynamics.BodyType.Static;
            testMoving_checkBox.Enabled = false;
            addWaypoint_button.Enabled = false;
            deleteWaypoint_button.Enabled = false;
        }

        private void dynamic_radioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (!dynamic_radioButton.Checked) return;
            GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");
            if (SelectedOBJ.body == null) return;
            ABarrier attr = (SelectedOBJ.data.ReadData("ABarrier") as ABarrier);
            SelectedOBJ.body.ResetDynamics();
            SelectedOBJ.body.ResetMassData();
            SelectedOBJ.body.BodyType = FarseerPhysics.Dynamics.BodyType.Dynamic;
            attr.bodyType = FarseerPhysics.Dynamics.BodyType.Dynamic;
            SelectedOBJ.body.Friction = 0.2f;
            SelectedOBJ.body.FixedRotation = true;
            testMoving_checkBox.Enabled = true;
            addWaypoint_button.Enabled = true;
            deleteWaypoint_button.Enabled = true;
        }

        private void moving_radioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (!moving_radioButton.Checked) return;
            GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");
            SelectedOBJ.DeAttach();
            if (SelectedOBJ.body == null) return;
            ABarrier attr = (SelectedOBJ.data.ReadData("ABarrier") as ABarrier);
            SelectedOBJ.body.ResetDynamics();
            SelectedOBJ.body.ResetMassData();
            SelectedOBJ.body.BodyType = FarseerPhysics.Dynamics.BodyType.Kinematic;
            attr.bodyType = FarseerPhysics.Dynamics.BodyType.Kinematic;
            SelectedOBJ.body.Friction = 0.2f;
            SelectedOBJ.body.Rotation = 0;
            testMoving_checkBox.Enabled = true;
            addWaypoint_button.Enabled = true;
            deleteWaypoint_button.Enabled = true;
        }

        private void testMoving_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            ABarrier attr = (lastSelected.data.ReadData("ABarrier") as ABarrier);
            attr.testMoving = testMoving_checkBox.Checked;
            if (!attr.testMoving) { lastSelected.Gtexture.Position = attr.waypoints[0]; }
            attr.moveWaypoints = false;
        }

        private void addWaypoint_button_Click(object sender, EventArgs e)
        {
            ABarrier attr = (lastSelected.data.ReadData("ABarrier") as ABarrier);
            attr.waypoints.Add(attr.waypoints[attr.waypoints.Count - 1] + new Microsoft.Xna.Framework.Vector2(100, 100));
        }

        private void deleteWaypoint_button_Click(object sender, EventArgs e)
        {
            ABarrier attr = (lastSelected.data.ReadData("ABarrier") as ABarrier);
            if (attr.waypoints.Count > 2) attr.waypoints.RemoveAt(attr.waypoints.Count - 1);
        }

        private void isCircular_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            ABarrier attr = (lastSelected.data.ReadData("ABarrier") as ABarrier);
            attr.isCircular = isCircular_checkBox.Checked;
            if (attr.isCircular) attr.isInstant = false;
        }

        private void isSensor_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            lastSelected.body.IsSensor = isSensor_checkBox.Checked;
        }

        private void setSpeed_textBox_TextChanged(object sender, EventArgs e)
        {
            ABarrier attr = (lastSelected.data.ReadData("ABarrier") as ABarrier);
            float f;
            if (float.TryParse(setSpeed_textBox.Text, out f))
                attr.speed = f;
        }
        private void setMass_textBox_TextChanged(object sender, EventArgs e)
        {
            float f;
            if (float.TryParse(setMass_textBox.Text, out f))
                lastSelected.body.Mass = f;
        }
        private void setFriction_textBox_TextChanged(object sender, EventArgs e)
        {
            float f;
            if (float.TryParse(setFriction_textBox.Text, out f))
                lastSelected.body.Friction = f;
        }
        private void moveWaypoints_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            ABarrier attr = (lastSelected.data.ReadData("ABarrier") as ABarrier);
            attr.moveWaypoints = moveWaypoints_checkBox.Checked;
        }
        private void isInstantPlatform_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            ABarrier attr = (lastSelected.data.ReadData("ABarrier") as ABarrier);
            attr.isInstant = isInstantPlatform_checkBox.Checked;
            if (attr.isInstant) attr.isCircular = false;
        }

        private void isCircleBody_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            if (Functions.HaveAttribute(Functions.Attr.MovementHero, SelectedOBJ))
            {
                if (!isCircleBody_checkBox.Checked) return;
                MessageBox.Show("Ты не удостоен чести менять барьер этому объекту по тому, что ему присвоен аттрибут MovementHero");
                return; // героям барьер не менять
            }
            if (SelectedOBJ.GetType() != typeof(GameObject))
            {
                if (!isCircleBody_checkBox.Checked) return;
                MessageBox.Show("Объект не является GameObject и ему запрещено изменять свойства барьера");
                return;
            }


            ABarrier attr = (lastSelected.data.ReadData("ABarrier") as ABarrier);
            lastSelected.isCircleBody = isCircleBody_checkBox.Checked;
            var Body = attr.GetBody(lastSelected);
            Body.UserData = null;
            GameManager.LvlController.PhysicWorld.RemoveBody(Body);
            lastSelected.body = null;
        }

        #endregion

        #region Object : Rpgcharacter
        private void textBox13_TextChanged(object sender, EventArgs e)
        {
            float hp = (SelectedOBJ.data.ReadData("ARpgCharacterParams") as ARpgCharacter.CharacterParams).HP;
            Texter.TextChange(textBox13, ref hp);
            (SelectedOBJ.data.ReadData("ARpgCharacterParams") as ARpgCharacter.CharacterParams).HP = hp;
        }

        private void textBox15_TextChanged(object sender, EventArgs e)
        {
            float mp = (SelectedOBJ.data.ReadData("ARpgCharacterParams") as ARpgCharacter.CharacterParams).MP;
            Texter.TextChange(textBox15, ref mp);
            (SelectedOBJ.data.ReadData("ARpgCharacterParams") as ARpgCharacter.CharacterParams).MP = mp;
        }

        private void textBox14_TextChanged(object sender, EventArgs e)
        {
            float max_hp = (SelectedOBJ.data.ReadData("ARpgCharacterParams") as ARpgCharacter.CharacterParams).MaxHP;
            Texter.TextChange(textBox14, ref max_hp);
            (SelectedOBJ.data.ReadData("ARpgCharacterParams") as ARpgCharacter.CharacterParams).MaxHP = max_hp;
        }

        private void textBox16_TextChanged(object sender, EventArgs e)
        {
            float max_mp = (SelectedOBJ.data.ReadData("ARpgCharacterParams") as ARpgCharacter.CharacterParams).MaxMp;
            Texter.TextChange(textBox16, ref max_mp);
            (SelectedOBJ.data.ReadData("ARpgCharacterParams") as ARpgCharacter.CharacterParams).MaxMp = max_mp;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Texter.TextChange(textBox1, ref (SelectedOBJ.data.ReadData("ARpgCharacterParams") as ARpgCharacter.CharacterParams).Coins);
        }
        #endregion

        #region Object : MovementController
        private void applyJumpParam_button_Click(object sender, EventArgs e)
        {
            GameObject hero = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");
            ENGINE.Logic.ObjAttributes.MoveController controller = (hero.data.ReadData("MController") as ENGINE.Logic.ObjAttributes.MoveController);
            float f = 0;
            if (float.TryParse(runSpeed_textBox.Text, out f)) controller.RunSpeed = f;
            if (float.TryParse(jumpStr_textBox.Text, out f)) controller.jumpStrength = f;
            if (float.TryParse(maxJumpSpeed_textBox.Text, out f)) controller.maxJumpSpeed = f;
            runSpeed_textBox.Text = "";
            gravity_textBox.Text = "";
            jumpStr_textBox.Text = "";
            maxJumpSpeed_textBox.Text = "";
        }

        //movement - run sound settings
        private void button23_Click(object sender, EventArgs e)
        {
            SoundSettings.ShowRandomSoundSettings((SelectedOBJ.data.ReadData("MController") as MoveController).RunSound);
        }

        //run - run sound settings
        private void button47_Click(object sender, EventArgs e)
        {
            SoundSettings.ShowSoundSettings((SelectedOBJ.data.ReadData("MController") as MoveController).JumpSound);
        }
        #endregion

        #region Object : Bonus
        private void button17_Click(object sender, EventArgs e) // apply bonus type and value
        {
            GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");

            for (int i = 0; i < Enum.GetNames(typeof(ENGINE.Logic.Bonus.BonusType)).Length; ++i)
            {
                ENGINE.Logic.Bonus.BonusType t = (ENGINE.Logic.Bonus.BonusType)i;

                if (i == comboBox3.SelectedIndex) (SelectedOBJ.data.ReadData("BonusStandart") as ENGINE.Logic.Bonus.BonusStandart).type = t;
            }


            comboBox3.SelectedIndex = -1;
        }


        private void textBox19_TextChanged(object sender, EventArgs e)
        {
            float val = (SelectedOBJ.data.ReadData("BonusStandart") as ENGINE.Logic.Bonus.BonusStandart).value;

            Texter.TextChange(textBox19, ref val);
            (SelectedOBJ.data.ReadData("BonusStandart") as ENGINE.Logic.Bonus.BonusStandart).value = val;
        }

        private void button18_Click(object sender, EventArgs e) // отступ
        {
            GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");

            float v = -1;

            try
            {
                v = (float)Convert.ToDouble(textBox20.Text);
            }
            catch { textBox20.Text = "200"; }

            if (v != -1 & v >= 0) (SelectedOBJ.data.ReadData("ABonus") as ABonus).CalculateAndSetHeight(v, SelectedOBJ, true);
        }

        private void button19_Click(object sender, EventArgs e) //отступ
        {
            GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");

            float v = -1;

            try
            {
                v = (float)Convert.ToDouble(textBox20.Text);
            }
            catch { textBox20.Text = "200"; }

            if (v != -1 & v >= 0) (SelectedOBJ.data.ReadData("ABonus") as ABonus).CalculateAndSetHeight(v, SelectedOBJ, false);
        }

        private void setBounceParam_button_Click(object sender, EventArgs e)
        {
            float f = 0;
            float.TryParse(period_textBox.Text, out f);
            ABonus.Period = f;
            float.TryParse(amplitude_textBox.Text, out f);
            ABonus.amplitude = (int)f;
            refreshBonus = true;
        }

        private void bounceBonus_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            ABonus.isBounced = bounceBonus_checkBox.Checked;
        }
        #endregion

        #region Object : Portal
        private void twoSides_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            (((GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ")).data.ReadData("APortal") as APortal).TwoSided = twoSides_checkBox.Checked;
        }

        private void transitionTime_textBox_TextChanged(object sender, EventArgs e)
        {
            float f = 0;
            if (!float.TryParse(transitionTime_textBox.Text, out f)) { transitionTime_textBox.Text = (lastSelected.data.ReadData("APortal") as APortal).transitionTime.ToString("0.00"); return; }
            (lastSelected.data.ReadData("APortal") as APortal).transitionTime = f;
        }

        private void exitTime_textBox_TextChanged(object sender, EventArgs e)
        {
            float f = 0;
            if (!float.TryParse(exitTime_textBox.Text, out f)) { exitTime_textBox.Text = (lastSelected.data.ReadData("APortal") as APortal).exitTime.ToString("0.00"); return; }
            (lastSelected.data.ReadData("APortal") as APortal).exitTime = f;
        }

        private void isInstant_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            (((GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ")).data.ReadData("APortal") as APortal).IsInstant = isInstant_checkBox.Checked;
        }

        private void isInteractive_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            (((GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ")).data.ReadData("APortal") as APortal).IsInteractive = isInteractive_checkBox.Checked;
        }
        #endregion

        #region Object : Tooltip
        private void ttOffsetX_numericUpDown_ValueChanged(object sender, EventArgs e)
        {
            ATooltip at = (lastSelected.data.ReadData("ATooltip") as ATooltip);
            at.offset.X = (int)ttOffsetX_numericUpDown.Value;
            at.CorrectConst(lastSelected);
        }

        private void ttOffsetY_numericUpDown_ValueChanged(object sender, EventArgs e)
        {
            ATooltip at = (lastSelected.data.ReadData("ATooltip") as ATooltip);
            at.offset.Y = (int)ttOffsetY_numericUpDown.Value;
            at.CorrectConst(lastSelected);
        }

        private void ttHeight_numericUpDown_ValueChanged(object sender, EventArgs e)
        {
            ATooltip at = (lastSelected.data.ReadData("ATooltip") as ATooltip);
            at.Height = (int)ttHeight_numericUpDown.Value;
            at.CorrectConst(lastSelected);
        }

        private void ttWidth_numericUpDown_ValueChanged(object sender, EventArgs e)
        {
            ATooltip at = (lastSelected.data.ReadData("ATooltip") as ATooltip);
            at.Width = (int)ttWidth_numericUpDown.Value;
            at.CorrectConst(lastSelected);
        }

        private void isAlwaysShow_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            (((GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ")).data.ReadData("ATooltip") as ATooltip).isAlwaysShow = isAlwaysShow_checkBox.Checked;
        }

        private void ttText_textBox_TextChanged(object sender, EventArgs e)
        {
            (((GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ")).data.ReadData("ATooltip") as ATooltip).Text = ttText_textBox.Text;
        }

        private void bgLoad_button_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            //dialog.Title = "Выберете файл текстуры xnb";
            //dialog.Filter = "XNB images (*.xnb)|*.xnb";
            dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + @"Content\Textures";

            DialogResult res = dialog.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {
                string file = dialog.FileName;

                file = file.Replace(".xnb", "");
                file = file.Replace(AppDomain.CurrentDomain.BaseDirectory, "");
                file = file.Substring(8);
                (lastSelected.data.ReadData("ATooltip") as ATooltip).BGName = file;
            }
        }

        private void fontLoad_button_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            //dialog.Title = "Выберете файл текстуры xnb";
            //dialog.Filter = "XNB images (*.xnb)|*.xnb";
            dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + @"Content\Source\Fonts";

            DialogResult res = dialog.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {
                string file = dialog.FileName;

                file = file.Replace(".xnb", "");
                file = file.Replace(AppDomain.CurrentDomain.BaseDirectory, "");
                file = file.Substring(8);
                (lastSelected.data.ReadData("ATooltip") as ATooltip).FontName = file;
            }
        }
        private void appearTime_textBox_TextChanged(object sender, EventArgs e)
        {
            ATooltip at = (lastSelected.data.ReadData("ATooltip") as ATooltip);
            float f = 0;
            if (!float.TryParse(appearTime_textBox.Text, out f)) { return; }
            at.appearTime = f;
            at.CorrectConst(lastSelected);
        }
        private void omega_textBox_TextChanged(object sender, EventArgs e)
        {
            ATooltip at = (lastSelected.data.ReadData("ATooltip") as ATooltip);
            float f = 0;
            if (!float.TryParse(omega_textBox.Text, out f)) { return; }
            at.omega = f;
            at.CorrectConst(lastSelected);
        }
        #endregion

        #region Object : DeathZone
        //моментально убивает
        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            (SelectedOBJ.data.ReadData("ADeathZone") as ADeathZone).InstantDeath = checkBox6.Checked;
        }

        //decr value
        private void button32_Click(object sender, EventArgs e)
        {
            try
            {
                (SelectedOBJ.data.ReadData("ADeathZone") as ADeathZone).DecreaseValue = (float)(Convert.ToDouble(textBox3.Text));
            }
            catch { };
            textBox3.Text = "";
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked) (SelectedOBJ.data.ReadData("ADeathZone") as ADeathZone).AbsoluteValue = true;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked) (SelectedOBJ.data.ReadData("ADeathZone") as ADeathZone).AbsoluteValue = false;
        }
        #endregion

        #region Object : Dialog

        private void DialogPage_Enter(object sender, EventArgs e)
        {
            currentDialogScreen = null;
            ADialog dialog = (SelectedOBJ.data.ReadData("ADialog") as ADialog);
            answers_listBox.Items.Clear();
            dialogText_textBox.Clear();
            RefreshDialogCombo(dialog);
            if (dialog.allDialogs.Count > 0)
            {
                screenId_label.Text = dialog.allDialogs[0].id.ToString();
                dialog.allDialogs[0].AnswersText.ForEach(a => answers_listBox.Items.Add(a));
                currentDialogScreen = dialog.allDialogs[0];
                dialogScreenList_comboBox.SelectedIndex = 0;
            }
            this.Refresh();
        }


        private void dialogScreenList_comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ADialog dialog = lastSelected.data.ReadData("ADialog") as ADialog;
            if (dialogScreenList_comboBox.Text != "")
            {
                DialogScreen screen = dialog.allDialogs.Find(d => d.Name == dialogScreenList_comboBox.Text);
                screenId_label.Text = screen.id.ToString();
                answers_listBox.Items.Clear();
                screen.AnswersText.ForEach(str => answers_listBox.Items.Add(str));
                currentDialogScreen = screen;
                currentAnswerText_textBox.Text = "";
                dialogText_textBox.Text = screen.Text;
            }
        }

        private void addDialogScreen_button_Click(object sender, EventArgs e)
        {
            ADialog dialog = lastSelected.data.ReadData("ADialog") as ADialog;
            DialogScreen screen = new DialogScreen(dialog);
            dialog.allDialogs.Add(screen);
            dialogScreenList_comboBox.Items.Add(screen.Name);
            dialogScreenList_comboBox.SelectedIndex = dialogScreenList_comboBox.Items.Count - 1;
            nextDialogScreen_comboBox.Items.Add(screen.Name);
        }

        private void deleteDialogScreen_button_Click(object sender, EventArgs e)
        {
            ADialog dialog = lastSelected.data.ReadData("ADialog") as ADialog;
            if (dialog.allDialogs.Count > 1)
            {
                dialog.allDialogs.Remove(currentDialogScreen);
                DialogPage_Enter(null, EventArgs.Empty);
                dialogScreenList_comboBox.SelectedIndex = 0;
            }
        }

        private void dialogScreenList_comboBox_KeyDown(object sender, KeyEventArgs e)
        {
            ADialog dialog = lastSelected.data.ReadData("ADialog") as ADialog;
            if (e.KeyCode == Keys.Enter && dialog.allDialogs.Count > 0 && currentDialogScreen != null)
            {
                bool exist = false;
                dialog.allDialogs.ForEach(d => { if (d.Name == dialogScreenList_comboBox.Text) exist = true; });
                if (exist) MessageBox.Show("Экран с таким именем уже есть.");
                else
                {
                    currentDialogScreen.Name = dialogScreenList_comboBox.Text;
                    RefreshDialogCombo(dialog);
                }
            }
        }

        private void setActorTexture_button_Click(object sender, EventArgs e)
        {
            ADialog dialog = lastSelected.data.ReadData("ADialog") as ADialog;



            OpenFileDialog opendialog = new OpenFileDialog();
            opendialog.Title = "Выберете файл текстуры xnb";
            opendialog.Filter = "XNB images (*.xnb)|*.xnb";
            opendialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + @"Content\Textures\";

            DialogResult res = opendialog.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {
                string file = opendialog.FileName;

                file = file.Replace(".xnb", "");
                file = file.Replace(AppDomain.CurrentDomain.BaseDirectory, "");
                file = file.Substring(8);
                var texture = new ENGINE.Graphics.GameTexture(true, file);
                dialog.actorTexture.Texture2d = texture.Texture2d;
            }
        }

        private void RefreshDialogCombo(ADialog dialog)
        {
            dialogScreenList_comboBox.Items.Clear();
            nextDialogScreen_comboBox.Items.Clear();
            nextDialogScreen_comboBox.Items.Add("End dialog");
            dialog.allDialogs.ForEach(d =>
            {
                dialogScreenList_comboBox.Items.Add(d.Name);
                nextDialogScreen_comboBox.Items.Add(d.Name);
            });
        }

        private void answers_listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ADialog dialog = lastSelected.data.ReadData("ADialog") as ADialog;
            //currentAnswerText_textBox.Text = "";
            int index = answers_listBox.SelectedIndex;
            int indexS = 0;
            currentAnswer = index;
            if (index >= 0)
            {
                currentAnswerText_textBox.Text = currentDialogScreen.GetAnswerText(index);
                DialogScreen target = currentDialogScreen.GetTarget(index);
                if (target == null)
                    nextDialogScreen_comboBox.SelectedIndex = 0;
                else
                {
                    indexS = dialog.allDialogs.FindIndex(s => s == target);
                    nextDialogScreen_comboBox.SelectedIndex = indexS + 1;
                }
            }
        }

        private void currentAnswerText_textBox_TextChanged(object sender, EventArgs e)
        {
            int index = answers_listBox.SelectedIndex;
            if (index >= 0)
            {
                string s = currentDialogScreen.SetAnswer(index, currentAnswerText_textBox.Text);
                answers_listBox.Items[index] = s;
            }
        }

        private void newAnswer_button_Click(object sender, EventArgs e)
        {
            if (currentDialogScreen == null) return;
            currentDialogScreen.AddAnswer("new answer", null);
            answers_listBox.Items.Clear();
            currentDialogScreen.AnswersText.ForEach(str => answers_listBox.Items.Add(str));
            answers_listBox.SelectedIndex = answers_listBox.Items.Count - 1;

        }

        private void deleteAnswer_button_Click(object sender, EventArgs e)
        {
            if (answers_listBox.Items.Count > 1)
            {
                currentDialogScreen.DeleteAnswer(currentAnswer);
                answers_listBox.Items.Remove(answers_listBox.Items[currentAnswer]);
                answers_listBox.SelectedIndex = 0;
                currentAnswer = 0;
            }
        }

        private void nextDialogScreen_comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ADialog dialog = lastSelected.data.ReadData("ADialog") as ADialog;
            int index = nextDialogScreen_comboBox.SelectedIndex;
            int indexA = currentAnswer;
            if (index > 0)
            {
                index--;
                currentDialogScreen.SetTarget(indexA, dialog.allDialogs[index]);
            }
            else currentDialogScreen.SetTarget(indexA, null);
        }

        private void dialogText_textBox_TextChanged(object sender, EventArgs e)
        {
            if (currentDialogScreen != null)
                currentDialogScreen.Text = dialogText_textBox.Text;
        }

        #endregion

        #region Object : Light
        private void lightOffsetX_textBox_TextChanged(object sender, EventArgs e)
        {
            ENGINE.Logic.Lights.Light light = (lastSelected.data.ReadData("ALight") as ALight).light;
            float f;
            if (float.TryParse(lightOffsetX_textBox.Text, out f))
                light.offset.X = f;
        }

        private void lightOffsetY_textBox_TextChanged(object sender, EventArgs e)
        {
            ENGINE.Logic.Lights.Light light = (lastSelected.data.ReadData("ALight") as ALight).light;
            float f;
            if (float.TryParse(lightOffsetY_textBox.Text, out f))
                light.offset.Y = f;
        }

        private void radius_textBox_TextChanged(object sender, EventArgs e)
        {
            ENGINE.Logic.Lights.Light light = (lastSelected.data.ReadData("ALight") as ALight).light;
            float f;
            if (float.TryParse(radius_textBox.Text, out f))
                light.LightRadius = f;
        }


        private void outerAngle_numericUpDown_ValueChanged(object sender, EventArgs e)
        {
            ENGINE.Logic.Lights.Light light = (lastSelected.data.ReadData("ALight") as ALight).light;
            light.outerAngle = (int)outerAngle_numericUpDown.Value * (float)Math.PI / 180;
        }

        private void innerAngle_numericUpDown_ValueChanged(object sender, EventArgs e)
        {
            ENGINE.Logic.Lights.Light light = (lastSelected.data.ReadData("ALight") as ALight).light;
            light.innerAngle = (int)innerAngle_numericUpDown.Value * (float)Math.PI / 180;
        }

        private void directionAngle_numericUpDown_ValueChanged(object sender, EventArgs e)
        {
            ENGINE.Logic.Lights.Light light = (lastSelected.data.ReadData("ALight") as ALight).light;
            light.direction.X = (float)Math.Cos((int)directionAngle_numericUpDown.Value * Math.PI / 180);
            light.direction.Y = (float)Math.Sin((int)directionAngle_numericUpDown.Value * Math.PI / 180);
        }
        private void lightColor_button_Click(object sender, EventArgs e)
        {
            ENGINE.Logic.Lights.Light light = (lastSelected.data.ReadData("ALight") as ALight).light;
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                lightColor_button.BackColor = colorDialog1.Color;

                light.color.R = colorDialog1.Color.R;
                light.color.G = colorDialog1.Color.G;
                light.color.B = colorDialog1.Color.B;
                light.color.A = 255;
            }
        }

        private void gammaLight_numericUpDown_ValueChanged(object sender, EventArgs e)
        {
            ENGINE.Logic.Lights.Light light = (lastSelected.data.ReadData("ALight") as ALight).light;
            light.gamma = (float)gammaLight_numericUpDown.Value / 100.0f;
        }
        #endregion

        #region Object : Trigger

        void RefreshTriggerPage()
        {
            ATrigger trigger = (SelectedOBJ.data.ReadData("ATrigger") as ATrigger);
            switch (trigger.WorkCondition)
            {
                case ATrigger.trigger_work_condition_enum.Always:
                    radioButton_Always.Checked = true;
                    break;
                case ATrigger.trigger_work_condition_enum.InCollision:
                    radioButton_OnBeing.Checked = true;
                    break;
                case ATrigger.trigger_work_condition_enum.OnCollisionFinish:
                    radioButton_OnLeaving.Checked = true;
                    break;
                case ATrigger.trigger_work_condition_enum.OnCollisionStart:
                    radioButton_OnCollisionStart.Checked = true;
                    break;
            }


            switch (trigger.LoopType)
            {
                case ATrigger.trigger_loop_type_enum.Once:
                    radioButton_loop_Once.Checked = true;
                    label161.Text = "";
                    textBox30.Enabled = false;
                    break;
                case ATrigger.trigger_loop_type_enum.CoolDown:
                    radioButton_loop_useCD.Checked = true;
                    textBox30.Enabled = true;
                    label161.Text = trigger.CoolDownSec.ToString();
                    break;
                case ATrigger.trigger_loop_type_enum.Unlimited:
                    radioButton_loop_Unlimited.Checked = true;
                    label161.Text = "";
                    textBox30.Enabled = false;
                    break;
            }


            label163.Text = Math.Round(trigger.RealCDleftBeforeNextUsing, 2).ToString();
            label160.Text = (SelectedOBJ.data.ReadData("ATrigger") as ATrigger).Radius.ToString();


            if (comboBox4.Items.Count == 0)
            {
                Type[] typelist = ENGINE.ScriptController.GetScripts();

                for (int i = 0; i < typelist.Length; i++)
                {
                    if (typelist[i] == typeof(ENGINE.Scripts.Scripts)) continue;
                    string s = typelist[i].Name;
                    comboBox4.Items.Add(s);
                }
            }


            if ((SelectedOBJ.data.ReadData("ATrigger") as ATrigger).work != null)
            {
                Type t = (SelectedOBJ.data.ReadData("ATrigger") as ATrigger).work.GetType();

                label167.Text = t.Name;


                FieldInfo[] fi = t.GetFields();

                string s = "";
                foreach (FieldInfo f in fi) s += f.FieldType.Name + " " + f.Name + ", ";
                label165.Text = s; //public param


                //fields
                bool needrefresh = false;
                for (int i = 0; i < fi.Length; ++i)
                {
                    if (comboBox_fields_settings.Items.Count != fi.Length)
                    {
                        needrefresh = true;
                        break;
                    }

                    string param = fi[i].FieldType.Name + " " + fi[i].Name;

                    if (comboBox_fields_settings.Items[i].ToString() != param)
                    {
                        needrefresh = true;
                        break;
                    }
                }

                if (needrefresh)
                {
                    comboBox_fields_settings.Items.Clear();
                    for (int i = 0; i < fi.Length; ++i)
                    {
                        string param = fi[i].FieldType.Name + " " + fi[i].Name;
                        comboBox_fields_settings.Items.Add(param);
                    }
                }

            }
            else
            {
                label165.Text = "";
                label167.Text = "";
                comboBox_fields_settings.Items.Clear();
            }
        }



        private void textBox29_TextChanged_1(object sender, EventArgs e)
        {
            int i = 0;
            if (int.TryParse(textBox29.Text, out i))
            {
                (SelectedOBJ.data.ReadData("ATrigger") as ATrigger).Radius = i;

                (SelectedOBJ.data.ReadData("ATrigger") as ATrigger).RefreshRadius(SelectedOBJ);
            }

            if (!textBox29.Focused) textBox29.Text = "";
        }

        //настроить
        private void public_param_b_1_Click(object sender, EventArgs e)
        {
            if (comboBox_fields_settings.SelectedIndex == -1) return;

            Type t = (SelectedOBJ.data.ReadData("ATrigger") as ATrigger).work.GetType();
            FieldInfo[] fi = t.GetFields();

            for (int i = 0; i < fi.Length; ++i)
            {
                string param = fi[i].FieldType.Name + " " + fi[i].Name;
                if (comboBox_fields_settings.SelectedItem.ToString() == param)
                {
                    FieldInfo nfo = fi[i];
                    object value = nfo.GetValue((SelectedOBJ.data.ReadData("ATrigger") as ATrigger).work); //значение открытой переменной


                    Editor.Managers.ScriptConfig.Settings(ref value, nfo.FieldType.ToString());

                    nfo.SetValue((SelectedOBJ.data.ReadData("ATrigger") as ATrigger).work, value);
                }
            }

        }



        //apply script
        private void button41_Click_1(object sender, EventArgs e)
        {
            string selected = comboBox4.SelectedItem.ToString();


            Type[] typelist = ENGINE.ScriptController.GetScripts();

            for (int i = 0; i < typelist.Length; i++)
            {
                if (selected == typelist[i].Name) //typelist[i].Name это выбранный нами из либы скрипт
                {
                    Type selectedScript = typelist[i];
                    object o = Activator.CreateInstance(selectedScript);
                    (SelectedOBJ.data.ReadData("ATrigger") as ATrigger).work = o;
                }
            }

            comboBox4.SelectedIndex = -1;
        }

        private void radioButton_OnCollisionStart_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_OnCollisionStart.Checked) (SelectedOBJ.data.ReadData("ATrigger") as ATrigger).WorkCondition = ATrigger.trigger_work_condition_enum.OnCollisionStart;
        }

        private void radioButton_OnLeaving_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_OnLeaving.Checked) (SelectedOBJ.data.ReadData("ATrigger") as ATrigger).WorkCondition = ATrigger.trigger_work_condition_enum.OnCollisionFinish;
        }

        private void radioButton_OnBeing_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_OnBeing.Checked) (SelectedOBJ.data.ReadData("ATrigger") as ATrigger).WorkCondition = ATrigger.trigger_work_condition_enum.InCollision;
        }

        private void radioButton_Always_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_Always.Checked) (SelectedOBJ.data.ReadData("ATrigger") as ATrigger).WorkCondition = ATrigger.trigger_work_condition_enum.Always;
        }

        private void radioButton_loop_Unlimited_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_loop_Unlimited.Checked) (SelectedOBJ.data.ReadData("ATrigger") as ATrigger).LoopType = ATrigger.trigger_loop_type_enum.Unlimited;
        }

        private void radioButton_loop_Once_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_loop_Once.Checked) (SelectedOBJ.data.ReadData("ATrigger") as ATrigger).LoopType = ATrigger.trigger_loop_type_enum.Once;
        }

        private void radioButton_loop_useCD_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_loop_useCD.Checked) (SelectedOBJ.data.ReadData("ATrigger") as ATrigger).LoopType = ATrigger.trigger_loop_type_enum.CoolDown;
        }

        private void textBox30_TextChanged(object sender, EventArgs e)
        {
            int i = 0;
            if (!int.TryParse(textBox30.Text, out i))
            {  //error
            }
            else
            {
                //cool
                (SelectedOBJ.data.ReadData("ATrigger") as ATrigger).CoolDownSec = i;
            }

            if (!textBox30.Focused) textBox30.Text = "";
        }

        private void button39_Click(object sender, EventArgs e)
        {
            (SelectedOBJ.data.ReadData("ATrigger") as ATrigger).RealCDleftBeforeNextUsing = 0;
        }

        private void textBox29_TextChanged(object sender, EventArgs e)
        {
            int i = 0;
            if (!int.TryParse(textBox29.Text, out i))
            {  //error
            }
            else
            {
                //cool
                (SelectedOBJ.data.ReadData("ATrigger") as ATrigger).Radius = i;
                (SelectedOBJ.data.ReadData("ATrigger") as ATrigger).ChangeRadius(SelectedOBJ);
            }

            if (!textBox29.Focused) textBox29.Text = "";
        }
        #endregion

        #region World
        //background music
        private void button38_Click(object sender, EventArgs e)
        {
            SoundSettings.ShowRandomSoundSettings(GameManager.LvlController.BGSound);
        }

        //editor music
        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {
            GameManager.LvlController.EnableSound_E = checkBox7.Checked;
            if (!GameManager.LvlController.EnableSound_E & Config.EditorMode)
            {/* GameManager.LvlController.BGSound.Stop()*/
                ;
            }
        }

        private void checkBox8_CheckedChanged(object sender, EventArgs e)
        {
            GameManager.LvlController.EnableSound_G = checkBox8.Checked;
            if (!GameManager.LvlController.EnableSound_G & !Config.EditorMode) GameManager.LvlController.BGSound.Stop();
        }



        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.Checked) Config.show_collision_rectangles = true; else Config.show_collision_rectangles = false;
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            GameManager.LvlController.Background.Show_in_game = checkBox4.Checked;
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            GameManager.LvlController.Background.Show_in_editor = checkBox5.Checked;
        }


        private void button21_Click(object sender, EventArgs e) //load bg texture
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Выберете файл текстуры xnb";
            dialog.Filter = "XNB images (*.xnb)|*.xnb";
            dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + @"Content\Textures\";

            DialogResult res = dialog.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {
                string file = dialog.FileName;

                file = file.Replace(".xnb", "");
                file = file.Replace(AppDomain.CurrentDomain.BaseDirectory, "");
                file = file.Substring(8);

                GameManager.LvlController.Background.AddTexture(file);
                backgroundList_comboBox.Items.Add(GameManager.LvlController.Background.backgrounds[GameManager.LvlController.Background.backgrounds.Count - 1].ToString());
                backgroundList_comboBox.SelectedIndex = backgroundList_comboBox.Items.Count - 1;
            }
        }

        private void button20_Click(object sender, EventArgs e)
        {
            string s = backgroundList_comboBox.Text;
            int num = backgroundList_comboBox.Items.IndexOf(s);
            num = (int)Microsoft.Xna.Framework.MathHelper.Clamp(num - 1, 0, backgroundList_comboBox.Items.Count);
            BackgroundDef currentBackground = GameManager.LvlController.Background.GetBackground(s);
            if (currentBackground == null) return;
            GameManager.LvlController.Background.DeleteBackground(currentBackground);
            backgroundList_comboBox.Items.Remove(currentBackground.ToString());
            if (backgroundList_comboBox.Items.Count == 0)
                backgroundList_comboBox.Text = "";
            else
                backgroundList_comboBox.SelectedItem = backgroundList_comboBox.Items[num];
        }

        //openLevel
        private void button10_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Выберете файл уровня .lvl";
            dialog.Filter = "levels (*.lvl)|*.lvl";
            dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + "Levels";

            DialogResult res = dialog.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {
                string file = dialog.FileName;
                file = file.Replace(AppDomain.CurrentDomain.BaseDirectory, "");

                GameManager.LvlController = LevelController.LoadLevelController(file);
                LevelController.ThisLevelFolder = file;

                backgroundList_comboBox.Items.Clear();
                foreach (var item in GameManager.LvlController.Background.backgrounds)
                {
                    backgroundList_comboBox.Items.Add(item.ToString());
                }
                if (backgroundList_comboBox.Items.Count != 0)
                    backgroundList_comboBox.SelectedIndex = 0;
            }
        }

        //save as
        private void button29_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Title = "Сохранить уровень как...";
            dialog.Filter = "level (*.lvl)|*.lvl";
            dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + "Levels";

            DialogResult res = dialog.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {
                string file = dialog.FileName;
                LevelController.ThisLevelFolder = file;
                GameManager.LvlController.Save(file);
            }
        }

        //savelevel
        private void button28_Click(object sender, EventArgs e)
        {
            if (LevelController.ThisLevelFolder != "")
            {
                GameManager.LvlController.Save(LevelController.ThisLevelFolder);
                MessageBox.Show("Saved");
            }
            else MessageBox.Show("Сначала откройте карту или сохраните текущую что бы определить путь к файлу");
        }

        private void DrawAllBody_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            Editor.Managers.EDrawManager.isDrawAllBody = DrawAllBody_checkBox.Checked;
        }

        private void textBox22_TextChanged(object sender, EventArgs e)
        {
            float f = 0;
            if (!float.TryParse(brightness_textbox.Text, out f)) return;
            string s = backgroundList_comboBox.Text;
            BackgroundDef currentBackground = GameManager.LvlController.Background.GetBackground(s);
            f = Microsoft.Xna.Framework.MathHelper.Clamp(f, 50, 255);
            currentBackground.brightness = (int)f;
        }

        private void positionX_textBox_TextChanged(object sender, EventArgs e)
        {
            float f = 0;
            if (!float.TryParse(positionX_textBox.Text, out f)) return;
            string s = backgroundList_comboBox.Text;
            BackgroundDef currentBackground = GameManager.LvlController.Background.GetBackground(s);
            if (currentBackground == null) return;
            currentBackground.gameTexture.Position.X = f;
        }

        private void positionY_textBox_TextChanged(object sender, EventArgs e)
        {
            float f = 0;
            if (!float.TryParse(positionY_textBox.Text, out f)) return;
            string s = backgroundList_comboBox.Text;
            BackgroundDef currentBackground = null;
            foreach (var item in GameManager.LvlController.Background.backgrounds)
            {
                if (s == item.ToString())
                {
                    currentBackground = item;
                    break;
                }
            }
            if (currentBackground == null) return;
            currentBackground.gameTexture.Position.Y = f;
        }

        private void parallax_textBox_TextChanged(object sender, EventArgs e)
        {
            float f = 0;
            string text = parallax_textBox.Text.Replace(".", ",");
            if (!float.TryParse(text, out f)) return;
            string s = backgroundList_comboBox.Text;
            BackgroundDef currentBackground = GameManager.LvlController.Background.GetBackground(s);
            if (currentBackground == null) return;
            currentBackground.Parallax = new Microsoft.Xna.Framework.Vector2(1 / f, 1 / f);
        }

        private void layer_textBox_TextChanged(object sender, EventArgs e)
        {
            float f = 0;
            string text = layer_textBox.Text.Replace(".", ",");
            if (!float.TryParse(text, out f)) return;
            string s = backgroundList_comboBox.Text;
            BackgroundDef currentBackground = GameManager.LvlController.Background.GetBackground(s);
            if (currentBackground == null) return;
            f = Microsoft.Xna.Framework.MathHelper.Clamp(f, 0, 1);
            currentBackground.layer = f;
        }

        private void tileHorizontal_checkbox_CheckedChanged(object sender, EventArgs e)
        {
            if (setParameters) return;
            string s = backgroundList_comboBox.Text;
            BackgroundDef currentBackground = GameManager.LvlController.Background.GetBackground(s);
            if (currentBackground == null) return;
            currentBackground.SetHorizontal(tileHorizontal_checkbox.Checked);
        }

        private void tileVertical_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            if (setParameters) return;
            string s = backgroundList_comboBox.Text;
            BackgroundDef currentBackground = GameManager.LvlController.Background.GetBackground(s);
            if (currentBackground == null) return;
            currentBackground.SetVertical(tileVertical_checkBox.Checked);

        }

        private void backgroundList_comboBox_TextChanged(object sender, EventArgs e)
        {
            string s = backgroundList_comboBox.Text;
            BackgroundDef currentBackground = GameManager.LvlController.Background.GetBackground(s);
            if (currentBackground == null) return;

            positionX_textBox.Text = currentBackground.gameTexture.Position.X.ToString();
            positionY_textBox.Text = currentBackground.gameTexture.Position.Y.ToString();
            parallax_textBox.Text = (Math.Round(1.0f / currentBackground.Parallax.X)).ToString();
            layer_textBox.Text = currentBackground.layer.ToString();
            brightness_textbox.Text = currentBackground.brightness.ToString();
            setParameters = true;
            tileHorizontal_checkbox.Checked = currentBackground.isTiledHorizontally;
            tileVertical_checkBox.Checked = currentBackground.isTiledVertically;
            setParameters = false;
            isSky_checkBox.Checked = currentBackground.isSky;
            currentBGSize_label.Text = currentBackground.gameTexture.TextureResolution.width + "x" + currentBackground.gameTexture.TextureResolution.height;
        }

        private void isSky_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            string s = backgroundList_comboBox.Text;
            BackgroundDef currentBackground = GameManager.LvlController.Background.GetBackground(s);
            currentBackground.isSky = isSky_checkBox.Checked;
        }

        private void backgroundColor_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                GameManager.LvlController.BackgroundFillColor.R = colorDialog1.Color.R;
                GameManager.LvlController.BackgroundFillColor.G = colorDialog1.Color.G;
                GameManager.LvlController.BackgroundFillColor.B = colorDialog1.Color.B;

                GameManager.LvlController.BackgroundFillColor.A = 255;
            }
        }

        private void textBox33_TextChanged(object sender, EventArgs e)
        {
            float f;
            if (float.TryParse(textBox33.Text, out f))
                GameManager.LvlController.AmbientLight = f;
        }

        private void gravity_textBox_TextChanged(object sender, EventArgs e)
        {
            float f;
            if (float.TryParse(gravity_textBox.Text, out f))
                GameManager.LvlController.Gravity = new Vector2(0, f);
        }

        private void button9_Click_1(object sender, EventArgs e)
        {
            if (GAME.GameManager.Map.HERO != null)
            {
                GameObject addhero = (GameObject)Functions.DeepClone(GAME.GameManager.Map.HERO);


                addhero.Gtexture.Position = new Vector2(0, 0);

                foreach (GameObject obj in GameManager.LvlController.ObjectArray)
                {
                    if (obj.GetType() == typeof(ENGINE.Logic.Zones.StartFinishZone))
                    {
                        if ((obj as ENGINE.Logic.Zones.StartFinishZone).Startzone)
                        {
                            addhero.Gtexture.Position = obj.Gtexture.Position;
                            break;
                        }
                    }
                }

                GameManager.LvlController.camera.CameraPosition = addhero.Gtexture.Position;
                GameManager.LvlController.ObjectArray.Add(addhero);
                GameManager.LvlController.RefindHero();
            }
        }
        #endregion

        #region Special Objects
        #region particles
        private void button24_Click(object sender, EventArgs e)
        {
            if (!SpecialObjectsTabControl.TabPages.Contains(CreateEmitterPage))
            {
                SpecialObjectsTabControl.TabPages.Clear();
                SpecialObjectsTabControl.TabPages.Add(CreateEmitterPage);
            }
        }

        private void CreateDotEmitterButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Выберете файл текстуры xnb";
            dialog.Filter = "XNB images (*.xnb)|*.xnb";
            dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + @"Content\Textures\";

            DialogResult res = dialog.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {
                string file = dialog.FileName; // "F:\\Git\\forcefield\\Something\\Editor\\Editor\\bin\\x86\\Debug\\Content\\Textures\\Particles\\p4.xnb";

                file = file.Replace(".xnb", "");
                file = file.Replace(AppDomain.CurrentDomain.BaseDirectory, "");
                file = file.Substring(8);


                GameObject ob = new ENGINE.Logic.Particles.DotEmitter(GameManager.LvlController.ParticleEngine, file);
                ob.Gtexture.Position = GameManager.LvlController.camera.CameraPosition;
                ob.Gtexture.Layer = 0.80f;

                GameManager.LvlController.ObjectArray.Add(ob);
                GameManager.LvlController.SortByLayer();

                (ob.data.ReadData("SelectParams") as SelectParams).Select(ob);
            }
        }

        private void CreateLineEmitterButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Выберете файл текстуры xnb";
            dialog.Filter = "XNB images (*.xnb)|*.xnb";
            dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + @"Content\Textures\";

            DialogResult res = dialog.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {
                string file = dialog.FileName; // "F:\\Git\\forcefield\\Something\\Editor\\Editor\\bin\\x86\\Debug\\Content\\Textures\\Particles\\p4.xnb";

                file = file.Replace(".xnb", "");
                file = file.Replace(AppDomain.CurrentDomain.BaseDirectory, "");
                file = file.Substring(8);

                GameObject ob = new ENGINE.Logic.Particles.LineEmitter(file);
                ob.Gtexture.Position = GameManager.LvlController.camera.CameraPosition;
                ob.Gtexture.Layer = 0.80f;
                GameManager.LvlController.ObjectArray.Add(ob);
                GameManager.LvlController.SortByLayer();
                (ob.data.ReadData("SelectParams") as SelectParams).Select(ob);
            }
        }

        private void CreateCircleEmitterButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Выберете файл текстуры xnb";
            dialog.Filter = "XNB images (*.xnb)|*.xnb";
            dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + @"Content\Textures\";

            DialogResult res = dialog.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {
                string file = dialog.FileName;

                file = file.Replace(".xnb", "");
                file = file.Replace(AppDomain.CurrentDomain.BaseDirectory, "");
                file = file.Substring(8);

                GameObject ob = new ENGINE.Logic.Particles.CircleEmitter(file);
                ob.Gtexture.Position = GameManager.LvlController.camera.CameraPosition;
                ob.Gtexture.Layer = 0.80f;
                GameManager.LvlController.ObjectArray.Add(ob);
                GameManager.LvlController.SortByLayer();
                (ob.data.ReadData("SelectParams") as SelectParams).Select(ob);
            }
        }

        private void alphablend_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).alphablend = alphablend_checkBox.Checked;
        }

        private void Pps_numericUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (Pps_numericUpDown.Value == 0) Pps_numericUpDown.Value = 1;
            ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).Pps = (int)Pps_numericUpDown.Value;
        }

        private void minSize_textBox_TextChanged(object sender, EventArgs e)
        {
            float f = 0;
            if (!float.TryParse(minSize_textBox.Text, out f)) minSize_textBox.Text = "0";
            ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).minSize = f;
        }

        private void maxSize_textBox_TextChanged(object sender, EventArgs e)
        {
            float f = 0;
            if (!float.TryParse(maxSize_textBox.Text, out f)) maxSize_textBox.Text = "0";
            ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).maxSize = f;
        }

        private void minSpeed_textBox_TextChanged(object sender, EventArgs e)
        {
            float f = 0;
            if (!float.TryParse(minSpeed_textBox.Text, out f)) minSpeed_textBox.Text = "0";
            ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).minSpeed = f;
        }

        private void maxSpeed_textBox_TextChanged(object sender, EventArgs e)
        {
            float f = 0;
            if (!float.TryParse(maxSpeed_textBox.Text, out f)) maxSpeed_textBox.Text = "0";
            ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).maxSpeed = f;
        }

        private void mainAngle_numericUpDown_ValueChanged(object sender, EventArgs e)
        {
            ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).angleMain = (float)mainAngle_numericUpDown.Value * (float)Math.PI / 180;
        }

        private void secondaryAngle_numericUpDown_ValueChanged(object sender, EventArgs e)
        {
            ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).angleSecondary = (float)secondaryAngle_numericUpDown.Value * (float)Math.PI / 360;
        }

        private void height_textBox_TextChanged(object sender, EventArgs e)
        {
            float f = 0;
            if (!float.TryParse(height_textBox.Text, out f)) height_textBox.Text = "0";
            ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).height = f;
        }

        private void width_textBox_TextChanged(object sender, EventArgs e)
        {
            float f = 0;
            if (!float.TryParse(width_textBox.Text, out f)) width_textBox.Text = "0";
            ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).width = f;
        }

        private void alphaVel_textBox_TextChanged(object sender, EventArgs e)
        {
            float f = 0;
            if (!float.TryParse(alphaVel_textBox.Text, out f)) alphaVel_textBox.Text = "0";
            ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).alphaVel = 255 / f;
        }

        private void bothWays_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).emitBothWay = bothWays_checkBox.Checked;
        }

        private void ttl_textBox_TextChanged(object sender, EventArgs e)
        {
            float f = 0;
            if (!float.TryParse(ttl_textBox.Text, out f)) ttl_textBox.Text = "0";
            ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).ttl = f;
        }

        private void angleVel_textBox_TextChanged(object sender, EventArgs e)
        {
            float f = 0;
            if (!float.TryParse(angleVel_textBox.Text, out f)) angleVel_textBox.Text = "0";
            ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).angleVelocity = f * (float)Math.PI / 180; ;
        }

        private void emittrtomega_textBox_TextChanged(object sender, EventArgs e)
        {
            float f = 0;
            if (!float.TryParse(emittrtomega_textBox.Text, out f)) emittrtomega_textBox.Text = "0";
            ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).omega = f * (float)Math.PI / 180; ;
        }

        private void startAngle_numericUpDown_ValueChanged(object sender, EventArgs e)
        {
            ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).startAngle = (float)startAngle_numericUpDown.Value * (float)Math.PI / 180;
        }

        private void sizeVel_textBox_TextChanged(object sender, EventArgs e)
        {
            float f = 0;
            if (!float.TryParse(sizeVel_textBox.Text, out f)) sizeVel_textBox.Text = "0";
            ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).sizeVel = f;
        }

        private void tMaxAlpha_textBox_TextChanged(object sender, EventArgs e)
        {
            float f = 0;
            if (!float.TryParse(tMaxAlpha_textBox.Text, out f)) tMaxAlpha_textBox.Text = "0";
            ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).tMaxAlpha = f;
        }

        private void mass_textBox_TextChanged(object sender, EventArgs e)
        {
            float f = 0;
            if (!float.TryParse(mass_textBox.Text, out f)) mass_textBox.Text = "0";
            ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).Mass = f;
        }

        private void Color_button_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                Color_button.BackColor = colorDialog1.Color;
                ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).color.R = colorDialog1.Color.R;
                ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).color.G = colorDialog1.Color.G;
                ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).color.B = colorDialog1.Color.B;
                ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).color.A = 255;
            }
        }

        private void isBackLayer_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).isBackLayer = isBackLayer_checkBox.Checked;
        }

        private void normalDistr_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).isNormalDistribution = normalDistr_checkBox.Checked;
        }

        private void isInteractveEmitter_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).isInteractive = isInteractveEmitter_checkBox.Checked;
        }

        private void isSolid_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).isSolid = isSolid_checkBox.Checked;
        }

        private void energyLoss_numericUpDown_ValueChanged(object sender, EventArgs e)
        {
            ((ENGINE.Logic.Particles.BaseEmitter)lastSelected).EnergyLoss = (int)energyLoss_numericUpDown.Value;
        }
        #endregion

        #region startfinish
        // spec objects : level start
        private void button30_Click(object sender, EventArgs e)
        {
            bool exist = false;
            foreach (GameObject obj in GameManager.LvlController.ObjectArray)
            {
                if (obj.GetType() == typeof(ENGINE.Logic.Zones.StartFinishZone))
                {
                    if ((obj as ENGINE.Logic.Zones.StartFinishZone).Startzone == true)
                    {
                        obj.Gtexture.Position = GameManager.LvlController.camera.CameraPosition;
                        exist = true;
                        break;
                    }
                }
            }

            if (!exist)
            {
                ENGINE.Logic.Zones.StartFinishZone ob = new ENGINE.Logic.Zones.StartFinishZone(true);
                ob.Gtexture.Position = GameManager.LvlController.camera.CameraPosition;
                ob.Gtexture.Layer = 0.40f;

                GameManager.LvlController.ObjectArray.Add(ob);
                GameManager.LvlController.SortByLayer();
            }
        }

        // spec objects : level finish
        private void button31_Click(object sender, EventArgs e)
        {
            bool exist = false;
            foreach (GameObject obj in GameManager.LvlController.ObjectArray)
            {
                if (obj.GetType() == typeof(ENGINE.Logic.Zones.StartFinishZone))
                {
                    if ((obj as ENGINE.Logic.Zones.StartFinishZone).Startzone == false)
                    {
                        obj.Gtexture.Position = GameManager.LvlController.camera.CameraPosition;
                        exist = true;
                        break;
                    }
                }
            }

            if (!exist)
            {
                ENGINE.Logic.Zones.StartFinishZone ob = new ENGINE.Logic.Zones.StartFinishZone(false);
                ob.Gtexture.Position = GameManager.LvlController.camera.CameraPosition;
                ob.Gtexture.Layer = 0.40f;

                GameManager.LvlController.ObjectArray.Add(ob);
                GameManager.LvlController.SortByLayer();
            }
        }
        #endregion

        //create enemy
        private void button34_Click(object sender, EventArgs e)
        {
            SpecialObjectsTabControl.TabPages.Clear();

            SpecialObjectsTabControl.TabPages.Add(CreateEnemyPage);
        }

        //create k
        private void button35_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Выберете файл текстуры xnb";
            dialog.Filter = "XNB images (*.xnb)|*.xnb";
            dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + @"Content\Textures\";

            DialogResult res = dialog.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {
                string file = dialog.FileName;

                file = file.Replace(".xnb", "");
                file = file.Replace(AppDomain.CurrentDomain.BaseDirectory, "");
                file = file.Substring(8);


                ENGINE.Logic.Enemy.Kamikaze enem = new ENGINE.Logic.Enemy.Kamikaze(file);
                enem.Gtexture.Position = GameManager.LvlController.camera.CameraPosition;
                enem.Gtexture.Layer = 0.80f;

                GameManager.LvlController.ObjectArray.Add(enem);
                GameManager.LvlController.SortByLayer();

                (enem.data.ReadData("SelectParams") as SelectParams).Select(enem);
            }
        }

        //set common settings
        private void button36_Click(object sender, EventArgs e)
        {
            ENGINE.Logic.Enemy.Enemy E = (SelectedOBJ as ENGINE.Logic.Enemy.Enemy);

            try { E.attacDelay = (float)Convert.ToDouble(textBox4.Text); }
            catch { };
            try { E.AttacRange = (float)Convert.ToDouble(textBox5.Text); }
            catch { };
            try { E.Damage = (float)Convert.ToDouble(textBox6.Text); }
            catch { };
            try { E.MaxHP = (float)Convert.ToDouble(textBox8.Text); }
            catch { };
            try { E.MaxMP = (float)Convert.ToDouble(textBox23.Text); }
            catch { };
            try { E.HP = (float)Convert.ToDouble(textBox24.Text); }
            catch { };
            try { E.MP = (float)Convert.ToDouble(textBox25.Text); }
            catch { };
            try { E.ActivationDist = (float)Convert.ToDouble(textBox7.Text); }
            catch { };
            try { E.DeactivationDist = (float)Convert.ToDouble(textBox2.Text); }
            catch { };

            //refresh
            label118.Text = E.attacDelay.ToString();
            label119.Text = E.AttacRange.ToString();
            label120.Text = E.Damage.ToString();
            label121.Text = E.MaxHP.ToString();
            label122.Text = E.MaxMP.ToString();
            label123.Text = E.HP.ToString();
            label124.Text = E.MP.ToString();
            label188.Text = E.DeactivationDist.ToString();

            label125.Text = E.ActivationDist.ToString();

            //clear
            textBox4.Text = textBox5.Text = textBox6.Text = textBox8.Text = textBox23.Text = textBox24.Text = textBox25.Text = textBox7.Text = textBox2.Text = "";
        }

        //set movable settings
        private void button37_Click(object sender, EventArgs e)
        {
            ENGINE.Logic.Enemy.Movable_E E = (SelectedOBJ as ENGINE.Logic.Enemy.Movable_E);

            try { E.RunSpeed = (float)Convert.ToDouble(textBox26.Text); }
            catch { };
            try { E.JumpStrength = (float)Convert.ToDouble(textBox27.Text); }
            catch { };
            try { E.maxJumpSpeed = (float)Convert.ToDouble(textBox28.Text); }
            catch { };


            //refresh
            label129.Text = E.RunSpeed.ToString();
            label130.Text = E.JumpStrength.ToString();
            label131.Text = E.maxJumpSpeed.ToString();


            //clear
            textBox26.Text = textBox26.Text = textBox28.Text = "";
        }
        #endregion

        #region events page

        private void button22_Click(object sender, EventArgs e)
        {
            EditorEventHandler.UndoEvent();
        }

        #endregion

        private void button14_Click(object sender, EventArgs e)
        {
            int i = 0;
            if (int.TryParse(textBox43.Text, out i)) Config.Snap_to_grid = i;
            textBox43.Text = "";
        }
    }



    public static class Texter
    {
        public static void TextChange(TextBox textbox, ref float value)
        {
            float f = value;
            if (float.TryParse(textbox.Text, out f)) value = f;
        }

        public static void TextChange(TextBox textbox, ref int value)
        {
            int f = value;
            if (int.TryParse(textbox.Text, out f)) value = f;
        }
    }
}

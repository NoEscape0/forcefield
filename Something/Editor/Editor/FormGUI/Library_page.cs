﻿using STORAGE;
using Microsoft.Xna.Framework;
using RamGecXNAControls;
using GAME;
using System;
using ENGINE.Logic;
using ENGINE.Logic.ObjAttributes;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;

namespace Editor.FormGUI
{
    public static class Library_page
    {
        public static RamGecXNAControls.GUIManager guiManager;
        public static RamGecXNAControls.Window _MainWindow;
        public static RamGecXNAControls.Window _ContextWindow;
        public static RamGecXNAControls.Window _RenameWindow;
        public static RamGecXNAControls.Window _YouSureWindow;

        public static void Load(Microsoft.Xna.Framework.Game Game)
        {
            guiManager = new RamGecXNAControls.GUIManager(Game);

            _MainWindow = new Window(new Rectangle
                (Editor.Managers.EDrawManager.FORM_gameeditor.Left, Editor.Managers.EDrawManager.FORM_gameeditor.Top,
                Editor.Managers.EDrawManager.FORM_gameeditor.Width , Editor.Managers.EDrawManager.FORM_gameeditor.Height), "Library", "Library");
            _MainWindow.Movable = false;
            _MainWindow.TopMost = false;

            Vector2 formsize = new Vector2(Editor.Managers.EDrawManager.FORM_gameeditor.Width , Editor.Managers.EDrawManager.FORM_gameeditor.Height);

            TextBox obj_name_textbox = new TextBox(new Rectangle(20, (int)formsize.Y - 36, 100, 24), "", "obj_name");
            _MainWindow.Controls.Add(obj_name_textbox);

            _MainWindow.Controls.Add(new Label(new Rectangle(35, 58,0,0), "search: "));

            TextBox searchw = new TextBox(new Rectangle(90, 55, 200, 24), "", "searchbox");
            searchw.OnKeyDown += (sender, key) =>
                {
                    if (search_mask != searchw.Text)
                    {
                        search_mask = searchw.Text;
                        rebuild_images();
                    }
                };
             _MainWindow.Controls.Add(searchw);

            #region Add selected in library
            Button Add_in_lib = new Button(new Rectangle(130, (int)formsize.Y - 36, 0, 0), "To library");
            Add_in_lib.OnClick += (sender) =>
            {
                GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");
                if (SelectedOBJ != null & obj_name_textbox.Text != "")
                {
                    Library.Add(Functions.DeepClone(SelectedOBJ) as GameObject , obj_name_textbox.Text);
                    obj_name_textbox.Text = ""; // обнуляем тестбокс
                    rebuild_images();
                    page_now = pages_count;
                    rebuild_images();
                }  
            };
            _MainWindow.Controls.Add(Add_in_lib);
            #endregion

            #region next_pre buttons
            Button next = new Button(new Rectangle((int)formsize.X - 50, (int)formsize.Y - 35, 0, 0), ">", "next");
            next.OnClick += (sender) =>
            {
                page_now++;
                rebuild_images();
            };
            _MainWindow.Controls.Add(next);

            Button pre = new Button(new Rectangle((int)formsize.X - 100, (int)formsize.Y - 35, 0, 0), "<", "previews");
            pre.OnClick += (sender) =>
            {
                page_now--;
                rebuild_images();
            };
            _MainWindow.Controls.Add(pre);
            #endregion

            _MainWindow.Controls.Add(new Label(new Rectangle((int)formsize.X - 190, (int)formsize.Y - 35, 0, 0), "Page */*", "page_label"));

            #region images
            for (int w = 0; w < 3; ++w)
            {
                for (int i = 0; i < 3; ++i)
                {
                    int number = i + w*3;

                    Image img = new Image(new Rectangle(35 + i * 128 + i * 25, 90 + w * 128 + w * 25, 128, 128), ENGINE.Logic.LevelController.nullTexture.Texture2d, String.Format("img{0}", number));

                    Label desc = new Label(new Rectangle(5+ 35 + i * 128 + i * 25, 90 + w * 128 + w * 25 + 128, 0, 0), "", String.Format("descr{0}", number));

                    img.OnMouseMove += (sender, sad) =>
                        {
                            if (img.Texture != ENGINE.Logic.LevelController.nullTexture.Texture2d )
                            {
                                _ContextWindow.Bounds.X = img.Bounds.X + _MainWindow.Bounds.X + 10;
                                _ContextWindow.Bounds.Y = img.Bounds.Y + _MainWindow.Bounds.Y;
                                _ContextWindow.Visible = true;
                                _ContextWindow.TopMost = true;
                                imageChoosen = number;
                            }
                        };
            


                    _MainWindow.Controls.Add(img);
                    _MainWindow.Controls.Add(desc);
                }
            }
            #endregion
            
            guiManager.Controls.Add(_MainWindow);

            #region context_window
            _ContextWindow = new Window(new Rectangle
            (0, 0, 100, 116), "Object", "Context");
            _ContextWindow.Movable = false;
            _ContextWindow.Visible = false;

            _ContextWindow.Transparency = 0.8f;

            Button AddInGame_cb = new Button(new Rectangle(4, 4, 100 - 8, 24), "Add", "AddInGame_cb");
            AddInGame_cb.OnClick += (sender) =>
            {
                GameObject SelectedOBJ = (GameObject)GameManager.LvlController.CommonData.ReadData("SelectedOBJ");
                if (SelectedOBJ != null) (SelectedOBJ.data.ReadData("SelectParams") as SelectParams).DeSelect(SelectedOBJ);

                GameObject ob = Functions.DeepClone(Library.GetLibraryArray()[selectedImageNumberInLib].obj) as GameObject;

                ob.Gtexture.Position = GameManager.LvlController.camera.CameraPosition;

                GameManager.LvlController.ObjectArray.Add(ob);
                GameManager.LvlController.SortByLayer();

                (ob.data.ReadData("SelectParams") as SelectParams).Select(ob);
            };
            _ContextWindow.Controls.Add(AddInGame_cb);

            Button Rename_cb = new Button(new Rectangle(4, 24*1 +4*2, 100 - 8, 24), "Rename", "Rename_cb");
            Rename_cb.OnClick += (sender) =>
            {
                hide_subb();
                _RenameWindow.Visible = true;
                _RenameWindow.Enabled = true;
                saved_number_selected = selectedImageNumberInLib;
                (_RenameWindow.GetControl("Textbox_T") as TextBox).Text = Library.GetLibraryArray()[selectedImageNumberInLib].name;
            };
            _ContextWindow.Controls.Add(Rename_cb);


            Button RemoveFromLib_cb = new Button(new Rectangle(4, 24 * 3  + 4 * 4, 100 - 8, 24), "Remove", "RemoveFromLib_cb");
            RemoveFromLib_cb.OnClick += (sender) =>
            {
                hide_subb();
                _YouSureWindow.Visible = true;
                _YouSureWindow.Enabled = true;
                saved_number_selected = selectedImageNumberInLib;
                _YouSureWindow.Title = Library.GetLibraryArray()[selectedImageNumberInLib].name;
            };
            _ContextWindow.Controls.Add(RemoveFromLib_cb);

            guiManager.Controls.Add(_ContextWindow);
            #endregion

            #region rename_window
            guiManager.LoadControls("Content\\GUIcontrols\\Rename_Window.xml");
            _RenameWindow = (Window)guiManager.GetControl("Rename_Window");
            _RenameWindow.Visible = false;
            _RenameWindow.Movable = false;
            _RenameWindow.Bounds.X = (int)Depository.ScreenResolution.X/2 - _RenameWindow.Bounds.Width / 2;
            _RenameWindow.Bounds.Y = (int)Depository.ScreenResolution.Y/2 - _RenameWindow.Bounds.Height - 1;

            (_RenameWindow.GetControl("Cancel_Button") as Button).OnClick += (sender) =>
            {
                _RenameWindow.Visible = false;
                _RenameWindow.Enabled = false;
            };

            (_RenameWindow.GetControl("Rename_Button") as Button).OnClick += (sender) =>
            {
                Library.GetLibraryArray()[saved_number_selected].name = (_RenameWindow.GetControl("Textbox_T") as TextBox).Text;
                _RenameWindow.Visible = false;
                _RenameWindow.Enabled = false;
                Library.save();
                rebuild_images();
            };
            #endregion

            #region you_sure_u_wanna_delete_window
            guiManager.LoadControls("Content\\GUIcontrols\\YouSure_Window.xml");
            _YouSureWindow = (Window)guiManager.GetControl("Usure_Window");
            _YouSureWindow.Visible = false;
            _YouSureWindow.Movable = false;
            _YouSureWindow.Bounds.X = (int)Depository.ScreenResolution.X / 2 - _YouSureWindow.Bounds.Width / 2;
            _YouSureWindow.Bounds.Y = (int)Depository.ScreenResolution.Y / 2 + 1;

            (_YouSureWindow.GetControl("Cancel_Button") as Button).OnClick += (sender) =>
            {
                _YouSureWindow.Visible = false;
                _YouSureWindow.Enabled = false;
            };

            (_YouSureWindow.GetControl("Delite_Button") as Button).OnClick += (sender) =>
            {
                Library.Remove(saved_number_selected);
                _YouSureWindow.Visible = false;
                _YouSureWindow.Enabled = false;
                rebuild_images();
            };
            #endregion

            rebuild_images();
        }

        public static void Hide()
        {
            (guiManager.GetControl("Library") as Window).Visible = false;
            (guiManager.GetControl("Library") as Window).Enabled = false;
            _ContextWindow.Visible = false;
        }
        public static void Show()
        {
            (guiManager.GetControl("Library") as Window).Visible = true;
            (guiManager.GetControl("Library") as Window).Enabled = true;
        }
        public static void Update()
        {
            guiManager.Update(Depository.gameTime);
        }
        public static void Draw()
        {
            guiManager.Draw(Depository.SpriteBatch);
        }
        static void hide_subb()
        {
            _RenameWindow.Visible = false;
            _YouSureWindow.Visible = false;
            _RenameWindow.Enabled = false;
            _YouSureWindow.Enabled = false;
        }

        //=====================================================
        static string search_mask = "";

        static void rebuild_images()
        {
            _ContextWindow.Visible = false;

            for (int i = 0; i < 9; ++i)
            {
                Image img = (Image)_MainWindow.GetControl(String.Format("img{0}", i));
                Label descr = (Label)_MainWindow.GetControl(String.Format("descr{0}", i));

                img.Bounds.Width = 128;
                img.Bounds.Height = 128;

                int imageNumberInLib = int.MaxValue;

                if (i + page_now * 9 < available_numbers.Count)  imageNumberInLib = available_numbers[i + page_now * 9];

                if (imageNumberInLib < Library.GetLibraryArray().Count)
                {
                    ENGINE.Graphics.GameTexture t = new ENGINE.Graphics.GameTexture(false, Library.GetLibraryArray()[imageNumberInLib].obj.Gtexture.Folder);
                    img.Texture = t.Texture2d;
                    descr.Text = Library.GetLibraryArray()[imageNumberInLib].name;

                    if (Library.GetLibraryArray()[imageNumberInLib].obj.Gtexture.TextureResolution.width > Library.GetLibraryArray()[imageNumberInLib].obj.Gtexture.TextureResolution.height)
                    {
                        float siz = Library.GetLibraryArray()[imageNumberInLib].obj.Gtexture.TextureResolution.width / Library.GetLibraryArray()[imageNumberInLib].obj.Gtexture.TextureResolution.height;
                        img.Bounds.Height = (int)(128 / siz);
                        if (img.Bounds.Height < 32) img.Bounds.Height = 32;
                    }
                    else
                    {
                        float siz = Library.GetLibraryArray()[imageNumberInLib].obj.Gtexture.TextureResolution.height / Library.GetLibraryArray()[imageNumberInLib].obj.Gtexture.TextureResolution.width;
                        img.Bounds.Width = (int)(128 / siz);
                        if (img.Bounds.Width < 32) img.Bounds.Width = 32;
                    }
                }
                else
                {
                    img.Texture = ENGINE.Logic.LevelController.nullTexture.Texture2d;
                    descr.Text = "";
                }
            }

            Label pagelabel = (Label)_MainWindow.GetControl("page_label");
            pagelabel.Text = String.Format("Page {0}/{1}", page_now , pages_count);

        }

        static List<int> available_numbers
        {
            get
            {
                List<int> list = new List<int>();
                for (int i = 0; i < Library.GetLibraryArray().Count; ++i)
                {
                    if (Library.GetLibraryArray()[i].name.IndexOf(search_mask) != -1) list.Add(i);
                }
                return list;
            }
        }

        static int pages_count
        {
            get
            {
                return available_numbers.Count / 9;
            }
        }

        static int page_now
        {
            get
            {
                if (_pagenow > pages_count)
                {
                    _pagenow = pages_count;
                }

                if (_pagenow < 0) _pagenow = 0;

                return _pagenow;
            }

            set
            {
                _pagenow = value;
            }
        }
        static int _pagenow = 0;

        static int imageChoosen = 0;

        static int selectedImageNumberInLib
        {
            get
            {
                return available_numbers[imageChoosen + page_now * 9];
            }
        }

        static int saved_number_selected;
    }
}

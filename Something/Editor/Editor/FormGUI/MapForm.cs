﻿using System;
using System.Windows.Forms;
using STORAGE;
using GAME;
using ENGINE.Logic;
using Microsoft.Xna.Framework;
using ENGINE.Logic.ObjAttributes;

namespace Editor.FormGUI
{
    public partial class MapForm : Form
    {
        public MapForm()
        {
            InitializeComponent();
        }

        public Level SelectedLevel
        {
            get
            {
                Level selected = null;

                foreach (Level l in GAME.GameManager.Map.Levels)
                {
                    if (l.selected) { selected = l; break; }
                }

                return selected;
            }
        }

        private void MapForm_LocationChanged(object sender, EventArgs e)
        {
            this.Location = location;
        }
        private void MapForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
            }
        }
        public bool hided = false;
        public System.Drawing.Point location
        {
            get
            {
                if (hided)
                {
                    return new System.Drawing.Point((int)(Depository.ScreenResolution.X + 25), 0);
                }
                else
                {
                    return new System.Drawing.Point((int)(Depository.ScreenResolution.X - this.Width), 0);
                }
            }
        }



        private void button1_Click(object sender, EventArgs e)
        {
            Level l = new Level(new Vector2(r.Next(100) + 100, r.Next(100) + 100));


            GAME.GameManager.Map.Levels.Add(l);
        }

        Random r = new Random();

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (SelectedLevel == null) groupBox1.Enabled = false; else groupBox1.Enabled = true;

            if (SelectedLevel != null)
            {
                checkBox1.Checked = SelectedLevel.hided ? true : false;
                checkBox2.Checked = SelectedLevel.completed ? true : false;
                label2.Text = SelectedLevel.Folder;
                textBox1.Text = SelectedLevel.title;
                textBox2.Text = SelectedLevel.description;
                label10.Text = SelectedLevel.ID.ToString();


                label11.Text = "";
                foreach (int i in SelectedLevel.LevelsToOpen) { label11.Text += i.ToString() + " "; }



                label5.Text = SelectedLevel.Preview_Image.Folder;
            }
            else
            {
                label2.Text = label5.Text = "";
            }
            hideHelpers_checkBox.Checked = GAME.GameManager.Map.hideHelpers;
        }


        private void button2_Click(object sender, EventArgs e)
        {
            SelectedLevel.Texture.Position = new Vector2(r.Next(100) + 100, r.Next(100) + 100);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                //hided
                SelectedLevel.hided = true;
                SelectedLevel.completed = false;
            }
            else
            {
                SelectedLevel.hided = false;
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked & !checkBox1.Checked)
            {
                SelectedLevel.completed = true;
            }
            else
            {
                SelectedLevel.completed = false;
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Выберете файл уровня .lvl";
            dialog.Filter = "levels (*.lvl)|*.lvl";
            dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + "Levels";

            DialogResult res = dialog.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {
                string file = dialog.FileName;
                file = file.Replace(AppDomain.CurrentDomain.BaseDirectory + @"Levels\", "");


                SelectedLevel.Folder = file;
            }
        }

        //save
        private void button3_Click(object sender, EventArgs e)
        {
            GAME.GameManager.Map.SaveGlobal();
            MessageBox.Show("saved");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            GAME.GameManager.Map = WorldMap.Load(System.IO.Directory.GetCurrentDirectory() + @"\Levels\worldmap.map");
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            SelectedLevel.title = textBox1.Text;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            SelectedLevel.description = textBox2.Text;
        }

        private void MapForm_Load(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            GAME.GameManager.Map = new WorldMap();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            GAME.GameManager.Map.Levels.ForEach(level => level.LevelsToOpen.Remove(SelectedLevel.ID));
            GAME.GameManager.Map.Levels.Remove(SelectedLevel);
        }

        //set pre
        private void button7_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Выберете файл текстуры xnb";
            dialog.Filter = "XNB images (*.xnb)|*.xnb";
            dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + @"Content\Textures\";

            DialogResult res = dialog.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {
                string file = dialog.FileName;

                file = file.Replace(".xnb", "");
                file = file.Replace(AppDomain.CurrentDomain.BaseDirectory, "");
                file = file.Substring(8);

                SelectedLevel.Preview_Image.Folder = file;
            }

        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (GAME.GameManager.LvlController.HERO != null)
            {
                GAME.GameManager.Map.HERO = GAME.GameManager.LvlController.HERO;

                GAME.GameManager.Map.SaveGlobal();
                MessageBox.Show("герой найден. карта сохранена");
            }
            else MessageBox.Show("ERROR");
        }

        private void hideHelpers_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            GAME.GameManager.Map.hideHelpers = hideHelpers_checkBox.Checked;
        }

        //add id
        private void button9_Click(object sender, EventArgs e)
        {
            foreach (Level l in GAME.GameManager.Map.Levels)
            {
                if (textBox3.Text == l.ID.ToString())
                {
                    if (!SelectedLevel.LevelsToOpen.Contains(l.ID) && SelectedLevel.ID != l.ID)
                    {
                        SelectedLevel.LevelsToOpen.Add(l.ID);
                        SelectedLevel.AddPath(SelectedLevel, l, new Vector2(0, 32));
                    }
                }
            }
            textBox3.Text = "";
        }

        private void button10_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < SelectedLevel.LevelsToOpen.Count; ++i)
            {
                if (SelectedLevel.LevelsToOpen[i].ToString() == textBox3.Text)
                {
                    SelectedLevel.DeletePath(SelectedLevel.LevelsToOpen[i]);
                    SelectedLevel.LevelsToOpen.Remove(SelectedLevel.LevelsToOpen[i]);
                }
            }
            textBox3.Text = "";
        }

        private void button11_Click(object sender, EventArgs e)
        {
            GameManager.SetGame();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            GameManager.SetMenu();
            Editor.Managers.EDrawManager.ActiveMenu = new Editor.Managers.MenuScreens.EPauseM(Editor.Managers.EDrawManager.ActiveMenu);
        }
    }
}

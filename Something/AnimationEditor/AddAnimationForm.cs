﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AnimationLib;

namespace AnimationLib.AnimationCreator
{
    public partial class AddNewAnimationForm : Form
    {
        public AddNewAnimationForm()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e) //gew animation
        {
            Animation Animation = new Animation();
            Animation.animationName = textBox1.Text;

            AnimationCreator.AnimationManager.Container.Animations.Add(Animation);
            this.Close();
        }

        private void AddNewAnimationForm_Load(object sender, EventArgs e)
        {
            textBox1.Text = "Animation-" + Convert.ToString( AnimationCreator.AnimationManager.Container.Animations.Count);
        }
    }
}

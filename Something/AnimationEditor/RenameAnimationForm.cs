﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AnimationLib;

namespace AnimationLib.AnimationCreator
{
    public partial class RenameAnimationForm : Form
    {
        public RenameAnimationForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AnimationLib.AnimationCreator.AnimationManager.Container.Animations[AnimationLib.AnimationCreator.AnimationManager.animationNow].animationName = textBox1.Text;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void RenameAnimationForm_Load(object sender, EventArgs e)
        {
            textBox1.Text = AnimationLib.AnimationCreator.AnimationManager.Container.Animations[AnimationLib.AnimationCreator.AnimationManager.animationNow].animationName;
        }
    }
}

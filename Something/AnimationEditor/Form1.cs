﻿using System;
using System.Windows.Forms;
using System.Drawing;
using AnimationLib;

namespace AnimationLib.AnimationCreator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e) //open
        {
            bool open = false;
            if (FileStatus.CurrentStatus == FileStatus.Status.Opened)
            {
                if (MessageBox.Show("На данный момент вы редактируете не сохраненную анимацию, хотите ли вы продолжить и потерять несохраненные данные?", "Открыть файл анимации", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    FileStatus.CurrentStatus = FileStatus.Status.Nothing;
                    open = true;
                }
            }
            else open = true;



            if (open)
            {
                OpenFileDialog Dialog = new OpenFileDialog();
                Dialog.Filter = "Анимация|*.anim|Все раcширения (*.*)|*.*";

                if (Dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    AnimationManager.OpenAnimation(Dialog.FileName);
                    FileStatus.LastLocation = Dialog.FileName;
                    FileStatus.CurrentStatus = FileStatus.Status.Opened;
                    FileStatus.recharge = true;
                    AnimationManager.frameNow = 0;
                    AnimationManager.animationNow = 0;
                }
                else
                {
                    reset();
                }
            }

            textBox1.Text = Convert.ToString(AnimationCreator.AnimationManager.Container.FrameHeight);
            textBox2.Text = Convert.ToString(AnimationCreator.AnimationManager.Container.FrameWidth);
        }

        private void новыйToolStripMenuItem_Click(object sender, EventArgs e) //new
        {
            AnimationManager.playing = false;

            bool create = false;


            if (FileStatus.CurrentStatus == FileStatus.Status.Opened)
            {
                if (MessageBox.Show("На данный момент вы редактируете не сохраненную анимацию, хотите ли вы продолжить и потерять несохраненные данные?", "Создать новый файл анимации", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    FileStatus.CurrentStatus = FileStatus.Status.Nothing;

                    create = true;
                }
            }
            else create = true;


            if (create)
            {
                FileStatus.LastLocation = "";
                AnimationManager.CreateAnimation();
                FileStatus.CurrentStatus = FileStatus.Status.Opened;
                AnimationManager.frameNow = 0;
                AnimationManager.showOnce = false;
                pictureBox1.Image = null;
            }

            textBox1.Text = Convert.ToString(AnimationCreator.AnimationManager.Container.FrameHeight);
            textBox2.Text = Convert.ToString(AnimationCreator.AnimationManager.Container.FrameWidth);
            FileStatus.recharge = true;
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FileStatus.CurrentStatus == FileStatus.Status.Opened)
            {
                if (FileStatus.LastLocation != "") AnimationManager.SaveAnimation();
                else MessageBox.Show("Путь к исходному файлу утерян или был создан новый файл анимации, попробуйте сохранить его через Сохранить как...");
            }
            else MessageBox.Show("Вы не открыли или не создали анимации, чтобы ее сохранить");
        }

        private void сохранитьКакToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FileStatus.CurrentStatus == FileStatus.Status.Opened)
            {
                FolderBrowserDialog Dialog = new FolderBrowserDialog();

                if (Dialog.ShowDialog() == DialogResult.OK)
                {
                    AnimationManager.SaveAnimationHow(Dialog.SelectedPath);
                }
            }
            else MessageBox.Show("Вы не открыли или не создали анимации, чтобы ее сохранить");
            
        }

        private bool first = true;

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (first)
            {
                first = !first;
                AnimationManager.pictureBox = pictureBox1;
            }



            if (AnimationManager.playing)
            {
                AnimationManager.tick(pictureBox1, timer1.Interval, label21);
            }



            bool sbros = false;

            if (FileStatus.CurrentStatus == FileStatus.Status.Opened)
            {
                if (AnimationManager.Container.Animations.Count != 0)
                {
                    label1.Text = Convert.ToString(AnimationManager.frameNow) + " / " + Convert.ToString(AnimationManager.Container.Animations[AnimationManager.animationNow].Frames.Count);
                    label6.Text = "\"" + AnimationCreator.AnimationManager.Container.Animations[AnimationCreator.AnimationManager.animationNow].animationName + "\"";
                    label8.Text = Convert.ToString(AnimationCreator.AnimationManager.Container.Animations[AnimationCreator.AnimationManager.animationNow].Frames.Count);
                    label20.Text = Convert.ToString(AnimationManager.frameNow);
                    
                    
                    if (!textBox3.Focused && !button15.Focused && !button16.Focused) textBox3.Text = Convert.ToString( AnimationManager.Container.Animations[AnimationManager.animationNow].nextAnimation);
                    if (AnimationCreator.AnimationManager.Container.Animations[AnimationCreator.AnimationManager.animationNow].Frames.Count != 0)
                    {
                        textBox4.Enabled = true;
                        textBox5.Enabled = true;
                        textBox6.Enabled = true;
                        textBox7.Enabled = true;
                        groupBox3.Enabled = true;
                        button22.Enabled = true;

                        if (!textBox4.Focused && !button18.Focused) textBox4.Text = Convert.ToString(
                            AnimationCreator.AnimationManager.Container.Animations[AnimationCreator.AnimationManager.animationNow].Frames[AnimationManager.frameNow].Xcoord);


                        if (!textBox5.Focused && !button19.Focused) textBox5.Text = Convert.ToString(
                          AnimationCreator.AnimationManager.Container.Animations[AnimationCreator.AnimationManager.animationNow].Frames[AnimationManager.frameNow].Ycoord);

                        if (!textBox6.Focused && !button20.Focused) textBox6.Text = Convert.ToString(
                          AnimationCreator.AnimationManager.Container.Animations[AnimationCreator.AnimationManager.animationNow].Frames[AnimationManager.frameNow].delay);

                        if (!textBox7.Focused && !button21.Focused) textBox7.Text = Convert.ToString(
                        AnimationCreator.AnimationManager.Container.Animations[AnimationCreator.AnimationManager.animationNow].Frames[AnimationManager.frameNow].spriteNumber);


                    }
                    else
                    {
                        groupBox3.Enabled = false;
                        button22.Enabled = false;
                        textBox4.Enabled = false;
                        textBox5.Enabled = false;
                        textBox6.Enabled = false;
                        textBox7.Enabled = false;
                    }


                    if (AnimationManager.Container.TextureFolders.Count != 0) button13.Enabled = true;
                      


                    checkBox1.Enabled = true;
                    button9.Enabled = true;
                    button8.Enabled = true;
                    label6.Enabled = true;

                    button5.Enabled = true;
                    button6.Enabled = true;
                    button1.Enabled = true;
                    button2.Enabled = true;

                    button3.Enabled = true;
                    button4.Enabled = true;
                    


                    if (!checkBox1.Checked)
                    {
                        textBox3.Enabled = true;
                        button15.Enabled = true;
                        button16.Enabled = true;
                    }
                    else
                    {
                        textBox3.Enabled = false;
                        button15.Enabled = false;
                        button16.Enabled = false;
                    }
                }
                else
                {
                    checkBox1.Enabled = false;
                    button9.Enabled = false;
                    button8.Enabled = false;
                    label6.Enabled = false;

                    textBox3.Enabled = false;
                    button15.Enabled = false;
                    button16.Enabled = false;

                    button3.Enabled = false;
                    button4.Enabled = false;

                    groupBox3.Enabled = false;
                    button13.Enabled = false;
                    button22.Enabled = false;
                }

                label23.Text = Convert.ToString(AnimationManager.Container.TextureFolders.Count);
                if (AnimationCreator.AnimationManager.playing) label13.Text = "воспроизводится"; else label13.Text = "пауза";
                button17.Enabled = true;
                label23.Enabled = true;
                groupBox2.Enabled = true;
                label3.Text = Convert.ToString(AnimationManager.animationNow) + " / " + Convert.ToString(AnimationManager.Container.Animations.Count);
            }
            else sbros = true;


            if (sbros | FileStatus.recharge)
            {
                label1.Text = "none";
                label3.Text = "none";
                label6.Text = "none";
                label8.Text = "none";
                label20.Text = "none";

                label13.Text = "none";
                button5.Enabled = false;
                button6.Enabled = false;
                button1.Enabled = false;
                button2.Enabled = false;
                groupBox2.Enabled = false;
                textBox3.Enabled = false;
                button3.Enabled = false;
                button4.Enabled = false;
                FileStatus.recharge = false;
                groupBox3.Enabled = false;
                button13.Enabled = false;

                button17.Enabled = false;
                label23.Enabled = false;
                label23.Text = "none";
            }
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(@"Версия: 0.1. Разработчик: NoEscape. Сайт: https://bitbucket.org/ff_devteam/forcefield/overview");
        }

        private void мануалToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("как нить позже добавлю");
        }

        private void button7_Click(object sender, EventArgs e) // добавить новую анимацию
        {
            AddNewAnimationForm a = new AddNewAnimationForm();
            a.ShowDialog();
        }

        private void button8_Click(object sender, EventArgs e) // rename
        {
            RenameAnimationForm a = new RenameAnimationForm();
            a.ShowDialog();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (AnimationManager.Container.Animations.Count != 0)
            {
                if (checkBox1.Checked)
                {
                    AnimationManager.Container.Animations[AnimationManager.animationNow].looped = true;
                }
                else AnimationManager.Container.Animations[AnimationManager.animationNow].looped = false;
            }
        }

        private void button9_Click(object sender, EventArgs e) //delite
        {
            AnimationCreator.AnimationManager.Container.Animations.RemoveAt(AnimationCreator.AnimationManager.animationNow);

            if (AnimationManager.Container.Animations.Count != 0)
            {
                AnimationManager.animationNow = 0;
                if (AnimationManager.Container.Animations[AnimationManager.animationNow].looped) checkBox1.Checked = true; else checkBox1.Checked = false;
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            textBox1.Text = Convert.ToString(AnimationCreator.AnimationManager.Container.FrameHeight);
            textBox2.Text = Convert.ToString(AnimationCreator.AnimationManager.Container.FrameWidth);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            try
            {
                AnimationCreator.AnimationManager.Container.FrameHeight = Convert.ToInt32(textBox1.Text);
                AnimationCreator.AnimationManager.Container.FrameWidth = Convert.ToInt32(textBox2.Text);
            }
            catch
            {
                MessageBox.Show("Одно или оба введденных значения неправильны. Оба числа должны быть положительными, целочисленными");
            }            
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (AnimationManager.Container.Animations[AnimationManager.animationNow].Frames.Count == 0)
            {
                MessageBox.Show("в текущей анимации нет кадров. нельзя начать просмотр");
            }
            else
            AnimationCreator.AnimationManager.playing = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            AnimationCreator.AnimationManager.playing = false;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (AnimationManager.animationNow != 0)
            {
                AnimationManager.animationNow--;
                AnimationManager.frameNow = 0;
                AnimationManager.ShowFrameNow();
                AnimationManager.playing = false;
            }

            if (AnimationManager.Container.Animations[AnimationManager.animationNow].looped) checkBox1.Checked = true; else checkBox1.Checked = false;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (AnimationManager.animationNow < AnimationManager.Container.Animations.Count - 1)
            {
                AnimationManager.animationNow++;
                AnimationManager.frameNow = 0;
                AnimationManager.ShowFrameNow();
                AnimationManager.playing = false;
            }
            
            if (AnimationManager.Container.Animations[AnimationManager.animationNow].looped) checkBox1.Checked = true; else checkBox1.Checked = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (AnimationManager.Container.Animations.Count != 0)
            {
                if (AnimationManager.frameNow != 0) AnimationManager.frameNow--;
                AnimationManager.ShowFrameNow();
                AnimationManager.playing = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (AnimationManager.Container.Animations.Count != 0)
            {
                if (AnimationManager.frameNow < AnimationManager.Container.Animations[AnimationManager.animationNow].Frames.Count - 1)
                {
                    AnimationManager.frameNow++;
                    AnimationManager.ShowFrameNow();
                    AnimationManager.playing = false;
                }
            }
        }

        private void сбросToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FileStatus.CurrentStatus == FileStatus.Status.Opened)
            {
                if (MessageBox.Show("На данный момент вы редактируете не сохраненную анимацию, хотите ли вы продолжить и потерять несохраненные данные?", "Сбросить", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    reset();
                }
            }          
        }

        private void reset()
        {
            FileStatus.LastLocation = "";
            AnimationContainer Container = new AnimationContainer();
            FileStatus.CurrentStatus = FileStatus.Status.Nothing;
            AnimationManager.playing = false;
            AnimationManager.showOnce = false;
            pictureBox1.Image = null;
        }


        private void button15_Click(object sender, EventArgs e)
        {
            bool bad = false;
            int nextanim = 0;
            try
            {
            nextanim = Convert.ToInt32(textBox3.Text);
            }
            catch
            {
                bad = true;
                MessageBox.Show("число должно быть integer");
            }

            if (!bad)
            {
                if (nextanim >= 0 && nextanim <= AnimationManager.Container.Animations.Count - 1)
                {
                    AnimationManager.Container.Animations[AnimationManager.animationNow].nextAnimation = nextanim;
                }
                else MessageBox.Show("вы указали анимацию за пределами диапазона от 0 до " + Convert.ToString(AnimationManager.Container.Animations.Count - 1));
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            textBox3.Text = Convert.ToString( AnimationManager.Container.Animations[AnimationManager.animationNow].nextAnimation);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            OpenFileDialog Dialog = new OpenFileDialog();
            Dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + @"Content\Textures\";

            Dialog.Filter = "Изображение|*.png";

            if (Dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                bool exist = false;

                foreach (string x in AnimationManager.Container.TextureFolders)
                {
                    if (x == Dialog.FileName)
                    {
                        exist = true;
                        break;
                    }
                }



                if (!exist)
                {
                    if (Dialog.FileName.IndexOf(@"Content\Textures") == -1)
                    {
                        MessageBox.Show(@"Вы должны добавить текстуру, которая находится в папке Content\Textures игры");
                    }
                    else
                    {
                        string s = Dialog.FileName;

                        int st = s.LastIndexOf(@"Content\Textures") + 8;
                        s = s.Substring(st, s.Length - st);

                        s = s.Substring(0, s.IndexOf("."));


                        MessageBox.Show("Добавлена текстура: " + s);

                        AnimationManager.Container.TextureFolders.Add(s);
                        AnimationManager.ReloadTextures();
                         
                    }
                }
                else MessageBox.Show("такая текстура уже есть");


            }
        }

        private void button17_Click(object sender, EventArgs e) //список задействованных текстур
        {
            TexturesForm a = new TexturesForm();
            a.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            AnimationManager.bar = progressBar1;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            Frame frame = new Frame();

            frame.delay = 0.1;
            frame.spriteNumber = 0;
            frame.Xcoord = 0;
            frame.Ycoord = 0;

            AnimationManager.Container.Animations[AnimationManager.animationNow].Frames.Add(frame);
        }

        private void button18_Click(object sender, EventArgs e)
        {
            AnimationManager.ShowFrameNow();
            int value = 0;
            try
            {
                value = Convert.ToInt32(textBox4.Text);
                AnimationManager.Container.Animations[AnimationManager.animationNow].Frames[AnimationManager.frameNow].Xcoord = value;
            }
            catch { MessageBox.Show("неверное значение"); }
        }

        private void button19_Click(object sender, EventArgs e)
        {
            AnimationManager.ShowFrameNow();
            int value = 0;
            try
            {
                value = Convert.ToInt32(textBox5.Text);
                AnimationManager.Container.Animations[AnimationManager.animationNow].Frames[AnimationManager.frameNow].Ycoord = value;
            }
            catch { MessageBox.Show("неверное значение"); }
        }

        private void button21_Click(object sender, EventArgs e)
        {
            AnimationManager.ShowFrameNow();
            int value = 0;
            try
            {
                value = Convert.ToInt32(textBox7.Text);

                if (value >= 0 && value < AnimationManager.Container.TextureFolders.Count)
                AnimationManager.Container.Animations[AnimationManager.animationNow].Frames[AnimationManager.frameNow].spriteNumber = value;
                else MessageBox.Show("за пределами диапазона, такой текстуры нет");
            }
            catch { MessageBox.Show("неверное значение"); }
        }

        private void button20_Click(object sender, EventArgs e)
        {
            AnimationManager.ShowFrameNow();
            double value = 0;
            try
            {
                value = Convert.ToDouble(textBox6.Text);
                AnimationManager.Container.Animations[AnimationManager.animationNow].Frames[AnimationManager.frameNow].delay = value;

            }
            catch { MessageBox.Show("неверное значение"); }
        }

        private void button22_Click(object sender, EventArgs e)
        {
            if (AnimationManager.playing)
            {
                MessageBox.Show("Нельзя удалять кадры во время просмотра анимации");
            }
            else
            {
                AnimationCreator.AnimationManager.Container.Animations[AnimationCreator.AnimationManager.animationNow].Frames.RemoveAt(AnimationManager.frameNow);
                AnimationManager.frameNow = 0;
                AnimationManager.ShowFrameNow();
            }
        }


        public void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            if (AnimationManager.playing | AnimationManager.showOnce && AnimationManager.Container.Animations[AnimationCreator.AnimationManager.animationNow].Frames.Count != 0)
            {
                Rectangle rec = new Rectangle(AnimationManager.Container.Animations[AnimationManager.animationNow].Frames[AnimationManager.frameNow].Xcoord,
                    AnimationManager.Container.Animations[AnimationManager.animationNow].Frames[AnimationManager.frameNow].Ycoord,
                    AnimationManager.Container.FrameWidth,
                    AnimationManager.Container.FrameHeight);

                double sizer = (double)AnimationManager.Container.FrameWidth / (double)AnimationManager.Container.FrameHeight; //соотношение длины фрейма к ширене, которое надо сохранить при подгонке

                //сайзер менее 1 -- длина не меняется. более 1- ширина не беменяется

                int outW = 0;
                int outH = 0;

                if (sizer <= 1)
                {
                    outW = (int)(pictureBox1.Width * sizer);

                    outH = pictureBox1.Height;
                }
                else
                {
                    outW = pictureBox1.Width;

                    outH = (int)(pictureBox1.Height * sizer);
                }




                Rectangle zoom = new Rectangle(0, 0, outW, outH);

                double val = sizer;

                e.Graphics.Clear(Color.White);

                e.Graphics.DrawImage(AnimationManager.LoadedTextures[AnimationManager.Container.Animations[AnimationManager.animationNow].Frames[AnimationManager.frameNow].spriteNumber], zoom, rec, GraphicsUnit.Pixel);

            }
        }

        private void button23_Click(object sender, EventArgs e)
        {
            MessageBox.Show(@"Зайдите в каталог с игрой и поместите в папку Content\Textures вашу .png текстуру. если нужна текстура из игры то конвентируйте xnb to png и оставьте в той же директории без смены имяни. после добавления и работы с текстурой конвентируйте ее в xnb если такой ещё нету.");
        }
    }
}

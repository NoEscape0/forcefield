﻿using System;
using System.Text;
using System.Windows.Forms;
using AnimationLib;

namespace AnimationLib.AnimationCreator
{
    public partial class TexturesForm : Form
    {
        public TexturesForm()
        {
            InitializeComponent();
        }

        private void TexturesForm_Load(object sender, EventArgs e)
        {
            StringBuilder text = new StringBuilder();
            for (int i = 0; i < AnimationManager.Container.TextureFolders.Count; ++i)
            {
               text.Append(Convert.ToString(i)+"-  "+AnimationManager.Container.TextureFolders[i]);
               text.AppendLine();
               text.AppendLine();
            }
            textBox1.Text = text.ToString();
            
            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int value = 0;
            bool good = true;

            try
            {
                value = Convert.ToInt32(textBox2.Text);
            }
            catch
            {
                MessageBox.Show("Неверное значение");
                textBox2.Text = "";
                good = false;
            }


            if (good)
            {
                if (value >= 0 && value < AnimationManager.Container.TextureFolders.Count)
                {
                    //проверка на использование этйо текстуры

                    bool used = false;

                    for (int i = 0; i < AnimationManager.Container.Animations.Count; ++i)
                    {
                        for (int i2 = 0; i2 < AnimationManager.Container.Animations[i].Frames.Count; ++i2)
                        {
                            if (AnimationManager.Container.Animations[i].Frames[i2].spriteNumber == value)
                            {
                                //used
                                MessageBox.Show("Эту текстуру удалить нельзя т.к. ее использует анимация "+Convert.ToString(i)+" в кадре "+Convert.ToString(i2));
                                used = true;
                                break;
                            }

                        }                
                    }

                    if (!used)
                    {
                        AnimationManager.Container.TextureFolders.RemoveAt(value);
                        AnimationManager.ReloadTextures();

                        StringBuilder text = new StringBuilder();
                        for (int i = 0; i < AnimationManager.Container.TextureFolders.Count; ++i)
                        {
                            text.Append(Convert.ToString(i) + "-  " + AnimationManager.Container.TextureFolders[i]);
                            text.AppendLine();
                            text.AppendLine();
                        }
                        textBox1.Text = text.ToString();

                    }



                }
                else MessageBox.Show("текстуры с таким номером не существует");
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            int value = 0;
            bool good = true;

            try
            {
                value = Convert.ToInt32(textBox2.Text);
            }
            catch
            {
                MessageBox.Show("Неверное значение");
                textBox2.Text = "";
                good = false;
            }


            if (good)
            {
                if (value >= 0 && value < AnimationManager.Container.TextureFolders.Count)
                {
                    //переназначение пути


                    OpenFileDialog Dialog = new OpenFileDialog();
                    Dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + @"Content\Textures\";

                    Dialog.Filter = "Изображение|*.png";


                    if (Dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        bool exist = false;

                        foreach (string x in AnimationManager.Container.TextureFolders)
                        {
                            if (x == Dialog.FileName)
                            {
                                exist = true;
                                break;
                            }
                        }



                        if (!exist)
                        {
                            string s = Dialog.FileName;

                            int st = s.LastIndexOf(@"Content\Textures") + 8;
                            s = s.Substring(st, s.Length - st);

                            s = s.Substring(0, s.IndexOf("."));

                            AnimationManager.Container.TextureFolders[value] = s;
                            AnimationManager.ReloadTextures();


                            StringBuilder text = new StringBuilder();
                            for (int i = 0; i < AnimationManager.Container.TextureFolders.Count; ++i)
                            {
                                text.Append(Convert.ToString(i) + "-  " + AnimationManager.Container.TextureFolders[i]);
                                text.AppendLine();
                                text.AppendLine();
                            }
                            textBox1.Text = text.ToString();
                        }

                        else MessageBox.Show("такая текстура уже есть");
                    }


                }
                else MessageBox.Show("текстуры с таким номером не существует");
            }

        }
    }
}

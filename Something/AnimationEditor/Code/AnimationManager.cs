﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using AnimationLib;


namespace AnimationLib.AnimationCreator
{
    public static class AnimationManager
    {
        public static void OpenAnimation(string folder)
        {
            FileStatus.LastLocation = folder;

            try
            {
                AnimationManager.Container = AnimationContainer.Load(folder);
                AnimationManager.ReloadTextures();
                ShowFrameNow();
            }
            catch (System.InvalidOperationException)
            {
                FileStatus.LastLocation = "";
                AnimationContainer Container = new AnimationContainer();
                FileStatus.CurrentStatus = FileStatus.Status.Nothing;
                AnimationManager.playing = false;
                AnimationManager.showOnce = false;

                MessageBox.Show("Не удалось открыть файл анимации. Это могло произойти по причине того, что файл поврежден, имеет недопустимый формат или же по тому, что вы пытаетесь открыть неверный тим анимации.");
            }

        }

        private static void save(string folder)
        {
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(AnimationManager.Container.GetType());

            //XmlWriter writer = new XmlTextWriter(folder, System.Text.Encoding.UTF8);
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            XmlWriter writer = XmlTextWriter.Create(folder, settings);
            serializer.Serialize(writer, AnimationManager.Container);

            writer.Close();

            MessageBox.Show("Сохранено в " + folder);
        }

        public static void SaveAnimation()
        {
            save(FileStatus.LastLocation);
        }

        public static void SaveAnimationHow(string folder)
        {
            if (Container.Animations.Count != 0) folder += @"\" + Container.Animations[0].animationName + ".anim";
            else folder += @"\noname.anim";
            save(folder);
        }

        public static void CreateAnimation()
        {
            Container = new AnimationContainer();
        }

        public static ProgressBar bar;
        public static int frameNow
        {
            get { return framenow; }
            set
            {
                framenow = value;
                try
                {
                    bar.Value = (int)((double)framenow / (double)(Container.Animations[animationNow].Frames.Count - 1) * (double)100);
                }
                catch
                {
                }
            }
        }
        private static int framenow = 0;
        public static int animationNow = 0;
        public static AnimationContainer Container = new AnimationContainer();
        public static bool showOnce = false;
        public static bool playing = false;
        public static int TotalElapsedMS = 0;
        public static List<Image> LoadedTextures;
        public static PictureBox pictureBox;

        public static bool serializeType = true; // true - мой класс, false - доменный



        public static void ReloadTextures()
        {
            LoadedTextures = null;
            LoadedTextures = new List<Image>();
            System.IO.Directory.SetCurrentDirectory(System.Windows.Forms.Application.StartupPath);
            foreach (string x in AnimationManager.Container.TextureFolders)
            {
                try
                {
                    Image img = Image.FromFile("Content\\" + x + ".png");
                    LoadedTextures.Add(img);
                    img = null;
                }
                catch (System.IO.FileNotFoundException)
                {
                    MessageBox.Show(x + " не найден");

                    FileStatus.LastLocation = "";
                    AnimationContainer Container = new AnimationContainer();
                    FileStatus.CurrentStatus = FileStatus.Status.Nothing;
                    AnimationManager.playing = false;
                    AnimationManager.showOnce = false;
                }
            }
        }

        public static void tick(PictureBox pictureBox, int msPassed, Label lab)
        {

            TotalElapsedMS -= msPassed; //уменьшение времени ожидания


            lab.Text = Convert.ToString(TotalElapsedMS); //


            if (TotalElapsedMS <= 0)
            {
                NextFrame();
                TotalElapsedMS = (int)(AnimationManager.Container.Animations[AnimationManager.animationNow].Frames[AnimationManager.frameNow].delay * 1000);
            }
        }

        public static void NextFrame()
        {
            if (frameNow == Container.Animations[animationNow].Frames.Count - 1)
            {
                //это последний кадр

                if (Container.Animations[animationNow].looped)
                {
                    frameNow = 0;
                }
                else
                {
                    frameNow = 0;
                    animationNow = Container.Animations[animationNow].nextAnimation;
                }
            }
            else
            {
                frameNow++;
            }

            ShowFrameNow();
        }

        public static void ShowFrameNow()
        {
            pictureBox.Invalidate();
            showOnce = true;

        }


    }
}

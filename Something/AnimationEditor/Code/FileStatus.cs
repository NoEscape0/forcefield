﻿using AnimationLib;

namespace AnimationLib.AnimationCreator
{

    public class FileStatus
    {

        public enum Status
        {
            Opened,
            Nothing,
        }


        public static string LastLocation = "";
        public static bool recharge = false;
        public static Status CurrentStatus = Status.Nothing;
    }
}

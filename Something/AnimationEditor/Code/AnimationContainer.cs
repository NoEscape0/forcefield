﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace AnimationLib
{
    [Serializable]
    public class AnimationContainer //контейнер всех анимаций в совокупности
    {
        public AnimationContainer()
        {
            Animations = new List<Animation>();
            TextureFolders = new List<string>();
        }

        public List<Animation> Animations; // каждый элемент представляет из себя анимацию юнита- последовательность спрайтов
        public List<string> TextureFolders; // массив со ссылками на текстуры

        public static AnimationContainer Load(string folder)
        {
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(AnimationContainer));
            XmlReader reader = new XmlTextReader(folder);


            AnimationContainer container = (AnimationContainer)serializer.Deserialize(reader);
            reader.Close();

            return container;
        }

        public int FrameWidth;
        public int FrameHeight;
    }

    [Serializable]
    public class Animation
    {
        public Animation()
        {
            Frames = new List<Frame>();
        }

        public List<Frame> Frames; // еймы одной анимации
        public bool looped = true;
        public string animationName;
        public int nextAnimation; // если looped == false то по этому параметры идет переключение на следующую анимацию
    }

    [Serializable]
    public class Frame
    {
        public int spriteNumber = 0; // номер спрайта из которого вырезан фрейм
        public int Xcoord;
        public int Ycoord;
        public double delay; //задержка до показа следующего кадра
    }
}

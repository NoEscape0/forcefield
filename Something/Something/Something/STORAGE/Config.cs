﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace STORAGE
{
    public static class Config
    {
        //загружаемые при запуске игры или переустановке настроек экрана
        public static bool TopMost = false;
        public static bool MouseVisible = false;
        public static bool isFixedTimeStep = true;
        public static bool SynchronizeWithVerticalRetrace = true;
        public static bool IsGameFullScreen = false;

        //cam
        public static float CameraSpeed = 1000f;
        public static bool CanZoom = true;
        public static int maxPlusSteps_E = 2;
        public static int maxMinusSteps_E = 6;
        public static int maxPlusSteps_G = 0;
        public static int maxMinusSteps_G = 1;
        public static float cameraGameSpeed = 7.0f;
        //info
        public static string GameVersion = "v_0.04";

        //keyboard
        public static class KeyboardCfg
        {
            public static Keys Jump = Keys.W;
            public static Keys MoveLeft = Keys.A;
            public static Keys MoveRight = Keys.D;
            public static Keys Shoot = Keys.D1;

            public static Keys ConsoleButton = Keys.OemTilde;
            public static Keys MapButton = Keys.M;


            public static Keys ActionKey = Keys.E;
        }

        //readonly
        public static bool EditorMode = false;

        //editor only
        public static bool show_collision_rectangles = true;

        public static int Snap_to_grid = 5;

        public static bool show_fps = false;
        public static bool godmode = false;

        public static bool LightOn = true;

    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Windows.Forms;
using Microsoft.Xna.Framework.Input;
using GAME;

namespace STORAGE
{
    /// <summary>
    /// Все параметры- ReadOnly
    /// </summary>
    public static class Depository
    {
        //screen
        public static GraphicsDeviceManager Graphics;
        public static SpriteBatch SpriteBatch;
        public static Form MainForm;
        public static GraphicsAdapter Adapter;
        public static GraphicsDevice GraphDevice;
        public static Vector2 ScreenResolution;
        public static bool GameFormActive;
        public static GameTime gameTime;

        public static ContentManager CommonContent;
        public static ContentManager GameContent;

        //keyboard
        public static KeyboardState keyboardState;

        public static KeyboardState oldKeyboardState;

        //mouse
        public static MouseState mouseState;        
        public static MouseState oldMouseState;


        public static bool CanUseMouse
        {
            get
            {
                //не давать выделять если пересекает форму редактора
                if (Config.EditorMode)
                {
                    if (GAME.CursorManager.ScreenPosition.X > Depository.EditorFormRect.X & GAME.CursorManager.ScreenPosition.Y < (Depository.EditorFormRect.Y + Depository.EditorFormRect.Height))
                        return false;
                }

                if (GAME.GameManager.GetGameStatus() == GAME.GameManager.gamestatus.map)
                {
                    if (GAME.CursorManager.ScreenPosition.X > Depository.ScreenResolution.X - 503 & GAME.CursorManager.ScreenPosition.Y < 627)
                        return false;
                }

                if (ConsoleWindowVisible)
                {
                    if (GAME.CursorManager.ScreenPosition.X < GAME.GUIManager.Console_W.Bounds.Width & GAME.CursorManager.ScreenPosition.Y < GAME.GUIManager.Console_W.Bounds.Height) 
                        return false;
                }

                foreach (ENGINE.GUI.GameButton b in GAME.GUIManager.buttons)
                {
                    if (b.CanWork)
                    {
                        if (Input.isHover(b.Texture.GetTextureArea(), CursorManager.ScreenPosition)) return false;
                    }
                }

                if (ENGINE.Logic.ObjAttributes.ADialog.DIALOGENABLED) return false;


                #warning заглушка
                // if (GAME.GUIManager._DialogWindow.Visible) return false;

                return true;
            }
        }

        public static bool CanUseKeyboard
        {
            get
            {
                //во время работы консоли не давать перемещаться
                if (ConsoleWindowVisible) return false;

                #warning заглушка
                //if (GAME.GUIManager._DialogWindow.Visible) return false;

                return true;
            }
        }

         //logic
        public static double Elapsed;
        public static Rectangle DrawViewport;
        public static Rectangle ScreenRectangle;

        //Editor
        public static Rectangle EditorFormRect;

        //Game
        public static Game Game;
        public static BasicEffect BasicEffect;

        //nfo
        public static bool ConsoleWindowVisible = false;
    }
}

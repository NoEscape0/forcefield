﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace STORAGE
{
    public static class Input
    {
        static KeyboardState kstate { get { return STORAGE.Depository.keyboardState; } }
        static KeyboardState oldkstate { get { return STORAGE.Depository.oldKeyboardState; } }
        static MouseState mstate { get { return STORAGE.Depository.mouseState; } }
        static MouseState oldmstate { get { return STORAGE.Depository.oldMouseState; } }

        public static bool IsKeyPressed(Keys key)
        {
            return kstate.IsKeyDown(key);
        }

        public static bool IsNewKeyPressed(Keys key)
        {
            return (kstate.IsKeyDown(key) && !oldkstate.IsKeyDown(key));
        }

        public static bool IsKeyReleased(Keys key)
        {
            return (kstate.IsKeyUp(key) && !oldkstate.IsKeyUp(key));
        }

        public static bool IsLeftButtonClick()
        {
            return (mstate.LeftButton == ButtonState.Pressed) && (oldmstate.LeftButton != ButtonState.Pressed);
        }

        public static bool IsRightButtonClick()
        {
            return (mstate.RightButton == ButtonState.Pressed) && (oldmstate.RightButton != ButtonState.Pressed);
        }

        public static bool IsLeftButtonPress()
        {
            return mstate.LeftButton == ButtonState.Pressed;
        }

        public static bool IsLeftButtonRelease()
        {
            return (mstate.LeftButton == ButtonState.Released) && (oldmstate.LeftButton != ButtonState.Released);
        }

        public static bool IsRightButtonPress()
        {
            return mstate.RightButton == ButtonState.Pressed;
        }

        public static bool IsRightButtonRelease()
        {
            return (mstate.RightButton == ButtonState.Released) && (oldmstate.RightButton != ButtonState.Released);

        }

        public static bool isMouseScrollUp()
        {
            return mstate.ScrollWheelValue - oldmstate.ScrollWheelValue > 0;
        }

        public static bool isMouseScrollDown()
        {
            return mstate.ScrollWheelValue - oldmstate.ScrollWheelValue < 0;
        }

        public static bool isHover(Rectangle rect, Vector2 mouse)
        {
            return mouse == Vector2.Clamp(mouse, new Vector2(rect.Left, rect.Top), new Vector2(rect.Right, rect.Bottom));
        }

        public static Vector2 MouseScreenVector()
        {
            return new Vector2(mstate.X, mstate.Y);
        }

        public static Keys[] GetKeys()
        {
            return kstate.GetPressedKeys();
        }
    }
}

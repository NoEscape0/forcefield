﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ENGINE.Graphics;
using STORAGE;
using GAME;
using Microsoft.Xna.Framework.Input;
using System;

namespace ENGINE.GUI
{
    [Serializable]
    public class GameButton
    {
        public static Sound Spress = new Sound(@"Sound\Menu\menu_GameButton");


        public GameButton(string TextureFolder, Vector2 pos, bSpace buttonSpace)
        {
            Texture = new GameTexture(false, TextureFolder);
            ButtonSpace = buttonSpace;
            Texture.Position = pos;
            Spress.playHard = true;
        }
         

        public void Update()
        {
            if (!CanWork) return;

            if (Input.isHover(Texture.GetTextureArea(), CursorManager.ScreenPosition))
                ButtonIsUnderCursor = true;  else ButtonIsUnderCursor = false;

            if (ButtonIsUnderCursor & !wasundercursor)           
                Button.Sfocus.Play();
            

            if (ButtonIsUnderCursor & OnButtonClick != null & CursorManager.LBclick & !PrLbClick)
            {
                OnButtonClick(false);
                Spress.Play();
            }

            if (Input.IsNewKeyPressed(HotKey) & Depository.CanUseKeyboard)
            {
                OnButtonClick(true);
                Spress.Play();
            }

            wasundercursor = ButtonIsUnderCursor;

            PrLbClick = CursorManager.LBclick;
        }
        bool wasundercursor = false;

        public void Draw()
        {
            if (!CanWork) return;

            if (ButtonIsUnderCursor & Mask_Shine)
            {
                shine_mask.Color = Color.Yellow;
                shine_mask.Position = Texture.Position;

                shine_mask.Scale = new Vector2(Texture.TextureResolution.width * Texture.Scale.X * 1.5f / shine_mask.TextureResolution.width,
                   Texture.TextureResolution.height * Texture.Scale.Y * 1.5f / shine_mask.TextureResolution.height);

                shine_mask.Draw();
            }

            Texture.Draw();

            if (ButtonIsUnderCursor & Mask_Up)
            {
                select_mask.Color = Color.Yellow;
                select_mask.Position = Texture.Position;
                select_mask.Scale = new Vector2(Texture.TextureResolution.width * Texture.Scale.X / select_mask.TextureResolution.width,
                   Texture.TextureResolution.height * Texture.Scale.Y / select_mask.TextureResolution.height);
                select_mask.Draw();               
            }
           


            Depository.SpriteBatch.DrawString(GUIManager.MenuFont, Text, new Vector2((float)((Texture.TopLeft.X + TextMarginX)),
               (float)((Texture.TopLeft.Y + TextMarginY))), TextColor, 0.0f, new Vector2(0, 0), TextSize, SpriteEffects.None, 0.0f);
        }


        public event deleg OnButtonClick;

        bool ButtonIsUnderCursor = false;

        public Keys HotKey = Keys.None;

        public string Text = "";
        public Color TextColor = Color.LightGray;

        public int TextMarginX = 30;
        public int TextMarginY = 7;
        public float TextSize = 1.0f;

        public bSpace ButtonSpace; 


        public delegate void deleg(bool IsHotKey);
        public GameTexture Texture;

        public bool Mask_Up = false;
        public bool Mask_Shine = false;

        #region private
        private bool PrLbClick = false;
        #endregion
        public enum bSpace
        {
            all, menu, game, map
        }

        public bool CanWork
        {
            get
            {
                if (Config.EditorMode) return false;

                if (GameManager.GetGameStatus() == GameManager.gamestatus.game & ButtonSpace == bSpace.game) return true;
                if (GameManager.GetGameStatus() == GameManager.gamestatus.map & ButtonSpace == bSpace.map) return true;
                if (GameManager.GetGameStatus() == GameManager.gamestatus.menu & ButtonSpace == bSpace.menu) return true;

                if (ButtonSpace == bSpace.all) return true;

               
                return false;
            }
        }

        public static GameTexture select_mask = new GameTexture(false, @"Textures\Advanced\selected_button_mask");
        public static GameTexture shine_mask = new GameTexture(false, @"Textures\Advanced\shine_button_mask");

    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ENGINE.Graphics;
using STORAGE;
using System;
using Microsoft.Xna.Framework.Audio;
using GAME;

namespace ENGINE.GUI
{
    [Serializable]
    public class Button
    {
        public static Sound Sfocus = new Sound(@"Sound\Menu\menu_focus");
        public static Sound Saccept = new Sound(@"Sound\Menu\menu_accept");


        public Button(GameTexture texture)
        {
            GTexture = texture;
            Sfocus.playHard = true;
            Saccept.playHard = true;
        }

        public void Update(Vector2 globMouseCoord, float camZoom, bool LbClick)
        {
            Rectangle textureArea = GTexture.GetTextureArea();


            if (Input.isHover(textureArea, globMouseCoord) & !wasundercursor & OnCursorUnderButton != null)
            {                
                OnCursorUnderButton(this);


                //cusor under button sound start
                Sfocus.Play();
            }


            if (Input.isHover(textureArea, globMouseCoord) & OnButtonClick != null & LbClick & !PrLbClick)
            {
                OnButtonClick(this);

                //click sound
                Saccept.Play();                
            }
            

            if (Input.isHover(textureArea, globMouseCoord)) wasundercursor = true; else wasundercursor = false;


            PrLbClick = LbClick;
        }
        bool wasundercursor = false;

        public void Draw(SpriteFont font)
        {
            GTexture.Draw();

            Depository.SpriteBatch.DrawString(font, Text, new Vector2((float)((GTexture.TopLeft.X + TextMarginX)),
               (float)((GTexture.TopLeft.Y + TextMarginY))), color, 0.0f, new Vector2(0, 0), TextSize, SpriteEffects.None, 0.0f);
        }


        public event senderOnly OnButtonClick;
        public event senderOnly OnCursorUnderButton;


        public string Text = "";
        public Color color = Color.LightGray;
        public int TextMarginX = 30;
        public int TextMarginY = 7;
        public float TextSize = 0.8f;


        public delegate void senderOnly(object sender);
        public GameTexture GTexture;


        #region private
        private bool PrLbClick = false;
        #endregion
    }
}

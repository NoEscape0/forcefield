﻿using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework.Graphics;
using System;
using Microsoft.Xna.Framework;
using STORAGE;

namespace ENGINE.GUI
{
    [Serializable]
    public class  Background
    {
        private List<Texture2D> Backgrounds = new List<Texture2D>();
        private int bgnow = 0;

        public void LoadBackgrounds()
        {
            string patch = Directory.GetCurrentDirectory() + @"\Content\Textures\Backgrounds";
            string[] bgs = Directory.GetFiles(patch);
            foreach (string s in bgs)
            {
                string a = "";

                a = s.Substring(s.IndexOf(@"Textures\Backgrounds"));

                a = a.Replace(".xnb", "");

                Backgrounds.Add(Depository.CommonContent.Load<Texture2D>(a)); 
            }
        }


        public void ChangeTexture()
        {
            int nextBG;
            Random rand = new Random();
            nextBG = rand.Next(0, Backgrounds.Count);
            bgnow = nextBG;
        }


        public void Draw()
        {
            Depository.SpriteBatch.Draw(Backgrounds[bgnow], new Rectangle(0, 0, (int)Depository.ScreenResolution.X, (int)Depository.ScreenResolution.Y), new Rectangle(0, 0, Backgrounds[bgnow].Width, Backgrounds[bgnow].Height),
                Color.White, 0.0f, new Vector2(0, 0), SpriteEffects.None, 1.0f);
        }
    }
}

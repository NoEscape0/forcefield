﻿using STORAGE;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using RamGecXNAControls;
using RamGecXNAControls.ExtendedControls; 

namespace ENGINE.GUI
{
    public  static class  AquaGUI
    {
        public static MessageBox MessageBox;
        public static void ShowMessage(string message)
        {
            MessageBox.Close();
            MessageBox = new MessageBox(GAME.GUIManager.AquaGUIgameManager, message);
            MessageBox.Show();
        }
        public static void ShowMessage(string message, int x, int y)
        {
            MessageBox.Close();
            MessageBox = new MessageBox(new Point(x, y), GAME.GUIManager.AquaGUIgameManager, message);
            MessageBox.Show();
        }
        public static void ShowMessage(string message, int x, int y, string title)
        {
            MessageBox.Close();
            MessageBox = new MessageBox(new Point(x, y), GAME.GUIManager.AquaGUIgameManager, message, title);
            MessageBox.Show();
        }

        public static bool console_button_was_pressed = false;
        public static string lastcommand = "";
    }
}

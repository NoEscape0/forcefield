﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using STORAGE;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Common;
using ENGINE.Logic;


namespace ENGINE.Graphics
{
    public static class Grid
    {
        public static Matrix halfPixelOffset = Matrix.CreateTranslation(-0.5f, -0.5f, 0);
        public static VertexPositionColor[] lineVertices = new VertexPositionColor[100];
        public static int nLine = 0;
        public static Texture2D dotTexture;
        public static Texture2D DotTexture
        {
            get
            {
                if (dotTexture == null) { dotTexture = new Texture2D(Depository.GraphDevice, 1, 1, false, SurfaceFormat.Color); dotTexture.SetData<Color>(new Color[1] { Color.White }); }
                return dotTexture;
            }
        }

        public static void DrawTextureLine(Vector2 v1, Vector2 v2, Color c, int width, float layer)
        {
            float halfW = width / 2.0f;
            float l = (v2 - v1).Length() / 2;
            float rot = (float)System.Math.Atan2(v2.Y - v1.Y, v2.X - v1.X);
            Depository.SpriteBatch.Draw(DotTexture, new Rectangle((int)v1.X, (int)(v1.Y), (int)(v2 - v1).Length(), width), null, c, rot, new Vector2(0, 0.5f), SpriteEffects.None, layer);
        }
        public static void DrawLine(float x1, float y1, float x2, float y2, Color c, ENGINE.Logic.Camera2d camera)
        {
            BasicEffect effect = STORAGE.Depository.BasicEffect;
            VertexPositionColor[] vertices = new VertexPositionColor[2];
            vertices[0].Position = new Vector3(x1, y1, 0);
            vertices[0].Color = c;
            vertices[1].Position = new Vector3(x2, y2, 0);
            vertices[1].Color = c;
            effect.View = camera.GetTransformation();
            effect.Projection = camera.GetProjection();
            effect.VertexColorEnabled = true;
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                STORAGE.Depository.GraphDevice.DrawUserPrimitives(PrimitiveType.LineList, vertices, 0, 1, VertexPositionColor.VertexDeclaration);
            }
        }

        public static void DrawLine(Vector2 p1, Vector2 p2, Color c, ENGINE.Logic.Camera2d camera)
        {
            DrawLine(p1.X, p1.Y, p2.X, p2.Y, c, camera);
        }

        public static void DrawLine(float x1, float y1, float x2, float y2, Color c, ENGINE.Logic.Camera2d camera, int width)
        {
            float halfW = width / 2.0f;

            Vector2 v1 = new Vector2(x1, y1);
            Vector2 v2 = new Vector2(x2, y2);
            Vector2 normal = new Vector2((v2 - v1).Y, -(v2 - v1).X);
            normal.Normalize();

            BasicEffect effect = STORAGE.Depository.BasicEffect;
            VertexPositionColor[] vertices = new VertexPositionColor[4];
            vertices[0].Position = new Vector3(v1 - normal * halfW, 0);
            vertices[0].Color = c;
            vertices[1].Position = new Vector3(v1 + normal * halfW, 0);
            vertices[1].Color = c;
            vertices[2].Position = new Vector3(v2 - normal * halfW, 0);
            vertices[2].Color = c;
            vertices[3].Position = new Vector3(v2 + normal * halfW, 0);
            vertices[3].Color = c;

            effect.View = camera.GetTransformation();
            effect.Projection = camera.GetProjection();
            effect.VertexColorEnabled = true;
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                STORAGE.Depository.GraphDevice.DrawUserPrimitives(PrimitiveType.TriangleStrip, vertices, 0, 2, VertexPositionColor.VertexDeclaration);
            }
        }

        public static void DrawLine(Vector2 p1, Vector2 p2, Color c, ENGINE.Logic.Camera2d camera, int width)
        {
            DrawLine(p1.X, p1.Y, p2.X, p2.Y, c, camera, width);
        }

        public static void DrawRectangle(float centerX, float centerY, float width, float height, Color c, ENGINE.Logic.Camera2d camera, float angle = 0)
        {
            float halfWidth = width * 0.5f;
            float halfHeight = height * 0.5f;
            Matrix rotate = Matrix.CreateTranslation(new Vector3(-centerX, -centerY, 0)) *
                            Matrix.CreateRotationZ(angle) *
                            Matrix.CreateTranslation(new Vector3(centerX, centerY, 0));

            BasicEffect effect = STORAGE.Depository.BasicEffect;
            VertexPositionColor[] vertices = new VertexPositionColor[5];

            vertices[0].Position = Vector3.Transform(new Vector3(centerX - halfWidth, centerY - halfHeight, 0.0f), rotate);
            vertices[0].Color = c;
            vertices[1].Position = Vector3.Transform(new Vector3(centerX - halfWidth, centerY + halfHeight, 0.0f), rotate);
            vertices[1].Color = c;
            vertices[2].Position = Vector3.Transform(new Vector3(centerX + halfWidth, centerY + halfHeight, 0.0f), rotate);
            vertices[2].Color = c;
            vertices[3].Position = Vector3.Transform(new Vector3(centerX + halfWidth, centerY - halfHeight, 0f), rotate);
            vertices[3].Color = c;

            vertices[4].Position = vertices[0].Position;
            vertices[4].Color = c;


            effect.View = camera.GetTransformation();
            effect.Projection = halfPixelOffset * camera.GetProjection();
            effect.VertexColorEnabled = true;
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                STORAGE.Depository.GraphDevice.DrawUserPrimitives(PrimitiveType.LineStrip, vertices, 0, 4, VertexPositionColorTexture.VertexDeclaration);
            }
        }

        public static void DrawRectangle(Rectangle rect, Color c, ENGINE.Logic.Camera2d camera)
        {
            DrawRectangle(rect.X + rect.Width / 2, rect.Y + rect.Height / 2, rect.Width, rect.Height, c, camera, 0);
        }

        public static void DrawRectangle(Vector2 center, float width, float height, Color c, ENGINE.Logic.Camera2d camera, float angle = 0)
        {
            DrawRectangle(center.X, center.Y, width, height, c, camera, angle);
        }

        public static void DrawFillRectangle(float x, float y, float width, float height, Color c, ENGINE.Logic.Camera2d camera)
        {
            DrawLine(x, y + height / 2, x + width, y + height / 2, c, camera, (int)height);
        }

        public static void DrawFillRectangle(Rectangle rect, Color c, ENGINE.Logic.Camera2d camera)
        {
            DrawFillRectangle(rect.X, rect.Y, rect.Width, rect.Height, c, camera);
        }

        public static void DrawCircle(float x, float y, int r, Color c, ENGINE.Logic.Camera2d camera, int n = 24)
        {
            BasicEffect effect = STORAGE.Depository.BasicEffect;
            VertexPositionColor[] vertices = new VertexPositionColor[n + 1];
            for (int i = 0; i < n + 1; i++)
            {
                Vector3 v = new Vector3(x + r * (float)System.Math.Cos(i * 2 * MathHelper.Pi / n), (y + r * (float)System.Math.Sin(i * 2 * MathHelper.Pi / n)), 0);
                vertices[i].Position = v;
                vertices[i].Color = c;
            }
            effect.View = camera.GetTransformation();
            effect.Projection = camera.GetProjection();
            effect.VertexColorEnabled = true;

            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                STORAGE.Depository.GraphDevice.DrawUserPrimitives(PrimitiveType.LineStrip, vertices, 0, n, VertexPositionColor.VertexDeclaration);
            }
        }

        public static void DrawCircle(Vector2 center, int r, Color c, ENGINE.Logic.Camera2d camera, int n = 24)
        {
            DrawCircle(center.X, center.Y, r, c, camera, n);
        }

        public static void DrawArc(float x, float y, int r, float angle1, float angle2, Color c, ENGINE.Logic.Camera2d camera, int n = 24)
        {
            BasicEffect effect = STORAGE.Depository.BasicEffect;
            VertexPositionColor[] vertices = new VertexPositionColor[n + 1];
            for (int i = 0; i < n + 1; i++)
            {
                Vector3 v = new Vector3(x + r * (float)System.Math.Cos(i * (angle2 - angle1) / n), (y + r * (float)System.Math.Sin(i * (angle2 - angle1) / n)), 0);
                v = Vector3.Transform(v, Matrix.CreateTranslation(new Vector3(-x, -y, 0)) * Matrix.CreateRotationZ(angle1) * Matrix.CreateTranslation(new Vector3(x, y, 0)));
                vertices[i].Position = v;
                vertices[i].Color = c;
            }
            effect.View = camera.GetTransformation();
            effect.Projection = camera.GetProjection();
            effect.VertexColorEnabled = true;

            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                STORAGE.Depository.GraphDevice.DrawUserPrimitives(PrimitiveType.LineStrip, vertices, 0, n, VertexPositionColor.VertexDeclaration);
            }
        }

        public static void DrawArc(Vector2 center, int r, float angle1, float angle2, Color c, ENGINE.Logic.Camera2d camera, int n = 24)
        {
            DrawArc(center.X, center.Y, r, angle1, angle2, c, camera, n);
        }


        public static void BatchLine(float x1, float y1, float x2, float y2, Color c)
        {
            if (lineVertices.Length < nLine + 2) { ResizeLineVertices(); }
            lineVertices[nLine].Position = new Vector3(x1, y1, 0);
            lineVertices[nLine].Color = c;
            nLine++;
            lineVertices[nLine].Position = new Vector3(x2, y2, 0);
            lineVertices[nLine].Color = c;
            nLine++;
        }

        public static void BatchLine(Vector2 p1, Vector2 p2, Color c)
        {
            BatchLine(p1.X, p1.Y, p2.X, p2.Y, c);
        }

        public static void BatchRectangle(Rectangle rect, Color c)
        {
            BatchLine(rect.Left, rect.Top, rect.Left, rect.Bottom, c);
            BatchLine(rect.Left, rect.Bottom, rect.Right, rect.Bottom, c);
            BatchLine(rect.Right, rect.Bottom, rect.Right, rect.Top, c);
            BatchLine(rect.Right, rect.Top, rect.Left, rect.Top, c);
        }

        public static void BatchCircle(float x, float y, int r, Color c, int n = 24)
        {
            for (int i = 0; i < n + 1; i++)
            {
                Vector3 v1 = new Vector3(x + r * (float)System.Math.Cos(i * 2 * MathHelper.Pi / n), (y + r * (float)System.Math.Sin(i * 2 * MathHelper.Pi / n)), 0);
                Vector3 v2 = new Vector3(x + r * (float)System.Math.Cos((i + 1) * 2 * MathHelper.Pi / n), (y + r * (float)System.Math.Sin((i + 1) * 2 * MathHelper.Pi / n)), 0);
                Grid.BatchLine(new Vector2(v1.X, v1.Y), new Vector2(v2.X, v2.Y), c);
            }

        }

        public static void BatchCircle(Vector2 center, int r, Color c, int n = 24)
        {
            BatchCircle(center.X, center.Y, r, c, n);
        }

        public static void DrawBatch(ENGINE.Logic.Camera2d camera)
        {
            if (nLine == 0) return;
            BasicEffect effect = STORAGE.Depository.BasicEffect;
            effect.View = camera.GetTransformation();
            effect.Projection = halfPixelOffset * camera.GetProjection();
            effect.VertexColorEnabled = true;
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                STORAGE.Depository.GraphDevice.DrawUserPrimitives(PrimitiveType.LineList, lineVertices, 0, nLine >> 1, VertexPositionColor.VertexDeclaration);
            }
            nLine = 0;
        }

        public static void BatchShape(Shape shape, Transform t, Color c)
        {
            Matrix m = Matrix.CreateRotationZ(t.Angle);
            m *= Matrix.CreateTranslation(new Vector3(ConvertUnits.ToDisplayUnits(t.Position), 0));

            switch (shape.ShapeType)
            {
                case FarseerPhysics.Collision.Shapes.ShapeType.Circle:
                    Vector2 pos = ConvertUnits.ToDisplayUnits((shape as FarseerPhysics.Collision.Shapes.CircleShape).Position + t.Position);
                    float radius = ConvertUnits.ToDisplayUnits((shape as FarseerPhysics.Collision.Shapes.CircleShape).Radius);
                    Grid.BatchCircle(pos, (int)radius, c);
                    break;
                case FarseerPhysics.Collision.Shapes.ShapeType.Edge:
                    Vector2 v1 = ConvertUnits.ToDisplayUnits((shape as FarseerPhysics.Collision.Shapes.EdgeShape).Vertex1);
                    Vector2 v2 = ConvertUnits.ToDisplayUnits((shape as FarseerPhysics.Collision.Shapes.EdgeShape).Vertex2);
                    Grid.BatchLine(v1, v2, c);
                    break;
                case FarseerPhysics.Collision.Shapes.ShapeType.Polygon:
                    FarseerPhysics.Common.Vertices vert = (shape as FarseerPhysics.Collision.Shapes.PolygonShape).Vertices;
                    for (int i = 0; i < vert.Count; i++)
                    {
                        v1 = Vector2.Transform(ConvertUnits.ToDisplayUnits(vert[i]), m);
                        v2 = Vector2.Transform(ConvertUnits.ToDisplayUnits(vert[(i + 1) % vert.Count]), m);
                        Grid.BatchLine(v1, v2, c);
                    }
                    break;
                case FarseerPhysics.Collision.Shapes.ShapeType.Loop:
                case FarseerPhysics.Collision.Shapes.ShapeType.TypeCount:
                case FarseerPhysics.Collision.Shapes.ShapeType.Unknown:
                // пока не знаю как их рисовать :P
                default:
                    break;
            }
        }

        private static void ResizeLineVertices()
        {
            VertexPositionColor[] tmp = new VertexPositionColor[lineVertices.Length * 2];
            for (int i = 0; i < lineVertices.Length; i++)
            {
                tmp[i] = lineVertices[i];
            }
            lineVertices = tmp;
        }
    }
}



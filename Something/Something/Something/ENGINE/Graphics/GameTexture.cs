﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using STORAGE;

namespace ENGINE.Graphics
{
    [Serializable]
    public class GameTexture
    {
        public GameTexture(bool gameContent, string folder)
        {
            GameContent = gameContent;
            Folder = folder;
            Position = new Vector2(0, 0);
            Scale = new Vector2(1.0f, 1.0f);
            Effects = SpriteEffects.None;
            Layer = 0.01f;
            Color = Color.White;
            RestoreTexture();
            PartOfSprite = new Rectangle(0, 0, TextureResolution.width, TextureResolution.height);
        }
        public GameTexture(bool gameContent, string folder, Vector2 pos)
        {
            GameContent = gameContent;
            Folder = folder;
            Position = pos;
            Scale = new Vector2(1.0f, 1.0f);
            Effects = SpriteEffects.None;
            Layer = 0.01f;
            Color = Color.White;

            RestoreTexture();
            PartOfSprite = new Rectangle(0, 0, TextureResolution.width, TextureResolution.height);
        }
        public GameTexture(bool gameContent, string folder, Vector2 pos, Rectangle partOfsprite, Vector2 scale, SpriteEffects effects, float layer, Color color)
        {
            GameContent = gameContent;
            Folder = folder;
            Position = pos;

            Scale = scale;
            Effects = effects;
            Layer = layer;
            Color = color;
            RestoreTexture();
            PartOfSprite = partOfsprite;
        }

        public void Draw()
        {
            Depository.SpriteBatch.Draw(Texture2d, Position, PartOfSprite, Color, 0, Origin, Scale, Effects, Layer);
        }

        public void Draw(Vector2 Pos, float layer = -999)
        {
            Depository.SpriteBatch.Draw(Texture2d, Pos, PartOfSprite, Color, 0, Origin, Scale, Effects, layer < 0 ? Layer : layer);
        }

        public void DrawScale(Vector2 Pos, float newScale)
        {
            Depository.SpriteBatch.Draw(Texture2d, Pos, PartOfSprite, Color, 0, Origin, newScale*1, Effects, 0);
        }

        public void Draw(Rectangle partoftexture)
        {
            Vector2 origin = new Vector2(partoftexture.Width / 2, partoftexture.Height / 2);
            Depository.SpriteBatch.Draw(Texture2d, Position, partoftexture, Color, 0, origin, Scale, Effects, Layer);
        }

        public void Draw(Rectangle partoftexture, Vector2 newPosition)
        {
            Vector2 origin = new Vector2(partoftexture.Width / 2, partoftexture.Height / 2);
            Depository.SpriteBatch.Draw(Texture2d, newPosition, partoftexture, Color, 0, origin, Scale, Effects, Layer);
        }

        public void Draw(Rectangle partoftexture, Texture2D texture)
        {
            Vector2 origin = new Vector2(partoftexture.Width / 2, partoftexture.Height / 2);
            Depository.SpriteBatch.Draw(texture, Position, partoftexture, Color, 0, origin, Scale, Effects, Layer);
        }

        public void DrawToDestination(Rectangle Destination)
        {
            Depository.SpriteBatch.Draw(Texture2d, Destination, PartOfSprite, Color, 0, Vector2.Zero, Effects, Layer);
        }

        public static Rectangle testRect = new Rectangle();
        public Rectangle GetTextureArea()
        {
            Rectangle rec = testRect;//new Rectangle();
            rec.X = (int)(Position.X - Origin.X * Scale.X);
            rec.Y = (int)(Position.Y - Origin.Y * Scale.Y);

            rec.Width = (int)(PartOfSprite.Width * Scale.X);
            rec.Height = (int)(PartOfSprite.Height * Scale.Y);
            return rec;
        }

        public void Draw(Vector2 ScaleV, Vector2 origin)
        {
            Depository.SpriteBatch.Draw(Texture2d, Position, PartOfSprite, Color, 0, origin, ScaleV, Effects, Layer);
        }

        public void Draw(float rotation)
        {
            Depository.SpriteBatch.Draw(Texture2d, Position, PartOfSprite, Color, rotation, Origin, Scale, Effects, Layer);
        }
        public Texture2D Texture2d
        {
            get
            {
                if (texture2d != null && !texture2d.IsDisposed) return texture2d;
                else
                {
                    RestoreTexture();
                    return texture2d;
                }
            }
            set
            {
                texture2d = value;
                TextureResolution = new textureRes(texture2d.Width, texture2d.Height);
                PartOfSprite = new Rectangle(0, 0, TextureResolution.width, TextureResolution.height);
            }
        }
        [NonSerialized]
        private Texture2D texture2d;

        public void RestoreTexture()
        {
            try
            {

                if (GameContent) texture2d = Depository.GameContent.Load<Texture2D>(Folder);
                else texture2d = Depository.CommonContent.Load<Texture2D>(Folder);
                TextureResolution = new textureRes(texture2d.Width, texture2d.Height);
                PartOfSprite = new Rectangle(0, 0, TextureResolution.width, TextureResolution.height);
            }
            catch (Microsoft.Xna.Framework.Content.ContentLoadException)
            {
                texture2d = Depository.CommonContent.Load<Texture2D>(@"Textures\System\null");
            }
        }

        public void DisposeTexture()
        {
            if (texture2d != null) texture2d.Dispose();
        }

        public Vector2 Position;
        public Vector2 Scale;
        public SpriteEffects Effects;
        public float Layer;
        public Color Color;
        public Rectangle PartOfSprite;
        public string Folder;
        public bool GameContent;

        public Vector2 Origin { get { return TextureResolution.origin; } }
        //public Vector2 Center { get { return Position + Origin; } set { Position = value - Origin; } }
        public Vector2 TopLeft { get { return Position - Origin; } set { Position = value + Origin; } }
        public Rectangle Rectangle { get { return TextureResolution.textureRectangle; } }

        public textureRes TextureResolution
        {
            get
            {
                if (textureResolution != null)
                {
                    return textureResolution;
                }
                else
                {
                    textureResolution = new textureRes(Texture2d.Width, Texture2d.Height);
                    return textureResolution;
                }
            }
            set { textureResolution = value; }
        }
        private textureRes textureResolution;

        [Serializable]
        public class textureRes
        {
            public Vector2 origin;
            public Rectangle textureRectangle;
            public textureRes(int w, int h)
            {
                width = w;
                height = h;
                origin = new Vector2(width * 0.5f, height * 0.5f);
                textureRectangle = new Rectangle(0, 0, width, height);
            }

            public int width;
            public int height;
        }
    }
}

﻿using System;
using Microsoft.Xna.Framework;
using System.Xml;
using System.Reflection;

using STORAGE;

namespace ENGINE.Logic.Particles
{
    [Serializable]
    public class BaseEmitter : GameObject
    {

        public Vector2 Pos { get { return Gtexture.Position; } set { Gtexture.Position = value; } }
        public Vector2 prevPos;

        // настройки
        public float Pps { get { return 1f / (float)pTime; } set { pTime = 1 / value; } } // particles per second
        public float minSpeed = 100.0f;
        public float maxSpeed = 100.0f;
        public float posVar = 0.0f;
        public float FullAlpha { get { return 255f / alphaVel; } set { alphaVel = 255 / value; } } // время до полной прозрачности
        public float alphaVel = 30;
        public float minSize = 2.0f;
        public float maxSize = 5.0f;
        public float sizeVel = 0.1f;
        public float ttl = 50;
        public Color color;
        public Color Color { get { return color; } set { color = value; } }
        public bool alphablend = false;
        public bool emitBothWay = false;
        public float angleMain = 0;
        public float angleSecondary = 0;
        public float width = 500;
        public float height = 1;
        public float startAngle = 0;
        public float angleVelocity = 1;
        public float tMaxAlpha = 0;
        public bool enable;
        public bool isBackLayer = true;
        public bool isNormalDistribution = false;
        public bool isInteractive = false;
        public bool isHeavyParticles = false;
        public float Mass = 0; // инвертированная масса частиц
        public bool isSolid;
        private float energyLoss = 0;
        public float EnergyLoss { get { return energyLoss; } set { energyLoss = value; } }
        public float omega;

        protected double pTime = 1 / 10f; // 1/pps
        private double toMachTime = 0;
        private double totalElapsed = 0;

        public Vector2 emiterVelosity = Vector2.Zero;
        public double emitterTtl = 0;
        public bool isTemporalEmitter = false;

        protected static Random rnd = new Random(111222);
        protected delegate void GenerateParticle();
        protected GenerateParticle generate;
        protected Vector2 generatedDir;
        protected Vector2 generatedPos;

        public ParticleEngine engine;

        public BaseEmitter(string folder, ParticleEngine particleEngine = null)
            : base(folder)
        {
            OnUpdate += UpdateEmitter;
            OnDraw += DrawEmitter;
            enable = true;            
            engine = particleEngine;
            if (engine==null) engine = GAME.GameManager.LvlController.particleEngine;
        }

        public void UpdateEmitter(GameObject sender)
        {

            if (!enable) return;// расходимся, тут нечего излучать.
            double elapsed = Depository.Elapsed;
            if (isTemporalEmitter)
            {
                emitterTtl -= elapsed;
                Gtexture.Position += (float)elapsed * emiterVelosity;
                if (emitterTtl < 0) GAME.GameManager.LvlController.ObjectArray.Remove(this);
            }
            if (engine == null) engine = GAME.GameManager.LvlController.particleEngine; // 13.08.2013 для совместимости с сохранениями, потом убрать
            double curTime = toMachTime;

            toMachTime += elapsed;
            totalElapsed += elapsed;
            double invElapsed = 0;
            if (toMachTime > pTime) invElapsed = 1 / totalElapsed;
            while (toMachTime > pTime)
            {
                Vector2 curPos = Vector2.Lerp(Pos, prevPos, (float)(toMachTime * invElapsed));

                generate();

                float pSize = RandomMinMax(minSize, maxSize);
                float randomAngleVelocity = RandomMinMax(angleVelocity * 0.9f, angleVelocity * 1.1f);
                engine.Add(Gtexture, generatedPos + curPos, generatedDir,
                           startAngle,
                           randomAngleVelocity,
                           new Vector4(color.R, color.G, color.B, color.A),
                           alphaVel,
                           pSize,
                           sizeVel,
                           ttl,
                           alphablend,
                           tMaxAlpha,
                           isBackLayer,
                           isInteractive, Mass, isSolid, EnergyLoss, omega);

                toMachTime -= pTime;
                totalElapsed = 0;
            }
            prevPos = Pos;
        }

        public virtual void DrawEmitter(GameObject sender)
        {
        }

        protected float RandomMinMax(float min, float max)
        {
            return (float)rnd.NextDouble() * (max - min) + min;
        }

        protected float Normal(float min, float max)
        {
            float sigma = (max - min) * 0.16667f;//   /6
            float tmp = 0;
            for (int i = 0; i < 12; i++)
            {
                tmp += (float)rnd.NextDouble();
            }
            tmp -= 6;
            tmp *= sigma;
            return tmp;
        }        
    }
}

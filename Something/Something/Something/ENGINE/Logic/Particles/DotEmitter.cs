﻿using System;
using Microsoft.Xna.Framework;

namespace ENGINE.Logic.Particles
{
    [Serializable]
    public class DotEmitter:BaseEmitter
    {
        public DotEmitter(ParticleEngine engine, string folder):base(folder)
        {
            Color = Color.White;
            generate = GenerateDot;
        }
       
        public void GenerateDot()
        {
            float angle = (float)rnd.NextDouble() * MathHelper.TwoPi;
            float x = (float)Math.Cos(angle);
            float y = (float)Math.Sin(angle);
            generatedDir= new Vector2(x, y);
            float pSpeed = RandomMinMax(minSpeed, maxSpeed);            
            //Vector2 randomDir = new Vector2((float)rnd.NextDouble() * 1.0f - 0.5f, (float)rnd.NextDouble() * 1.0f - 0.5f);
            generatedPos = new Vector2((float)rnd.NextDouble() * 1.0f - 0.5f, (float)rnd.NextDouble() * 1.0f - 0.5f);
            generatedPos *= posVar;
            generatedDir *= pSpeed;
        }       
    }
}

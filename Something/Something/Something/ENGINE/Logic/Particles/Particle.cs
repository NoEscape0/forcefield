﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using STORAGE;
using ENGINE.Graphics;

namespace ENGINE.Logic.Particles
{
    [Serializable]
    public class Particle
    {
        public GameTexture texture;
        public Vector2 pos;
        public Vector2 velocity;
        public float angle;
        public float angleVelocity;
        public Vector4 color;
        public float alphaVelocity;
        public float size;
        public float sizeVelocity;
        float ttl;
        public bool isBlendStateAlpha;
        public float tMaxAlpha;
        public bool isBacklayer;
        public bool isInteractive;
        public bool isSolid = true;
        public float Mass;
        float currentTime;
        Vector4 currentColor;
        private float omega;
        private static float i255 = 1f / 255;
        private float energyLoss = 0;
        public float EnergyLoss { get { return energyLoss; } set { energyLoss = value * 0.01f; } }

        public bool Active;

        public bool toRemove;

        public Rectangle pRectangle { get { return new Rectangle((int)pos.X, (int)pos.Y, texture.TextureResolution.width, texture.TextureResolution.height); } }

        public Particle()
        {
            Active = false;
            toRemove = false;
            this.isInteractive = false;
        }

        public void Update(float elapsed)
        {
            //float elapsed = (float)Depository.Elapsed;

            if (currentTime < tMaxAlpha)
            {
                currentColor.W = MathHelper.Lerp(0.01f, color.W, currentTime / tMaxAlpha);
                currentTime += elapsed;
            }
            else
                currentColor.W -= elapsed * alphaVelocity;
            if (omega != 0)
            {
                Vector2 normal = new Vector2(velocity.Y, -velocity.X);
                normal.Normalize();
                normal *= omega;
                velocity += normal;
                omega -= Math.Sign(omega) * 1 * elapsed;
                if (Math.Abs(omega) < 1.0f) omega = 0;
            }
            Vector2 gravity = GAME.GameManager.LvlController.Gravity;

            velocity += gravity * Mass * 0.01f; // 0.01 просто для удобства задания в параметрах
            ttl -= elapsed;
            pos += elapsed * velocity;
            angle += elapsed * angleVelocity;
            size = MathHelper.Clamp(size, size - elapsed * sizeVelocity, 0);
            if (ttl < 0 || size <= 0 || currentColor.W < 0) { toRemove = true; Active = false; }

            #region Взаимодействие с барьерами
            if (isSolid)
            {
                FarseerPhysics.Dynamics.Fixture f = GAME.GameManager.LvlController.PhysicWorld.TestPoint(ConvertUnits.ToSimUnits(this.pos));
                if (f != null && !f.IsSensor)
                    if ((f.Body.UserData as GameObject) != null)
                    {
                        pos -= 1 * velocity * elapsed;
                        GameObject go = f.Body.UserData as GameObject;
                        Rectangle rect = go.Gtexture.GetTextureArea();
                        var barrier = go.data.ReadData("ABarrier") as ObjAttributes.ABarrier;
                        Vector2 center = Vector2.Zero;
                        if (barrier != null)
                        {
                            center = barrier.realCenter;
                            rect.X += (int)barrier.MarginLeft;
                            rect.Y += (int)barrier.MarginTop;
                            rect.Width -= ((int)barrier.MarginRight + (int)barrier.MarginLeft);
                            rect.Height -= ((int)barrier.MarginDown + (int)barrier.MarginTop);
                        }
                        Matrix m = Matrix.CreateTranslation(new Vector3(-(go.Gtexture.Position + center), 0)) * Matrix.CreateRotationZ(go.rotation) * Matrix.CreateTranslation(new Vector3(go.Gtexture.Position + center, 0));
                        System.Collections.Generic.List<Vector2> corners = new System.Collections.Generic.List<Vector2>();
                        corners.Add(Vector2.Transform(new Vector2(rect.Left, rect.Top), m));
                        corners.Add(Vector2.Transform(new Vector2(rect.Right, rect.Top), m));
                        corners.Add(Vector2.Transform(new Vector2(rect.Right, rect.Bottom), m));
                        corners.Add(Vector2.Transform(new Vector2(rect.Left, rect.Bottom), m));
                        float minsin = float.MaxValue;
                        Vector2 normal = Vector2.UnitX;
                        for (int i = 0; i < corners.Count; i++)
                        {
                            Vector2 c1 = corners[i];
                            Vector2 c2 = corners[(i + 1) % corners.Count];
                            float currensin = sinV(this.pos - c1, c2 - c1);
                            if (currensin < 0) continue;
                            if (minsin > Math.Abs(currensin))
                            {
                                normal.X = c2.Y - c1.Y;
                                normal.Y = c1.X - c2.X;
                                minsin = Math.Abs(currensin);
                            }
                        }
                        normal.Normalize();
                        velocity = Vector2.Reflect(velocity, normal);
                        velocity *= (1 - EnergyLoss);
                        if (velocity.LengthSquared() < 20.0f) { toRemove = true; Active = false; }
                    }
            }
            #endregion

            #region влияние игрока на частицы
            if (Active && isInteractive && GAME.EnviromentInfo.HERO.Hero != null)
            {
                if (GAME.EnviromentInfo.HERO.Hero.body == null) return;

                GameObject hero = GAME.EnviromentInfo.HERO.Hero;
                Vector2 speed = ConvertUnits.ToDisplayUnits(hero.body.LinearVelocity);

                Rectangle rect = new Rectangle((int)(pos.X - texture.Origin.X),
                                               (int)(pos.Y - texture.Origin.Y),
                                               texture.TextureResolution.width,
                                               texture.TextureResolution.height);
                Rectangle heroRect = hero.Gtexture.GetTextureArea();

                if (rect.Intersects(heroRect))
                {
                    this.velocity += 0.01f * speed;// +new Vector2((float)rnd.NextDouble() * 4 - 2, 0);
                    omega = 0.005f * speed.X;// +((float)rnd.NextDouble() * 0.02f - 0.01f);
                    angleVelocity = 0.5f * omega;
                }
            }
            #endregion
        }


        float sinV(Vector2 v1, Vector2 v2)
        {
            float result = v1.X * v2.Y - v2.X * v1.Y;
            return result;
        }

        public void Draw()
        {
            Depository.SpriteBatch.Draw(texture.Texture2d, pos, null, new Color(currentColor), angle, texture.Origin, size, SpriteEffects.None, isBacklayer ? 1 : 0);
        }

        /// <summary>
        /// Установки частицы
        /// </summary>
        /// <param name="texture">Текстура</param>
        /// <param name="pos">Положение</param>
        /// <param name="speed">Скорость  точк/сек</param>
        /// <param name="angle">угол</param>
        /// <param name="angleVelocity">уголовая скорость рад/сек</param>
        /// <param name="color">цвет</param>
        /// <param name="alphaVelocity">скорость гашения цвета ед в сек.</param>
        /// <param name="size">размер</param>
        /// <param name="sizeVelocity">скорость уменьшения</param>
        /// <param name="ttl">время жизни в сек.</param>
        /// <param name="alphablend">альфабленд</param>
        /// <param name="layer">true = передний план</param>
        /// <param name="isInteractive">true = игрок воздействует на эту частицу</param>
        /// <param name="Mass">условная масса частицы</param>
        /// <param name="isSolid">true - ударяется о барьеры</param>
        /// <param name="energyLoss">потеря скорости при ударе</param>
        /// <param name="omega">Начальная угловая скорость</param>
        public void SetParam(GameTexture texture, Vector2 pos, Vector2 speed,
                        float angle, float angleVelocity,
                        Vector4 color, float alphaVelocity,
                        float size, float sizeVelocity,
                        float ttl, bool alphablend, float tMaxAlpha, bool layer, bool isInteractive, float Mass, bool isSolid, float energyLoss, float omega)
        {
            this.texture = texture;

            this.pos = pos;
            this.velocity = speed;

            this.angle = angle;
            this.angleVelocity = angleVelocity;
            this.color = color;
            this.color.W *= i255;
            this.color.X *= i255;
            this.color.Y *= i255;
            this.color.Z *= i255;
            this.alphaVelocity = alphaVelocity * i255;
            this.size = size;
            this.sizeVelocity = sizeVelocity;
            this.ttl = ttl;
            this.toRemove = false;
            this.Active = true;
            this.isBlendStateAlpha = alphablend;
            this.currentColor = this.color;
            this.tMaxAlpha = tMaxAlpha;
            this.isBacklayer = layer;
            if (tMaxAlpha != 0)
            {
                this.currentColor.W = 0;
            }
            currentTime = 0;
            this.omega = omega;
            this.isInteractive = isInteractive;
            this.Mass = Mass;
            this.isSolid = isSolid;
            this.EnergyLoss = energyLoss;
        }

        public override string ToString()
        {
            return Active.ToString() + " " + pos.ToString();
        }
    }
}

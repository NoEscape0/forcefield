﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using STORAGE;
using ENGINE.Graphics;

namespace ENGINE.Logic.Particles
{
    [Serializable]
    public class ParticleEngine
    {
        [NonSerialized]
        List<Particle> MainList_alphablend;

        static int MAX_PARTICLES_ONSCREEN = 10000;
        static Dictionary<string, GameTexture> explosionSprites;

        public int Count { get { return MainList.Length; } }
        int poolSize = 0;
        Particle[] MainList;
        Queue<Particle> freeParticles;
        [NonSerialized]
        bool init = false;
        //
        string name = "default";

        static Dictionary<string, GameTexture> ExplosionSprites
        {
            get
            {
                if (explosionSprites != null) return explosionSprites;
                else
                {
                    explosionSprites = new Dictionary<string, GameTexture>();
                    explosionSprites.Add("smoke1", new GameTexture(false, "Textures\\Particles\\Cloud002"));
                    explosionSprites.Add("smoke2", new GameTexture(false, "Textures\\Particles\\Cloud001"));
                    explosionSprites.Add("smoke3", new GameTexture(false, "Textures\\Particles\\Particle002"));
                    explosionSprites.Add("explos1", new GameTexture(false, "Textures\\Particles\\Particle001"));
                    explosionSprites.Add("smoke4", new GameTexture(false, "Textures\\Particles\\Particle006"));
                    explosionSprites.Add("flash", new GameTexture(false, "Textures\\Particles\\Spikey001"));
                    explosionSprites.Add("dot", new GameTexture(false, "Textures\\Particles\\p4"));
                    explosionSprites.Add("blood1", new GameTexture(false, "Textures\\Particles\\blood1"));
                    explosionSprites.Add("blood2", new GameTexture(false, "Textures\\Particles\\blood2"));



                    return explosionSprites;
                }
            }
        }

        public ParticleEngine(string name = "default")
        {
            poolSize = 1000;
            MainList = new Particle[poolSize];
            freeParticles = new Queue<Particle>(poolSize);
            for (int i = 0; i < poolSize; i++)
            {
                MainList[i] = new Particle();
                freeParticles.Enqueue(MainList[i]);
            }
            MainList_alphablend = new List<Particle>();
            init = true;
            this.name = name;
        }

        public void Draw(Matrix View, bool isBackLayer = true)
        {
            if (!init) Init();

            bool onScreen = false;

            //  if (Config.EditorMode & Show_in_editor || !Config.EditorMode & Show_in_game)
            {
                Depository.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Additive, null, null, null, null, View);
                for (int i = 0; i < MainList.Length; i++)
                {
                    if (MainList[i].Active && MainList[i].isBacklayer == isBackLayer)
                    {
                        Vector2 transform = Vector2.Transform(MainList[i].pos, View);
                        onScreen = Depository.DrawViewport.Intersects(MainList[i].pRectangle) || Depository.ScreenRectangle.Contains((int)transform.X, (int)transform.Y);
                        if (!onScreen) continue;
                        if (MainList[i].isBlendStateAlpha) MainList_alphablend.Add(MainList[i]);
                        else
                            MainList[i].Draw();
                    }
                }

                Depository.SpriteBatch.End();

                Depository.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, View);
                foreach (Particle p in MainList_alphablend)
                {
                    if (p.Active && p.isBacklayer == isBackLayer && p.isBlendStateAlpha )
                    {
                        Vector2 transform = Vector2.Transform(p.pos, View);
                        onScreen = Depository.DrawViewport.Intersects(p.pRectangle) || Depository.ScreenRectangle.Contains((int)transform.X, (int)transform.Y);
                        if (!onScreen) continue;
                        p.Draw();
                    }
                }
                Depository.SpriteBatch.End();
                MainList_alphablend.Clear();
            }
        }

        public void Add(GameTexture texture, Vector2 pos, Vector2 speed,
                        float angle, float angleVelocity,
                        Vector4 color, float alphaVelocity,
                        float size, float sizeVelocity,
                        float ttl, bool alphablend, float tMaxAlpha, bool backlayer = true, bool isInteractive = false, float Mass = 0, bool isSolid = false, float energyLoss = 0, float omega = 0)
        {
            Particle pollen = null;
            Rectangle largescreen = Depository.DrawViewport;
            largescreen.Inflate(3 * (int)Depository.ScreenResolution.X, 2 * (int)Depository.ScreenResolution.Y);
            if (!largescreen.Contains(new Point((int)pos.X, (int)pos.Y)) && MainList.Length > MAX_PARTICLES_ONSCREEN)
            { return; }

            if (freeParticles.Count == 0) EnlargePool();
            pollen = freeParticles.Dequeue();
            pollen.SetParam(texture, pos, speed, angle, angleVelocity, color, alphaVelocity, size, sizeVelocity, ttl, alphablend, tMaxAlpha, backlayer, isInteractive, Mass, isSolid, energyLoss, omega);

        }

        public void Update()
        {
            for (int i = 0; i < MainList.Length; i++)
            {
                if (MainList[i].Active)
                {
                    MainList[i].Update((float)Depository.Elapsed);
                    if (!MainList[i].Active) { freeParticles.Enqueue(MainList[i]); }
                }
            }
        }

        void EnlargePool()
        {
            poolSize += 500;
            Particle[] newArray = new Particle[poolSize];
            MainList.CopyTo(newArray, 0);
            for (int i = poolSize - 500; i < newArray.Length; i++)
            {
                newArray[i] = new Particle();
                freeParticles.Enqueue(newArray[i]);
            }
            MainList = newArray;
        }

        /// <summary>
        /// Взрыв в точке
        /// </summary>
        /// <param name="position">координаты взрыва</param>
        /// <param name="numParticles">количество частиц в взрыве</param>
        /// <param name="c">цвет взрыва</param>
        public static void CreateExplosion(Vector2 position, int numParticles, Color c)
        {
            ParticleEngine engine = GAME.GameManager.LvlController.ParticleEngine;
            Random random = GAME.GameManager.LvlController.Rnd;
            float smoke1num = numParticles * 0.1f;
            float smoke2num = numParticles * 0.3f;
            float explos1num = numParticles * 0.5f;

            float intensity = numParticles * 0.5f;

            float alphaVelocity = 255 / 0.5f;
            float appearTime = 0.5f;
            float min = intensity * 1.5f;
            float max = intensity * 3.5f;
            Color color = Color.Black;

            for (int i = 0; i < smoke1num; i++)
            {
                float angle = (float)random.NextDouble() * MathHelper.TwoPi;
                float x = (float)Math.Cos(angle);
                float y = (float)Math.Sin(angle);
                Vector2 speed = new Vector2(x, y);
                speed *= RandomMinMax(min, max);
                float size = 3;
                engine.Add(ExplosionSprites["smoke1"], position, speed, (float)random.NextDouble() * MathHelper.TwoPi,
                           3 * (float)random.NextDouble(), new Vector4(color.R, color.G, color.B, color.A), alphaVelocity, size, 0, 5, true, appearTime, true, false, 0, false);
                engine.Add(ExplosionSprites["smoke1"], position, speed, (float)random.NextDouble() * MathHelper.TwoPi,
                           3 * (float)random.NextDouble(), new Vector4(255, 255, 255, 255), alphaVelocity, size * 1.5f, 0, 5, false, appearTime, false, false, 0, false);
            }

            alphaVelocity = 255 / 0.6f;
            appearTime = 0.2f;
            min = intensity * 4.0f;
            max = intensity * 8.0f;
            color = c;
            for (int i = 0; i < explos1num; i++)
            {
                float angle = (float)random.NextDouble() * MathHelper.TwoPi;
                float x = (float)Math.Cos(angle);
                float y = (float)Math.Sin(angle);
                Vector2 speed = new Vector2(x, y);
                speed *= RandomMinMax(min, max);
                float size = 1;
                engine.Add(ExplosionSprites["smoke3"], position, speed, (float)random.NextDouble() * MathHelper.TwoPi,
                           3 * (float)random.NextDouble(), new Vector4(255, 255, 255, 255), alphaVelocity, size * 1.2f, 0, 5, false, appearTime, false, false, 0, false);
                engine.Add(ExplosionSprites["explos1"], position, speed, (float)random.NextDouble() * MathHelper.TwoPi,
                           3 * (float)random.NextDouble(), new Vector4(color.R, color.G, color.B, color.A), alphaVelocity, size, 0, 5, false, appearTime, false, false, 0, false);
            }

            alphaVelocity = 255 / 0.2f;
            appearTime = 0.8f;
            min = intensity * 3.0f;
            max = intensity * 4.0f;
            for (int i = 0; i < smoke2num; i++)
            {
                float angle = (float)random.NextDouble() * MathHelper.TwoPi;
                float x = (float)Math.Cos(angle);
                float y = (float)Math.Sin(angle);
                Vector2 speed = new Vector2(x, y);
                speed *= RandomMinMax(min, max);
                float size = 3.5f;
                engine.Add(ExplosionSprites["smoke2"], position, speed, (float)random.NextDouble() * MathHelper.TwoPi,
                           3 * (float)random.NextDouble(), new Vector4(color.R, color.G, color.B, color.A), alphaVelocity, size, 0, 5, false, appearTime, false, false, 0, false);
            }
        }

        /// <summary>
        /// Взрыв в точке 2
        /// </summary>
        /// <param name="position">координаты взрыва</param>
        /// <param name="numParticles">количество частиц в взрыве</param>
        /// <param name="c">цвет взрыва</param>
        public static void CreateExplosion2(Vector2 position, int numParticles, Color c)
        {
            ParticleEngine engine = GAME.GameManager.LvlController.ParticleEngine;
            Random random = GAME.GameManager.LvlController.Rnd;
            float smokeN = numParticles * 0.5f;
            float sparksN = numParticles * 0.5f;
            float roundSparksN = numParticles * 0.7f;

            float sSpeed = 0, angle = 0, alphaVelocity = 0, appearTime = 0, size;
            //огонь
            Color color = c;
            for (int i = 0; i < smokeN; i++)
            {
                size = RandomMinMax(1, 3);
                sSpeed = RandomMinMax(25, 80);
                angle = (float)random.NextDouble() * MathHelper.TwoPi;
                float x = (float)Math.Cos(angle);
                float y = (float)Math.Sin(angle);
                Vector2 speed = new Vector2(x, y) * sSpeed;
                alphaVelocity = 255 / (RandomMinMax(0.5f, 0.7f));
                appearTime = 0.0f;
                engine.Add(ExplosionSprites["smoke4"], position, speed, (float)random.NextDouble() * MathHelper.TwoPi,
                           3 * (float)random.NextDouble(), new Vector4(color.R, color.G, color.B, color.A), alphaVelocity, size, 0, 5, false, appearTime, false, false, 0, false);
            }

            //вспышка
            color = Color.White;
            size = 3;
            alphaVelocity = 255 / 0.2f;
            appearTime = 0.1f;
            engine.Add(ExplosionSprites["flash"], position, Vector2.Zero, 0,
                           3 * (float)random.NextDouble(), new Vector4(color.R, color.G, color.B, color.A), alphaVelocity, size, 0.1f, 5, false, appearTime, false, false, 0, false);
            engine.Add(ExplosionSprites["flash"], position, Vector2.Zero, MathHelper.Pi,
                          3 * (float)random.NextDouble(), new Vector4(color.R, color.G, color.B, color.A), alphaVelocity, size, 0.1f, 5, false, appearTime, false, false, 0, false);

            //искры и их следы
            color = c;
            for (int i = 0; i < sparksN; i++)
            {
                size = RandomMinMax(1, 3);
                sSpeed = RandomMinMax(80, 100);
                angle = (float)random.NextDouble() * MathHelper.TwoPi;
                float x = (float)Math.Cos(angle);
                float y = (float)Math.Sin(angle);
                Vector2 speed = new Vector2(x, y) * sSpeed;
                alphaVelocity = 255 / (RandomMinMax(0.2f, 1.2f));
                appearTime = (RandomMinMax(0.05f, 0.2f));
                engine.Add(ExplosionSprites["spark"], position, speed, angle + MathHelper.PiOver2,
                           0, new Vector4(color.R, color.G, color.B, color.A), alphaVelocity, size, 0, 5, false, appearTime, false, false, 0, false);
                engine.Add(ExplosionSprites["explos1"], position + speed * size * 0.14f, speed, 0,
                           0, new Vector4(255, 255, 255, 255), alphaVelocity, size * 0.2f, 0, 5, false, appearTime, false, false, 0, false);
            }

            //искорки после взрыва
            color = Color.DarkRed;
            for (int i = 0; i < roundSparksN; i++)
            {
                size = RandomMinMax(0.1f, 0.3f);
                sSpeed = RandomMinMax(0, 50);
                angle = (float)random.NextDouble() * MathHelper.TwoPi;
                float x = (float)Math.Cos(angle);
                float y = (float)Math.Sin(angle);
                Vector2 speed = new Vector2(x, y) * sSpeed;
                alphaVelocity = 255 / (RandomMinMax(0.01f, 0.1f));
                appearTime = (RandomMinMax(0.9f, 1.1f));
                engine.Add(ExplosionSprites["dot"], position + speed, speed * 0.1f, 0,
                           0, new Vector4(color.R, color.G, color.B, color.A), alphaVelocity, size, 0, 5, false, appearTime, false, false, 0, false);
            }
        }

        public static void CreateExplosion3(Vector2 position, int numParticle, int randomizeArea)
        {
            ParticleEngine engine = GAME.GameManager.LvlController.ParticleEngine;
            Random random = GAME.GameManager.LvlController.Rnd;
            GameTexture texture;
            Vector2 V;
            float speed;
            float Mass = 10.0f;
            float energyLoss = 80; // в %
            float minSpeed = 70;

            for (int i = 0; i < numParticle; i++)
            {
                int v = randomizeArea;

                position.X += (float)GAME.GameManager.LvlController.Rnd.Next(-v * 1000, v * 1000) / 1000.0f;
                position.Y += (float)GAME.GameManager.LvlController.Rnd.Next(-v * 1000, v * 1000) / 1000.0f;

                float angle = RandomMinMax(MathHelper.PiOver4 * 0.5f, MathHelper.PiOver2);
                if (random.Next(100) > 50) angle *= -1;
                V = -Vector2.UnitY;
                V = Vector2.Transform(V, Matrix.CreateRotationZ(angle));
                speed = RandomMinMax(minSpeed, 300);
                if (V.X > 0) texture = ExplosionSprites["blood1"];
                else
                    texture = ExplosionSprites["blood2"];//     


                int transp = GAME.GameManager.LvlController.Rnd.Next(80, 160);


                engine.Add(texture, position, V * speed, 0, 3 * V.X, new Vector4(255, 255, 255, 255), transp, 1, 0.0f, 30, true, 0.5f, false, true, Mass, false, energyLoss);
            }

        }


        private static float RandomMinMax(float min, float max)
        {
            return (float)GAME.GameManager.LvlController.Rnd.NextDouble() * (max - min) + min;
        }

        private void Init()
        {
            poolSize = 1000;
            MainList = new Particle[poolSize];
            freeParticles = new Queue<Particle>(poolSize);
            for (int i = 0; i < poolSize; i++)
            {
                MainList[i] = new Particle();
                freeParticles.Enqueue(MainList[i]);
            }
            MainList_alphablend = new List<Particle>();
            init = true;
        }

        public void Flush()
        {
            int n = 0;
            for (int i = 0; i < MainList.Length; i++)
            {
                if (MainList[i].Active) n++;
            }
            Particle[] newArray = new Particle[n];
            n = 0;
            for (int i = 0; i < MainList.Length; i++)
            {
                if (MainList[i].Active) newArray[n++] = MainList[i];
            }
            MainList = newArray;
            poolSize = n;
        }
    }
}

﻿using System;
using Microsoft.Xna.Framework;

namespace ENGINE.Logic.Particles
{
    [Serializable]
    public class CircleEmitter:BaseEmitter
    {
        public CircleEmitter( string folder)
            : base(folder)
        {
            Color = Color.White;
            generate = GenerateCircle;
            angleMain = MathHelper.PiOver4;
            angleSecondary = MathHelper.PiOver4;
            width = 1; // r1
            height = 100; // r2
        }

        public void GenerateCircle()
        {
            float randomAngle = RandomMinMax(-angleSecondary, angleSecondary);
            float x = (float)Math.Cos(randomAngle);
            float y = (float)Math.Sin(randomAngle);            
            generatedDir = Vector2.Transform(new Vector2(x, y), Matrix.CreateRotationZ(angleMain));
            float randomLength = RandomMinMax(width, height);
            generatedPos = generatedDir * randomLength;
            float pSpeed = RandomMinMax(minSpeed, maxSpeed);
            generatedDir *= pSpeed;
            if (emitBothWay) generatedDir *= -1;
        }

        public override void DrawEmitter(GameObject sender)
        {
            if (STORAGE.Config.EditorMode)
            {
                float cos1 = (float)Math.Cos(angleSecondary + angleMain);
                float sin1 = (float)Math.Sin(angleSecondary + angleMain);
                float cos2 = (float)Math.Cos(-angleSecondary + angleMain);
                float sin2 = (float)Math.Sin(-angleSecondary + angleMain);
                ENGINE.Graphics.Grid.DrawArc(Pos, (int)height, -angleSecondary + angleMain, angleSecondary + angleMain, Color.Purple, GAME.GameManager.LvlController.camera);
                ENGINE.Graphics.Grid.DrawArc(Pos, (int)width, -angleSecondary + angleMain, angleSecondary + angleMain, Color.Purple, GAME.GameManager.LvlController.camera);
                ENGINE.Graphics.Grid.DrawLine(new Vector2(Pos.X + width * cos1, Pos.Y + width * sin1), new Vector2(Pos.X + height * cos1, Pos.Y + height * sin1), Color.Purple, GAME.GameManager.LvlController.camera);
                ENGINE.Graphics.Grid.DrawLine(new Vector2(Pos.X + width * cos2, Pos.Y + width * sin2), new Vector2(Pos.X + height * cos2, Pos.Y + height * sin2), Color.Purple, GAME.GameManager.LvlController.camera);
            }
            base.DrawEmitter(sender);
        }
    }
}

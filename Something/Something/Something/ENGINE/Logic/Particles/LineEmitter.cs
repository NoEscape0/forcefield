﻿using System;
using Microsoft.Xna.Framework;

namespace ENGINE.Logic.Particles
{
    [Serializable]
    public class LineEmitter : BaseEmitter
    {
        public LineEmitter(string folder, ParticleEngine engine = null)
            : base(folder, engine)
        {
            Color = Color.White;
            generate = GenerateLine;
        }


        public void GenerateLine()
        {
            float x = 0;
            if (isNormalDistribution)
                x = Normal(-width / 2, width / 2);
            else
                x = RandomMinMax(-width / 2, width / 2);

            float y = RandomMinMax(-height / 2, height / 2);
            generatedDir = Vector2.Transform(Vector2.UnitY, Matrix.CreateRotationZ(angleMain));
            float pSpeed = RandomMinMax(minSpeed, maxSpeed);
            generatedDir *= pSpeed;
            if (emitBothWay && rnd.Next(100) > 50) generatedDir *= -1;

            generatedPos = Vector2.Transform(new Vector2(x, y), Matrix.CreateRotationZ(angleMain));
        }

        public override void DrawEmitter(GameObject sender)
        {
            if (STORAGE.Config.EditorMode)
                ENGINE.Graphics.Grid.DrawRectangle(Pos, width, height, Color.Green, GAME.GameManager.LvlController.camera, angleMain);
            base.DrawEmitter(sender);
        }
    }
}

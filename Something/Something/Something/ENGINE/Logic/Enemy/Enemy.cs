﻿using ENGINE.Logic.ObjAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GAME;
using Microsoft.Xna.Framework;

namespace ENGINE.Logic.Enemy
{
    [Serializable]
    public class Enemy : GameObject
    {
        public Enemy(string folder) : base (folder)
        {
            Functions.AddAttribute(Functions.Attr.Barrier, this);
            isEnemy = true;
        }

        public enum MovementType
        {
            Immovable,
            Movable,
            Fly
        }

        //movement
        public MovementType movementType;

        //attack
        public float attacDelay;
        public float AttacRange; // 0- не атакует вовсе
        public float Damage;


        public float ActivationDist;
        public float DeactivationDist;

        //hp & mp
        public float HP
        {
            get { return hp; }
            set
            {
                if (value <= maxhp & value >= 0) hp = value;
                else if (value > maxhp) hp = maxhp;
                else if (value < 0) hp = 0;
            }
        }
        public float MaxHP
        {
            get { return maxhp; }
            set
            {
                if (value >= 0) maxhp = value;
                if (hp > maxhp) hp = maxhp;
            }
        }
        public float MP
        {
            get { return mp; }
            set
            {
                if (value <= maxmp & value >= 0) mp = value;
                else if (value > maxmp) mp = maxmp;
            }
        }
        public float MaxMP
        {
            get { return maxmp; }
            set
            {
                if (value >= 0) maxmp = value;
                if (mp > maxmp) mp = maxmp;
            }
        }

        private float hp;
        private float maxhp;
        private float mp;
        private float maxmp;


        //
        public enum Orientation
        {
            Right, Left
        }
        public Orientation orientation;


        public override void Update()
        {
            base.Update();

            //враг в зоне реагирования?
            if (EnviromentInfo.GetDistanceToHero_points(this) < ActivationDist)
            {
                if (!_inActionzone) OnEnteringActionZone();
                if (_inActionzone) OnBeingInActionZone();
            }
            else
            {
                // деактивируем если игрок отбежал на двойное расстояние реакции
                if (EnviromentInfo.GetDistanceToHero_points(this) > DeactivationDist & _inActionzone)
                {
                    _inActionzone = false;
                    OnLivingActionZone();
                }
            }
        }

        public override void Draw()
        {
            base.Draw();

            if (STORAGE.Config.EditorMode)
            {
                Graphics.Grid.DrawCircle(this.Gtexture.Position, (int)ActivationDist, Color.Red, GameManager.LvlController.camera);
                Graphics.Grid.DrawCircle(this.Gtexture.Position, (int)DeactivationDist, Color.Green, GameManager.LvlController.camera);



            }
        }

        //smart interaction ==================
        //action
        public virtual void OnEnteringActionZone()
        {
            _inActionzone = true;
        }

        public virtual void OnBeingInActionZone()
        {
            if (EnviromentInfo.GetDistanceToHero_points(this) < AttacRange)
            {
                if (!_inAttaczone) OnEnteringAttacZone();
                OnBeingInAttacZone();
            }
            else
            {
                if (_inAttaczone) OnLivingAttacZone();
            }
        }

        public virtual void OnLivingActionZone()
        {
            _inActionzone = false;
        }

        //attac
        public virtual void OnEnteringAttacZone()
        {
            _inAttaczone = true;
        }

        public virtual void OnBeingInAttacZone()
        {
        }

        public virtual void OnLivingAttacZone()
        {
            _inAttaczone = false;
        }


        public virtual void OnReceivingDamage()
        {
            _inActionzone = true;
        }

        #region protected (interaction)
        protected bool _inActionzone = false;
        protected bool _inAttaczone = false; 
        #endregion
    }
}

﻿using System;
using Microsoft.Xna.Framework;
using STORAGE;
using GAME;
using FarseerPhysics.Dynamics;


namespace ENGINE.Logic.Enemy
{
    /// подвержен гравитации, может передвигаться
    [Serializable]
    public class Movable_E : Enemy 
    {
        public Movable_E(string folder)
            : base(folder)
        {
            movementType = MovementType.Movable;

            Init(this);
        }


        public override void Update()
        {
            base.Update();

            Body MainBody = GetBody(this);
            int gravitySign = Math.Sign(GameManager.LvlController.PhysicWorld.Gravity.Y);


            float diff = Math.Abs(lastSpeed.X - MainBody.LinearVelocity.X) * Math.Sign(lastSpeed.X);
            if (diff > 5) rightCollision = true; else rightCollision = false;
            if (diff < -5) leftCollision = true; else leftCollision = false;

            if (_jump && !isFalling && !inJump)
            {
                this.DeAttach();
                Jump2(MainBody, -JumpStrength * (float)Depository.Elapsed);
            }

            if (_moveto == moveto.left && !leftCollision)
            {
                Move(MainBody, -RunSpeed); orientation = Orientation.Left;
            }
            else
                if (_moveto == moveto.right && !rightCollision)
                {
                    Move(MainBody, RunSpeed);
                    orientation = Orientation.Right;
                }
                else StopX(MainBody);


            //начало падения при достижении максимальной скорости (=высоты)
            if (Math.Abs(MainBody.LinearVelocity.Y) > ConvertUnits.ToSimUnits(maxJumpSpeed)) { isFalling = true; inJump = true; }
            // просто падение, например с края платформы
            if (gravitySign * MainBody.LinearVelocity.Y > 1.01f) inJump = true; else inJump = false;

            if (attachedTo != null && attachedTo.body != null)
            {
                MainBody.LinearVelocity = new Vector2(MainBody.LinearVelocity.X + attachedTo.body.LinearVelocity.X, MainBody.LinearVelocity.Y);
                isFalling = false;
            }

            MainBody.Rotation = 0;
            MainBody.AngularVelocity = 0;
            lastSpeed = MainBody.LinearVelocity;

            #region orientation
            if (orientation == Orientation.Right)
            {
                Gtexture.Effects = Microsoft.Xna.Framework.Graphics.SpriteEffects.FlipHorizontally;
            }
            else
            {
                Gtexture.Effects = Microsoft.Xna.Framework.Graphics.SpriteEffects.None;
            }
            #endregion

            float tonearestRight = EnviromentInfo.GetDistanceToNearestBarrier(EnviromentInfo.CollisionDirection.Right, this) -
                Gtexture.TextureResolution.width * GameManager.LvlController.camera.Zoom * Gtexture.Scale.X;

            if (tonearestRight < 40 & _inActionzone)
                OnStayingBehindWall(EnviromentInfo.side_horisontal.right);

            float tonearestLeft = EnviromentInfo.GetDistanceToNearestBarrier(EnviromentInfo.CollisionDirection.Left, this) -
                 Gtexture.TextureResolution.width * GameManager.LvlController.camera.Zoom * Gtexture.Scale.X;
            if (tonearestLeft < 40 & _inActionzone)
                OnStayingBehindWall(EnviromentInfo.side_horisontal.left);


            if (tonearestLeft > 50 & tonearestRight > 50 & BehindWall) 
                OnStopStayingBehindWall();
        }

        //hard interaction
        protected enum moveto { none, left, right }
        protected moveto _moveto;
        protected bool _jump;

        //smart interaction
        public void Interaction_Stop()
        {
            _jump = false;
            _moveto = moveto.none;
        }


        // при условии что объект в ActionZone
        public virtual void OnStayingBehindWall(EnviromentInfo.side_horisontal WallSide)
        {
            BehindWall = true;
        }

        public virtual void OnStopStayingBehindWall()
        {
            BehindWall = false;
        }

        protected bool BehindWall;

        protected bool BehindCliff;

        //phisics
        #region
        [NonSerialized]
        public bool InitComplete;

        //========== Move Parametrs
        public float RunSpeed = 1000.0f;
        public float JumpStrength = 30000.0f;
        public float maxFallingSpeed { get { return 2 * RunSpeed; } }
        public float maxJumpSpeed = 2000;
        //==========

        private bool inJump = false;
        private bool isFalling;
        private bool leftCollision;
        private bool rightCollision;
        private Vector2 lastSpeed;


        public void Init(GameObject owner)
        {
            Body mainBody = (owner.data.ReadData("ABarrier") as ObjAttributes.ABarrier).GetBody(owner);
            mainBody.BodyType = BodyType.Dynamic;
            mainBody.OnCollision += new OnCollisionEventHandler(OnCollision);
            mainBody.OnSeparation += new OnSeparationEventHandler(OnSeparation);
            mainBody.FixedRotation = true;
            mainBody.SleepingAllowed = false;
            mainBody.Mass = 100.0f;
            mainBody.UserData = owner;
            owner.body = mainBody;
        }
        public Body GetBody(GameObject owner)
        {
            if (owner.body != null && owner.body.UserData != null && InitComplete) return owner.body;
            InitComplete = true;
            Init(owner);
            InitComplete = true;
            return owner.body;
        }
        public bool OnCollision(Fixture fixtureA, Fixture fixtureB, FarseerPhysics.Dynamics.Contacts.Contact contact)
        {
            int gravitySign = Math.Sign(GameManager.LvlController.PhysicWorld.Gravity.Y);
            GameObject player = fixtureA.Body.UserData as GameObject;
            GameObject platform = fixtureB.Body.UserData as GameObject;
            if (gravitySign * fixtureA.Body.LinearVelocity.Y > 0.1) { isFalling = false; }
            if (gravitySign * fixtureA.Body.LinearVelocity.Y < -0.1) isFalling = true;

            if (fixtureB.Body.BodyType != BodyType.Dynamic || isFalling)
            {
                if (fixtureA.Body.LinearVelocity.X > 0 && Vector2.Dot(Vector2.UnitY, contact.Manifold.LocalNormal) == 0) { rightCollision = true; }
                if (fixtureA.Body.LinearVelocity.X < 0 && Vector2.Dot(Vector2.UnitY, contact.Manifold.LocalNormal) == 0) { leftCollision = true; }
            }
            return true;
        }
        public void OnSeparation(Fixture fixtureA, Fixture fixtureB)
        {
        }

        void SetSpeed(Body body, Vector2 speed)
        {
            body.LinearVelocity = ConvertUnits.ToSimUnits(speed);
        }
        void AddSpeed(Body body, Vector2 speed)
        {
            body.LinearVelocity += ConvertUnits.ToSimUnits(speed);
        }
        void StopX(Body body)
        {
            body.LinearVelocity = new Vector2(0, body.LinearVelocity.Y);
        }
        void StopY(Body body)
        {
            body.LinearVelocity = new Vector2(body.LinearVelocity.X, 0);
        }
        void Jump2(Body body, float addSpeed)
        {
            addSpeed *= Math.Sign(GameManager.LvlController.PhysicWorld.Gravity.Y);
            body.LinearVelocity = new Vector2(body.LinearVelocity.X, body.LinearVelocity.Y + ConvertUnits.ToSimUnits(addSpeed));
        }
        void Move(Body body, float speed)
        {
            SetSpeed(body, new Vector2(speed, ConvertUnits.ToDisplayUnits(body.LinearVelocity.Y)));
        }
        #endregion
    }
}

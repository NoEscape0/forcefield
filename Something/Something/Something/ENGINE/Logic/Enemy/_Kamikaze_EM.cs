﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using STORAGE;
using GAME;
using ENGINE.Logic.ObjAttributes;
using ENGINE.Graphics;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;

namespace ENGINE.Logic.Enemy
{
    [Serializable]
    public class Kamikaze : Movable_E //враги- самоубийцы
    {
        public Kamikaze(string texturefolder)
            : base(texturefolder)
        {
            RunSpeed = 500;

            MaxHP = 15;
            HP = 15;

            MaxMP = 0;

            ActivationDist = 800;
            DeactivationDist = 2000;


            maxJumpSpeed = 1000;
            AttacRange = 30;
            Damage = 20;
        }

        //smart interaction
        public override void Update()
        {
            base.Update();
        }

        public override void OnBeingInActionZone()
        {
            base.OnBeingInActionZone();

            //двигаться на героя
            if (EnviromentInfo.HeroSide_horisontal(this) == EnviromentInfo.side_horisontal.left)
            {
                if (lastChange_Delay <= 0)
                {
                    _moveto = moveto.left;
                    lastChange_Delay = 2;
                }
            }
            else
                if (EnviromentInfo.HeroSide_horisontal(this) == EnviromentInfo.side_horisontal.right)
                {
                    if (lastChange_Delay <= 0)
                    {
                        _moveto = moveto.right;
                        lastChange_Delay = 2;
                    }
                }
            lastChange_Delay -= (float)Depository.Elapsed;

            //условия взрыва
            float toHeroDist = EnviromentInfo.GetDistanceToHero_points(this);
            if (toHeroDist <= AttacRange && Functions.HaveAttribute(Functions.Attr.RpgCharacter, EnviromentInfo.HERO.Hero))
            {
                //boom
                (EnviromentInfo.HERO.Hero.data.ReadData("ARpgCharacterParams") as ARpgCharacter.CharacterParams).HP -= Damage;

                Particles.ParticleEngine.CreateExplosion(this.Gtexture.Position, 100, Color.Orange);

                if (body != null) GameManager.LvlController.PhysicWorld.RemoveBody(body);
                GameManager.LvlController.ObjectArray.Remove(this);
            }
        }

        public override void OnLivingActionZone()
        {
            base.OnLivingActionZone();

            Interaction_Stop();
        }

        public override void OnStayingBehindWall(EnviromentInfo.side_horisontal WallSide)
        {
            base.OnStayingBehindWall(WallSide);

            _jump = true;
        }


        public override void OnStopStayingBehindWall()
        {
            base.OnStopStayingBehindWall();

            _jump = false;
        }

        float lastChange_Delay = 0;
    }
}

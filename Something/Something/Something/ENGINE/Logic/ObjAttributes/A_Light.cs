﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ENGINE.Logic.Lights;
using Microsoft.Xna.Framework;

namespace ENGINE.Logic.ObjAttributes
{
    [Serializable]
    public class ALight : AttributeStandart
    {

        public Light light;

        public override void Initialize(GameObject sender, Functions.Attr attribute)
        {
            sender.data.WriteDataAttr("ALight", this, Functions.Attr.Light);
            base.Initialize(sender, attribute);
            light = new Light(sender, Vector2.Zero, Color.White, 100);
            light.outerAngle = 360 * (float)Math.PI / 180;
            light.innerAngle = 360 * (float)Math.PI / 180;
            light.LightRadius = 512;
            sender.HasLight = true;
        }
        
        public override void Update(GameObject sender)
        { }
        /*
        public override void Draw(GameObject sender)
        { }
         */
    }
}

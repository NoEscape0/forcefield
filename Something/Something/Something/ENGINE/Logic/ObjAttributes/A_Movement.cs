﻿using System;

namespace ENGINE.Logic.ObjAttributes
{
    [Serializable]
    public class AMovement : AttributeStandart
    {
        public override void Initialize(GameObject sender, ENGINE.Logic.ObjAttributes.Functions.Attr attribute)
        {            
            MoveController MController = new MoveController();
            MController.Init(sender);
            sender.data.WriteDataAttr("MController", MController, Functions.Attr.MovementHero);

            base.Initialize(sender, attribute);
            sender.data.WriteDataAttr("AMovement", this, Functions.Attr.MovementHero);


            Functions.AddAttribute(Functions.Attr.Barrier, sender);

            ABarrier attr = (sender.data.ReadData("ABarrier") as ABarrier);
            sender.isCircleBody = false;
            var Body = attr.GetBody(sender);
            if (Body != null)
            {
                Body.UserData = null;
                GAME.GameManager.LvlController.PhysicWorld.RemoveBody(Body);
                sender.body = null;
            }
        }


        public override void  Update(GameObject sender)
        {
            IsActive(Functions.Attr.Select, sender); // проверяет следует ли удалить аттрибут или перевести в разряд редакторных

            if (Enable)
            {
                (sender.data.ReadData("MController") as MoveController).Update(sender);



                //collision with death zone
                if (Functions.HaveAttribute(Functions.Attr.RpgCharacter, sender))
                {
                    if ((sender.data.ReadData("ARpgCharacterParams") as ARpgCharacter.CharacterParams).HP <= 0) Die();
                }
            }               
        }

        public void Die()
        {
            if (!STORAGE.Config.godmode & GAME.EnviromentInfo.HERO.Hero != null)
            {
                if (!STORAGE.Config.EditorMode)
                {
                    GAME.GameManager.LvlController.ObjectArray.Remove(GAME.EnviromentInfo.HERO.Hero);

                    GAME.GUIManager.HeroDie();
                }
                else
                {
                    GAME.EnviromentInfo.HERO.Hero.Gtexture.Position = new Microsoft.Xna.Framework.Vector2(0, 0);


                    foreach (GameObject obj in GAME.GameManager.LvlController.ObjectArray)
                    {
                        if (obj.GetType() == typeof(ENGINE.Logic.Zones.StartFinishZone))
                        {
                            if ((obj as ENGINE.Logic.Zones.StartFinishZone).Startzone)
                            {
                                GAME.EnviromentInfo.HERO.Hero.Gtexture.Position = obj.Gtexture.Position;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}

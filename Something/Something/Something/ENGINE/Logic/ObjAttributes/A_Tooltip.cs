﻿using System;
using System.Text;
using ENGINE.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;

namespace ENGINE.Logic.ObjAttributes
{
    [Serializable]
    public class ATooltip : AttributeStandart
    {
        public float appearTime = 0.50f;
        public Vector2 offset;
        public float omega = 3.0f;
        public bool isAlwaysShow;
        public string text = "123";

        GameTexture background;
        GameObject anchor;
        float width;
        float height;
        Vector2 realScale;
        float currentTime = 0;

        float sigmaY;
        float maxAY;
        float sigmaX;
        float maxAX;

        bool inCollision;
        StringBuilder alltxt;

        Vector2 safeZone = new Vector2(10, 10);

        [NonSerialized]
        SpriteFont font;
        string fontString = "Source\\Fonts\\menu";

        public float Height
        {
            get { return height; }
            set
            {
                height = value;
                realScale.Y = value / background.TextureResolution.height;
            }
        }

        public float Width
        {
            get { return width; }
            set
            {
                width = value;
                realScale.X = value / background.TextureResolution.width;
            }
        }

        public SpriteFont Font
        {
            get
            {
                if (font == null) font = STORAGE.Depository.CommonContent.Load<SpriteFont>(fontString);
                return font;
            }
        }

        public string Text
        {
            get
            {
                return alltxt.ToString();
            }
            set
            {
                text = value;
                Wrap();
            }
        }

        public string BGName
        {
            get { return background.Folder; }
            set
            {
                GameTexture tmptex;
                try
                {
                    tmptex = new GameTexture(true, value);
                }
                catch { return; }
                background = tmptex;
                Width = background.TextureResolution.width;
                Height = background.TextureResolution.height;
                offset = new Vector2(-width * 0.5f,
                                 -1.05f * height);
            }
        }

        public string FontName
        {
            get { return fontString; }
            set
            {
                SpriteFont tmpfont;
                try
                {
                    tmpfont = STORAGE.Depository.CommonContent.Load<SpriteFont>(fontString);
                }
                catch { return; }
                font = tmpfont;
            }
        }

        public override void Initialize(GameObject sender, Functions.Attr attribute)
        {
            sender.data.WriteDataAttr("ATooltip", this, Functions.Attr.Tooltip);
            anchor = sender;

            background = new GameTexture(true, @"Textures\System\tooltipBG");
            width = background.TextureResolution.width;
            height = background.TextureResolution.height;
            realScale = new Vector2(1.0f, 1.0f);

            offset = new Vector2(-width * 0.5f,
                                  -1.05f * height);
            alltxt = new StringBuilder(100);
            CorrectConst(sender);
            inCollision = false;
            isAlwaysShow = true;
            InitBody(sender);
            base.Initialize(sender, attribute);
        }

        public override void Update(GameObject sender)
        {
            IsActive(Functions.Attr.Tooltip, sender);
            if (!Enable) return;
            GetBody(sender).Position = ConvertUnits.ToSimUnits(sender.Gtexture.Position);

            if (inCollision || isAlwaysShow) { currentTime += (float)STORAGE.Depository.Elapsed; }
            else currentTime -= (float)STORAGE.Depository.Elapsed;

            currentTime = MathHelper.Clamp(currentTime, 0, appearTime);
        }
       
        public override void Draw(GameObject sender)
        {
            Vector2 realPos = sender.Gtexture.Position + offset + background.Origin;
            Vector2 scale = Vector2.Zero;

            float fx1 = (float)Math.Pow(Math.E, -sigmaY * currentTime) * (float)Math.Cos(omega * currentTime * MathHelper.Pi);
            float fx2 = MathHelper.Clamp(fx1, 0, 1);
            fx1 *= maxAY;

            float fx3 = (float)Math.Pow(Math.E, -sigmaX * currentTime) * (float)Math.Cos(omega * currentTime * MathHelper.Pi + 0);
            float fx4 = MathHelper.Clamp(fx3, 0, 1);
            fx3 *= maxAX;

            scale.X = MathHelper.Lerp(realScale.X, 0, fx4);
            scale.Y = MathHelper.Lerp(realScale.Y, 0, fx2);

            Vector2 textScale = Vector2.Zero;
            textScale.X = MathHelper.Lerp(1, 0, fx4);
            textScale.Y = MathHelper.Lerp(1, 0, fx2);

            background.Position = new Vector2(realPos.X + fx3, realPos.Y + fx1);

            background.Draw(scale, background.Origin);
            STORAGE.Depository.SpriteBatch.DrawString(Font, alltxt.ToString(), background.Position + new Vector2(10, 10), Color.White, 0, background.Origin * realScale, textScale, SpriteEffects.None, 0);
            base.Draw(sender);
        }

        public void CorrectConst(GameObject sender)
        {
            background.Position = sender.Gtexture.Position + offset + background.Origin;
            Vector2 diff = sender.Gtexture.Position - background.Position;
            maxAX = diff.X;
            maxAY = diff.Y;
            diff.X = Math.Abs(diff.X);
            diff.Y = Math.Abs(diff.Y);

            if (diff.Y > 15)
                sigmaY = -(float)Math.Log(2 / diff.Y) / appearTime; // колебания до 2 пикселей = остановка
            else { maxAY = 0; sigmaY = 10; }

            if (diff.X > 15)
                sigmaX = -(float)Math.Log(2 / diff.X) / appearTime; // колебания до 2 пикселей = остановка
            else { maxAX = 0; sigmaX = 10; }

            //safeZone = new Vector2(10, 10) * realScale;
            Wrap();
        }

        void Wrap()
        {
            alltxt.Clear();
            int index = 0;
            while (index < text.Length)
            {
                string nextline = text.Substring(index, PixelToCharLength(index));
                alltxt.Append(nextline);
                index += PixelToCharLength(index);
                if (index < text.Length) alltxt.AppendLine();
            }
            string res = alltxt.ToString();
        }

        int PixelToCharLength(int startindex)
        {
            int safeWidth = (int)(Width - safeZone.X * 2);
            if (safeWidth / Font.MeasureString("O").X < 1) return 1;//для узких текстур, которых никто и не должен использовать
            float res = 0;
            int len = 0;
            while (res < safeWidth && len + startindex < text.Length)
            {
                res = Font.MeasureString(text.Substring(startindex, ++len)).X;
            }
            if (len + startindex < text.Length || res > safeWidth) len--;
            return len;
        }

        protected override void InitBody(GameObject owner)
        {
            Body mainBody = BodyFactory.CreateRectangle(GAME.GameManager.LvlController.PhysicWorld,
                                                  ConvertUnits.ToSimUnits(owner.Gtexture.TextureResolution.width) * owner.Gtexture.Scale.X,
                                                  ConvertUnits.ToSimUnits(owner.Gtexture.TextureResolution.height) * owner.Gtexture.Scale.Y,
                                                  1);
            mainBody.Position = ConvertUnits.ToSimUnits(owner.Gtexture.Position);
            mainBody.BodyType = BodyType.Static;
            mainBody.UserData = owner;
            mainBody.OnCollision += new OnCollisionEventHandler(OnCollisionWithTooltipObj);
            mainBody.OnSeparation += new OnSeparationEventHandler(OnSeperationWithTooltipObj);
            mainBody.IsSensor = true;
            owner.body = mainBody;
        }

        private bool OnCollisionWithTooltipObj(Fixture fixtureA, Fixture fixtureB, FarseerPhysics.Dynamics.Contacts.Contact contact)
        {
            GameObject player = fixtureB.Body.UserData as GameObject;
            if (player == null) return false;
            if (!Functions.HaveAttribute(Functions.Attr.MovementHero, player)) return false;
            inCollision = true;
            return true;
        }
        private void OnSeperationWithTooltipObj(Fixture fixtureA, Fixture fixtureB)
        {
            inCollision = false;
        }

    }
}

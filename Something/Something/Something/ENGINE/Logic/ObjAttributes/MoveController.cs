﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using STORAGE;
using GAME;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;

namespace ENGINE.Logic.ObjAttributes
{
    [Serializable]
    public class MoveController
    {
        public MoveController()
        {
            RunSound = new RandomSound(new Sound(@"Sound\Hero\run_1", 0.8f),
                new Sound(@"Sound\Hero\run_2", 0.8f));

            JumpSound = new Sound(@"Sound\Hero\jump");
            JumpSound.Volume = 0.4f;

            allWeapons = new List<Weapons.Weapon>();
        }

        public enum Orientation
        {
            Right, Left
        }
        public Orientation orientation;

        //========== Move Parametrs
        public float RunSpeed = 1000.0f;
        public float jumpStrength = 30000.0f;
        public float maxFallingSpeed { get { return 2 * RunSpeed; } }
        public float maxJumpSpeed = 2000;
        //========== Sounds
        public RandomSound RunSound;
        public Sound JumpSound;
        //========== 


        private bool inJump = false;
        private Vector2 currentSpeed;
        private bool isFalling;
        private bool buttonReleased = true;
        private bool leftCollision;
        private bool rightCollision;
        private bool bottomCollision;

        private Vector2 lastSpeed;
        [NonSerialized]
        public bool InitComplete;
        public Weapons.Weapon currentWeapon;
        List<Weapons.Weapon> allWeapons;
        public bool inDialog;

        public void Update(GameObject Subject)
        {
            if (Depository.keyboardState.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.F1) & STORAGE.Config.EditorMode & Depository.CanUseKeyboard)
            {
                Subject.Gtexture.Position = new Vector2(0, 0);
                currentSpeed = Vector2.Zero;
            }
            Body MainBody = GetBody(Subject);
            int gravitySign = Math.Sign(GameManager.LvlController.PhysicWorld.Gravity.Y);

            #region Collision
            //Упирание в левую/правую стенку
            leftCollision = rightCollision = false;

            FarseerPhysics.Dynamics.Contacts.ContactEdge edge = MainBody.ContactList;
            int numContacts = 0;
            while (edge != null)
            {
                if (edge.Contact.IsTouching() && !edge.Contact.FixtureA.IsSensor) numContacts++;
                edge = edge.Next;
            }
            // если есть коллизия в  прыжке/падении, управление не учитывается
            // для предотвращения зависания в воздухе
            if (isFalling || inJump || !buttonReleased || numContacts == 1)
            {
                rightCollision = EnviromentInfo.CheckCollision(Subject, EnviromentInfo.CollisionDirection.Right);
                leftCollision = EnviromentInfo.CheckCollision(Subject, EnviromentInfo.CollisionDirection.Left);
                bottomCollision = EnviromentInfo.CheckCollision(Subject, EnviromentInfo.CollisionDirection.Down);
            }

            #endregion

            #region Input
            bool jumpingOrmoveing = false;

            bool haveAnim = Functions.HaveAttribute(Functions.Attr.Animation, Subject) ? true : false;

            if (Input.IsKeyPressed(Config.KeyboardCfg.Jump) && Depository.CanUseKeyboard && !isFalling && !inJump)
            {
                Jump(MainBody, -jumpStrength * (float)Depository.Elapsed);
                buttonReleased = false;
                JumpSound.Play();
                jumpingOrmoveing = true;
            }

            if (Input.IsKeyPressed(Config.KeyboardCfg.MoveLeft) && Depository.CanUseKeyboard && !leftCollision && !inDialog)
            {
                Move(MainBody, -RunSpeed); orientation = Orientation.Left;
                if (!isFalling)
                {
                    RunSound.Play();                     
                }
                if (haveAnim) (Subject.data.ReadData("ANIMATION") as Animations).PlayAnimation("run");

                jumpingOrmoveing = true;
            }
            else
                if (Input.IsKeyPressed(Config.KeyboardCfg.MoveRight) && Depository.CanUseKeyboard && !rightCollision && !inDialog)
                {
                    Move(MainBody, RunSpeed); orientation = Orientation.Right;
                    if (!isFalling)
                    {
                        RunSound.Play();                       
                    }
                    if (haveAnim) (Subject.data.ReadData("ANIMATION") as Animations).PlayAnimation("run");

                    jumpingOrmoveing = true;
                }
                else StopX(MainBody);

            if (!jumpingOrmoveing)
            {
                if (haveAnim) (Subject.data.ReadData("ANIMATION") as Animations).PlayAnimation("idle");
            }


            if (inDialog) { StopX(MainBody); StopY(MainBody); }

            if (Input.IsKeyReleased(Config.KeyboardCfg.MoveLeft)) { leftCollision = false; }
            if (Input.IsKeyReleased(Config.KeyboardCfg.MoveRight)) { rightCollision = false; }
            if (Input.IsKeyReleased(Config.KeyboardCfg.Jump))
            {
                inJump = false;
                buttonReleased = true;
                if (Math.Abs(MainBody.LinearVelocity.Y) > 0.05f) isFalling = true; // падать начинаем, если только летим
            }

            if (Input.IsLeftButtonPress() && currentWeapon != null && Depository.CanUseMouse)
            {
                Vector2 direction = GAME.CursorManager.ScreenToWorld(Input.MouseScreenVector(), GAME.GameManager.LvlController.camera.GetTransformation()) - GAME.EnviromentInfo.HERO.Hero.Gtexture.Position;
                direction.Normalize();
                currentWeapon.Shoot(direction);
            }

            #warning настройку сделать в редакторе
            if (Input.IsNewKeyPressed(Microsoft.Xna.Framework.Input.Keys.D1)) { ChangeWeapon(Subject, 0); }
            if (Input.IsNewKeyPressed(Microsoft.Xna.Framework.Input.Keys.D2)) { ChangeWeapon(Subject, 1); }
            if (Input.IsNewKeyPressed(Microsoft.Xna.Framework.Input.Keys.D3)) { ChangeWeapon(Subject, 2); }
            #endregion

            #region проверка падения
            //начало падения при достижении максимальной скорости (=высоты)
            if (Math.Abs(MainBody.LinearVelocity.Y) > ConvertUnits.ToSimUnits(maxJumpSpeed)) { isFalling = true; if (!buttonReleased) inJump = true; }
            // просто падение, например с края платформы
            if (gravitySign * MainBody.LinearVelocity.Y > 1.01f) {if (!buttonReleased) inJump = true; else inJump = false; }

            if (Subject.attachedTo != null && Subject.attachedTo.body != null) { MainBody.LinearVelocity = new Vector2(MainBody.LinearVelocity.X + Subject.attachedTo.body.LinearVelocity.X, MainBody.LinearVelocity.Y); isFalling = false; }

            MainBody.Rotation = 0;
            MainBody.AngularVelocity = 0;
            lastSpeed = MainBody.LinearVelocity;

            #endregion

            if (currentWeapon != null) currentWeapon.Update();

            #region cam
            if (!Config.EditorMode)
            {
                if ((GameManager.LvlController.camera.CameraPosition - Subject.Gtexture.Position).LengthSquared() < 10f)
                    GameManager.LvlController.camera.CameraPosition = Subject.Gtexture.Position;
                else
                    GameManager.LvlController.camera.CameraPosition -= (float)Depository.Elapsed * Config.cameraGameSpeed * (GameManager.LvlController.camera.CameraPosition - Subject.Gtexture.Position);
            }
            #endregion

            #region orientation
#warning сделать ориентацию в настройках передвижения а не в коде
            if (orientation == Orientation.Right)
            {
                Subject.Gtexture.Effects = Microsoft.Xna.Framework.Graphics.SpriteEffects.FlipHorizontally;
            }
            else
            {
                Subject.Gtexture.Effects = Microsoft.Xna.Framework.Graphics.SpriteEffects.None;
            }
            #endregion

        }

        public bool MoveController_OnCollision(Fixture fixtureA, Fixture fixtureB, FarseerPhysics.Dynamics.Contacts.Contact contact)
        {
            if (fixtureB.IsSensor) return true;
            int gravitySign = Math.Sign(GameManager.LvlController.PhysicWorld.Gravity.Y);
            GameObject player = fixtureA.Body.UserData as GameObject;
            GameObject platform = fixtureB.Body.UserData as GameObject;
            if (gravitySign * fixtureA.Body.LinearVelocity.Y > 0.1) { isFalling = false; }
            if (gravitySign * fixtureA.Body.LinearVelocity.Y < -0.1) isFalling = true;

            if (fixtureB.Body.BodyType != BodyType.Dynamic || isFalling)
            {
                if (fixtureA.Body.LinearVelocity.X > 0 && Vector2.Dot(Vector2.UnitY, contact.Manifold.LocalNormal) == 0) { rightCollision = true; }
                if (fixtureA.Body.LinearVelocity.X < 0 && Vector2.Dot(Vector2.UnitY, contact.Manifold.LocalNormal) == 0) { leftCollision = true; }
            }
            return true;
        }

        public void MoveController_OnSeparation(Fixture fixtureA, Fixture fixtureB)
        {
        }

        void SetSpeed(Body body, Vector2 speed)
        {
            body.LinearVelocity = ConvertUnits.ToSimUnits(speed);
        }

        void AddSpeed(Body body, Vector2 speed)
        {
            body.LinearVelocity += ConvertUnits.ToSimUnits(speed);
        }

        void StopX(Body body)
        {
            body.LinearVelocity = new Vector2(0, body.LinearVelocity.Y);
        }

        void StopY(Body body)
        {
            body.LinearVelocity = new Vector2(body.LinearVelocity.X, 0);
        }

        void Jump(Body body, float addSpeed)
        {
            addSpeed *= Math.Sign(GameManager.LvlController.PhysicWorld.Gravity.Y);
            body.LinearVelocity = new Vector2(body.LinearVelocity.X, body.LinearVelocity.Y + ConvertUnits.ToSimUnits(addSpeed));
        }

        void Move(Body body, float speed)
        {
            SetSpeed(body, new Vector2(speed, ConvertUnits.ToDisplayUnits(body.LinearVelocity.Y)));
        }

        public Body GetBody(GameObject owner)
        {
            if (owner.body != null && owner.body.UserData != null && InitComplete) return owner.body;
            Init(owner);
            InitComplete = true;
            return owner.body;
        }

        public void Init(GameObject owner)
        {
            if ((owner.data.ReadData("ABarrier") as ObjAttributes.ABarrier) == null) return; 

            Body mainBody = (owner.data.ReadData("ABarrier") as ObjAttributes.ABarrier).GetBody(owner);
            if (mainBody != null) GAME.GameManager.LvlController.PhysicWorld.RemoveBody(mainBody);
            ABarrier barrier = (owner.data.ReadData("ABarrier") as ObjAttributes.ABarrier);

            if (!owner.isCircleBody)
            {
                mainBody = BodyFactory.CreateRectangle(GAME.GameManager.LvlController.PhysicWorld,
                                                      ConvertUnits.ToSimUnits(owner.Gtexture.TextureResolution.width * owner.Gtexture.Scale.X - barrier.MarginLeft - barrier.MarginRight),
                                                      ConvertUnits.ToSimUnits(owner.Gtexture.TextureResolution.height * owner.Gtexture.Scale.Y - barrier.MarginTop - barrier.MarginDown),
                                                      1);
                barrier.realCenter = new Vector2(0.5f * (barrier.MarginLeft - barrier.MarginRight), 0.5f * (barrier.MarginTop - barrier.MarginDown));
            }
            else
            {
                int totalMargin = barrier.MarginDown + barrier.MarginTop + barrier.MarginLeft + barrier.MarginRight;
                mainBody = BodyFactory.CreateCircle(GAME.GameManager.LvlController.PhysicWorld,
                                                ConvertUnits.ToSimUnits(owner.Gtexture.TextureResolution.height / 2 - totalMargin), 1,
                                                 owner);
                barrier.realCenter = Vector2.Zero;
            }

            owner.body = mainBody;
            mainBody.Position = ConvertUnits.ToSimUnits(owner.Gtexture.Position);
            mainBody.BodyType = BodyType.Dynamic;
            mainBody.OnCollision += new OnCollisionEventHandler(MoveController_OnCollision);
            mainBody.OnSeparation += new OnSeparationEventHandler(MoveController_OnSeparation);
            mainBody.FixedRotation = true;
            mainBody.IsBullet = true;
            mainBody.SleepingAllowed = false;
            //mainBody.Mass = 0.1f;
            mainBody.UserData = owner;
            owner.body = mainBody;


            allWeapons.Add(new Weapons.Railgun(owner));
            allWeapons.Add(new Weapons.LightningGun(owner));
            allWeapons.Add(new Weapons.MachineGun(owner));
            currentWeapon = null;



            InitComplete = true;
        }

        private void ChangeWeapon(GameObject owner, int index)
        {
            if (index >= allWeapons.Count) return;
            if (allWeapons.IndexOf(currentWeapon) == index) return;

            if (currentWeapon != null)
                owner.OnDraw -= currentWeapon.Draw;
            currentWeapon = allWeapons[index];
            owner.OnDraw += currentWeapon.Draw;
            currentWeapon.CanShoot = true;
            string txt = currentWeapon.GetType().ToString();
            FloatingText ft = FloatingText.Create(FloatingText.TextType.SlideUp, txt, 1, Color.Purple, owner.Gtexture.Position, 100, 2);
        }
    }
}

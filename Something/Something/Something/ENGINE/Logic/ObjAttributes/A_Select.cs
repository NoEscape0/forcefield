﻿using Microsoft.Xna.Framework;
using STORAGE;
using System;
using ENGINE.Graphics;

namespace ENGINE.Logic.ObjAttributes
{
    [Serializable]
    public class ASelect : AttributeStandart
    {
        public override void Initialize(ENGINE.Logic.GameObject sender, ENGINE.Logic.ObjAttributes.Functions.Attr attribute)
        {
            base.Initialize(sender, attribute);
              sender.data.WriteDataAttr("ASelect", this, Functions.Attr.Select);

              SelectParams param = new SelectParams();
              param.GSelector = new GameTexture(false, "Textures/Advanced/selector_point");
              param.GSelector.Layer = 0.0f;
              sender.data.WriteDataAttr("SelectParams", param, Functions.Attr.Select);             
        }

        public override void  Update(GameObject sender)
        {
            IsActive(Functions.Attr.Select, sender); // проверяет следует ли удалить аттрибут или перевести в разряд редакторных

            if (Enable)
            {
                bool SmthAlreadySelected = (bool)GAME.GameManager.LvlController.CommonData.ReadData("SmthAlreadySelected");

                int ImageW = sender.Gtexture.TextureResolution.width;
                int ImageH = sender.Gtexture.TextureResolution.height;


                if (GAME.CursorManager.LBclick && !lastLBclick && Depository.CanUseMouse && !SmthAlreadySelected)
                {  
                    // матрица трансформации из мировых координат в локальную сист. коорд sendera
                    Matrix realPosMatrix = Matrix.CreateTranslation(new Vector3(-sender.Gtexture.Position, 0)) *
                                   Matrix.CreateRotationZ(-sender.rotation) *
                                  /* Matrix.CreateScale(new Vector3(1/sender.Gtexture.Scale.X, 1/sender.Gtexture.Scale.Y, 1.0f)) **/
                                   Matrix.CreateTranslation(new Vector3(sender.Gtexture.Position, 0));
                    Vector2 worldMouse = GAME.CursorManager.ScreenToWorld(Input.MouseScreenVector(), GAME.GameManager.LvlController.camera.GetTransformation());
                    worldMouse = Vector2.Transform(worldMouse, realPosMatrix);

                    if (Input.isHover(sender.Gtexture.GetTextureArea(),worldMouse))
                    {

                        (sender.data.ReadData("SelectParams") as SelectParams).Select(sender);

                        GAME.GameManager.LvlController.CommonData.WriteDataSimple("SmthAlreadySelected", true);
                        GAME.GameManager.LvlController.CommonData.WriteDataSimple("SelectedOBJ", sender);
                    }
                    else
                    {
                        if ((sender.data.ReadData("SelectParams") as SelectParams).Selected & !SmthAlreadySelected)
                        {
                            GAME.GameManager.LvlController.CommonData.WriteDataSimple("SelectedOBJ", null);
                        }


                        (sender.data.ReadData("SelectParams") as SelectParams).DeSelect(sender);
                    }
                }

                if (SmthAlreadySelected) (sender.data.ReadData("SelectParams") as SelectParams).DeSelect(sender);

                lastLBclick = GAME.CursorManager.LBclick;
            }
            else
            {
                (sender.data.ReadData("SelectParams") as SelectParams).DeSelect(sender);
            }
        }

        bool lastLBclick;         
    }

    [Serializable]
    public class SelectParams
    {
        public bool Selected = false;

        public bool Selectors_red = true;
        public bool Selectors_points = true;

        public GameTexture GSelector;


        public void Select(GameObject sender)
        {
            Selected = true;
            if (Selectors_red)   sender.Gtexture.Color = Color.Red;
            GAME.GameManager.LvlController.CommonData.WriteDataSimple("SelectedOBJ", sender);
        }

        public void DeSelect(GameObject sender)
        {
            Selected = false;
            sender.Gtexture.Color = Color.White;

            if( (GAME.GameManager.LvlController.CommonData.ReadData("SelectedOBJ") as GameObject) == sender)    GAME.GameManager.LvlController.CommonData.WriteDataSimple("SelectedOBJ", null);
        }
    }
}

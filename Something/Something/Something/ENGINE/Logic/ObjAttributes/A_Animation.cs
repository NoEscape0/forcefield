﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using STORAGE;
using System.Collections.Generic;
using ENGINE.Graphics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ENGINE.Logic.ObjAttributes
{
    [Serializable]
    public class AAnimation : AttributeStandart
    {
        public override void Initialize(GameObject sender, Functions.Attr attribute)
        {
            sender.data.WriteDataAttr("ANIMATION", new Animations(), Functions.Attr.Animation);
            base.Initialize(sender, attribute);
        }

        public override void Update(GameObject sender)
        {
            IsActive(Functions.Attr.Animation, sender);

            if (Enable)
            {
                (sender.data.ReadData("ANIMATION") as Animations).Update(sender);
            }
        }
    }


    [Serializable]
    public class Animations
    {
        public Animations()
        {
            timeBeforeNextFrame = 0;
            AnimationsArray = new List<SingleAnimation>();
            AdvancedTextures = new List<GameTexture>();
        }

        public List<SingleAnimation> AnimationsArray;

        public double TimeLeftBeforeNextFrame
        {
            get
            {
                return timeBeforeNextFrame;
            }
        }
        private double timeBeforeNextFrame;

        public bool IsPlaying
        {
            get
            {
                return isplaying;
            }
        }
        bool isplaying;


        public List<GameTexture> AdvancedTextures;

        public bool AnimationEnabled = false;

        public void Update(GameObject AnimHaver)
        {
            if (!AnimationEnabled) return;

            if (current_animation == null)
            {
                isplaying = false;
                return;
            }

            isplaying = true;

            if (TimeLeftBeforeNextFrame <= 0) //переход на след кадр
            {
                current_animation.SwitchToNextFrame();

                if (current_animation.current_frame == null)
                {
                    isplaying = false;
                    real_current_animation = -1;
                }
                else timeBeforeNextFrame = current_animation.current_frame.delay;
            }
            else timeBeforeNextFrame -= Depository.Elapsed;
        }

        public Rectangle GetDrawRectangle(GameObject AnimHaver)
        {
            if (current_animation == null) return new Rectangle(0, 0, AnimHaver.Gtexture.TextureResolution.width, AnimHaver.Gtexture.TextureResolution.height);

            if (current_animation.current_frame == null) return new Rectangle(0, 0, AnimHaver.Gtexture.TextureResolution.width, AnimHaver.Gtexture.TextureResolution.height);


            return new Rectangle(current_animation.current_frame.Xcoord, current_animation.current_frame.Ycoord,
                current_animation.FrameResolution.width, current_animation.FrameResolution.height);
        }
        public GameTexture GetDrawTexture(GameObject AnimHaver)
        {
            if (current_animation == null) return AnimHaver.Gtexture;
            if (current_animation.current_frame == null) return AnimHaver.Gtexture;


            foreach (GameTexture texture in AdvancedTextures)
            {
                if (texture.Folder == current_animation.current_frame.TextureF) return texture;
            }

            return AnimHaver.Gtexture;
        }


        public bool PlayAnimation(string anim_name, bool ResetIfAlreadyPlaying = false)
        {
            if (Config.EditorMode & !AnimationEnabled) return false;

            for (int i = 0; i < AnimationsArray.Count; ++i)
            {
                if (AnimationsArray[i].Name == anim_name)
                {
                    real_current_animation = i;
                    AnimationEnabled = true;
                    if (ResetIfAlreadyPlaying) current_animation.current_frame = current_animation.Frames[0];
                    return true;
                }
            }

            return false;
        }
        public bool PlayAnimation(int anim_number, bool ResetIfAlreadyPlaying = false)
        {
            if (Config.EditorMode & !AnimationEnabled) return false;

            if (anim_number == -1) return false;
            AnimationEnabled = true;

            if (anim_number < AnimationsArray.Count)
            {
                real_current_animation = anim_number;
                if (ResetIfAlreadyPlaying) current_animation.current_frame = current_animation.Frames[0];
                return true;
            }

            return false;
        }

        public void StopAnimation()
        {
            real_current_animation = -1;
        }


        public SingleAnimation current_animation
        {
            get
            {
                if (AnimationsArray.Count - 1 >= real_current_animation & real_current_animation != -1) return AnimationsArray[real_current_animation]; else return null;
            }
            set
            {
                for (int i = 0; i < AnimationsArray.Count; ++i)
                {
                    if (AnimationsArray[i] == value)
                    {
                        real_current_animation = i;
                        break;
                    }
                }
            }
        }

        private int real_current_animation;
    }


    [Serializable]
    public class SingleAnimation
    {
        public SingleAnimation(GameTexture.textureRes FrameResolution)
        {
            Frames = new List<SingleFrame>();
            this.FrameResolution = FrameResolution;
        }

        public List<SingleFrame> Frames; // фреймы одной анимации
        public bool looped = true;
        public string Name;
        public GameTexture.textureRes FrameResolution;

        public SingleFrame current_frame
        {
            get
            {
                if (Frames.Count > real_current_frame - 1 & Frames.Count > 0 & real_current_frame != -1) return Frames[real_current_frame]; else return null;
            }
            set
            {

                for (int i = 0; i < Frames.Count; ++i)
                {
                    if (Frames[i] == value)
                    {
                        real_current_frame = i;
                        break;
                    }
                }
            }
        }
        public int real_current_frame;
        private float currentTime = 0;

        public Vector2 Position;
        public string Tag;
        public bool isPlaying  // используется в StaticAnimations
        {
            get
            {
                return real_current_frame != -1;
            }
            set
            {
                real_current_frame = value ? 0 : -1;
            }
        }

        public void SwitchToNextFrame()
        {
            for (int i = 0; i < Frames.Count; ++i)
            {
                if (current_frame == Frames[i])
                {
                    if (i < Frames.Count - 1) real_current_frame = i + 1;
                    else
                    {
                        if (looped)
                        {
                            real_current_frame = 0;
                        }
                        else real_current_frame = -1;
                    }
                    break;
                }
            }
        }

        public void Update()
        {
            float elapsed = (float)Depository.Elapsed;
            currentTime += elapsed;
            if (currentTime > current_frame.delay)
            {
                SwitchToNextFrame();
                currentTime = 0;
            }
        }

        public SingleAnimation Clone()
        {
            MemoryStream mstream = new MemoryStream();
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(mstream, this);
            mstream.Seek(0, SeekOrigin.Begin);
            SingleAnimation a = binaryFormatter.Deserialize(mstream) as SingleAnimation;
            mstream.Close();
            return a;
        }
    }

    [Serializable]
    public class SingleFrame
    {
        public SingleFrame()
        {
        }

        public string TextureF = "";
        public int Xcoord = 0;
        public int Ycoord = 0;
        public double delay = 0.3; //задержка до показа следующего кадра


        public SingleFrame Clone()
        {
            SingleFrame sf = new SingleFrame();

            sf.TextureF = TextureF;
            sf.Xcoord = Xcoord;
            sf.Ycoord = Ycoord;
            sf.delay = delay;

            return sf;
        }
    }
}

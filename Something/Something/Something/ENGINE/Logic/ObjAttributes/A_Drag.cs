﻿using STORAGE;
using Microsoft.Xna.Framework;
using System;


namespace ENGINE.Logic.ObjAttributes
{
    [Serializable]
    public class ADrag : AttributeStandart
    {
        public override void Initialize(GameObject sender, Functions.Attr attribute)
        {
            GAME.GameManager.LvlController.CommonData.WriteDataSimple("SmthAlreadyDragging", false);

            base.Initialize(sender, attribute);
            sender.data.WriteDataAttr("ADrag", this, Functions.Attr.Drag);
        }

        public override void Update(GameObject sender)
        {
            IsActive(Functions.Attr.Drag, sender); // проверяет следует ли удалить аттрибут или перевести в разряд редакторных

            if (Enable)
            {
              
                bool Selected = false;
                if ((GAME.GameManager.LvlController.CommonData.ReadData("SelectedOBJ") as GameObject) == sender) Selected = true;


                bool SmthAlreadyDragging = false;

                try
                {
                    SmthAlreadyDragging = (bool)GAME.GameManager.LvlController.CommonData.ReadData("SmthAlreadyDragging");
                }
                catch { };
                

                if (Selected & GAME.CursorManager.LBclick & !lastLBclick & !SmthAlreadyDragging & Depository.CanUseMouse)
                {
                    Drag = true;
                    GAME.GameManager.LvlController.CommonData.WriteDataSimple("SmthAlreadyDragging", true);
                    notConvertedPos = sender.Gtexture.Position;
                    CoordIn = (GAME.CursorManager.GlobalPosition(GAME.GameManager.LvlController.camera.CameraPosition, GAME.GameManager.LvlController.camera.Zoom) - sender.Gtexture.Position);
                }

                if (!GAME.CursorManager.LBclick & Drag)
                {
                    Drag = false;
                    GAME.GameManager.LvlController.CommonData.WriteDataSimple("SmthAlreadyDragging", false);
                    if (Functions.HaveAttribute(Functions.Attr.Bonus, sender))
                    {
                        (sender.data.ReadData("ABonus")as ABonus).originalPosition = sender.Gtexture.Position;
                    }
                }

                #region Dragging
                if (Drag)
                {
                    notConvertedPos = new Vector2
                        ((GAME.CursorManager.GlobalPosition(GAME.GameManager.LvlController.camera.CameraPosition, GAME.GameManager.LvlController.camera.Zoom).X - CoordIn.X),
                            (GAME.CursorManager.GlobalPosition(GAME.GameManager.LvlController.camera.CameraPosition, GAME.GameManager.LvlController.camera.Zoom).Y - CoordIn.Y));

                    int pp = Config.Snap_to_grid;

                    sender.Gtexture.Position = notConvertedPos;

                    sender.Gtexture.Position.X = (int)(notConvertedPos.X / pp) * pp;;
                    sender.Gtexture.Position.Y = (int)(notConvertedPos.Y / pp) * pp;
                }
                #endregion

                lastLBclick = GAME.CursorManager.LBclick;
            }
            else
            {
                GAME.GameManager.LvlController.CommonData.WriteDataSimple("SmthAlreadyDragging", false);
            }
        }

        public bool Drag;
        bool lastLBclick;

        Vector2 CoordIn;

        Vector2 notConvertedPos;
    }
}

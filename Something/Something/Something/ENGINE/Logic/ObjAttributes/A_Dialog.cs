﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using STORAGE;
using System;
using ENGINE.Graphics;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;


namespace ENGINE.Logic.ObjAttributes
{
    [Serializable]
    public class ADialog : AttributeStandart
    {
        enum DialogState
        {
            Ready,
            Progress,
            Ending,
        }

        public List<DialogScreen> allDialogs;
        DialogState dialogState;


        float currentTime;
        float appearTime = 1;
        public GameTexture actorTexture;

        public static GameTexture dialogBG = new GameTexture(true, "Textures\\gameGUI\\dialog_screen");

        DialogScreen currentScreen;

        static SpriteFont dialogFont;
        public static SpriteFont DialogFont
        {
            get
            {
                if (dialogFont != null) return dialogFont;
                else
                {
                    dialogFont = Depository.CommonContent.Load<SpriteFont>("Source\\Fonts\\dialogFont");
                    return dialogFont;
                }
            }
        }

        public override void Initialize(GameObject sender, ENGINE.Logic.ObjAttributes.Functions.Attr attribute)
        {
            base.Initialize(sender, attribute);
            sender.data.WriteDataAttr("ADialog", this, Functions.Attr.Dialog);

            dialogState = DialogState.Ready;
            allDialogs = new List<DialogScreen>();
            actorTexture = new GameTexture(true, @"Textures\System\TransparentSquare");
            actorTexture.Layer = 0.0f;

            dialogBG.Layer = 0.2f;
        }

        public static GameTexture texture = new GameTexture(false, @"Textures\System\!");

        public override void Update(GameObject sender)
        {
            IsActive(Functions.Attr.Select, sender);

            if (Enable)
            {
                if (allDialogs.Count == 0) return;

                GameObject hero = GAME.EnviromentInfo.HERO.Hero;
                if (hero == null) return;
                Rectangle rect = sender.Gtexture.GetTextureArea();
                Rectangle heroRect = hero.Gtexture.GetTextureArea();
                float elapsed = (float)Depository.Elapsed;
                currentTime += elapsed;

                switch (dialogState)
                {
                    case DialogState.Ready:
                        if (rect.Intersects(heroRect) && Input.IsNewKeyPressed(Config.KeyboardCfg.ActionKey))
                        {
                            dialogState = DialogState.Progress;
                            currentTime = 0;
                            currentScreen = allDialogs.Find(d => d.isStartingScreen);
                            if (currentScreen == null) currentScreen = allDialogs[0];
                            (hero.data.ReadData("MController") as MoveController).inDialog = true;
                        }
                        break;
                    case DialogState.Progress:
                        currentScreen.Update();
                        if (rect.Intersects(heroRect) && Input.IsNewKeyPressed(Config.KeyboardCfg.ActionKey))
                        {
                            currentScreen.ResetScreen();
                            dialogState = DialogState.Ready;
                            (hero.data.ReadData("MController") as MoveController).inDialog = false;
                        }
                        if (Input.isMouseScrollDown() || Input.IsNewKeyPressed(Keys.Down))
                        {
                            currentScreen.ScrollDown();                       
                        }
                        if (Input.isMouseScrollUp() || Input.IsNewKeyPressed(Keys.Up))
                        {
                            currentScreen.ScrollUp();
                        }
                        break;
                    case DialogState.Ending:
                        if (currentTime > appearTime)
                        {
                            dialogState = DialogState.Ready;
                            (hero.data.ReadData("MController") as MoveController).inDialog = false;
                        }
                        break;
                    default:
                        break;
                }
            }

        }

        public static bool DIALOGENABLED = false;

        public override void Draw(GameObject sender)
        {
            base.Draw(sender);
            DIALOGENABLED = false;
            if (currentScreen == null) return;
            if (Enable)
            {
                switch (dialogState)
                {
                    case DialogState.Ready:
                        //вывод приглашения к диалогу над нпс
                        break;
                    case DialogState.Progress:
                        {
                            DIALOGENABLED = true;
                            currentScreen.Draw(sender, currentTime);
                            if (actorTexture != null)
                            {
                                Vector2 positionOnScreen = new Vector2(Depository.ScreenResolution.X * 0.65f, Depository.ScreenResolution.Y * 5 / 6 - 180);
                                actorTexture.Position = GAME.CursorManager.ScreenToWorld(positionOnScreen, GAME.GameManager.LvlController.camera.GetTransformation());
                                actorTexture.Draw();
                            }
                            break;
                        }
                    case DialogState.Ending:
                        //закрывание диалога
                        break;
                    default:
                        break;
                }
            }
        }

        public void SetNewScreen(DialogScreen newscreen)
        {
            if (newscreen != null)
            {
                currentScreen = newscreen;
                dialogState = DialogState.Progress;
                currentTime = 0;
            }
            else
            {
                currentScreen = null;
                dialogState = DialogState.Ending;
            }
        }

    }


    [Serializable]
    public class DialogScreen
    {
        [Serializable]
        public struct Response
        {
            public string answer;
            public DialogScreen targetScreen;
        }

        static int DialogCount = 0;

        public int id;
        List<Response> answers;
        public int Count { get { return answers.Count; } }
        public bool isStartingScreen;
        string name;
        public string Name { get { return name == "" ? "New screen " + id : name; } set { name = value; } }
        ADialog parent;
        public List<string> AnswersText { get { RefreshShortList(); return shortAnswers; } }
        public List<Response> Answers { get { return answers; } }
        List<string> shortAnswers;
        string text;
        public string Text { get { return text; } set { text = value; } }
        float symbolDelay = 0.01f;
        int topAnswer = 0;

        public DialogScreen(ADialog parent, string name = "")
        {
            answers = new List<Response>();
            shortAnswers = new List<string>();
            this.parent = parent;
            id = DialogCount++;
            this.Name = name;
            this.text = "npc answer text";
            Response r;
            r.answer = "Хорошо";
            r.targetScreen = null;
            answers.Add(r);
            r.answer = "Пойдем с нами";
            answers.Add(r);
            r.answer = "Хорошая идея";
            answers.Add(r);
            r.answer = "Да";
            answers.Add(r);
            r.answer = "Идем";
            answers.Add(r);
        }

        public void Update()
        {
            Keys[] keys = Input.GetKeys();
            foreach (var key in keys)
            {
                int k = (int)key - 48;
                if (k > 0 && k < answers.Count)
                {
                    if (Input.IsNewKeyPressed(key))
                    {
                        ResetScreen();
                        parent.SetNewScreen(answers[k - 1].targetScreen);
                    }
                }
            }


        }


        public void Draw(GameObject sender, float curentTime)
        {
            if (curentTime == 0) return;
            float marginX = 50;
            float marginY = 60;


            Matrix view = GAME.GameManager.LvlController.camera.GetTransformation();
            float scale = 1 / view.M11;
            string wrapText = WrapText(this.Text);

            Color textColor = new Color(20, 20, 20);

            Rectangle onScreen = GetScreen(4, marginY); // 4 ответа одновременно на экране
            string textOnScreen = SplitText(wrapText, curentTime);

            #region Draw BG & Main Text
            ADialog.dialogBG.DrawToDestination(onScreen);
            Vector2 answerPosition = new Vector2(onScreen.X + marginX * scale, onScreen.Y + marginY * scale + 30);
            Depository.SpriteBatch.DrawString(ADialog.DialogFont, textOnScreen, answerPosition - new Vector2(0, marginY * scale ), textColor, 0, Vector2.Zero, scale, SpriteEffects.None, 0.05f);
            #endregion


            #region Draw Answers
            for (int i = topAnswer; i < answers.Count && i < topAnswer + 4; i++)
            {
                Response item = answers[i];
                Vector2 origin = ADialog.DialogFont.MeasureString(item.answer) * 0.5f;
                Depository.SpriteBatch.DrawString(ADialog.DialogFont, (i + 1) + ". " + item.answer, answerPosition, textColor, 0, Vector2.Zero, scale, SpriteEffects.None, 0.05f);
                answerPosition.Y += 30 * scale;
            }

            #endregion

            if (Config.EditorMode) Grid.BatchRectangle(onScreen, Color.Purple);
        }

        private Rectangle GetScreen(int lineCount, float margin)
        {
            Matrix view = GAME.GameManager.LvlController.camera.GetTransformation();
            int rWidth = (int)(Depository.ScreenResolution.X * 0.7f);
            int rHeight = (int)(lineCount * 30 + 2 * margin);
            Rectangle onScreen = new Rectangle((int)(Depository.ScreenResolution.X * 0.5f) - rWidth / 2, (int)(Depository.ScreenResolution.Y * 5 / 6 - rHeight / 2), rWidth, rHeight);
            onScreen = GAME.CursorManager.ScreenToWorld(onScreen, view);
            return onScreen;
        }

        private string SplitText(string wrapText, float curentTime)
        {
            float totalTextOnScreen = MathHelper.Clamp(curentTime / symbolDelay, 0, wrapText.Length);
            int fullSymbol = (int)Math.Floor(totalTextOnScreen);
            return wrapText.Substring(0, fullSymbol);
        }

        public string GetAnswerText(int index)
        {
            return answers[index].answer;
        }

        private string WrapText(string str)
        {
            str = str.Replace("\n", "");
            str = str.Replace("\r", "");
            int maxWidth = (int)(Depository.ScreenResolution.X * 0.5f);
            string[] arr = str.Split(' ');
            string currentLine = "";
            str = "";
            for (int i = 0; i < arr.Length; i++)
            {
                currentLine += " " + arr[i];
                if (ADialog.DialogFont.MeasureString(currentLine).X > maxWidth)
                {
                    str += currentLine + "\n";
                    currentLine = "";
                }
            }
            str += currentLine;
            return str;
        }

        public void ScrollUp()
        {
            topAnswer = (int)MathHelper.Clamp(topAnswer - 1, 0, answers.Count - 4);
        }

        public void ScrollDown()
        {
            topAnswer = (int)MathHelper.Clamp(topAnswer + 1, 0, answers.Count - 4);
        }

        public void AddAnswer(string variant, DialogScreen screen)
        {
            Response r;
            r.answer = variant;
            r.targetScreen = screen;
            answers.Add(r);
        }

        public void DeleteAnswer(int num)
        {
            answers.RemoveAt(num);
        }

        public string SetAnswer(int index, string text)
        {
            if (index < 0) return "";
            Response r;
            r.answer = text;
            r.targetScreen = answers[index].targetScreen;
            answers[index] = r;
            RefreshShortList();
            return shortAnswers[index];
        }

        public DialogScreen GetTarget(int index)
        {
            if (index < 0) return null;
            return answers[index].targetScreen;
        }

        public void SetTarget(int index, DialogScreen screen)
        {
            Response r;
            r.answer = answers[index].answer;
            r.targetScreen = screen;
            answers[index] = r;
        }

        private void RefreshShortList()
        {
            shortAnswers.Clear();
            answers.ForEach(ans =>
            {
                if (ans.answer.Length > 8)
                    shortAnswers.Add(ans.answer.Substring(0, 8));
                else shortAnswers.Add(ans.answer);
            });
        }

        public void ResetScreen()
        {
            topAnswer = 0;
        }
    }
}

﻿using System;

namespace ENGINE.Logic.ObjAttributes
{
    [Serializable]
    public class ARpgCharacter : AttributeStandart
    {
        public override void Initialize(GameObject sender, Functions.Attr attribute)
        {
            sender.data.WriteDataAttr("ARpgCharacter", this, Functions.Attr.RpgCharacter);

            CharacterParams param = new CharacterParams();

            param.MaxHP = 100;
            param.MaxMp = 100;
            param.HP = 50;
            param.MP = 50;

            sender.data.WriteDataAttr("ARpgCharacterParams", param, Functions.Attr.RpgCharacter);

            base.Initialize(sender, attribute);
        }
        [Serializable]
        public class CharacterParams
        {
            public float HP
            {
                get { return hp; }
                set
                {
                    if (value <= maxhp & value >= 0) hp = value;
                    else if (value > maxhp) hp = maxhp;
                    else if (value < 0) hp = 0;
                }
            }
            public float MaxHP
            {
                get { return maxhp; }
                set
                {
                    if (value >= 0) maxhp = value;
                    if (hp > maxhp) hp = maxhp;
                }
            }
            public float MP
            {
                get { return mp; }
                set
                {
                    if (value <= maxmp & value >= 0) mp = value;
                    else if (value > maxmp) mp = maxmp;
                }
            }
            public float MaxMp
            {
                get { return maxmp; }
                set
                {
                    if (value >= 0) maxmp = value;
                    if (mp > maxmp) mp = maxmp;
                }
            }
            public int Coins;

            private float hp;
            private float maxhp;
            private float mp;
            private float maxmp;
        }

        public override void  Update(GameObject sender)
        {
            IsActive(Functions.Attr.Select, sender); // проверяет следует ли удалить аттрибут или перевести в разряд редакторных
        }
    }
}

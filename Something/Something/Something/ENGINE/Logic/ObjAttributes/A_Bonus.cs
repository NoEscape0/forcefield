﻿using System;
using Microsoft.Xna.Framework;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;


namespace ENGINE.Logic.ObjAttributes
{
    [Serializable]
    public class ABonus : AttributeStandart
    {
        public static int amplitude = 25;
        public static float Period { get { return 1 / invPeriod; } set { invPeriod = 1 / value; } }
        public Vector2 originalPosition;
        public static bool isBounced = true;

        float time = 0;
        static float invPeriod = 0.5f;

        public override void Initialize(GameObject sender, Functions.Attr attribute)
        {
            sender.data.WriteDataAttr("ABonus", this, Functions.Attr.Bonus);
            Bonus.BonusStandart st = new Bonus.BonusStandart();
            st.type = Bonus.BonusType.coin;
            st.value = 1;
            sender.data.WriteDataAttr("BonusStandart", st, Functions.Attr.Bonus);
            originalPosition = sender.Gtexture.Position;
            InitBody(sender);
            base.Initialize(sender, attribute);
        }
        public override void Update(GameObject sender)
        {
            base.Update(sender);

            Body MainBody = GetBody(sender);

            if (!(sender.data.ReadData("ADrag") as ADrag).Drag)
            {
                if (isBounced)
                {
                    sender.Gtexture.Position.Y = originalPosition.Y + amplitude * (float)Math.Sin(time * MathHelper.TwoPi * invPeriod + 0.015f * originalPosition.X);
                    time += 1 * (float)STORAGE.Depository.Elapsed;
                }
                else
                {
                    sender.Gtexture.Position = originalPosition;
                    time = 0;
                }
                MainBody.Position = ConvertUnits.ToSimUnits(sender.Gtexture.Position);
            }

        }
        public void OnCollisionWithBonusObj(GameObject hero, GameObject BonusObj)
        {
            if (Functions.HaveAttribute(Functions.Attr.RpgCharacter, hero))
            {
                Bonus.BonusAction.Action((ARpgCharacter.CharacterParams)hero.data.ReadData("ARpgCharacterParams"),
                    (Bonus.BonusStandart)BonusObj.data.ReadData("BonusStandart"));

                GAME.GameManager.LvlController.ObjectArray.Remove(BonusObj);
                GAME.GameManager.LvlController.PhysicWorld.RemoveBody(GetBody(BonusObj));
                BonusObj = null;

            }
        }
        public void CalculateAndSetHeight(float heigh, GameObject BonusObj, bool top)
        {
            if (top)
            {
                int ra = (int)GAME.EnviromentInfo.GetDistanceToNearestBarrier(GAME.EnviromentInfo.CollisionDirection.Top, BonusObj);
                if (ra < int.MaxValue & ra > heigh)
                {
                    BonusObj.Gtexture.Position.Y -= ra - heigh;
                    originalPosition = BonusObj.Gtexture.Position;
                }
            }
            else
            {
                int ra = (int)GAME.EnviromentInfo.GetDistanceToNearestBarrier(GAME.EnviromentInfo.CollisionDirection.Down, BonusObj);
                if (ra < int.MaxValue & ra > heigh)
                {
                    BonusObj.Gtexture.Position.Y += ra - heigh;
                    originalPosition = BonusObj.Gtexture.Position;
                }
            }

        }

        protected override void InitBody(GameObject owner)
        {
            Body mainBody = BodyFactory.CreateRectangle(GAME.GameManager.LvlController.PhysicWorld,
                                                  ConvertUnits.ToSimUnits(owner.Gtexture.TextureResolution.width) * owner.Gtexture.Scale.X,
                                                  ConvertUnits.ToSimUnits(owner.Gtexture.TextureResolution.height) * owner.Gtexture.Scale.Y,
                                                  1);
            mainBody.Position = ConvertUnits.ToSimUnits(owner.Gtexture.Position);
            mainBody.BodyType = BodyType.Static;
            mainBody.UserData = owner;
            mainBody.OnCollision += new OnCollisionEventHandler(Bonus_OnCollision);
            mainBody.IsSensor = true;
            owner.body = mainBody;
        }

        public bool Bonus_OnCollision(Fixture fixtureA, Fixture fixtureB, FarseerPhysics.Dynamics.Contacts.Contact contact)
        {
            GameObject player = fixtureB.Body.UserData as GameObject;
            if (player == null) return false;
            if (!Functions.HaveAttribute(Functions.Attr.MovementHero, player)) return true;
            OnCollisionWithBonusObj(player, fixtureA.Body.UserData as GameObject);
            return false;
        }
    }
}

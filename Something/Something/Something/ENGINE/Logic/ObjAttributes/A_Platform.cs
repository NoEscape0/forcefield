﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Joints;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using STORAGE;

namespace ENGINE.Logic.ObjAttributes
{
    [Serializable]
    class APlatform : ABarrier
    {
        [NonSerialized]
        private FixedRevoluteJoint platformJoint;
        //private FixedDistanceJoint testJoint;


        public override void Initialize(GameObject sender, Functions.Attr attribute)
        {
            base.Initialize(sender, attribute);
            bodyType = BodyType.Dynamic;
            sender.body.BodyType = bodyType;
            Vector2 localCenter = sender.body.GetLocalVector(sender.body.Position);

            //testJoint = JointFactory.CreateFixedDistanceJoint(GAME.GameManager.LvlController.PhysicWorld, sender.body, Vector2.Zero, Vector2.Zero);
            platformJoint = JointFactory.CreateFixedRevoluteJoint(GAME.GameManager.LvlController.PhysicWorld, sender.body, Vector2.Zero, sender.body.Position);
            //sender.body.FixedRotation = true;
            //testJoint.DampingRatio = 0.5f;
            //testJoint.Frequency = 1.0f;

        }

        public override void Update(GameObject sender)
        {
            base.Update(sender);
            Body MainBody = GetBody(sender);            
            if (sender.body.JointList == null) return;
        }

        protected override void InitBody(GameObject owner)
        {
            base.InitBody(owner);
            platformJoint = JointFactory.CreateFixedRevoluteJoint(GAME.GameManager.LvlController.PhysicWorld, owner.body, Vector2.Zero, owner.body.Position);
        }
    }
}

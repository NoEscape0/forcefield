﻿using System;
using System.Collections.Generic;
using STORAGE;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Microsoft.Xna.Framework;

namespace ENGINE.Logic.ObjAttributes
{
    [Serializable]
    public class AttributeStandart
    {
        public virtual void Draw(GameObject sender)
        {

        }

        public virtual void Update(GameObject sender)
        {
            IsActive(Functions.Attr.Barrier, sender); // проверяет следует ли удалить аттрибут или перевести в разряд редакторных
            if (!Enable) return;
            FarseerPhysics.Dynamics.Body MainBody = GetBody(sender);
            if (MainBody == null) return;
            #region Ручные изменения положения в редакторе
            Matrix m = Matrix.CreateTranslation(new Vector3(realCenter, 0)) * Matrix.CreateRotationZ(sender.rotation) * Matrix.CreateTranslation(new Vector3(-realCenter, 0));
            Vector2 tranlation = new Vector2(m.Translation.X, m.Translation.Y);
            if (oldPos != sender.Gtexture.Position || oldRot != sender.rotation)
            {
                MainBody.SetTransform(ConvertUnits.ToSimUnits(tranlation + sender.Gtexture.Position + realCenter), sender.rotation);
                MainBody.ResetDynamics();
                MainBody.Awake = true;
            }
            sender.Gtexture.Position = ConvertUnits.ToDisplayUnits(MainBody.Position) - (tranlation + realCenter);
            sender.rotation = MainBody.Rotation;
            oldPos = sender.Gtexture.Position;
            oldRot = sender.rotation;
            #endregion
        }

        public virtual void Initialize(GameObject sender, Functions.Attr attribute)
        {
            CurrentAttribute = attribute;
            InitComplete = true;
        }

        object DeepClone(object obj)
        {
            object objResult = null;
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, obj);

                ms.Position = 0;
                objResult = bf.Deserialize(ms);
            }
            return objResult;
        }

        public void IsActive(Functions.Attr attribute, GameObject sender)
        {
            //определяем удалили или добавили ли этот объект в массив эдиторОнли

            if (!Functions.HaveAttribute(CurrentAttribute, sender)) Enable = false;
            else
            {
                bool EditorOnly;

                List<Functions.Attr> edOnlyArrt = (List<Functions.Attr>)sender.data.ReadData("editorOnly_attr"); //вызываем массив для изменения аттрибутов

                if (edOnlyArrt.Contains(CurrentAttribute))
                    EditorOnly = true;
                else EditorOnly = false;


                if (EditorOnly)
                {
                    if (Config.EditorMode)
                    {
                        if (Enable == false) Initialize(sender, attribute);
                        Enable = true;
                    }
                    else Enable = false;
                }
                else
                {
                    if (Enable == false) Initialize(sender, attribute);
                    Enable = true;
                }
            }

        }


        public bool Enable = true;
        public Functions.Attr CurrentAttribute;
        [NonSerialized]
        protected bool InitComplete;
        protected Microsoft.Xna.Framework.Vector2 oldPos;
        public Microsoft.Xna.Framework.Vector2 realCenter = Microsoft.Xna.Framework.Vector2.Zero;
        protected float oldRot;

        public FarseerPhysics.Dynamics.Body GetBody(GameObject owner)
        {
            if (owner.body != null && owner.body.UserData != null && InitComplete) return owner.body;
            InitComplete = true;
            InitBody(owner);
            return owner.body;
        }

        protected virtual void InitBody(GameObject owner) { }
    }
}

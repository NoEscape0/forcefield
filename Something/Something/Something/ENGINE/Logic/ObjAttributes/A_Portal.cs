﻿using System;
using System.Collections.Generic;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;

namespace ENGINE.Logic.ObjAttributes
{
    enum PortalState
    {
        ready,
        exitDelay,
        transitionDelay
    }

    [Serializable]
    public class APortal : AttributeStandart
    {
        public GameObject dest;
        public float transitionTime = 0.01f;
        public float exitTime = 5.0f;
        float currentTime = 0;
        public bool isExit;
        public bool twoSided;
        PortalState state;
        public bool isInstant = false;
        public bool isInteractive = false;
        [NonSerialized]
        public bool inCollision = false;
        [NonSerialized]
        GameObject hero;

        public bool TwoSided
        {
            get { return twoSided; }
            set { this.twoSided = value; (dest.data.ReadData("APortal") as APortal).twoSided = value; }
        }

        public bool IsInstant
        {
            get { return isInstant; }
            set { this.isInstant = value; (dest.data.ReadData("APortal") as APortal).isInstant = value; }
        }

        public bool IsInteractive
        {
            get { return isInteractive; }
            set { this.isInteractive = value; (dest.data.ReadData("APortal") as APortal).isInteractive = value; }
        }


        public APortal()
        { }

        public override void Initialize(GameObject sender, Functions.Attr attribute)
        {
            sender.data.WriteDataAttr("APortal", this, Functions.Attr.Portal);
            dest = new GameObject(@"Textures\System\Marker");
            dest.Gtexture.Position = GAME.GameManager.LvlController.camera.CameraPosition;
            dest.Gtexture.Layer = 0.80f;

            dest.AddedAttributes.Add(Functions.Attr.Portal);
            dest.data.WriteDataAttr("APortal", new APortal(), Functions.Attr.Portal);
            (dest.data.ReadData("APortal") as APortal).dest = sender;
            (dest.data.ReadData("APortal") as APortal).state = PortalState.ready;
            (dest.data.ReadData("APortal") as APortal).isExit = true;
            (dest.data.ReadData("APortal") as APortal).CurrentAttribute = Functions.Attr.Portal;
            dest.OnUpdate += (dest.data.ReadData("APortal") as APortal).Update;
            List<Functions.Attr> edOnlyArrt2 = (List<Functions.Attr>)dest.data.ReadData("editorOnly_attr");
            edOnlyArrt2.Add(Functions.Attr.Draw);

            GameObject SelectedOBJ = (GameObject)GAME.GameManager.LvlController.CommonData.ReadData("SelectedOBJ");
            List<Functions.Attr> edOnlyArrt = (List<Functions.Attr>)SelectedOBJ.data.ReadData("editorOnly_attr");
            edOnlyArrt.Add(Functions.Attr.Draw);

            GAME.GameManager.LvlController.ObjectArray.Add(dest);
            GAME.GameManager.LvlController.SortByLayer();
            TwoSided = false;
            state = PortalState.ready;

            InitBody(sender);
            base.Initialize(sender, attribute);
        }

        public override void Update(GameObject sender)
        {
            GetBody(sender);

            IsActive(Functions.Attr.Portal, sender); // проверяет следует ли удалить аттрибут или перевести в разряд редакторных
            if (Enable)
            {
                if (inCollision) inCollisionWithPortalObj(hero);
                if (state == PortalState.exitDelay) { currentTime += (float)STORAGE.Depository.Elapsed; }
                if (currentTime > exitTime) { currentTime = 0; state = PortalState.ready; }

                if (isExit && !Functions.HaveAttribute(Functions.Attr.Portal, dest)) // внезапно нет входа
                {
                    GAME.GameManager.LvlController.ObjectArray.Remove(sender);
                }
            }
        }

        public bool inCollisionWithPortalObj(GameObject hero)
        {
            if (hero == null) return false;
            if (isExit && !twoSided) return false;
            switch (state)
            {
                case PortalState.ready:
                    if (!IsInteractive || STORAGE.Input.IsKeyPressed(Microsoft.Xna.Framework.Input.Keys.Space))
                    {
                        currentTime = 0;
                        state = PortalState.transitionDelay;
                    }
                    break;
                case PortalState.exitDelay:
                    break;
                case PortalState.transitionDelay:
                    currentTime += (float)STORAGE.Depository.Elapsed;
                    if (currentTime > transitionTime)
                    {
                        currentTime = 0;
                        state = PortalState.exitDelay;
                        hero.Gtexture.Position = dest.Gtexture.Position;
                        hero.body.Position = ConvertUnits.ToSimUnits(hero.Gtexture.Position);
                        (dest.data.ReadData("APortal") as APortal).state = PortalState.exitDelay;
                        (dest.data.ReadData("APortal") as APortal).inCollision = true;
                        (dest.data.ReadData("APortal") as APortal).currentTime = 0;
                        this.inCollision = false;
                        state = PortalState.ready;
                        hero.DeAttach();
                        if (isInstant && !STORAGE.Config.EditorMode) GAME.GameManager.LvlController.camera.CameraPosition = hero.Gtexture.Position;
                        hero = null;
                        return false;
                    }
                    else
                    {
                        //hero.Gtexture.Position = PortalObj.Gtexture.Center - hero.Gtexture.Origin;
                        //currentTime += (float)STORAGE.Depository.Elapsed;
                        return false;
                    }
                //break;
                default:
                    break;
            }
            return false;
        }

        public override void Draw(GameObject sender)
        {
            if (STORAGE.Config.EditorMode)
            {
                ENGINE.Graphics.Grid.DrawLine(sender.Gtexture.Position, dest.Gtexture.Position, Microsoft.Xna.Framework.Color.Green, GAME.GameManager.LvlController.camera);
            }
            base.Draw(sender);
        }

        protected override void InitBody(GameObject owner)
        {
            Body mainBody = BodyFactory.CreateRectangle(GAME.GameManager.LvlController.PhysicWorld,
                                                  ConvertUnits.ToSimUnits(owner.Gtexture.TextureResolution.width) * owner.Gtexture.Scale.X,
                                                  ConvertUnits.ToSimUnits(owner.Gtexture.TextureResolution.height) * owner.Gtexture.Scale.Y,
                                                  1);
            mainBody.Position = ConvertUnits.ToSimUnits(owner.Gtexture.Position);
            mainBody.Rotation = owner.rotation;
            mainBody.BodyType = BodyType.Static;
            mainBody.UserData = owner;
            mainBody.OnCollision += new OnCollisionEventHandler(Portal_OnCollision);
            mainBody.OnSeparation += new OnSeparationEventHandler(Portal_OnSeparation);
            mainBody.IsSensor = true;
            owner.body = mainBody;
            //GAME.GameManager.LvlController.ObjectArray.Add(dest);
            //GAME.GameManager.LvlController.SortByLayer();
        }

        private bool Portal_OnCollision(Fixture fixtureA, Fixture fixtureB, FarseerPhysics.Dynamics.Contacts.Contact contact)
        {
            GameObject player = fixtureB.Body.UserData as GameObject;
            if (player == null) return false;
            if (!Functions.HaveAttribute(Functions.Attr.MovementHero, player)) return false;
            inCollision = true;
            hero = player;
            return true;
        }
        private void Portal_OnSeparation(Fixture fixtureA, Fixture fixtureB)
        {
            inCollision = false;
            hero = null;
            state = PortalState.ready;
        }
    }
}

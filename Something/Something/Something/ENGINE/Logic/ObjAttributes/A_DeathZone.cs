﻿using System;
using Microsoft.Xna.Framework;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;


namespace ENGINE.Logic.ObjAttributes
{
    [Serializable]
    public class ADeathZone : AttributeStandart
    {
        private bool inCollision = false;
        [NonSerialized]
        private GameObject hero = null;

        public override void Initialize(GameObject sender, Functions.Attr attribute)
        {
            sender.data.WriteDataAttr("ADeathZone", this, Functions.Attr.DeathZone);
            InitBody(sender);
            base.Initialize(sender, attribute);
        }

        public bool InstantDeath = false;
        public bool AbsoluteValue = true; // отнимает определенное кол во или % от максимального хп или 
        public float DecreaseValue = 10; //сколько хп или %от максимального хп отнимается за секунду


        public override void Update(GameObject sender)
        {
            Body b = GetBody(sender);
            if (inCollision && hero != null)
            {
                //collision with death zone
                if (Functions.HaveAttribute(Functions.Attr.RpgCharacter, hero) & Functions.HaveAttribute(Functions.Attr.MovementHero, hero))
                {
                    if (InstantDeath) (hero.data.ReadData("ARpgCharacterParams") as ARpgCharacter.CharacterParams).HP = 0;
                    else
                    {
                        if (AbsoluteValue)
                            (hero.data.ReadData("ARpgCharacterParams") as ARpgCharacter.CharacterParams).HP -= (float)(DecreaseValue * STORAGE.Depository.Elapsed);
                        else (hero.data.ReadData("ARpgCharacterParams") as ARpgCharacter.CharacterParams).HP -= (hero.data.ReadData("ARpgCharacterParams") as ARpgCharacter.CharacterParams).MaxMp / 100.0f * DecreaseValue * (float)STORAGE.Depository.Elapsed;
                    }
                }
            }
            if (!Functions.HaveAttribute(Functions.Attr.Barrier, sender))
                base.Update(sender);
        }


        public bool DeathZone_OnCollision(Fixture fixtureA, Fixture fixtureB, FarseerPhysics.Dynamics.Contacts.Contact contact)
        {
            GameObject hero = fixtureB.Body.UserData as GameObject;
            if (hero == null)
                return false;

            if (!Functions.HaveAttribute(Functions.Attr.MovementHero, hero))
                return true;
            inCollision = true;
            this.hero = hero;
            GameObject deathZoneObj = fixtureA.Body.UserData as GameObject;
            return false;
        }

        public void DeathZone_OnSeparation(Fixture fixtureA, Fixture fixtureB)
        {
            GameObject hero = fixtureB.Body.UserData as GameObject;
            if (!Functions.HaveAttribute(Functions.Attr.MovementHero, hero))
                return;
            inCollision = false;
            this.hero = null;
        }

        protected override void InitBody(GameObject owner)
        {
            Body mainBody = null;
            int MarginLeft, MarginRight, MarginTop, MarginDown;
            MarginLeft = MarginRight = MarginTop = MarginDown = 0;
            ABarrier barrier = owner.data.ReadData("ABarrier") as ABarrier;
            if (barrier != null)
            {
                MarginLeft = barrier.MarginLeft;
                MarginRight = barrier.MarginRight;
                MarginTop = barrier.MarginTop;
                MarginDown = barrier.MarginDown;
            }

            if (owner.body == null)
            {
                if (!owner.isCircleBody)
                {
                    mainBody = BodyFactory.CreateRectangle(GAME.GameManager.LvlController.PhysicWorld,
                                                          ConvertUnits.ToSimUnits(owner.Gtexture.TextureResolution.width * owner.Gtexture.Scale.X - MarginLeft - MarginRight),
                                                          ConvertUnits.ToSimUnits(owner.Gtexture.TextureResolution.height * owner.Gtexture.Scale.Y - MarginTop - MarginDown),
                                                          1);
                    realCenter = new Vector2(0.5f * (MarginLeft - MarginRight), 0.5f * (MarginTop - MarginDown));
                }
                else
                {
                    int totalMargin = MarginDown + MarginTop + MarginLeft + MarginRight;
                    mainBody = BodyFactory.CreateCircle(GAME.GameManager.LvlController.PhysicWorld,
                                                    ConvertUnits.ToSimUnits(owner.Gtexture.TextureResolution.height / 2 - totalMargin), 1,
                                                     owner);
                    realCenter = Vector2.Zero;
                }               
                realCenter = new Vector2(0.5f * (MarginLeft - MarginRight), 0.5f * (MarginTop - MarginDown));
                if (barrier != null) barrier.realCenter = realCenter;
                mainBody.Position = ConvertUnits.ToSimUnits(owner.Gtexture.Position + realCenter);
                mainBody.BodyType = BodyType.Static;
                //mainBody.UserData = owner;
            }
            else mainBody = owner.body;

            mainBody.OnCollision += new OnCollisionEventHandler(DeathZone_OnCollision);
            mainBody.OnSeparation += new OnSeparationEventHandler(DeathZone_OnSeparation);
            mainBody.UserData = owner;
            mainBody.IsSensor = true;
            owner.body = mainBody;
        }
    }
}

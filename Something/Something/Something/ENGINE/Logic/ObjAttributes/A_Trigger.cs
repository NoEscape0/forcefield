﻿using System;
using Microsoft.Xna.Framework;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;

namespace ENGINE.Logic.ObjAttributes
{
    [Serializable]
    public class ATrigger : AttributeStandart
    {
        public int Radius = 300;        
        public trigger_work_condition_enum WorkCondition = trigger_work_condition_enum.OnCollisionStart;
        public trigger_loop_type_enum LoopType = trigger_loop_type_enum.Once;
        public int CoolDownSec = 0;

        public float RealCDleftBeforeNextUsing = 0;

        public override void Initialize(GameObject sender, Functions.Attr attribute)
        {
            sender.data.WriteDataAttr("ATrigger", this, Functions.Attr.Trigger);
            InitBody(sender);
            base.Initialize(sender, attribute);
        }
        public override void Update(GameObject sender)
        {
            RealCDleftBeforeNextUsing -= (float)STORAGE.Depository.Elapsed;
            if (RealCDleftBeforeNextUsing < 0) RealCDleftBeforeNextUsing = 0;

            centrPos = sender.Gtexture.Position;

            if (inCollision) InCollision(sender);

            if (WorkCondition == trigger_work_condition_enum.Always & RealCDleftBeforeNextUsing == 0) DOWORK();


            if (!Functions.HaveAttribute(Functions.Attr.Barrier,sender))
                base.Update(sender);
        }

        public object work; //класс, наследник IScript

        //то, что выполняется по скрипту
        void DOWORK()
        {
            if (work != null)
            {
                bool workCool = (work as ScriptController.IScript).DoWork();

                if (workCool)
                {
                    if (STORAGE.Config.EditorMode & WorkCondition != trigger_work_condition_enum.Always) FloatingText.Create(FloatingText.TextType.SlideRandom, "Trigger: DOWORK()", 1.0f, Microsoft.Xna.Framework.Color.Green, centrPos, 4, 9);
                }
                else
                {
                    if (STORAGE.Config.EditorMode) FloatingText.Create(FloatingText.TextType.SlideRandom, "Trigger: ERROR", 1.0f, Microsoft.Xna.Framework.Color.Red, centrPos, 4, 9);
                }
            }
            else FloatingText.Create(FloatingText.TextType.SlideRandom, "Trigger: not enough settings", 1.0f, Microsoft.Xna.Framework.Color.Yellow, centrPos, 4, 9);


            //work done
            switch (LoopType)
            {
                case ATrigger.trigger_loop_type_enum.Once:
                    RealCDleftBeforeNextUsing = 50000;
                    break;
                case ATrigger.trigger_loop_type_enum.CoolDown:
                    RealCDleftBeforeNextUsing = CoolDownSec;
                    break;
                case ATrigger.trigger_loop_type_enum.Unlimited:
                    RealCDleftBeforeNextUsing = 0;
                    break;
            }

        }


        void InCollision(GameObject TriggerObj)
        {
            Body b = GetBody(TriggerObj);
            if (hero == null) return;

            
            //collision with death Trigger
            if (WorkCondition == trigger_work_condition_enum.InCollision & RealCDleftBeforeNextUsing == 0) DOWORK();
        }
        bool OnCollisionStart(Fixture fixtureA, Fixture fixtureB, FarseerPhysics.Dynamics.Contacts.Contact contact)
        {
            GameObject hero = fixtureB.Body.UserData as GameObject;
            if (hero == null)
                return false;

            if (!Functions.HaveAttribute(Functions.Attr.MovementHero, hero))
                return true;
            inCollision = true;
            this.hero = hero;
            GameObject TriggerObj = fixtureA.Body.UserData as GameObject;



            //collision start
            if (WorkCondition == trigger_work_condition_enum.OnCollisionStart & RealCDleftBeforeNextUsing == 0) 
                DOWORK();

            return false;
        }
        void OnCollisionFinish(Fixture fixtureA, Fixture fixtureB)
        {
            GameObject hero = fixtureB.Body.UserData as GameObject;
            if (!Functions.HaveAttribute(Functions.Attr.MovementHero, hero))
                return;
            inCollision = false;

            GameObject TriggerObj = fixtureA.Body.UserData as GameObject;



            //collision finish
            if (WorkCondition == trigger_work_condition_enum.OnCollisionFinish & RealCDleftBeforeNextUsing == 0) DOWORK();


            this.hero = null;
        }


        public enum trigger_work_condition_enum
        {
            OnCollisionStart, InCollision, OnCollisionFinish, Always
        }
        public enum trigger_loop_type_enum
        {
            Unlimited, Once, CoolDown
        }

        public void ChangeRadius(GameObject owner)
        {
            owner.body = null;
            InitBody(owner);
        }

        public void RefreshRadius(GameObject owner)
        {
            owner.body = null;
            InitBody(owner);
        }

        #region private
        protected override void InitBody(GameObject owner)
        {
            Body mainBody = null;
            if (owner.body == null)
            {
                mainBody = BodyFactory.CreateRectangle(GAME.GameManager.LvlController.PhysicWorld,
                                                      ConvertUnits.ToSimUnits(owner.Gtexture.TextureResolution.width * owner.Gtexture.Scale.X + Radius),
                                                      ConvertUnits.ToSimUnits(owner.Gtexture.TextureResolution.height * owner.Gtexture.Scale.Y + Radius),
                                                      1);
                mainBody.Position = ConvertUnits.ToSimUnits(owner.Gtexture.Position);
                mainBody.BodyType = BodyType.Static;
                mainBody.UserData = owner;
            }
            else mainBody = owner.body;
            mainBody.OnCollision += new OnCollisionEventHandler(OnCollisionStart);
            mainBody.OnSeparation += new OnSeparationEventHandler(OnCollisionFinish);
            mainBody.IsSensor = true;
            owner.body = mainBody;
        }

        private bool inCollision = false;
        [NonSerialized]
        private GameObject hero = null;
        Vector2 centrPos = new Vector2(0, 0);
        #endregion
    }



}

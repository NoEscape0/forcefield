﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using ENGINE.Graphics;
using STORAGE;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;

namespace ENGINE.Logic.ObjAttributes
{
    [Serializable]
    public class ABarrier : AttributeStandart
    {
        static Rectangle dotRectangle = new Rectangle(0, 0, 50, 50);
        static GameTexture marker = new GameTexture(true, @"Textures\System\Marker");
        static GameTexture Marker
        {
            get
            {
                if (marker == null || marker.Texture2d.IsDisposed) marker = new GameTexture(true, @"Textures\System\Marker");
                return marker;
            }
        }

        public bool testMoving = true;
        public bool isCircular = true;
        public bool isInstant = false;

        public float speed = 300;
        public List<Vector2> waypoints;
        public bool moveWaypoints = true;

        private bool isReversemove = false;
        private int moveWaypoint = -1;
        private Vector2 CoordIn;
        private int currentWaypoint = 0;

        public BodyType bodyType = BodyType.Static;

        public override void Initialize(GameObject sender, Functions.Attr attribute)
        {
            sender.data.WriteDataAttr("ABarrier", this, Functions.Attr.Barrier);
            base.Initialize(sender, attribute);
            waypoints = new List<Vector2>();
            Vector2 v = new Vector2(0, 300);
            waypoints.Add(sender.Gtexture.Position);
            waypoints.Add(sender.Gtexture.Position - v);
            InitBody(sender);
        }

        public int MarginLeft = 0;
        public int MarginRight = 0;
        public int MarginTop = 0;
        public int MarginDown = 0;

        public bool stopMovement = true;

        public override void Update(GameObject sender)
        {
            Vector2 correction = sender.Gtexture.Position - oldPos;
            base.Update(sender);
            Body MainBody = GetBody(sender);
            if (MainBody == null) return;


            #region коррекция точек пути для движущихся платфори
            if (moveWaypoints || MainBody.BodyType != BodyType.Kinematic)
            {
                for (int i = 0; i < waypoints.Count; i++)
                {
                    waypoints[i] += correction;
                }
            }
            #endregion

            if (MainBody.BodyType == BodyType.Dynamic) CheckFloor(sender);

            if (MainBody.BodyType == BodyType.Dynamic && sender.attachedTo != null && !Functions.HaveAttribute(Functions.Attr.MovementHero, sender))
                MainBody.LinearVelocity = sender.attachedTo.body.LinearVelocity;

            if (MainBody.BodyType != BodyType.Kinematic) return;

            #region mousedrag waypoints
            bool SmthAlreadyDragging = (bool)GAME.GameManager.LvlController.CommonData.ReadData("SmthAlreadyDragging");
            if (Input.IsLeftButtonClick() && !SmthAlreadyDragging)
            {
                Vector2 mouse = GAME.CursorManager.ScreenToWorld(Input.MouseScreenVector(), GAME.GameManager.LvlController.camera.GetTransformation());

                for (int i = 0; i < waypoints.Count; i++)
                {
                    dotRectangle.Offset(ToPoint(waypoints[i] - Marker.Origin));
                    if (Input.isHover(dotRectangle, mouse)) { moveWaypoint = i; CoordIn = mouse - waypoints[i]; GAME.GameManager.LvlController.CommonData.WriteDataSimple("SmthAlreadyDragging", true); }
                    dotRectangle.Offset(ToPoint(-waypoints[i] + Marker.Origin));
                }
            }

            if (Input.IsLeftButtonRelease()) { moveWaypoint = -1; GAME.GameManager.LvlController.CommonData.WriteDataSimple("SmthAlreadyDragging", false); return; }

            if (moveWaypoint != -1 && Input.IsLeftButtonPress())
            {
                waypoints[moveWaypoint] = GAME.CursorManager.ScreenToWorld(Input.MouseScreenVector(), GAME.GameManager.LvlController.camera.GetTransformation()) - CoordIn;
            }
            #endregion

            #region moving
            if (testMoving)
            {
                int nextwaypoint = -1;
                nextwaypoint = (currentWaypoint + (isReversemove ? -1 : 1)) % waypoints.Count;
                if (nextwaypoint == 0)
                {
                    if (isInstant)
                    {
                        isReversemove = false;
                        nextwaypoint = 1;
                        currentWaypoint = 0;
                        oldPos = sender.Gtexture.Position = waypoints[currentWaypoint];
                        MainBody.Position = ConvertUnits.ToSimUnits(sender.Gtexture.Position);
                    }
                    else
                        if (!isCircular)
                        {
                            isReversemove = true;
                            nextwaypoint = currentWaypoint - 1;
                        }
                }
                if (nextwaypoint == -1) { isReversemove = false; nextwaypoint = 1; }
                if (isNear(sender.Gtexture.Position, waypoints[nextwaypoint])) { currentWaypoint = nextwaypoint; return; }
                Vector2 direction = waypoints[nextwaypoint] - sender.Gtexture.Position;
                if (direction != Vector2.Zero) direction.Normalize();
                MainBody.LinearVelocity = direction * ConvertUnits.ToSimUnits(speed);
            }
            else
            {
                waypoints[0] = sender.Gtexture.Position;
                currentWaypoint = 0;
            }
            #endregion

        }

        bool isNear(Vector2 v1, Vector2 v2)
        {
            return (v1 - v2).LengthSquared() < speed * speed * Depository.Elapsed * Depository.Elapsed;
        }

        Point ToPoint(Vector2 v)
        {
            return new Point((int)v.X, (int)v.Y);
        }

        public override void Draw(GameObject sender)
        {
            if (bodyType != BodyType.Kinematic) return;
            if (!Config.EditorMode) return;

            for (int i = 0; i < waypoints.Count; i++)
            {
                Marker.Draw(waypoints[i]);
                if (i < waypoints.Count - 1) Grid.BatchLine(waypoints[i], waypoints[i + 1], Color.Blue);
            }
            if (isCircular) Grid.BatchLine(waypoints[waypoints.Count - 1], waypoints[0], Color.Blue);
            base.Draw(sender);
        }

        private bool Barrier_OnCollision(Fixture fixtureA, Fixture fixtureB, FarseerPhysics.Dynamics.Contacts.Contact contact)
        {
            if (fixtureB.IsSensor) return false;
            return true;
        }
        private void Barrier_OnSeparation(Fixture fixtureA, Fixture fixtureB)
        {
        }

        protected override void InitBody(GameObject owner)
        {
            Body mainBody = null;
            if (Functions.HaveAttribute(Functions.Attr.MovementHero, owner)) return;
            if (owner.body != null) return;
            if (!owner.isCircleBody)
            {
                mainBody = BodyFactory.CreateRectangle(GAME.GameManager.LvlController.PhysicWorld,
                                                      ConvertUnits.ToSimUnits(owner.Gtexture.TextureResolution.width * owner.Gtexture.Scale.X - MarginLeft - MarginRight),
                                                      ConvertUnits.ToSimUnits(owner.Gtexture.TextureResolution.height * owner.Gtexture.Scale.Y - MarginTop - MarginDown),
                                                      1);
                realCenter = new Vector2(0.5f * (MarginLeft - MarginRight), 0.5f * (MarginTop - MarginDown));
            }
            else
            {
                int totalMargin = MarginDown + MarginTop + MarginLeft + MarginRight;
                mainBody = BodyFactory.CreateCircle(GAME.GameManager.LvlController.PhysicWorld,
                                                ConvertUnits.ToSimUnits(owner.Gtexture.TextureResolution.height / 2 - totalMargin), 1,
                                                 owner);
                realCenter = Vector2.Zero;
            }

            mainBody.Position = ConvertUnits.ToSimUnits(owner.Gtexture.Position + realCenter);
            mainBody.Rotation = owner.rotation;
            mainBody.BodyType = bodyType;
            mainBody.FixedRotation = true;
            mainBody.UserData = owner;
            mainBody.OnCollision += new OnCollisionEventHandler(Barrier_OnCollision);
            mainBody.OnSeparation += new OnSeparationEventHandler(Barrier_OnSeparation);
            mainBody.UserData = owner;
            owner.body = mainBody;
        }

        private void CheckFloor(GameObject Subject)
        {
            if (Subject.body != null)
            {
                bool up, down = false;
                GameObject platform = null;
                bool gravitySign = Math.Sign(GAME.GameManager.LvlController.Gravity.Y) > 0 ? true : false;
                up = GAME.EnviromentInfo.CheckCollision(Subject, GAME.EnviromentInfo.CollisionDirection.Top, out platform);
                if (!up)
                    down = GAME.EnviromentInfo.CheckCollision(Subject, GAME.EnviromentInfo.CollisionDirection.Down, out platform);
                if (platform == null) { Subject.DeAttach(); return; }

                if (gravitySign) // обычная гравитация
                {
                    if (down)
                    {
                        if (Subject.attachedTo != platform && !Functions.HaveAttribute(Functions.Attr.MovementHero, platform))
                            platform.AttachObject(Subject);
                    }
                    else
                        Subject.DeAttach();
                }
                else
                    if (up)
                    {
                        if (Subject.attachedTo != platform && !Functions.HaveAttribute(Functions.Attr.MovementHero, platform))
                            platform.AttachObject(Subject);
                    }
                    else
                        Subject.DeAttach();
            }
        }
    }
}

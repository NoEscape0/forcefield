﻿using STORAGE;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using ENGINE.Graphics;

namespace ENGINE.Logic.ObjAttributes
{
    [Serializable]
    public class ADraw : AttributeStandart
    {
        public static GameTexture CircleTexture = new GameTexture(false, @"Textures\System\circle");

        public override void Initialize(ENGINE.Logic.GameObject sender, ENGINE.Logic.ObjAttributes.Functions.Attr attribute)
        {
            base.Initialize(sender, attribute);
        }

        public override void Update(GameObject sender)
        {
            sender.data.WriteDataAttr("ADraw", this, Functions.Attr.Draw);
        }

        public override void Draw(GameObject sender)
        {
            IsActive(Functions.Attr.Draw, sender); // проверяет следует ли удалить аттрибут или перевести в разряд редакторных

            if (Enable)
            {
                Rectangle obj = sender.Gtexture.GetTextureArea();
                Vector2 center = Vector2.Zero;
                var barrier = sender.data.ReadData("ABarrier") as ABarrier;
                if (barrier != null) center = barrier.realCenter;

                #region light area
                Rectangle lightR = new Rectangle();
                bool hasLight = false;
                if (sender.HasLight)
                {
                    hasLight = true;
                    ENGINE.Logic.Lights.Light light = (sender.data.ReadData("ALight") as ENGINE.Logic.ObjAttributes.ALight).light;
                    lightR = new Rectangle((int)(sender.Gtexture.Position.X - light.LightRadius), (int)(sender.Gtexture.Position.Y - light.LightRadius),
                        (int)(light.LightRadius * 2), (int)(light.LightRadius * 2));
                }
                #endregion

                if (Depository.DrawViewport.Intersects(obj) || hasLight & Depository.DrawViewport.Intersects(lightR))
                {
                    if ((sender.GetType()).BaseType != typeof(Particles.BaseEmitter) || Config.EditorMode)
                    {
                        if (!Functions.HaveAttribute(Functions.Attr.Animation, sender))
                        {
                            sender.Gtexture.Draw(sender.rotation);
                        }
                        else
                        {
                            sender.Gtexture.Draw(
                                (sender.data.ReadData("ANIMATION") as Animations).GetDrawRectangle(sender),
                                (sender.data.ReadData("ANIMATION") as Animations).GetDrawTexture(sender).Texture2d);
                        }

                        //editor only- lights
                        if (sender.HasLight & Config.EditorMode)
                        {
                            ENGINE.Logic.Lights.Light light = (sender.data.ReadData("ALight") as ENGINE.Logic.ObjAttributes.ALight).light;
                            Grid.BatchCircle(sender.Gtexture.Position, (int)(light.LightRadius), Color.Magenta);
                        }
                    }


                    SelectParams param = (sender.data.ReadData("SelectParams") as SelectParams);

                    if (param.Selectors_points & param.Selected)
                    {
                        List<Vector2> selectors = new List<Vector2>();
                        Matrix realPosMatrix = Matrix.CreateTranslation(new Vector3(-(sender.Gtexture.Position), 0)) *
                                   Matrix.CreateScale(new Vector3(sender.Gtexture.Scale.X, sender.Gtexture.Scale.Y, 1.0f)) *
                                   Matrix.CreateRotationZ(sender.rotation) *
                                   Matrix.CreateTranslation(new Vector3(sender.Gtexture.Position, 0));
                        selectors.Add(Vector2.Transform(sender.Gtexture.TopLeft, realPosMatrix));
                        selectors.Add(Vector2.Transform(new Vector2(sender.Gtexture.TopLeft.X + sender.Gtexture.TextureResolution.width, sender.Gtexture.TopLeft.Y), realPosMatrix));
                        selectors.Add(Vector2.Transform(new Vector2(sender.Gtexture.TopLeft.X + sender.Gtexture.TextureResolution.width, sender.Gtexture.TopLeft.Y + sender.Gtexture.TextureResolution.height), realPosMatrix));
                        selectors.Add(Vector2.Transform(new Vector2(sender.Gtexture.TopLeft.X, sender.Gtexture.TopLeft.Y + sender.Gtexture.TextureResolution.height), realPosMatrix));

                        foreach (var selectorPos in selectors)
                        {
                            param.GSelector.Draw(selectorPos);
                        }
                    }

                    //editor only- область органичения
                    if (Config.show_collision_rectangles & Functions.HaveAttribute(Functions.Attr.Barrier, sender) & Config.EditorMode)
                    {
                        //ABarrier barrier = (ABarrier)sender.data.ReadData("ABarrier");
                        FarseerPhysics.Dynamics.Body body = barrier.GetBody(sender);
                        if (body == null) return; // если не успело инициализироваться физ.тело
                        foreach (var fxl in body.FixtureList)
                        {
                            FarseerPhysics.Common.Transform t;
                            barrier.GetBody(sender).GetTransform(out t);
                            Color color = sender.body.BodyType == FarseerPhysics.Dynamics.BodyType.Kinematic ? Color.Blue : Color.Gold;
                            Grid.BatchShape(fxl.Shape, t, color);
                        }
                    }

                    //editor only- Trigger
                    if (Functions.HaveAttribute(Functions.Attr.Trigger, sender) & Config.EditorMode)
                    {
                        ATrigger trigger = (ATrigger)sender.data.ReadData("ATrigger");

                        for (int i = 0; i < trigger.GetBody(sender).FixtureList.Count; ++i)
                        {
                            FarseerPhysics.Common.Transform t;
                            trigger.GetBody(sender).GetTransform(out t);
                            Grid.BatchShape(trigger.GetBody(sender).FixtureList[i].Shape, t, Color.Green);
                        }

                        {
                            List<FarseerPhysics.Dynamics.Fixture> fxl = trigger.GetBody(sender).FixtureList;
                            FarseerPhysics.Common.Vertices vert = (fxl[0].Shape as FarseerPhysics.Collision.Shapes.PolygonShape).Vertices;
                            FarseerPhysics.Common.Transform t;
                            trigger.GetBody(sender).GetTransform(out t);
                            Matrix m = Matrix.CreateRotationZ(t.Angle);
                            m *= Matrix.CreateTranslation(new Vector3(ConvertUnits.ToDisplayUnits(t.Position), 0));

                            Grid.BatchLine(Vector2.Transform(ConvertUnits.ToDisplayUnits(vert[0]), m),
                                Vector2.Transform(ConvertUnits.ToDisplayUnits(vert[2]), m), Color.Green);


                            Grid.BatchLine(Vector2.Transform(ConvertUnits.ToDisplayUnits(vert[1]), m),
                                Vector2.Transform(ConvertUnits.ToDisplayUnits(vert[3]), m), Color.Green);
                        }
                    }


                    //editor only- enemy activate area
                    if (sender.GetType() == typeof(Enemy.Enemy))
                    {
                        //ABarrier barrier = (ABarrier)sender.data.ReadData("ABarrier");

                        List<FarseerPhysics.Dynamics.Fixture> fxl = barrier.GetBody(sender).FixtureList;
                        FarseerPhysics.Common.Vertices vert = (fxl[0].Shape as FarseerPhysics.Collision.Shapes.PolygonShape).Vertices;
                        FarseerPhysics.Common.Transform t;
                        barrier.GetBody(sender).GetTransform(out t);
                        Matrix m = Matrix.CreateRotationZ(t.Angle);
                        m *= Matrix.CreateTranslation(new Vector3(ConvertUnits.ToDisplayUnits(t.Position), 0));
                        Vector2 v1, v2;
                        for (int i = 0; i < vert.Count; i++)
                        {
                            v1 = Vector2.Transform(ConvertUnits.ToDisplayUnits(vert[i]), m);
                            v2 = Vector2.Transform(ConvertUnits.ToDisplayUnits(vert[(i + 1) % vert.Count]), m);
                            Color color = Color.Red;

                            switch (i)
                            {
                                case 0:
                                    {
                                        v1.Y += 10;
                                        v2.Y += 10;
                                        break;
                                    }
                                case 1:
                                    {
                                        v1.X -= 10;
                                        v2.X -= 10;
                                        break;
                                    }
                                case 2:
                                    {
                                        v1.Y -= 10;
                                        v2.Y -= 10;
                                        break;
                                    }
                                case 3:
                                    {
                                        v1.X += 10;
                                        v2.X += 10;
                                        break;
                                    }
                            }

                            Grid.BatchLine(v1, v2, color);
                        }
                    }


                    //editor only- система частиц
                    if ((sender.GetType()).BaseType == typeof(Particles.BaseEmitter) && Config.EditorMode)
                    {
                        CircleTexture.Draw(sender.Gtexture.Position, sender.Gtexture.Layer - 0.002f);
                    }
                }
            }
        }
    }
}

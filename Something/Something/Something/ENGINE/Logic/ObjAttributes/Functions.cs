﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


namespace ENGINE.Logic.ObjAttributes
{
    public static class Functions
    {
        /// <summary>
        /// при добавлении нового аттрибута нужно добавить в EditorMenu в SetTabs() отображение своей настройки и саму вкладку 
        /// </summary>
        public enum Attr
        {
            Draw,

            Select,

            Animation,

            Drag,

            Barrier,

            Platform,

            MovementHero,

            RpgCharacter,

            Bonus,

            Portal,

            Tooltip,

            DeathZone,

            Dialog,

            Light,

            Trigger
        }

        public static void AddAttribute(Attr attribute, GameObject objSender, bool EditorOnly = false)
        {
            //проверка 1 или более героев
            if (attribute == Attr.MovementHero)
            {
                GAME.GameManager.LvlController.RefindHero();
                if (GAME.GameManager.LvlController.HERO != null)  return;
            }




            if (attribute == Attr.Trigger)
            {
                if (Functions.HaveAttribute(Functions.Attr.MovementHero, objSender)) return;
            }

            if (attribute == Attr.Trigger)
            {
                if (Functions.HaveAttribute(Functions.Attr.Barrier, objSender)) return;
            }

            if (attribute == Attr.Barrier)
            {
                if (Functions.HaveAttribute(Functions.Attr.Trigger, objSender)) return;
            }


            if (!HaveAttribute(attribute, objSender))
            {
                //==============================
                switch (attribute)
                {
                    case Attr.Select:
                        {
                            ApplyFunctions(new ASelect(), objSender, Attr.Select);
                            break;
                        }

                    case Attr.Draw:
                        {
                            ApplyFunctions(new ADraw(), objSender, Attr.Draw);
                            break;
                        }
                    case Attr.Animation:
                        {
                            ApplyFunctions(new AAnimation(), objSender, Attr.Animation);
                            break;
                        }
                    case Attr.Drag:
                        {
                            if (!HaveAttribute(Attr.Select, objSender))
                                ApplyFunctions(new ASelect(), objSender, Attr.Select);

                            ApplyFunctions(new ADrag(), objSender, Attr.Drag);
                            break;
                        }
                    case Attr.Barrier:
                        {
                            ApplyFunctions(new ABarrier(), objSender, Attr.Barrier);
                            break;
                        }
                    case Attr.Platform:
                        {
                            ApplyFunctions(new APlatform(), objSender, Attr.Platform);
                            break;
                        }
                    case Attr.MovementHero:
                        {
                            ApplyFunctions(new AMovement(), objSender, Attr.MovementHero);
                            GAME.GameManager.LvlController.RefindHero();
                            break;
                        }
                    case Attr.RpgCharacter:
                        {
                            ApplyFunctions(new ARpgCharacter(), objSender, Attr.RpgCharacter);
                            break;
                        }
                    case Attr.Bonus:
                        {
                            ApplyFunctions(new ABonus(), objSender, Attr.Bonus);
                            break;
                        }
                    case Attr.Portal:
                        {
                            ApplyFunctions(new APortal(), objSender, Attr.Portal);
                            break;
                        }
                    case Attr.Tooltip:
                        {
                            ApplyFunctions(new ATooltip(), objSender, Attr.Tooltip);
                            break;
                        }
                    case Attr.DeathZone:
                        {
                            ApplyFunctions(new ADeathZone(), objSender, Attr.DeathZone);
                            break;
                        }
                    case Attr.Dialog:
                        {
                            ApplyFunctions(new ADialog(), objSender, Attr.Dialog);
                            break;
                        }
                    case Attr.Light:
                        {
                            ApplyFunctions(new ALight(), objSender, Attr.Light);
                            break;
                        }
                    case Attr.Trigger:
                        {
                            ApplyFunctions(new ATrigger(), objSender, Attr.Trigger);
                            break;
                        }
                }
                //==============================
                if (EditorOnly)
                {
                    List<Functions.Attr> edOnlyArrt = (List<Functions.Attr>)objSender.data.ReadData("editorOnly_attr"); //вызываем массив для изменения аттрибутов
                    edOnlyArrt.Add(attribute);
                    objSender.data.WriteDataSimple("editorOnly_attr", edOnlyArrt);
                }
            }

        }

        public static void DeleteAttribute(Attr attribute, GameObject obj)
        {
            if (obj.AddedAttributes.Contains(attribute))
            {
                obj.AddedAttributes.Remove(attribute);

                for (int i = 0; i < obj.data.pool.Count; ++i)
                {
                    if (obj.data.pool[i][2] != null)
                    {
                        try
                        {
                            if ((Functions.Attr)obj.data.pool[i][2] == attribute)
                            {
                                AttributeStandart standart = (AttributeStandart)obj.data.pool[i][1];

                                obj.OnUpdate -= standart.Update;
                                obj.OnDraw -= standart.Draw;

                                standart = null;
                                GAME.GameManager.LvlController.RefindHero();
                                obj.data.pool.RemoveAt(i);
                            }
                        }
                        catch { };
                    }
                }
                obj.data.DeleteData(attribute);
            }
        }

        public static bool HaveAttribute(Attr attribute, GameObject Obj)
        {
            if (Obj == null) return false;

            bool have = false;

            for (int i = 0; i < Obj.AddedAttributes.Count; ++i)
            {
                if (Obj.AddedAttributes[i] == attribute)
                {
                    have = true;
                    break;
                }
            }
            return have;
        }

        private static void ApplyFunctions(AttributeStandart standart, GameObject objSender, Attr attribute)
        {
            objSender.AddedAttributes.Add(attribute);
            standart.Initialize(objSender, attribute);
            objSender.OnUpdate += standart.Update;
            objSender.OnDraw += standart.Draw;
        }

        public static object DeepClone(object obj)
        {
            object objResult = null;
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, obj);

                ms.Position = 0;
                objResult = bf.Deserialize(ms);
            }
            return objResult;
        }
    }
}

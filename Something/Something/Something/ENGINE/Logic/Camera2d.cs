﻿using System;
using Microsoft.Xna.Framework;
using STORAGE;


namespace ENGINE.Logic
{
    [Serializable]
    public class Camera2d
    {
        public  Matrix GetTransformation()
        {
            if (Zoom <= 0)
            {
                Zoom = 1.0f;
            }

            transform = Matrix.CreateTranslation(new Vector3(-CameraPosition.X, -CameraPosition.Y, 0)) *
                Matrix.CreateRotationZ(Rotation) * Matrix.CreateScale(new Vector3(Zoom, Zoom, 1)) *
                Matrix.CreateTranslation(new Vector3(Depository.ScreenResolution.X * 0.5f, Depository.ScreenResolution.Y *
                0.5f, 0));

            return transform;
        }
        public  void ResetCamera()
        {
            CameraPosition = new Vector2(0, 0);
            Zoom = 1f;
            scrollValueWas = 0;
            step = 0;
        }

        public Matrix GetProjection()
        {
            return Matrix.CreateOrthographicOffCenter(0, Depository.ScreenResolution.X, Depository.ScreenResolution.Y, 0, 0, 1);
        }

        public  Vector2 CameraPosition;
        public float Zoom;
        public  bool IsCamMovingNow = false;
        public  float Rotation;

        public  int step;
        public  int scrollValueWas = 0;
        public  Matrix transform;

        public bool enabledzoom = true;
        public bool CameraCanMove = false;


        public void CamMoveAndZoom()
        {
            if ((CameraCanMove || Config.EditorMode) & Depository.GameFormActive)
            {
                if (GAME.CursorManager.ScreenPosition.X >= Depository.ScreenResolution.X - 2)
                {
                    CameraPosition += (new Vector2(+Config.CameraSpeed * (float)Depository.Elapsed / Zoom, 0));
                }

                if (GAME.CursorManager.ScreenPosition.X <= 0)
                {
                    CameraPosition += (new Vector2(-Config.CameraSpeed * (float)Depository.Elapsed / Zoom, 0));
                }

                if (GAME.CursorManager.ScreenPosition.Y <= 0)
                {
                    CameraPosition += (new Vector2(0, -Config.CameraSpeed * (float)Depository.Elapsed / Zoom));
                }
                

                if (GAME.CursorManager.ScreenPosition.Y > Depository.ScreenResolution.Y - 2)
                {
                    CameraPosition += (new Vector2(0, +Config.CameraSpeed * (float)Depository.Elapsed / Zoom));
                }
            }

            if (GAME.CursorManager.ScreenPosition.X >= Depository.ScreenResolution.X - 2 | GAME.CursorManager.ScreenPosition.X == 0 |
                GAME.CursorManager.ScreenPosition.Y == 0 | GAME.CursorManager.ScreenPosition.Y > Depository.ScreenResolution.Y - 2)
            IsCamMovingNow = true;
            else  IsCamMovingNow = false;

            if (enabledzoom & Depository.CanUseMouse)
            {
                int Msteps = 0;
                int Psteps = 0;

                if (Config.EditorMode)
                { Msteps = Config.maxMinusSteps_E; Psteps = Config.maxPlusSteps_E; }
                else { Msteps = Config.maxMinusSteps_G; Psteps = Config.maxPlusSteps_G; }

                if (Depository.mouseState.ScrollWheelValue > scrollValueWas && step < Psteps && Config.CanZoom)
                {
                    step++;
                    Zoom = Zoom * 2;
                }
                else
                {
                    if (Depository.mouseState.ScrollWheelValue < scrollValueWas && step > -Msteps
                    && Config.CanZoom)
                    {
                        step--;

                        Zoom = Zoom / 2;
                    }
                }
            }

            scrollValueWas = Depository.mouseState.ScrollWheelValue;
        }
    }
}
﻿using System.Collections.Generic;
using System;
using ENGINE.Logic.ObjAttributes;

namespace ENGINE.Logic
{
    [Serializable]
    public class DataPool
    {
        public DataPool()
        {
            pool = new List<object[]>();
        }

        public void WriteDataAttr(string dataname, object value, Functions.Attr attribute) // 0 - name 1-value
        {
            write(dataname, value, true, attribute);
        }

        public void WriteDataSimple(string dataname, object value) // 0 - name 1-value
        {
            write(dataname, value, false, Functions.Attr.Animation);
        }


        public object ReadData(string dataname)
        {
            for (int i = 0; i < pool.Count; ++i)
            {
                if ((string)pool[i][0] == dataname)
                {
                    return pool[i][1];
                }
            }
            return null;
        }

        public void DeleteData(string dataname)
        {
            for (int i = 0; i < pool.Count; ++i)
            {
                if ((string)pool[i][0] == dataname)
                {
                    pool.RemoveAt(i);
                }
            }
        }

        public void DeleteData(Functions.Attr attribute)
        {
            for (int i = 0; i < pool.Count; ++i)
            {
                if (pool[i][2] != null)
                {
                    if ((Functions.Attr)pool[i][2] == attribute)
                    {
                        pool.RemoveAt(i);
                    }
                }
            }
        }


        public List<object[]> pool;

        private void write(string dataname, object value, bool belong_to_attr, Functions.Attr attribute)
        {
            bool alreadyexist = false;
            int ifexistvalue = 0;
            for (int i = 0; i < pool.Count; ++i)
            {
                if ((string)pool[i][0] == dataname)
                {
                    alreadyexist = true;
                    ifexistvalue = i;
                    break;
                }
            }

            if (!alreadyexist)
            {
                object[] note = new object[3];
                note[0] = dataname;
                note[1] = value;

                if (belong_to_attr) note[2] = attribute;
                else note[2] = null;


                pool.Add(note);
            }
            else
            {
                pool[ifexistvalue][1] = value;

                if (belong_to_attr) pool[ifexistvalue][2] = attribute; else pool[ifexistvalue][2] = null;
            }
        }
    }
}

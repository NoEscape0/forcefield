﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ENGINE.Logic.Particles;
using Microsoft.Xna.Framework.Graphics;
using STORAGE;
using Microsoft.Xna.Framework;

namespace ENGINE.Logic
{
    [Serializable]
    public class Weather
    {
        public enum WeatherType
        {
            Rain,
            Snow,
            SandStorm,
            Mist,
        }

        ParticleEngine particleEngine;
        WeatherType type = WeatherType.Rain;

        //BaseEmitter weatherEmitter;
        public bool online = false;

        // stat
        /*
         speed = 2000-3500
         size =  0.2-1
         h = 1000
         w = 1500
         ttl = 50
         pps = 250
         до прозрачности 1с
         
        */
        float angle;
        float intensity = 1;
        List<BaseEmitter> emitters;

        public Weather()
        {
            particleEngine = new ParticleEngine("weather");
            ChangeType(WeatherType.Snow);

        }

        public void Update()
        {
            particleEngine.Update();
            foreach (var e in emitters)
            {
                e.Update();
            }
        }

        public void Draw()
        {
            if (!online) return;
            //SpriteBatch sb = Depository.SpriteBatch;
            particleEngine.Draw(Matrix.Identity, false);

        }

        public void SetIntensity(float newIntensity)
        {
            switch (type)
            {
                case WeatherType.Rain:
                    break;
                case WeatherType.Snow:
                    break;
                default:
                    break;
            }
        }

        public void ChangeType(WeatherType newType)
        {
            switch (newType)
            {
                case WeatherType.Rain:
                    CreateRain();
                    break;
                case WeatherType.Snow:
                    CreateSnow();
                    break;
                case WeatherType.Mist:
                    CreateMist();
                    break;
                default:
                    break;
            }
        }

        private void CreateRain()
        {
            emitters = new List<BaseEmitter>();
            LineEmitter e = new LineEmitter("Textures\\Particles\\lineParticle", particleEngine);
            e.minSpeed = 1500;
            e.maxSpeed = 3000;
            e.minSize = 0.2f;
            e.maxSize = 1.0f;
            e.width = Depository.ScreenResolution.X * 1.2f;
            e.height = 1000;
            e.Pps = 250;
            e.tMaxAlpha = 0;
            e.FullAlpha = 1;
            e.isBackLayer = false;
            e.Pos = e.prevPos = new Vector2(Depository.ScreenResolution.X * 0.5f, -100);
            e.startAngle = MathHelper.PiOver2;
            e.angleVelocity = 0;
            emitters.Add(e);

            e = new LineEmitter("Textures\\Particles\\longlineParticle", particleEngine);
            e.minSpeed = 2000;
            e.maxSpeed = 3500;
            e.minSize = 0.2f;
            e.maxSize = 1.0f;
            e.width = Depository.ScreenResolution.X * 1.2f;
            e.height = 1000;
            e.Pps = 250;
            e.tMaxAlpha = 0;
            e.FullAlpha = 1;
            e.isBackLayer = false;
            e.Pos = e.prevPos = new Vector2(Depository.ScreenResolution.X * 0.5f, -100);
            e.startAngle = MathHelper.PiOver2;
            e.angleVelocity = 0;
            emitters.Add(e);
        }

        private void CreateSnow()
        {
            emitters = new List<BaseEmitter>();
            LineEmitter e = new LineEmitter("Textures\\Particles\\Spikey001", particleEngine);
            e.minSpeed = 500;
            e.maxSpeed = 1000;
            e.minSize = 1.0f;
            e.maxSize = 5.0f;
            e.width = Depository.ScreenResolution.X * 1.5f;
            e.height = 1000;
            e.Pps = 250;
            e.tMaxAlpha = 0;
            e.FullAlpha = 3;
            e.isBackLayer = false;
            e.Pos = e.prevPos = new Vector2(Depository.ScreenResolution.X * 0.5f, -500);
            e.angleVelocity = 120 * MathHelper.Pi / 180;
            e.omega = 6;
            emitters.Add(e);
        }
        
        private void CreateMist()
        {
            emitters = new List<BaseEmitter>();
            LineEmitter e = new LineEmitter("Textures\\Particles\\Cloud002", particleEngine);
            e.minSpeed = 10;
            e.maxSpeed = 50;
            e.minSize = 20.0f;
            e.maxSize = 25.0f;
            e.width = Depository.ScreenResolution.X * 1.5f;
            e.height = Depository.ScreenResolution.Y;
            e.Pps = 10;
            e.tMaxAlpha = 1;
            e.FullAlpha = 3;
            e.isBackLayer = false;
            e.Pos = e.prevPos = new Vector2(Depository.ScreenResolution.X * 0.5f, Depository.ScreenResolution.Y * 0.5f);
            e.angleVelocity = 10 * MathHelper.Pi / 180;
            e.omega = 100f * MathHelper.Pi / 180;
            emitters.Add(e);
        }
    }
}

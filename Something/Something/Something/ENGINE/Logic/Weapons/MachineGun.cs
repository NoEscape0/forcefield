﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using FarseerPhysics.Dynamics;

namespace ENGINE.Logic.Weapons
{
    [Serializable]
    public class MachineGun : Weapon
    {
        float bulletSpeed = 700;
        float bulletSize;

        public MachineGun(GameObject owner)
            : base(owner)
        {
            RechargeRate = 0.2f;
            bulletSize = 0.5f;
        }

        public override bool Shoot(Vector2 direction)
        {
            if (!CanShoot) return false;
            currentTime = 0;
            Bullet b = new Weapons.Bullet(GAME.GameManager.LvlController.PhysicWorld, "Textures\\Particles\\p4", bulletSize, bulletSpeed * direction, owner.Gtexture.Position);
            b.body.Position = ConvertUnits.ToSimUnits(Position);
            //b.Gtexture.Center = owner.Gtexture.Center;                        
            b.emitter.maxSpeed = 1.1f;
            b.emitter.minSpeed = 1;
            b.emitter.sizeVel = 0.1f;
            b.emitter.maxSize = bulletSize;
            b.emitter.minSize = bulletSize;
            b.emitter.Pps = 800;
            b.emitter.color = Color.Green;
            //b.emitter.SetPos(Gtexture.Center);
            //b.emitter.Gtexture.Position = Gtexture.Position;
            b.emitter.alphaVel = 3000;
            return false;
        }

        public override void Draw(GameObject sender)
        {
            return;
        }
    }
}

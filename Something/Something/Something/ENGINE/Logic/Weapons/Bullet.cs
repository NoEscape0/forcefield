﻿using System;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;


namespace ENGINE.Logic.Weapons
{
    [Serializable]
    public class Bullet : GameObject
    {

        float radius;
        public Particles.DotEmitter emitter;
        float ttl = 3; // макс. время жизни.
        float damage = 10;
        [NonSerialized]
        protected bool InitComplete;

        Body Body { get { if (body == null) InitBody(); return body; } }

        public Bullet(World world, string path, float radius, Vector2 startSpeed, Vector2 position)
            : base(path)
        {
            this.radius = radius;
            body = BodyFactory.CreateCircle(world, ConvertUnits.ToSimUnits(radius), 1);
            body.BodyType = BodyType.Dynamic;
            body.LinearVelocity = ConvertUnits.ToSimUnits(startSpeed);
            body.IgnoreGravity = true;
            body.IsBullet = true;
            body.IsSensor = false;
            body.Position = ConvertUnits.ToSimUnits(position);
            body.OnCollision += new OnCollisionEventHandler(body_OnCollision);
            Gtexture.Position = position;
            GAME.GameManager.LvlController.ObjectArray.Add(this);
            OnUpdate += new Del(Bullet_firstOnUpdate);
            OnDraw += new Del(Bullet_OnDraw);
            emitter = new Particles.DotEmitter(GAME.GameManager.LvlController.ParticleEngine, path);
            emitter.maxSpeed = 5f;
            emitter.minSpeed = 2;
            emitter.sizeVel = 0.1f;
            emitter.maxSize = 1;
            emitter.minSize = 1;
            emitter.Pps = 1000;
            emitter.color = Color.Green;
            emitter.prevPos = emitter.Pos = Gtexture.Position;
            emitter.alphaVel = 150;
            isBullet = true;
        }

        bool body_OnCollision(Fixture fixtureA, Fixture fixtureB, FarseerPhysics.Dynamics.Contacts.Contact contact)
        {
            Body other = fixtureB.Body;
            GameObject otherObject = other.UserData as GameObject;

            if ((other.BodyType == BodyType.Static|| other.BodyType == BodyType.Kinematic) && !fixtureB.IsSensor)
            {
                EndBulletLife();
                return true;
            }
            if (otherObject != null && otherObject.isEnemy)
            {
                EndBulletLife();
                FloatingText ft = FloatingText.Create(FloatingText.TextType.SlideUp,
                                                       "-" + damage.ToString(),
                                                       3,
                                                       Color.Red,
                                                       otherObject.Gtexture.Position,
                                                       200, 1.0f);
                float angle = (float)GAME.GameManager.LvlController.Rnd.NextDouble() * MathHelper.PiOver4 + MathHelper.PiOver4 * 0.5f;
                angle *= Math.Sign(Body.LinearVelocity.X);
                Vector2 newSpeed = Vector2.Transform(-Vector2.UnitY * 200, Matrix.CreateRotationZ(angle));
                ft.Speed = newSpeed;
                return true;
            }
            return false;
        }

        void EndBulletLife()
        {
            ttl = -1;
            // излучаем напоследок пропорционально последнему отрезку
            Vector2 length = (float)STORAGE.Depository.Elapsed * ConvertUnits.ToDisplayUnits(Body.LinearVelocity);
            Gtexture.Position = emitter.Pos = ConvertUnits.ToDisplayUnits(Body.Position);
            float ppsCorrection = (emitter.Pos - emitter.prevPos).LengthSquared() / length.LengthSquared();
            emitter.Pps *= (float)Math.Sqrt(ppsCorrection);
            emitter.UpdateEmitter(null);
        }

        public void Bullet_OnUpdate(GameObject sender)
        {
            ttl -= (float)STORAGE.Depository.Elapsed;
            if (ttl < 0)
            {
                GAME.GameManager.LvlController.ObjectArray.Remove(this);
                GAME.GameManager.LvlController.PhysicWorld.RemoveBody(this.body);
                emitter = null;
                return;
            }
            Gtexture.Position = emitter.Pos = ConvertUnits.ToDisplayUnits(Body.Position);
            emitter.UpdateEmitter(null);
        }

        public void Bullet_firstOnUpdate(GameObject sender)
        {   //первый раз без излучения частиц
            ttl -= (float)STORAGE.Depository.Elapsed;
            if (ttl < 0)
            {
                GAME.GameManager.LvlController.ObjectArray.Remove(this);
                GAME.GameManager.LvlController.PhysicWorld.RemoveBody(this.body);
                emitter = null;
                return;
            }
            Gtexture.Position = emitter.Pos = ConvertUnits.ToDisplayUnits(Body.Position);
            OnUpdate -= new Del(Bullet_firstOnUpdate);
            OnUpdate += new Del(Bullet_OnUpdate);
        }
        public void Bullet_OnDraw(GameObject sender)
        {
            Gtexture.Draw();
        }

        protected void InitBody()
        {
            body = BodyFactory.CreateCircle(GAME.GameManager.LvlController.PhysicWorld, ConvertUnits.ToSimUnits(radius), 1);
            body.Position = ConvertUnits.ToSimUnits(Gtexture.Position);
            InitComplete = true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace ENGINE.Logic.Weapons
{
    [Serializable]
    public struct Segment
    {
        public Vector2 start;
        public Vector2 end;
        public float lum;
        public Segment(Vector2 start, Vector2 end, float lum = 1)
        {
            this.start = start;
            this.end = end;
            this.lum = lum;
        }
    }

    [Serializable]
    public class LightningGun : Weapon
    {
        float length = 6000;// максимальная длина разряда
        float squareSearchRadius = 80 * 80; // для поиска целей в области удара
        float fadeOut = 0.3f;
        List<Segment> lightning1 = new List<Segment>();
        List<Segment> lightning2 = new List<Segment>();
        float time1, time2 = 0;
        float damage = 1;

        public LightningGun(GameObject owner)
            : base(owner)
        {
            RechargeRate = 0.2f;
        }

        public override bool Shoot(Vector2 direction)
        {
            if (!CanShoot) return false;
            currentTime = 0;
            direction.Normalize();
            Vector2 worldMouse = GAME.CursorManager.GetWorldMouse();
            if ((worldMouse - Position).LengthSquared() < length * length)
                direction *= (worldMouse - Position).Length();
            else
                direction *= length;
            Vector2 strikePoint = Position + direction;
            GameObject enemy = FindTarget(strikePoint);
            if (enemy != null)
            {
                strikePoint = enemy.Gtexture.Position;
                HitEnemy(enemy);
            }
            time1 = time2 = 0;
            lightning1.Clear();
            lightning2.Clear();
            lightning1.Add(new Segment(Position, strikePoint));
            lightning2.Add(new Segment(Position, strikePoint));
            Generate(lightning1, 8);
            Generate(lightning2, 8);
            time1 = fadeOut;
            time2 = fadeOut * 0.5f;
            return true;
        }

        public override void Update()
        {
            base.Update();
            float elapsed = (float)STORAGE.Depository.Elapsed;
            time1 -= elapsed;
            time2 -= elapsed;
        }

        public override void Draw(GameObject sender)
        {
            Color c1 = Color.Lerp(new Color(0, 0, 0, 0), Color.White, time1 / fadeOut);
            Color c2 = Color.Lerp(new Color(0, 0, 0, 0), Color.White, time2 / fadeOut);
            if (lightning1.Count != 0) DrawLightning(lightning1, c1, 2);
            if (lightning2.Count != 0) DrawLightning(lightning2, c2, 2);
            return;
        }

        private GameObject FindTarget(Vector2 strikePoint)
        {
            foreach (var go in GAME.GameManager.LvlController.ObjectArray)
            {
                if (!go.isEnemy) continue;
                Vector2 length = go.Gtexture.Position - strikePoint;
                if (length.LengthSquared() < squareSearchRadius) return go;
            }
            return null;
        }

        private void HitEnemy(GameObject enemy)
        {
            //enemy.Hit(damage);
            FloatingText ft = FloatingText.Create(FloatingText.TextType.SlideUp,
                                                   "-" + damage.ToString(),
                                                   2,
                                                   Color.Cyan,
                                                   enemy.Gtexture.Position,
                                                   200, 10.5f);
            float angle = (float)GAME.GameManager.LvlController.Rnd.NextDouble() * MathHelper.PiOver4 + MathHelper.PiOver4 * 0.5f;
            angle *= Math.Sign(enemy.Gtexture.Position.X - owner.Gtexture.Position.X);
            Vector2 newSpeed = Vector2.Transform(-Vector2.UnitY * 10, Matrix.CreateRotationZ(angle));
            ft.Speed = newSpeed;
        }

        private void Generate(List<Segment> lightning, int totalGen)
        {
            Random rnd = GAME.GameManager.LvlController.Rnd;
            //float R2 = 0.6f;
            float R = 0.4f;
            for (int i = 0; i < lightning.Count; i++)
            {
                R /= 2;
            }

            for (int g = 0; g < totalGen; g++)
            {
                float startcount = lightning.Count;
                for (int i = 0; i < startcount; i++)
                {
                    Segment seg = lightning[0];
                    Vector2 start = seg.start;
                    Vector2 end = seg.end;
                    Vector2 line = end - start;
                    float length = line.Length();
                    if (length < 10) { continue; }
                    Vector2 normal = new Vector2(line.Y, -line.X);
                    normal.Normalize();
                    Vector2 midpoint = start + line / 2;
                    Vector2 newpoint = midpoint + R * normal * ((float)rnd.NextDouble() * 2 * length - length);
                    lightning.RemoveAt(0);

                    lightning.Add(new Segment(start, newpoint, seg.lum));
                    lightning.Add(new Segment(newpoint, end, seg.lum));
                    if (rnd.Next(100) < 100 / (g + 1) - 0)
                    {
                        Vector2 direction = newpoint - start;
                        direction = Vector2.Transform(direction, Matrix.CreateRotationZ((float)rnd.NextDouble() * 0.3f - 0.6f)) * 0.9f + newpoint;
                        lightning.Add(new Segment(newpoint, direction, seg.lum * 0.75f));
                    }

                }
                R *= 0.9f;
            }
        }

        private void DrawLightning(List<Segment> list, Color c, int width)
        {
            for (int i = 0; i < list.Count; i++)
            {
                Graphics.Grid.DrawLine(list[i].start, list[i].end, c * list[i].lum, GAME.GameManager.LvlController.camera, width);
            }
        }
    }
}

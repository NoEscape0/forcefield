﻿using System;
using Microsoft.Xna.Framework;

namespace ENGINE.Logic.Weapons
{
    [Serializable]
    public abstract class Weapon:IEquatable<Weapon>
    {
        public Weapon(GameObject owner)
        {
            this.owner = owner;
            currentTime = RechargeRate;
        }


        private float rechargeRate = 10; // в секундах
        protected float currentTime = 0;
        protected GameObject owner;
        protected Vector2 Position { get { if (owner == null) return Vector2.Zero; return owner.Gtexture.Position; } }
        public bool CanShoot
        {
            get { return currentTime >= RechargeRate; }
            set
            {
                if (value) currentTime = RechargeRate;
                else currentTime = 0;
            }
        }

        public float RechargeRate { get { return rechargeRate; } set { rechargeRate = value; } }

        public Bullet currentBullet;


        public virtual void Update()
        {
            currentTime += (float)STORAGE.Depository.Elapsed;
        }

        public virtual void Remove(GameObject sender)
        {
            sender.OnDraw -= Draw;
        }
        public abstract bool Shoot(Vector2 direction);
        public abstract void Draw(GameObject sender);

        public bool Equals(Weapon other)
        {
            return this.GetHashCode() == other.GetHashCode();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;

namespace ENGINE.Logic.Weapons
{
    [Serializable]
    public class Railgun : Weapon
    {
        float bulletSpeed = 8000;//при 60фпс макс скорость ~128*60 точек/сек :/
        float bulletSize;

        public Railgun(GameObject owner):base(owner)
        {
            RechargeRate = 2.0f;
            bulletSize = 0.5f;
        }

        public override bool Shoot(Vector2 direction)
        {
            if (!CanShoot) return false;
            currentTime = 0;
            Bullet b = new Weapons.Bullet(GAME.GameManager.LvlController.PhysicWorld, "Textures\\Particles\\p4", bulletSize, bulletSpeed * direction, owner.Gtexture.Position);
            b.body.Position = ConvertUnits.ToSimUnits(Position);
            //b.Gtexture.Center = owner.Gtexture.Center;                        
            b.emitter.maxSpeed = 0.1f;
            b.emitter.minSpeed = 0;
            b.emitter.sizeVel = 0.1f;
            b.emitter.maxSize = bulletSize;
            b.emitter.minSize = bulletSize;
            b.emitter.Pps = 3000;
            b.emitter.color = new Color(0,255,0);
            //b.emitter.SetPos(Gtexture.Center);
            //b.emitter.Gtexture.Position = Gtexture.Position;
            b.emitter.alphaVel = 150;
            ObjAttributes.Functions.AddAttribute(ObjAttributes.Functions.Attr.Light, b);
            var light = (b.data.ReadData("ALight") as ObjAttributes.ALight).light;
            light.LightRadius = 50;
            return false;
        }

        public override void Draw(GameObject sender)
        {
            return;
        }
    }
}

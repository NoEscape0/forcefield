﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ENGINE.Logic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ENGINE.Logic.Lights
{
    [Serializable]
    public class Light
    {
        public GameObject owner;
        public Vector2 offset;
        public float lightRadius;
        public float realRadius = 1000;
        public float scaleRadius = 1;
        public float invScaleRadius = 1;
        public Color color;

        //private GraphicsDevice graphicsDevice;
        [NonSerialized]
        public RenderTarget2D RenderTarget;
        [NonSerialized]
        bool init;
        public Vector2 LightPosition { get { return owner.Gtexture.Position + offset; } set { offset = value; } }
        public Vector2 LightAreaSize { get; set; }
        public float LightRadius { get { return realRadius; } set { realRadius = value; scaleRadius = this.lightRadius / value; invScaleRadius = 1 / scaleRadius; } }
        int shadowMapSize;
        public float innerAngle;
        public float outerAngle;
        public Vector2 direction;
        public float gamma = 1.0f;


        public Light(GameObject owner, Vector2 offset, Color color, float lightRadius)
        {
            this.owner = owner;
            this.offset = offset;
            this.color = color;

            shadowMapSize = GAME.GameManager.LvlController.LightEngine.ShadowMapSize;
            LightAreaSize = new Vector2(shadowMapSize);
            RenderTarget = new RenderTarget2D(STORAGE.Depository.GraphDevice, shadowMapSize, shadowMapSize);
            this.lightRadius = shadowMapSize * 0.5f;
            direction = Vector2.UnitX;
            this.realRadius = lightRadius;
            this.scaleRadius = this.lightRadius / realRadius;
            invScaleRadius = 1 / scaleRadius;
            init = true;
        }

        public Vector2 ToRelativePosition(Vector2 worldPosition)
        {
            return scaleRadius * (worldPosition - LightPosition) + LightAreaSize * 0.5f;
        }

        public void BeginDrawingShadowCasters()
        {
            if (!init) { Init(); }
            STORAGE.Depository.GraphDevice.SetRenderTarget(RenderTarget);
            STORAGE.Depository.GraphDevice.Clear(Color.Transparent);
        }

        public void EndDrawingShadowCasters()
        {
            STORAGE.Depository.GraphDevice.SetRenderTarget(null);
        }

        void Init()
        {
            shadowMapSize = GAME.GameManager.LvlController.LightEngine.ShadowMapSize;
            LightAreaSize = new Vector2(shadowMapSize);
            RenderTarget = new RenderTarget2D(STORAGE.Depository.GraphDevice, shadowMapSize, shadowMapSize);
            this.lightRadius = shadowMapSize * 0.5f;
            this.scaleRadius = this.lightRadius / realRadius;
            invScaleRadius = 1 / scaleRadius;
            init = true;
        }
    }
}

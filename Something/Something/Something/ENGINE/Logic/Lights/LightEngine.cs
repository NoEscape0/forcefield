﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using STORAGE;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using Microsoft.Xna.Framework.Content;

namespace ENGINE.Logic.Lights
{
    public enum ShadowmapSize
    {
        Size128 = 6,
        Size256 = 7,
        Size512 = 8,
        Size1024 = 9,
    }

    public class QuadRenderer
    {
        VertexPositionTexture[] verts = null;
        short[] ib = null;

        public QuadRenderer()
        {
            verts = new VertexPositionTexture[]
            {
                new VertexPositionTexture(
                    new Vector3(0,0,0),
                    new Vector2(1,1)),
                new VertexPositionTexture(
                    new Vector3(0,0,0),
                    new Vector2(0,1)),
                new VertexPositionTexture(
                    new Vector3(0,0,0),
                    new Vector2(0,0)),
                new VertexPositionTexture(
                    new Vector3(0,0,0),
                    new Vector2(1,0))
            };

            ib = new short[] { 0, 1, 2, 2, 3, 0 };

        }

        public void Render(Vector2 v1, Vector2 v2)
        {
            verts[0].Position.X = v2.X;
            verts[0].Position.Y = v1.Y;

            verts[1].Position.X = v1.X;
            verts[1].Position.Y = v1.Y;

            verts[2].Position.X = v1.X;
            verts[2].Position.Y = v2.Y;

            verts[3].Position.X = v2.X;
            verts[3].Position.Y = v2.Y;

            Depository.GraphDevice.DrawUserIndexedPrimitives<VertexPositionTexture>(PrimitiveType.TriangleList, verts, 0, 4, ib, 0, 2);
        }
    }


    public class LightEngine
    {
        private GraphicsDevice graphicsDevice;

        private int reductionChainCount;
        private int baseSize;
        //private int depthBufferSize;

        Effect resolveShadowsEffect;
        Effect reductionEffect;

        RenderTarget2D distortRT;
        RenderTarget2D shadowMap;
        RenderTarget2D shadowsRT;
        RenderTarget2D processedShadowsRT;
        RenderTarget2D screenShadows;
        RenderTarget2D result;

        QuadRenderer quadRender;
        RenderTarget2D distancesRT;
        RenderTarget2D[] reductionRT;

        List<GameObject> objToRender;
        [NonSerialized]
        Light currentLight;
        public int ShadowMapSize { get { return baseSize; } }

        public LightEngine(GraphicsDevice graphicsDevice, ShadowmapSize maxShadowmapSize)
        {
            this.graphicsDevice = graphicsDevice;
            this.quadRender = new QuadRenderer();

            reductionChainCount = (int)maxShadowmapSize;
            baseSize = 2 << reductionChainCount;
            //depthBufferSize = 2 << (int)maxDepthBufferSize;
        }

        public void LoadContent(ContentManager content)
        {
            reductionEffect = content.Load<Effect>("Shaders\\reductionEffect");
            resolveShadowsEffect = content.Load<Effect>("Shaders\\resolveShadowsEffect");

            distortRT = new RenderTarget2D(graphicsDevice, baseSize, baseSize, false, SurfaceFormat.HalfVector2, DepthFormat.None);
            distancesRT = new RenderTarget2D(graphicsDevice, baseSize, baseSize, false, SurfaceFormat.HalfVector2, DepthFormat.None);
            shadowMap = new RenderTarget2D(graphicsDevice, 2, baseSize, false, SurfaceFormat.HalfVector2, DepthFormat.None);
            reductionRT = new RenderTarget2D[reductionChainCount];
            for (int i = 0; i < reductionChainCount; i++)
            {
                reductionRT[i] = new RenderTarget2D(graphicsDevice, 2 << i, baseSize, false, SurfaceFormat.HalfVector2, DepthFormat.None);
            }


            shadowsRT = new RenderTarget2D(graphicsDevice, baseSize, baseSize);
            processedShadowsRT = new RenderTarget2D(graphicsDevice, baseSize, baseSize);

            screenShadows = new RenderTarget2D(graphicsDevice, graphicsDevice.Viewport.Width, graphicsDevice.Viewport.Height);
            result = new RenderTarget2D(graphicsDevice, graphicsDevice.Viewport.Width, graphicsDevice.Viewport.Height);
            objToRender = new List<GameObject>();
        }

        public Texture2D DrawShadows(Texture2D source)
        {
            if (!Config.LightOn) return source;

            List<GameObject> obj = GAME.GameManager.LvlController.ObjectArray;
            objToRender.Clear();
            for (int i = 0; i < obj.Count; i++)
            {
                GameObject go = obj[i];
                if (!go.HasLight) continue;
                Light light = (go.data.ReadData("ALight") as ENGINE.Logic.ObjAttributes.ALight).light;
                currentLight = light;

                Rectangle lightR = new Rectangle((int)(go.Gtexture.Position.X - light.LightRadius), (int)(go.Gtexture.Position.Y - light.LightRadius),
                    (int)(light.LightRadius * 2), (int)(light.LightRadius * 2));


                if (light != null && STORAGE.Depository.DrawViewport.Intersects(lightR))
                {
                    light.BeginDrawingShadowCasters();
                    DrawCasters(light);
                    light.EndDrawingShadowCasters();
                    ResolveShadows(light.RenderTarget, light.RenderTarget, light.LightPosition);
                    objToRender.Add(go);
                }                
            }

            graphicsDevice.SetRenderTarget(screenShadows);
            graphicsDevice.Clear(Color.Black);
            Depository.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Additive, null, null, null, null, GAME.GameManager.LvlController.camera.GetTransformation());

            for (int i = 0; i < objToRender.Count; i++)
            {
                GameObject go = objToRender[i];
                Light light = (go.data.ReadData("ALight") as ENGINE.Logic.ObjAttributes.ALight).light;
                Depository.SpriteBatch.Draw(light.RenderTarget, light.LightPosition, null, light.color, 0, light.LightAreaSize*0.5f, light.invScaleRadius, SpriteEffects.None, 0);
            }
            Depository.SpriteBatch.End();

            graphicsDevice.SetRenderTarget(null);
            graphicsDevice.BlendState = BlendState.Opaque;
            resolveShadowsEffect.Parameters["ShadowMapTexture"].SetValue(screenShadows);
            ExecuteTechnique(source, result, "Ambient");
            return result;

        }

        private void DrawCasters(Light lightArea)
        {
            List<GameObject> obj = GAME.GameManager.LvlController.ObjectArray;
            Depository.SpriteBatch.Begin();
            Light light;
            for (int i = 0; i < obj.Count; i++)
            {
                if (!STORAGE.Depository.DrawViewport.Intersects(obj[i].Gtexture.GetTextureArea())) continue;
                light = null;
                if (obj[i].HasLight)
                    light = (obj[i].data.ReadData("ALight") as ENGINE.Logic.ObjAttributes.ALight).light;
                if (light != lightArea)
                    obj[i].DrawShadowCasters(lightArea.ToRelativePosition(obj[i].Gtexture.Position), lightArea.scaleRadius);
            }
            Depository.SpriteBatch.End();
        }

        public void ResolveShadows(Texture2D shadowCastersTexture, RenderTarget2D result, Vector2 lightPosition)
        {

            graphicsDevice.BlendState = BlendState.Opaque;

            ExecuteTechnique(shadowCastersTexture, distancesRT, "ComputeDistances");
            ExecuteTechnique(distancesRT, distortRT, "Distort");
            ApplyHorizontalReduction(distortRT, shadowMap);
            ExecuteTechnique(null, shadowsRT, "DrawShadows", shadowMap);
            ExecuteTechnique(shadowsRT, processedShadowsRT, "BlurHorizontally");
            ExecuteTechnique(processedShadowsRT, result, "BlurVerticallyAndAttenuate");
        }

        private void ExecuteTechnique(Texture2D source, RenderTarget2D destination, string techniqueName)
        {
            ExecuteTechnique(source, destination, techniqueName, null);
        }

        private void ExecuteTechnique(Texture2D source, RenderTarget2D destination, string techniqueName, Texture2D shadowMap)
        {
            Vector2 renderTargetSize;
            renderTargetSize = new Vector2((float)baseSize, (float)baseSize);
            graphicsDevice.SetRenderTarget(destination);
            graphicsDevice.Clear(Color.White);
            resolveShadowsEffect.Parameters["renderTargetSize"].SetValue(renderTargetSize);
            resolveShadowsEffect.Parameters["ambient"].SetValue(GAME.GameManager.LvlController.AmbientLight);
            if (techniqueName != "Ambient")
            {
                resolveShadowsEffect.Parameters["direction"].SetValue(currentLight.direction);
                resolveShadowsEffect.Parameters["innerAngle"].SetValue(currentLight.innerAngle);
                resolveShadowsEffect.Parameters["outerAngle"].SetValue(currentLight.outerAngle);
                resolveShadowsEffect.Parameters["gamma"].SetValue(currentLight.gamma);
            }
            if (source != null)
                resolveShadowsEffect.Parameters["InputTexture"].SetValue(source);
            if (shadowMap != null)
                resolveShadowsEffect.Parameters["ShadowMapTexture"].SetValue(shadowMap);

            resolveShadowsEffect.CurrentTechnique = resolveShadowsEffect.Techniques[techniqueName];

            foreach (EffectPass pass in resolveShadowsEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                quadRender.Render(Vector2.One * -1, Vector2.One);
            }
            graphicsDevice.SetRenderTarget(null);
        }


        private void ApplyHorizontalReduction(RenderTarget2D source, RenderTarget2D destination)
        {
            int step = reductionChainCount - 1;
            RenderTarget2D s = source;
            RenderTarget2D d = reductionRT[step];
            reductionEffect.CurrentTechnique = reductionEffect.Techniques["HorizontalReduction"];

            while (step >= 0)
            {
                d = reductionRT[step];

                graphicsDevice.SetRenderTarget(d);
                graphicsDevice.Clear(Color.White);

                reductionEffect.Parameters["SourceTexture"].SetValue(s);
                Vector2 textureDim = new Vector2(1.0f / (float)s.Width, 1.0f / (float)s.Height);
                reductionEffect.Parameters["TextureDimensions"].SetValue(textureDim);

                foreach (EffectPass pass in reductionEffect.CurrentTechnique.Passes)
                {
                    pass.Apply();
                    quadRender.Render(Vector2.One * -1, new Vector2(1, 1));
                }

                graphicsDevice.SetRenderTarget(null);

                s = d;
                step--;
            }

            //copy to destination
            graphicsDevice.SetRenderTarget(destination);
            reductionEffect.CurrentTechnique = reductionEffect.Techniques["Copy"];
            reductionEffect.Parameters["SourceTexture"].SetValue(d);

            foreach (EffectPass pass in reductionEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                quadRender.Render(Vector2.One * -1, new Vector2(1, 1));
            }

            reductionEffect.Parameters["SourceTexture"].SetValue(reductionRT[reductionChainCount - 1]);
            graphicsDevice.SetRenderTarget(null);
        }
    }
}

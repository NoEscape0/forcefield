﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using STORAGE;
using System;
using Microsoft.Xna.Framework;
using ENGINE.Graphics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Microsoft.Xna.Framework.Input;
using GAME;
using ENGINE.Logic.ObjAttributes;

namespace ENGINE.Logic
{
    [Serializable]
    public class WorldMap
    {
        public WorldMap()
        {
            camera = new Camera2d();
            camera.enabledzoom = false;
            camera.CameraCanMove = true;
            camera.Zoom = 1.0f;

            Levels = new List<Level>();

            mapTextures = new List<GameTexture>();
            mapTextures.Add(new GameTexture(false, @"Textures\Map\map", Vector2.Zero));


            mapTextures[0].Position = mapTextures[0].Origin;
            //mapTextures.Add(new GameTexture(false, @"Textures\Map\example", new Vector2(mapTextures[0].TextureResolution.width, 0)+mapTextures[0].Position));
            //mapTextures.Add(new GameTexture(false, @"Textures\Map\example", new Vector2(0, mapTextures[0].TextureResolution.height) + mapTextures[0].Position));
            //mapTextures.Add(new GameTexture(false, @"Textures\Map\example", new Vector2(mapTextures[0].TextureResolution.width, mapTextures[0].TextureResolution.height) + mapTextures[0].Position));



            helperTexture = new GameTexture(false,@"Textures\System\Marker");
            pathTexture = new GameTexture(false, @"Textures\System\pathDot");
        }

        public GameObject HERO;

        public List<GameTexture> mapTextures;
        public List<Level> Levels;
        public GameTexture helperTexture;
        public GameTexture pathTexture;

        public Camera2d camera;
        public bool hideHelpers = false;

        public string LoadedFrom
        {
            get { return loaded_from; }
        }
        private string loaded_from = "";

        public void Draw()
        {
            Depository.GraphDevice.Clear(Color.White);

            Depository.SpriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, null, null, null, null, camera.GetTransformation());
            foreach (GameTexture t in mapTextures)
            {
                t.Draw();
            }
            Depository.SpriteBatch.End();

            Depository.SpriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, null, null, null, null, camera.GetTransformation());
            foreach (Level l in Levels)
            {
                if (Config.EditorMode)
                {
                    l.Draw(true, true);
                }
                else
                    if (!l.hided)
                    {
                        l.Draw(false, true);
                    }
            }
            Depository.SpriteBatch.End();
            Grid.DrawBatch(camera);
        }

        public void Update()
        {
            camera.CamMoveAndZoom();
            if (camera.CameraPosition.X < Depository.ScreenResolution.X / 2) camera.CameraPosition.X = Depository.ScreenResolution.X / 2;
            if (camera.CameraPosition.Y < Depository.ScreenResolution.Y / 2) camera.CameraPosition.Y = Depository.ScreenResolution.Y / 2;

            float maxX = 0;
            float maxY = 0;

            foreach (GameTexture t in mapTextures)
            {
                if (t.TopLeft.X + t.TextureResolution.width > maxX) maxX = t.TopLeft.X + t.TextureResolution.width;
                if (t.TopLeft.Y + t.TextureResolution.height > maxY) maxY = t.TopLeft.Y + t.TextureResolution.height;
            }

            if (camera.CameraPosition.X > maxX - Depository.ScreenResolution.X / 2) camera.CameraPosition.X = maxX - Depository.ScreenResolution.X / 2;
            if (camera.CameraPosition.Y > maxY - Depository.ScreenResolution.Y / 2) camera.CameraPosition.Y = maxY - Depository.ScreenResolution.Y / 2;

            foreach (Level l in Levels)
            {
                if (!l.hided | Config.EditorMode) l.Update();
            }
        }

        public void SaveGlobal(string filename = @"Levels\worldmap.map")
        {
            System.IO.Directory.SetCurrentDirectory(System.Windows.Forms.Application.StartupPath);
            FileStream fstream = File.Open(filename, FileMode.Create);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(fstream, this);
            fstream.Close();
        }

        public void SaveToProfile()
        {
            System.IO.Directory.SetCurrentDirectory(System.Windows.Forms.Application.StartupPath);
            FileStream fstream = File.Open(loaded_from, FileMode.Create);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(fstream, this);
            fstream.Close();
        }

        public static WorldMap Load(string fileName)
        {
            WorldMap map = null;
            try
            {
                FileStream fstream = File.Open(fileName, FileMode.Open);
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                map = (WorldMap)binaryFormatter.Deserialize(fstream);
                fstream.Close();

                map.loaded_from = fileName;
            }
            catch
            {
                map = new WorldMap();
            }

            return map;
        }

        public void CompleteLevel()
        {
            SelectedLevel.completed = true;


            foreach (int lvl in SelectedLevel.LevelsToOpen)
            {
                Levels[lvl].hided = false;
            }

            GameManager.SetMap();

            #region сохранение героя
            if (GAME.GameManager.LvlController.HERO != null)
            {
                if (GAME.GameManager.LvlController.HERO.body != null) GAME.GameManager.LvlController.HERO.DeleteBody(GameManager.LvlController.PhysicWorld);
                GAME.GameManager.LvlController.HERO.AttachedObjects.Clear();
                GAME.GameManager.LvlController.HERO.attachedTo = null;

                HERO = GAME.GameManager.LvlController.HERO;
                SaveToProfile();
            }
            #endregion

            //очищаем старые ресурсы
            if (GameManager.LvlController != null) GameManager.LvlController.UnloadResources();
            GameManager.LvlController = null;
        }

        public void OpenLevel(Level lv)
        {
            lastLevel = lv;

            if (lv.Folder == "")
            {
                return;
            }

            //очищаем старые ресурсы
            if (GameManager.LvlController != null) GameManager.LvlController.UnloadResources();


            GameManager.LvlController = LevelController.LoadLevelController(@"Levels\" + lv.Folder);
            DelHeroes();

            //add from pool
            if (HERO != null)
            {
                GameObject addhero = (GameObject)Functions.DeepClone(HERO);
                if (addhero.body != null) addhero.DeleteBody(GameManager.LvlController.PhysicWorld);
                addhero.AttachedObjects.Clear();
                addhero.attachedTo = null;


                addhero.Gtexture.Position = new Vector2(0, 0);

                foreach (GameObject obj in GameManager.LvlController.ObjectArray)
                {
                    if (obj.GetType() == typeof(ENGINE.Logic.Zones.StartFinishZone))
                    {
                        if ((obj as ENGINE.Logic.Zones.StartFinishZone).Startzone)
                        {
                            addhero.Gtexture.Position = obj.Gtexture.Position;
                            break;
                        }
                    }
                }

                GameManager.LvlController.camera.CameraPosition = addhero.Gtexture.Position;
                GameManager.LvlController.ObjectArray.Add(addhero);
            }


            if (!Config.EditorMode)
            {
                GameManager.LvlController.camera.Zoom = 1f;
                GameManager.LvlController.camera.scrollValueWas = 0;
                GameManager.LvlController.camera.step = 0;               
            }



            GameManager.SetGame();
        }

        public void ReopenLastLevel()
        {
            OpenLevel(lastLevel);
        }

        void DelHeroes()
        {
            for (int i = 0; i < GameManager.LvlController.ObjectArray.Count; ++i)
            {
                if (Functions.HaveAttribute(Functions.Attr.MovementHero, GameManager.LvlController.ObjectArray[i]))
                {
                    GameManager.LvlController.ObjectArray.Remove(GameManager.LvlController.ObjectArray[i]);
                }
            }
        }

        Level lastLevel;

        public Level SelectedLevel
        {
            get
            {
                Level selected = null;

                foreach (Level l in GAME.GameManager.Map.Levels)
                {
                    if (l.selected) { selected = l; break; }
                }

                return selected;
            }
            set
            {
                if (SelectedLevel != null)
                {
                    SelectedLevel.Texture.Color = Color.White;
                    SelectedLevel.selected = false;
                }
                value.selected = true;
                value.Texture.Color = Color.Red;
            }
        }

        public Level GetLevelByID(int id)
        {
            foreach (Level l in GAME.GameManager.Map.Levels)
            {
                if (l.ID == id) return l;
            }
            return null;
        }
    }

    [Serializable]
    public class Level
    {
        [Serializable]
        class Curve
        {
            Vector2 anchor1, anchor2; // начало/конец кривой
            Vector2 control1, control2; // направляющие
            bool control1Drag, control2Drag;
            List<Vector2> Path;
            public int toId;
            Vector2 oldAnchor1, oldAnchor2;
            public bool toRemove;
            int endID;
            Vector2 offset;

            public Curve(Level start, Level end, Vector2 offset)
            {
                this.offset = offset;
                this.endID = end.ID;
                oldAnchor1 = anchor1 = start.Texture.Position+offset;//start.Texture.Center;
                oldAnchor2 = anchor2 = end.Texture.Position+offset;//end.Texture.Center;
                control1 = anchor1 - Vector2.UnitX * 50;
                control2 = anchor2 + Vector2.UnitX * 50;
                toId = end.ID;
                if (Path == null) Path = new List<Vector2>();
                Path.Clear();
                bezierLength(anchor1, control1, control2, anchor2);
            }

            public void Update(Level owner, Vector2 worldMouse)
            {
                bool changes = false;
                if (owner == null) return;
                Level endLevel = GAME.GameManager.Map.GetLevelByID(toId);                              
                if (endLevel == null) { toRemove = true; return; }

                anchor1 = owner.Texture.Position+offset;
                anchor2 = GAME.GameManager.Map.GetLevelByID(toId).Texture.Position + offset;
                if (oldAnchor1 != anchor1)
                {
                    control1 -= oldAnchor1 - anchor1;
                    changes = true;
                }
                if (oldAnchor2 != anchor2)
                {
                    control2 -= oldAnchor2 - anchor2;
                    changes = true;
                }                

                Rectangle control1Rect = new Rectangle((int)Math.Round(control1.X) - 25, (int)Math.Round(control1.Y) - 25, 50, 50);
                Rectangle control2Rect = new Rectangle((int)Math.Round(control2.X) - 25, (int)Math.Round(control2.Y) - 25, 50, 50);
                if (Input.IsLeftButtonPress() && !smthalreadyselected && Depository.CanUseMouse)
                {
                    if (!GAME.GameManager.Map.hideHelpers)
                    {
                        if (Input.IsKeyPressed(Keys.LeftControl) && Input.isHover(control1Rect, worldMouse) && Depository.CanUseMouse && !smthalreadyselected & Config.EditorMode)
                        {
                            smthalreadyselected = true;
                            control1Drag = true;
                        }
                        if (Input.IsKeyPressed(Keys.LeftControl) && Input.isHover(control2Rect, worldMouse) && Depository.CanUseMouse && !smthalreadyselected)
                        {
                            smthalreadyselected = true;
                            control2Drag = true;
                        }
                    }
                }
                if (control1Drag) { control1 = worldMouse; changes = true; }
                if (control2Drag) { control2 = worldMouse; changes = true; }               
                if (changes)
                {
                    if (Path == null) Path = new List<Vector2>();
                    Path.Clear();
                    bezierLength(anchor1, control1, control2, anchor2);
                }
                oldAnchor1 = anchor1;
                oldAnchor2 = anchor2;
            }

            public void Release()
            {
                control1Drag = control2Drag = false;
            }

            public void Draw(bool lines, bool patches)
            {
                bool hideHelpers = GAME.GameManager.Map.hideHelpers;
                GameTexture helperTexture = GAME.GameManager.Map.helperTexture;
                if (!hideHelpers & Config.EditorMode)
                {
                    Depository.SpriteBatch.Draw(helperTexture.Texture2d, control1, null, Color.White, 0, helperTexture.Origin, 0.5f, SpriteEffects.None, 1);
                    Depository.SpriteBatch.Draw(helperTexture.Texture2d, control2, null, Color.White, 0, helperTexture.Origin, 0.5f, SpriteEffects.None, 1);
                }

                float invDensity = 20f; // точка пути через каждые 20 пикселей кривой

                if (lines)
                {
                    if (!GAME.GameManager.Map.hideHelpers)
                    {
                        Grid.BatchLine(control1, anchor1, Color.Cyan);
                        Grid.BatchLine(control2, anchor2, Color.Cyan);
                    }
                }

                if (patches && !GAME.GameManager.Map.GetLevelByID(endID).hided | Config.EditorMode)
                {
                    float sum = 0;
                    for (int i = 1; i < Path.Count; i++)
                    {
                        if (!GAME.GameManager.Map.hideHelpers & Config.EditorMode) Grid.BatchLine(Path[i], Path[i - 1], Color.Purple);
                        sum += (Path[i] - Path[i - 1]).Length();
                        if (sum > invDensity)
                        {
                            Depository.SpriteBatch.Draw(GAME.GameManager.Map.pathTexture.Texture2d, Path[i], null, Color.White, 0, GAME.GameManager.Map.pathTexture.Origin, 1.0f, SpriteEffects.None, 0.0f);
                            sum = 0;
                        }
                    }
                }
            }

            double bezierLength(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4)
            {
                //http://www.rsdn.ru/article/multimedia/Bezier.xml

                //double m_distance_tolerance = 0.5 * 0.5;
                double minDistance = 4;
                // Вычислить все средние точек отрезков
                //----------------------
                Vector2 p12 = (p1 + p2) * 0.5f;
                Vector2 p23 = (p2 + p3) * 0.5f;
                Vector2 p34 = (p3 + p4) * 0.5f;
                Vector2 p123 = (p12 + p23) * 0.5f;
                Vector2 p234 = (p23 + p34) * 0.5f;
                Vector2 p1234 = (p123 + p234) * 0.5f;

                float currentDistance = (p1234 - p1).LengthSquared();
                /* 
                // адаптивное распределение
                Vector2 dp = p4 - p1;
                double d2 = Math.Abs((p2.X - p4.X) * dp.Y - (p2.Y - p4.Y) * dp.X);
                double d3 = Math.Abs((p3.X - p4.X) * dp.Y - (p3.Y - p4.Y) * dp.X);
                if ((d2 + d3) * (d2 + d3) < m_distance_tolerance * dp.LengthSquared()) 
                */ 
                if (currentDistance < minDistance) // равномерное распределение
                {
                    Path.Add(p1234);
                    return 2 * Math.Sqrt(currentDistance);
                }

                double s1 = bezierLength(p1, p12, p123, p1234);
                double s2 = bezierLength(p1234, p234, p34, p4);
                return s1 + s2;
            }
        }

        public Level(Vector2 position)
        {
            this.Texture.Position = position;
            
            //generate id
            for (int i = 0; i < GameManager.Map.Levels.Count + 1; ++i)
            {
                bool exist = false;
                foreach (Level l in GameManager.Map.Levels)
                {
                    if (l.ID == i) exist = true;
                }

                if (!exist)
                {
                    id = i;
                    break;
                }
            }
        }

        static bool smthalreadyselected = false;
        public GameTexture Texture = new GameTexture(false, @"Textures\Map\p_explored");
        public string Folder = "";
        public bool completed = false;
        public bool hided = true; // виден лишь в редакторе      
        public bool levelDrag;
        Vector2 oldMpos;
        public bool selected;

        public string title = "";
        public string description = "";

        public GameTexture Preview_Image = new GameTexture(false, @"Textures\Map\t_image");
        public List<int> LevelsToOpen = new List<int>();
        List<Curve> Curves = new List<Curve>();

        public int ID
        {
            get
            {
                return id;
            }
        }
        private int id;
        bool undercursor = false;

        public void Update()
        {
            Matrix realPosMatrix = Matrix.CreateTranslation(new Vector3(-Texture.Position, 0)) *
                               Matrix.CreateRotationZ(0) *
                               Matrix.CreateScale(new Vector3(1.0f)) *
                               Matrix.CreateTranslation(new Vector3(Texture.Position, 0));

            Vector2 worldMouse = GAME.CursorManager.ScreenToWorld(Input.MouseScreenVector(), GAME.GameManager.Map.camera.GetTransformation());
            worldMouse = Vector2.Transform(worldMouse, realPosMatrix);

            Rectangle levelRect = new Rectangle((int)Math.Round(Texture.Position.X-Texture.Origin.X),
                                                (int)Math.Round(Texture.Position.Y-Texture.Origin.Y),
                                                Texture.TextureResolution.width,
                                                Texture.TextureResolution.height);
            //select
            if (Input.IsLeftButtonPress() && !smthalreadyselected && Depository.CanUseMouse)
            {
                if (Input.isHover(levelRect, worldMouse))
                {
                    //selected
                    smthalreadyselected = true;
                    GAME.GameManager.Map.SelectedLevel = this;
                    Texture.Color = Color.LightBlue;
                }
                else
                {
                    //UNselected
                    if (selected) smthalreadyselected = false;
                    selected = false;
                    Texture.Color = Color.White;
                }

                if (GAME.GameManager.Map.SelectedLevel == this && Input.IsKeyPressed(Keys.LeftShift) & Config.EditorMode)
                {
                    levelDrag = true;
                }
                else levelDrag = false;
            }

            if (Input.isHover(levelRect, worldMouse)) undercursor = true; else undercursor = false;
            

            for (int i = 0; i < Curves.Count; i++)
            {
                Curves[i].Update(this, worldMouse);
                if (Curves[i].toRemove) Curves.Remove(Curves[i--]);
            }
            

            if (levelDrag)
            {
                Texture.Position = worldMouse;//new Vector2(worldMouse.X - Texture.TextureResolution.width / 2, worldMouse.Y - Texture.TextureResolution.height / 2);
            }

            if (Input.IsLeftButtonRelease())
            {
                Curves.ForEach(path => path.Release());
                smthalreadyselected = false;
                levelDrag = false;
            }
            if (Depository.CanUseMouse) oldMpos = worldMouse;
        }

        public void Draw(bool lines, bool patches)
        {
            if (!hided || Config.EditorMode)
            {
                //Texture.Draw();

                if (!completed)
                {
                    if (!undercursor) Texture.Draw(Explored.PartOfSprite, Explored.Texture2d); else Texture.Draw(Explored_Choosed.PartOfSprite, Explored_Choosed.Texture2d);
                }
                else
                {
                    if (!undercursor) Texture.Draw(Completed.PartOfSprite, Completed.Texture2d); else Texture.Draw(Completed_Choosed.PartOfSprite, Completed_Choosed.Texture2d);
                }
                Curves.ForEach(c => c.Draw(lines, patches));
            }
        }

        public void AddPath(Level start, Level end, Vector2 offset)
        {
            Curve c = new Curve(start, end, offset);
            Curves.Add(c);
        }
        public void DeletePath(int id)
        {
            Curve toDelete = Curves.Find(c => c.toId == id);
            Curves.Remove(toDelete);
        }

        public static GameTexture Explored = new GameTexture(false, @"Textures\Map\p_explored");
        public static GameTexture Explored_Choosed = new GameTexture(false, @"Textures\Map\p_explored_choosed");


        public static GameTexture Completed = new GameTexture(false, @"Textures\Map\p_completed");
        public static GameTexture Completed_Choosed = new GameTexture(false, @"Textures\Map\p_completed_choosed");



    }
}

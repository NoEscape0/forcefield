﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using STORAGE;
using System;
using Microsoft.Xna.Framework;
using ENGINE.Graphics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using ENGINE.Logic.ObjAttributes;
using FarseerPhysics.Dynamics;
using GAME;

namespace ENGINE.Logic
{
    [Serializable]
    public class LevelController
    {
        public static string ThisLevelFolder = "";

        public LevelController()
        {
            ObjectArray = new List<GameObject>();
            CommonData = new DataPool();
            camera = new Camera2d();
            camera.CameraPosition = new Vector2(0, 0);
            Background = new GameBackground();

            SortByLayer();
            ConvertUnits.SetDisplayUnitToSimUnitRatio(64);
            mainScene = new RenderTarget2D(Depository.GraphDevice, (int)Depository.ScreenResolution.X, (int)Depository.ScreenResolution.Y);
            BGSound = new RandomSound();
        }

        public List<GameObject> ObjectArray;
        public DataPool CommonData;
        public Camera2d camera;
        public GameBackground Background;
        public static GameTexture nullTexture = new GameTexture(false, @"Textures\System\null");
        public Particles.ParticleEngine particleEngine;
        [NonSerialized]
        public Lights.LightEngine lightEngine;
        [NonSerialized]
        private World world;
        private Vector2 levelGravity = new Vector2(0, 100);
        public Vector2 Gravity { get { return levelGravity; } set { PhysicWorld.Gravity = ConvertUnits.ToSimUnits(value); levelGravity = value; } }
        public static Random rnd = new Random(111222);
        [NonSerialized]
        RenderTarget2D mainScene;
        [NonSerialized]
        bool init = false;
        float ambientLight = 1;
        public float AmbientLight { get { return ambientLight; } set { ambientLight = (float)Math.Round(MathHelper.Clamp(value, -1, 1), 2); } }
        public Color BackgroundFillColor = Color.Black;
        public RandomSound BGSound;
        public bool EnableSound_E = true;
        public bool EnableSound_G = true;
        public static StaticAnimations staticAnimations;

        public Weather weather;

        public GameObject HERO
        {
            get
            {
                return hero;
            }
        }
        private GameObject hero;

        public void RefindHero()
        {
            hero = null;
            for (int i = 0; i < ObjectArray.Count; ++i)
            {
                if (Functions.HaveAttribute(Functions.Attr.MovementHero, ObjectArray[i]))
                    hero = ObjectArray[i];
            }
        }


        public Particles.ParticleEngine ParticleEngine
        {
            get
            {
                if (particleEngine != null) return particleEngine;
                else
                {
                    particleEngine = new Particles.ParticleEngine();
                    return particleEngine;
                }
            }
            set { particleEngine = new Particles.ParticleEngine(); }
        }

        public Lights.LightEngine LightEngine
        {
            get
            {
                if (lightEngine != null) return lightEngine;
                else
                {
                    lightEngine = new Lights.LightEngine(Depository.GraphDevice, Lights.ShadowmapSize.Size256);
                    lightEngine.LoadContent(STORAGE.Depository.CommonContent);
                    return lightEngine;
                }
            }
        }

        public World PhysicWorld
        {
            get
            {
                if (world != null) return world;
                else
                {
                    world = new World(levelGravity);
                    world.BodyRemoved += new BodyDelegate(RemoveBodyFromWorld);
                    return world;
                }
            }
        }

        public Random Rnd
        {
            get
            {
                if (rnd != null) return rnd;
                else
                {
                    rnd = new Random(111222);
                    return rnd;
                }
            }
        }

        public Weather Weather
        {
            get
            {
                if (weather != null) return weather;
                else
                {
                    weather = new Weather();
                    return weather;
                }
            }
        }


        void RemoveBodyFromWorld(Body body)
        {
            body.UserData = null;
        }

        public void SortByLayer()
        {
            ObjectArray.Sort(delegate(GameObject a, GameObject b)
            {
                return a.Gtexture.Layer.CompareTo(b.Gtexture.Layer);
            });
        }


        public void DrawAll()
        {
            if (!init) Init();
            Depository.GraphDevice.SetRenderTarget(mainScene);
            Depository.GraphDevice.Clear(Color.Transparent);
            Background.Draw(camera);
            ParticleEngine.Draw(camera.GetTransformation());

            Depository.SpriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, camera.GetTransformation());
            //all obj
            for (int i = 0; i < ObjectArray.Count; ++i)
            { ObjectArray[i].Draw(); }
            staticAnimations.Draw();
            Depository.SpriteBatch.End();
            ParticleEngine.Draw(camera.GetTransformation(), false);

            Depository.GraphDevice.SetRenderTarget(null);

            Texture2D shadows = LightEngine.DrawShadows(mainScene);

            Depository.GraphDevice.SetRenderTarget(null);
            Depository.GraphDevice.Clear(GAME.GameManager.LvlController.BackgroundFillColor);
            Background.Draw(camera, true);
            Depository.SpriteBatch.Begin();
            Depository.SpriteBatch.Draw(shadows, Vector2.Zero, Color.White);
            Depository.SpriteBatch.End();

            Weather.Draw();


            Grid.DrawBatch(camera);
        }

        public void UpdateAll()
        {
            if (!init) Init();
            camera.CamMoveAndZoom();
            ParticleEngine.Update();

            CommonData.WriteDataSimple("SmthAlreadySelected", false);

            for (int i = 0; i < ObjectArray.Count; ++i) { ObjectArray[i].Update(); }

            #region sound
            if (Config.EditorMode & EnableSound_E || !Config.EditorMode & EnableSound_G)
            {
                BGSound.Play();
            }
            #endregion


            if (Input.IsNewKeyPressed(Microsoft.Xna.Framework.Input.Keys.L)) Config.LightOn ^= true;
            
            //вкл-вкл дождика
            if (Input.IsNewKeyPressed(Microsoft.Xna.Framework.Input.Keys.D1))
            {
                weather.online ^= true;
            }
            if (Input.IsNewKeyPressed(Microsoft.Xna.Framework.Input.Keys.D2))
            {
                weather.ChangeType(Weather.WeatherType.Rain);
            }
            if (Input.IsNewKeyPressed(Microsoft.Xna.Framework.Input.Keys.D3))
            {
                weather.ChangeType(Weather.WeatherType.Snow);
            }
            if (Input.IsNewKeyPressed(Microsoft.Xna.Framework.Input.Keys.D4))
            {
                weather.ChangeType(Weather.WeatherType.Mist);
            }
            // тесты анимации
            if (Input.IsLeftButtonClick())
            {
              //  Vector2 mouse = CursorManager.GetWorldMouse();
              //  staticAnimations.PlayAnimation("anim1", mouse);
            }


            PhysicWorld.Step((float)Depository.Elapsed);
            Weather.Update();
            staticAnimations.Update();
        }

        public void Save(string filename)
        {
            ResetTriggersCD(); // сбрасывает все триггеры

            System.IO.Directory.SetCurrentDirectory(System.Windows.Forms.Application.StartupPath);
            FileStream fstream = File.Open(filename, FileMode.Create);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(fstream, this);
            fstream.Close();
        }

        public void ResetTriggersCD()
        {
            for (int i = 0; i < ObjectArray.Count; ++i)
            {
                if (Functions.HaveAttribute(Functions.Attr.Trigger, ObjectArray[i])) (ObjectArray[i].data.ReadData("ATrigger") as ATrigger).RealCDleftBeforeNextUsing = 0;
            }
        }


        public static LevelController LoadLevelController(string fileName)
        {
            LevelController objToSerialize = null;
            System.IO.Directory.SetCurrentDirectory(System.Windows.Forms.Application.StartupPath);
            try
            {
                FileStream fstream = File.Open(fileName, FileMode.Open);
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                objToSerialize = (LevelController)binaryFormatter.Deserialize(fstream);
                fstream.Close();
            }
            catch
            {
                objToSerialize = new LevelController();
            }

            //зум
            if (!Config.EditorMode)
            {
                objToSerialize.camera.Zoom = 0.5f;
                objToSerialize.camera.step = -1;
            }

            //физика
            ConvertUnits.SetDisplayUnitToSimUnitRatio(64);

            //подгружаем текстуры
            foreach (GameObject obj in objToSerialize.ObjectArray)
            {
                obj.Gtexture.RestoreTexture();

                if (Functions.HaveAttribute(Functions.Attr.Animation, obj))
                {
                    Animations animations = obj.data.ReadData("ANIMATION") as Animations;

                    foreach (GameTexture o in animations.AdvancedTextures)
                    {
                        o.RestoreTexture();
                    }
                }
            }

            SoundManager.StopAll(false);
            return objToSerialize;
        }

        public void UnloadResources()
        {
            for (int i = 0; i < ObjectArray.Count; ++i)
            {
                ObjectArray[i].Gtexture.DisposeTexture();
            }
            ObjectArray.Clear();
            Depository.GameContent.Unload();
        }

        private void Init()
        {
            mainScene = new RenderTarget2D(Depository.GraphDevice, (int)Depository.ScreenResolution.X, (int)Depository.ScreenResolution.Y);
            if (staticAnimations == null) InitAnimations();
            init = true;
        }

        private void InitAnimations()
        {
            staticAnimations = new StaticAnimations();
            staticAnimations.AddAnimation("Anim\\run.anim", "anim1");
            staticAnimations.Init();
        }

        private void PlayAnimation(string anim, Vector2 position)
        {
            staticAnimations.PlayAnimation(anim, position);
        }

    }
}

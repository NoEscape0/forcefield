﻿using System.Windows.Forms;
using STORAGE;

namespace ENGINE.Logic
{
    public static class ScreenParametrs
    {
        /// <summary>
        /// Используются параметры из конфига.
        /// </summary>
        public static void SetScreenParams(int Width = 0 , int Height = 0)
        {
            int w = Width;
            int h = Height;
            if (Width == 0 | Height == 0) { w = (int)Depository.Adapter.CurrentDisplayMode.Width; h = (int)Depository.Adapter.CurrentDisplayMode.Height; }

            Depository.ScreenResolution.X = w;
            Depository.ScreenResolution.Y = h;

            Depository.Graphics.PreferMultiSampling = false;
            Depository.Graphics.PreferredBackBufferWidth = w;
            Depository.Graphics.PreferredBackBufferHeight = h;
            Depository.Graphics.SynchronizeWithVerticalRetrace = Config.SynchronizeWithVerticalRetrace;
            Depository.Graphics.ApplyChanges();


            Depository.MainForm.FormBorderStyle = FormBorderStyle.None;

            Depository.MainForm.TopMost = Config.TopMost;

            Depository.MainForm.Top = 0;
            Depository.MainForm.Left = 0;

            if (!Config.EditorMode)
            {
                FullScreen(Config.IsGameFullScreen);
            }
        }


        public static void FullScreen(bool v)
        {
            if (v & !Config.EditorMode)
            {
                Depository.Graphics.IsFullScreen = true;
                Depository.Graphics.ApplyChanges();
            }
            else
            {
                Depository.Graphics.IsFullScreen = false;
                Depository.Graphics.ApplyChanges();
            }
        }
    }
}

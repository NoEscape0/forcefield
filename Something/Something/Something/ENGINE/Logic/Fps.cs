﻿
namespace ENGINE.Logic
{
    public static class Fps
    {

        public static int FramesPerSecond;
        public static int AverageFps = 0;

        public static int UpdatesPerSecond;
        public static int AverageUps = 0;

        public static void UpdateFps(double elapsed)
        {
            globElapsed += elapsed;
            frames++;
            if (globElapsed > 1)
            {
                FramesPerSecond = frames;
                frames = 0;
                globElapsed = 0;
                AverageFps = (AverageFps + FramesPerSecond) / 2;
            }
        }



        public static void UpdateUps(double elapsed)
        {
            globElapsedU += elapsed;
            updates++;
            if (globElapsedU > 1)
            {
                UpdatesPerSecond = frames;
                updates = 0;
                globElapsedU = 0;
                AverageUps = (AverageUps + UpdatesPerSecond) / 2;
            }
        }

        #region private fps
        private static int frames = 0;
        private static double globElapsed;
        #endregion

        #region private ups
        private static int updates = 0;
        private static double globElapsedU;
        #endregion
    }
}

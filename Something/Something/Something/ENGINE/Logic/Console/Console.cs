﻿using System.Collections.Generic;
using System;
using Microsoft.Xna.Framework;
using STORAGE;
using RamGecXNAControls;


namespace ENGINE.Logic.Console
{
    [Serializable]
    public static class Console
    {
        public static List<Command> ConsoleCommands = new List<Command>();
        public static TextArea TextArea;
        static System.Reflection.PropertyInfo scrollIndexProperty;
        static System.Reflection.PropertyInfo scrollerSizeProperty;
        static System.Reflection.FieldInfo scrollPositionField;

        public static void LoadContent() // тут добавляются общие команды, только для консоли добавляются в EDrawManager
        { 
            Type textAreaType = TextArea.GetType();
            scrollerSizeProperty = textAreaType.GetProperty("scrollerSize", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            scrollPositionField = textAreaType.GetField("scrollerPosition", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            scrollIndexProperty = textAreaType.GetProperty("scrollIndex", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);

            ConsoleCommands = new List<Command>();
                { // clear
                    Command Command = new Command("clear");
                    Command.help = "Clean console window";
                    Command.OnCommandAction += delegate
                    {
                        TextArea.Text = "";
                    };
                    ConsoleCommands.Add(Command);
                }

                { // help
                    Command Command = new Command("help");
                    Command.help = "View information abaut all available commands";
                    Command.OnCommandAction += delegate
                    {
                        set_darkblue();
                        write_line("Available commands:");
                        set_blue();
                        for (int i = 0; i < ConsoleCommands.Count; ++i)
                        {
                            write_line("["+ConsoleCommands[i].command+"]");                           
                        }
                        set_black();
                    };
                    ConsoleCommands.Add(Command);
                }

                { // version
                    Command Command = new Command("version");
                    Command.help = "Show programm version";
                    Command.OnCommandAction += delegate
                    {
                        set_blue();
                        write_line("Game version: "+Config.GameVersion);
                        set_black();
                    };
                    ConsoleCommands.Add(Command);
                }

                { // mode
                    Command Command = new Command("mode");
                    Command.help = "Show programm mode (GAME or EDITOR)";
                    Command.OnCommandAction += delegate
                    {
                        set_blue();
                        if (Config.EditorMode)  write_line("Mode: EDITOR");
                        else write_line("Mode: GAME");
                        set_black();
                    };
                    ConsoleCommands.Add(Command);
                }
            
                { // worldinfo
                    Command Command = new Command("worldinfo");
                    Command.help = "Show rorldsize, cam position and smth like that";
                    Command.OnCommandAction += delegate
                    {
                        if (GAME.GameManager.LvlController != null)
                        {
                            set_blue();
                            write_line(string.Format("Camera Position: x {0}  y {1}", Math.Round(GAME.GameManager.LvlController.camera.CameraPosition.X, 2),
                                Math.Round(GAME.GameManager.LvlController.camera.CameraPosition.Y), 2));

                            write_line(string.Format("Total objects: {0}", GAME.GameManager.LvlController.ObjectArray.Count));
                            write_line(string.Format("Library objects: {0}", Library.GetLibraryArray().Count));

                            set_black();
                        }
                        else
                        {
                            set_red();

                            write_line("LEVEL == null");

                            set_black();
                        }
                    };
                    ConsoleCommands.Add(Command);
                }

                { // focusonhero
                    Command Command = new Command("focusonhero");
                    Command.OnCommandAction += delegate
                    {
                        if (GAME.GameManager.LvlController != null)
                        {
                            set_blue();
                            GameObject hero = GAME.EnviromentInfo.HERO.Hero;
                            if (hero == null) write_line("Hero is not found");
                            else
                            {
                                GAME.GameManager.LvlController.camera.CameraPosition.X = hero.Gtexture.Position.X + hero.Gtexture.TextureResolution.width / 2;
                                GAME.GameManager.LvlController.camera.CameraPosition.Y = hero.Gtexture.Position.Y + hero.Gtexture.TextureResolution.height / 2;
                                write_line("Focused");
                            }

                            set_black();
                        }
                        else
                        {
                            set_red();

                            write_line("LEVEL == null");

                            set_black();
                        }
                    };
                    ConsoleCommands.Add(Command);
                }

                { // set Zoom
                    Command Command = new Command("zoom");
                    Command.OnCommandAction += delegate(string value)
                    {
                        if (GAME.GameManager.LvlController != null)
                        {
                            float nzoom = 1.0f;
                            try
                            {
                                nzoom = (float)Convert.ToDouble(value);
                                GAME.GameManager.LvlController.camera.Zoom = nzoom;
                                set_blue();
                                write_line("Done");
                                set_black();
                            }
                            catch
                            {
                                set_red();
                                write_line("Incorrect value");
                                set_black();
                            }
                        }
                         else
                         {
                             set_red();

                             write_line("LEVEL == null");

                             set_black();
                         }
                    };
                    ConsoleCommands.Add(Command);
                }

                { // fps
                    Command Command = new Command("fps");
                    Command.help = "Enable(1) or disable(0) showing fps";
                    Command.OnCommandAction += delegate(string value)
                    {
                        int d = -1;

                        if (!Int32.TryParse(value, out d)) { d = -1; }

                        if (d == -1)
                        {
                            Console.set_red();
                            Console.write_line("Incorrect value. Input 0 or 1");
                            Console.set_black();
                        }

                        Console.set_blue();
                        if (d == 0)
                        {
                            Config.show_fps = false;
                            Console.write_line("FPS Disabled");
                        }
                        if (d == 1)
                        {
                            Config.show_fps = true;
                            Console.write_line("FPS Enabled");
                        }
                        Console.set_black();

                    };
                    Console.ConsoleCommands.Add(Command);
                }

                { // 
                    Command Command = new Command("fps_SynchronizeWithVerticalRetrace");
                    Command.help = "Enable(1) or disable(0) SynchronizeWithVerticalRetrace";
                    Command.OnCommandAction += delegate(string value)
                    {
                        int d=-1;
                        if (!Int32.TryParse(value, out d)){d = -1;}

                        if (d == -1)
                        {
                            Console.set_red();
                            Console.write_line("Incorrect value. Input 0 or 1");
                            Console.write_line("SynchronizeWithVerticalRetrace is " + (Depository.Graphics.SynchronizeWithVerticalRetrace?"on":"off"));
                            Console.set_black();
                        }

                        if (d == 1)
                        {
                            Console.write_line("SynchronizeWithVerticalRetrace on.");
                            Depository.Graphics.SynchronizeWithVerticalRetrace = true;
                            Depository.Game.IsFixedTimeStep = true;
                            Depository.Graphics.ApplyChanges();
                        }
                        if (d == 0)
                        {
                            Console.write_line("SynchronizeWithVerticalRetrace off.");
                            Depository.Graphics.SynchronizeWithVerticalRetrace = false;
                            Depository.Game.IsFixedTimeStep = false;
                            Depository.Graphics.ApplyChanges();
                        }

                    };
                    ConsoleCommands.Add(Command);
                }

                { // quit
                    Command Command = new Command("quit");
                    Command.help = "Quit Game/Editor.";
                    Command.OnCommandAction += delegate
                    {
                        Depository.Game.Exit();
                    };
                    ConsoleCommands.Add(Command);
                }

                { // fullscreen
                    Command Command = new Command("fullscreen");
                    Command.help = "1 - set fullscreen 0 - sen window without borders.";
                    Command.OnCommandAction += delegate(string value)
                    {
                        int d = -1;

                        if (!Int32.TryParse(value, out d)) { d = -1; }

                        if (d == -1)
                        {
                            Console.set_red();
                            Console.write_line("Incorrect value. Input 0 or 1");
                            Console.set_black();
                        }

                        Console.set_blue();
                        if (d == 0 & !Config.EditorMode)
                        {
                            ENGINE.Logic.ScreenParametrs.FullScreen(false);
                            Console.write_line("FullScreen OFF");
                        }
                        else
                        {
                            Console.set_red();
                            Console.write_line("u cant set FullScreen in editor mode");
                        }


                        if (d == 1)
                        {
                            ENGINE.Logic.ScreenParametrs.FullScreen(true);
                            Console.write_line("FullScreen ON");
                        }
                        Console.set_black();
                    };
                    ConsoleCommands.Add(Command);
                }


                { // hero pos
                    Command Command = new Command("heropos");

                    Command.OnCommandAction += delegate
                    {
                        if (GAME.GameManager.LvlController != null)
                        {
                            Console.set_darkblue();
                            if (GAME.EnviromentInfo.HERO.Hero != null)    Console.write_line(GAME.EnviromentInfo.HERO.Hero.Gtexture.Position.ToString());
                            Console.set_black();
                        }
                    };
                    ConsoleCommands.Add(Command);
                }

                { // god mode
                    Command Command = new Command("god");
                    Command.help = "u cant die in god mode";

                    Command.OnCommandAction += delegate
                    {
                        set_blue();
                        Config.godmode = !Config.godmode;
                        if (Config.godmode) write_line("god mode enabled");
                        else write_line("god mode disabled");
                        set_black();
                    };


                    ConsoleCommands.Add(Command);
                }


                { // del heroes
                    Command Command = new Command("deletehero");

                    Command.OnCommandAction += delegate
                    {
                        if (GAME.GameManager.LvlController != null)
                        {
                            for (int i = 0; i < GAME.GameManager.LvlController.ObjectArray.Count; ++i)
                            {
                                if (ENGINE.Logic.ObjAttributes.Functions.HaveAttribute(ENGINE.Logic.ObjAttributes.Functions.Attr.MovementHero, GAME.GameManager.LvlController.ObjectArray[i]))
                                {
                                    GAME.GameManager.LvlController.ObjectArray.Remove(GAME.GameManager.LvlController.ObjectArray[i]);
                                }
                            }
                            GAME.GameManager.LvlController.RefindHero();


                            Console.set_darkblue();
                            Console.write_line("done");
                            Console.set_black();
                        }
                    };
                    ConsoleCommands.Add(Command);
                }

            #region colors
            TextArea.ColorTable.Add('B', Color.Blue);
            TextArea.ColorTable.Add('N', Color.Black);
            TextArea.ColorTable.Add('R', Color.Red);
            TextArea.ColorTable.Add('D', Color.DarkBlue);
            TextArea.ColorTable.Add('G', Color.DarkGreen);
            #endregion
        }

        public static void set_red() { write("$R"); }
        public static void set_black() { write("$N"); }
        public static void set_blue() { write("$B"); }
        public static void set_darkblue() { write("$D"); }
        public static void set_green() { write("$G"); }

        public static void ReadCommand(string UnparcedCommand)
        {
            write_line(">" + UnparcedCommand);
             
            string command = "";
            string value = "";
            if (UnparcedCommand.IndexOf(" ") != -1)
            {
                command = UnparcedCommand.Substring(0, UnparcedCommand.IndexOf(" "));
                string tmp = UnparcedCommand.Substring(UnparcedCommand.IndexOf(" ") + 1);
                if (tmp.IndexOf(" ") != -1) value = tmp.Substring(0, tmp.IndexOf(" "));
                else value = tmp;
            }
            else command = UnparcedCommand;

            bool found = false;
            for (int i = 0; i < ConsoleCommands.Count; ++i)
            {
                if (ConsoleCommands[i].command == command)
                {
                    if (value != "help")
                    {
                        ConsoleCommands[i].DoCommandAction(value);
                    }
                    else
                    {
                        set_green();
                        write_line(ConsoleCommands[i].help);
                        set_black();
                    }
                    found = true;
                }
            }
            if (!found)
            {
                set_red();
                write("unknown command");
                set_black();
                write_line();
            }
        }

        public static void write(string s)
        {
            TextArea.Text += s;
        }
        public static void write_line(string line)
        {            
            TextArea.Text += line;
            TextArea.Text += "\n";

            int scrollIndex = TextArea.Text.Split('\n').Length - 17; //Lines.Count - displayableItemsCount)); не совсем точное количество линий с учетом переноса строк
            float scrollerSize = (float)scrollerSizeProperty.GetValue(TextArea, new object[] { });
            float num3 = (TextArea.AbsoluteBounds.Height - TextArea.Theme.SkinTextAreaScrollbarTop.Height - TextArea.Theme.SkinTextAreaScrollerBottom.Height)*scrollerSize;
            float max = (TextArea.AbsoluteBounds.Height - TextArea.Theme.SkinTextAreaScrollerBottom.Height) - (num3 / 2f);
            scrollPositionField.SetValue(TextArea, (int)max);
            scrollIndexProperty.SetValue(TextArea, scrollIndex, new object[] { });
        }
        public static void write_line()
        {
            write_line("\n");
            //TextArea.Text += "\n";
        }

        public static string FindCommand(string UnfinishedCommand, int cursorPosition)
        {
            List<string> s = new List<string>();
            string result =string.Empty;
            string realUnfinishedCommand = UnfinishedCommand.Substring(0, cursorPosition);

            foreach (var comm in ConsoleCommands)
            {
                if (System.Text.RegularExpressions.Regex.Match(comm.command, "^" + realUnfinishedCommand).Groups[0].Value != string.Empty) { s.Add(comm.command); }
            }
            if (s.Count==0) return UnfinishedCommand;
            
            if (s.Find(str=>str==UnfinishedCommand) != null)
            {
                result = s[(s.IndexOf(UnfinishedCommand) + 1) % s.Count];
            }
            else
            {
                s.ForEach(str => { write_line(str); });
                result = s[0];
            }
          
            return result;
        }

    }
}

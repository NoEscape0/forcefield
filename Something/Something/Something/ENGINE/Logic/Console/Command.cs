﻿using System;

namespace ENGINE.Logic.Console
{
    [Serializable]
    public class Command
    {
        public Command(string Command)
        {
            command = Command;
        }

        public string command;
        public event work OnCommandAction;
        public delegate void work(string value);

        public void DoCommandAction(string value)
        {
            if (OnCommandAction != null) OnCommandAction(value);
        }

        public string help = "[no description]";
    }
}

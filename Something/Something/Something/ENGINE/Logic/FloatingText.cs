﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using STORAGE;

namespace ENGINE.Logic
{
    [Serializable]
    public class FloatingText : GameObject
    {
        public enum TextType
        {
            SlideLeft,
            SlideRight,
            SlideUp,
            SlideDown,
            SlideRandomHorizontal,
            SlideRandomVertical,
            SlideRandom,
            BounceRandom,
            BounceRight,
            BounceLeft,

        }

        private float size;
        private Color color;
        private string text;
        private Vector2 position;
        private float currentTime;
        private float totalTime = 1;
        private Vector2 accelerate;
        private Vector2 currentSpeed;
        //private float startSpeed = 150;
        [NonSerialized]
        public Texture2D texture = null;
        Texture2D Texture { get { return texture; } set { texture = value; textureOrigin = new Vector2(texture.Width / 2, texture.Height / 2); } }
        Vector2 textureOrigin;
        Vector2 textOrigin;
        Vector2 prevPos;
        Color currentColor;
        Color shadowColor;
        float currentSize;

        public Vector2 Speed
        {
            get
            {
                return currentSpeed;
            }
            set
            {
                float length = value.Length() / currentSpeed.Length();
                currentSpeed = value;                
                accelerate *= length;
            }
        }
        public Vector2 Accelerate { get { return accelerate; } set { accelerate = value; } }

        private static SpriteFont mainFont;
        private static SpriteFont Font
        {
            get
            {
                if (mainFont != null) return mainFont;
                else
                {
                    mainFont = Depository.CommonContent.Load<SpriteFont>("Source\\Fonts\\floatingTextFont");
                    return mainFont;
                }
            }
        }

        FloatingText(TextType textType, string text, float size, Color color, Vector2 position, float startSpeed, float time)
        {
            Vector2 direction = Vector2.Zero;
            this.text = text;
            this.position = position;
            this.size = size;
            this.color = color;
            this.totalTime = time;

            textOrigin = Font.MeasureString(text) * 0.5f;

            float randomNum1 = GAME.GameManager.LvlController.Rnd.Next(100) < 50 ? -1 : 1;
            float randomNum2 = GAME.GameManager.LvlController.Rnd.Next(100) < 50 ? -1 : 1;

            switch (textType)
            {
                case TextType.SlideLeft:
                    OnUpdate += UpdateSlide;
                    direction = new Vector2(-1, 0);
                    accelerate = -startSpeed / totalTime * direction;
                    break;
                case TextType.SlideRight:
                    OnUpdate += UpdateSlide;
                    direction = new Vector2(1, 0);
                    accelerate = -startSpeed / totalTime * direction;
                    break;
                case TextType.SlideUp:
                    OnUpdate += UpdateSlide;
                    direction = new Vector2(0, -1);
                    accelerate = -startSpeed / totalTime * direction;
                    break;
                case TextType.SlideDown:
                    OnUpdate += UpdateSlide;
                    direction = new Vector2(0, 1);
                    accelerate = -startSpeed / totalTime * direction;
                    break;
                case TextType.SlideRandomHorizontal:
                    OnUpdate += UpdateSlide;
                    direction = new Vector2(randomNum1, 0);
                    accelerate = -startSpeed / totalTime * direction;
                    break;
                case TextType.SlideRandomVertical:
                    OnUpdate += UpdateSlide;
                    direction = new Vector2(0, randomNum1);
                    accelerate = -startSpeed / totalTime * direction;
                    break;
                case TextType.SlideRandom:
                    OnUpdate += UpdateSlide;
                    direction = randomNum2 == 1 ? new Vector2(randomNum1, 0) : new Vector2(0, randomNum1);
                    accelerate = -startSpeed / totalTime * direction;
                    break;
                case TextType.BounceLeft:
                    direction = new Vector2(-1, -1);
                    OnUpdate += UpdateSlide;
                    accelerate = (new Vector2(0, 1)) * 2 * startSpeed / totalTime;
                    break;
                case TextType.BounceRight:
                    direction = new Vector2(1, -1);
                    OnUpdate += UpdateSlide;
                    accelerate = (new Vector2(0, 1)) * 2 * startSpeed / totalTime;
                    break;
                case TextType.BounceRandom:
                    direction = new Vector2(randomNum1, -1);
                    OnUpdate += UpdateSlide;
                    accelerate = (new Vector2(0, 1)) * 2 * startSpeed / totalTime;
                    break;
                default:
                    break;
            }
            OnUpdate += UpdateSlide;
            currentTime = 0;
            currentSpeed = direction * startSpeed;
            OnDraw += Draw;
            prevPos = position;
        }

        public void UpdateSlide(GameObject sender)
        {
            float moment = currentTime / totalTime;
            Vector2 correction = Vector2.Zero;
            if (attachedTo != null) correction = attachedTo.Gtexture.Position - prevPos;
            float elapsed = (float)Depository.Elapsed;
            currentTime += elapsed;
            currentSpeed += accelerate * elapsed;
            position += currentSpeed * elapsed + correction;
            if (currentTime > totalTime) GAME.GameManager.LvlController.ObjectArray.Remove(this);
            if (attachedTo != null) prevPos = attachedTo.Gtexture.Position;

            if (moment < 0.15f)
            {
                currentColor = color;
                shadowColor = Color.Black;
                currentSize = MathHelper.Lerp(0, 1.5f, moment / 0.15F);
            }
            else
                if (moment < 0.5f)
                {
                    currentColor = color;
                    currentSize = 1.5f;
                    shadowColor = Color.Black;
                }
                else
                {
                    currentColor = Color.Lerp(color, new Color(10, 10, 10, 10), (moment - 0.5f) / 0.5f);
                    shadowColor = Color.Lerp(Color.Black, new Color(10, 10, 10, 10), (moment - 0.5f) / 0.5f);
                    currentSize = MathHelper.Lerp(1.5f, 0.5f, (moment - 0.5f) / 0.5f);
                }

        }
        /*
        public void UpdateBounce(GameObject sender)
        {
            float elapsed = (float)Depository.Elapsed;
            currentTime += elapsed;
            currentSpeed += accelerate * elapsed;
            position += currentSpeed * elapsed;
            if (currentTime > totalTime) GAME.GameManager.LvlController.ObjectArray.Remove(this);
        }
        */
        public void Draw(GameObject sender)
        {
            Depository.SpriteBatch.DrawString(Font, text, position + Vector2.UnitX + Vector2.UnitY, shadowColor, 0, textOrigin, currentSize * size, SpriteEffects.None, 0.01f);
            Depository.SpriteBatch.DrawString(Font, text, position, currentColor, 0, textOrigin, currentSize * size, SpriteEffects.None, 0f);
            if (texture != null)
                Depository.SpriteBatch.Draw(texture, position, null, currentColor, 0, textureOrigin, currentSize * size, SpriteEffects.None, 0.0f);
        }

        public void AttachTo(GameObject go)
        {
            this.attachedTo = go;
            prevPos = go.Gtexture.Position;
        }

        #region фабрики
        /// <summary>
        /// Создает затухающий текст
        /// </summary>
        /// <param name="type">Тип, отпределяющий направление начальной скорости и ускорения</param>
        /// <param name="text">текст</param>
        /// <param name="size">множитель размера</param>
        /// <param name="color">начальный цвет</param>
        /// <param name="position">начальная позиция</param>
        /// <param name="startSpeed">стартовая скорость</param>
        /// <param name="time">время до полного затухания</param>
        /// <returns></returns>
        public static FloatingText Create(TextType type, string text, float size, Color color, Vector2 position, float startSpeed, float time = 1, bool screenPos = false)
        {
            if (screenPos)
            {
                position.X += GAME.GameManager.LvlController.camera.CameraPosition.X - Depository.ScreenResolution.X/2;
                position.Y += GAME.GameManager.LvlController.camera.CameraPosition.Y - Depository.ScreenResolution.Y/2;

            }

            FloatingText ft = new FloatingText(type, text, size, color, position, startSpeed, time);
            GAME.GameManager.LvlController.ObjectArray.Add(ft);
            ft.data = new DataPool();
            ft.AddedAttributes = new List<ENGINE.Logic.ObjAttributes.Functions.Attr>();
            ft.Gtexture = new Graphics.GameTexture(true, "Textures\\System\\null");
            return ft;
        }
        /// <summary>
        /// Создает затухающий текст
        /// </summary>
        /// <param name="type">Тип, отпределяющий направление начальной скорости и ускорения</param>
        /// <param name="text">текст</param>
        /// <param name="texture">текстура</param>
        /// <param name="size">множитель размера</param>
        /// <param name="color">начальный цвет</param>
        /// <param name="position">начальная позиция</param>
        /// <param name="startSpeed">стартовая скорость</param>
        /// <param name="time">время до полного затухания</param>
        public static FloatingText Create(TextType type, string text, Texture2D texture, float size, Color color, Vector2 position, float startSpeed, float time = 1)
        {
            FloatingText ft = new FloatingText(type, text, size, color, position, startSpeed, time);
            ft.texture = texture;
            GAME.GameManager.LvlController.ObjectArray.Add(ft);
            ft.data = new DataPool();
            ft.AddedAttributes = new List<ENGINE.Logic.ObjAttributes.Functions.Attr>();
            return ft;
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Microsoft.Xna.Framework;
using ENGINE.Logic.ObjAttributes;
using ENGINE.Graphics;



namespace ENGINE.Logic
{
    public class StaticAnimations
    {
        List<SingleAnimation> sourceAnimations;
        List<SingleAnimation> currentAnimations;
        Dictionary<string, GameTexture> textures;
        Dictionary<string, GameTexture> Textures
        {
            get
            {
                if (textures == null) textures = new Dictionary<string, GameTexture>();
                return textures;
            }
        }

        public StaticAnimations()
        {
            sourceAnimations = new List<SingleAnimation>();
            currentAnimations = new List<SingleAnimation>();
        }

        public void Update()
        {
            for (int i = 0; i < currentAnimations.Count; i++)
            {
                SingleAnimation anim = currentAnimations[i];
                if (anim.isPlaying)
                {
                    anim.Update();
                }
                else { currentAnimations.RemoveAt(i--); }
            }

        }

        public void Draw()
        {

            foreach (var anim in currentAnimations)
            {
                if (anim.isPlaying)
                {
                    Rectangle rect = new Rectangle(anim.current_frame.Xcoord,
                                                   anim.current_frame.Ycoord,
                                                   anim.FrameResolution.width,
                                                   anim.FrameResolution.height);
                    Textures[anim.current_frame.TextureF].Draw(rect, anim.Position);
                }
            }

        }

        /// <summary>
        /// Создание новой анимации из источника с именем name
        /// и проигрывание.
        /// </summary>
        /// <param name="name">Имя в списке источников</param>
        /// <param name="position">Позиция</param>
        /// <param name="tag">Опцциональное имя этого клона</param>
        public void PlayAnimation(string name, Vector2 position, string tag = "none")
        {

            SingleAnimation anim = sourceAnimations.Find(a => a.Name == name);
            if (anim == null) return;
            SingleAnimation newanim = anim.Clone();
            newanim.isPlaying = true;
            newanim.Position = position;
            newanim.Tag = tag;
            newanim.isPlaying = true;
            currentAnimations.Add(newanim);
        }

        public void Init()
        {
            foreach (var anim in sourceAnimations)
            {
                foreach (var frame in anim.Frames)
                {
                    if (!Textures.ContainsKey(frame.TextureF))
                    {
                        GameTexture gt = new GameTexture(false, frame.TextureF);
                        Textures.Add(frame.TextureF, gt);
                    }
                }
                anim.isPlaying = false;
            }
        }

        /// <summary>
        /// Добавление анимации в список источников анимации
        /// </summary>
        /// <param name="path">Путь от папки контента</param>
        /// <param name="name">Имя</param>
        public void AddAnimation(string path, string name = "")
        {
            string file = AppDomain.CurrentDomain.BaseDirectory + STORAGE.Depository.CommonContent.RootDirectory + "\\" + path;
            FileStream fstream = File.Open(file, FileMode.Open);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            SingleAnimation animation = (SingleAnimation)binaryFormatter.Deserialize(fstream);
            fstream.Close();
            if (name != "") animation.Name = name;
            animation.FrameResolution.origin = 0.5f * (new Vector2(animation.FrameResolution.width, animation.FrameResolution.height));
            sourceAnimations.Add(animation);
        }


        /// <summary>
        /// Останавливает запущенную анимацию
        /// в следующем апдейте она удалится из листа
        /// активных анимаций
        /// </summary>
        /// <param name="tag">Таг анимации</param>
        /// <returns>true - анимация найдена и остановлена
        /// false - анимация не найдена в списке активных</returns>
        public bool Stop(string tag)
        {
            SingleAnimation anim = currentAnimations.Find(a => a.Tag == tag);
            if (anim != null) 
            {
                anim.isPlaying = false; 
                return true; 
            }
            return false;
        }
    }

}

﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using STORAGE;
using ENGINE.Graphics;
using System.Collections.Generic;

namespace ENGINE.Logic
{
    [Serializable]
    public class BackgroundDef
    {
        public enum TileType
        {
            None,
            Vertical,
            Horizontal,
            Both
        }
        public GameTexture gameTexture;
        public Vector2 Parallax;
        public TileType tileType;
        public float layer;
        public int brightness;
        public bool isSky;


        public bool isTiledVertically { get { return tileType == TileType.Vertical || tileType == TileType.Both; } }
        public bool isTiledHorizontally { get { return tileType == TileType.Horizontal || tileType == TileType.Both; } }
        /// <summary>
        /// Конструктор бекграунда
        /// </summary>
        /// <param name="tex">Текстура бекграунда</param>
        /// <param name="p">параллакс</param>
        /// <param name="l">слой</param>
        /// <param name="tV">тайлить по вертикали</param>
        /// <param name="tH">тайлить по горизонтали</param>
        public BackgroundDef(GameTexture tex)
        {
            this.gameTexture = tex;
            this.Parallax = new Vector2(0.001f, 0.001f);
            this.layer = 0;
            this.brightness = 255;
            SetTileType(false, false);
        }

        public override string ToString()
        {
            string s = gameTexture.Folder;
            s = s.Replace("Textures\\", "");
            return s;
        }

        public void SetTileType(bool tV, bool tH)
        {
            if (tV && tH) tileType = TileType.Both;
            else if (tV) tileType = TileType.Vertical;
            else if (tH) tileType = TileType.Horizontal;
            else tileType = TileType.None;
        }

        public void SetVertical(bool tV)
        {
            switch (tileType)
            {
                case TileType.None:
                    if (tV) tileType = TileType.Vertical;
                    break;
                case TileType.Vertical:
                    if (!tV) tileType = TileType.None;
                    break;
                case TileType.Horizontal:
                    if (tV) tileType = TileType.Both;
                    break;
                case TileType.Both:
                    if (!tV) tileType = TileType.Horizontal;
                    break;
                default:
                    break;
            }
        }

        public void SetHorizontal(bool tH)
        {
            switch (tileType)
            {
                case TileType.None:
                    if (tH) tileType = TileType.Horizontal;
                    break;
                case TileType.Vertical:
                    if (tH) tileType = TileType.Both;
                    break;
                case TileType.Horizontal:
                    if (!tH) tileType = TileType.None;
                    break;
                case TileType.Both:
                    if (!tH) tileType = TileType.Vertical;
                    break;
                default:
                    break;
            }
        }
    }

    [Serializable]
    public class GameBackground
    {
        public List<BackgroundDef> backgrounds = new List<BackgroundDef>();

        public bool Show_in_game = true;
        public bool Show_in_editor = true;
        public int StartHeight = 0;

        public bool full = false;

        public void Draw(Camera2d camera, bool isSkyBackground = false)
        {
            if (!Show_in_game && !Config.EditorMode) return;
            if (!Show_in_editor) return;

            Rectangle DrawViewport = new Rectangle(
                       (int)Math.Round(camera.CameraPosition.X - Depository.ScreenResolution.X / 2 / camera.Zoom),
                       (int)Math.Round(camera.CameraPosition.Y - Depository.ScreenResolution.Y / 2 / camera.Zoom),
                       (int)Math.Round(Depository.ScreenResolution.X / camera.Zoom),
                       (int)Math.Round(Depository.ScreenResolution.Y / camera.Zoom));

            Depository.SpriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, SamplerState.PointWrap, null, null, null, camera.GetTransformation());
            foreach (BackgroundDef b in backgrounds)
            {
                if (isSkyBackground ^ b.isSky)
                    continue;

                Vector2 displacement = -camera.CameraPosition * b.Parallax;
                Rectangle DestinationRect = new Rectangle(0, 0, 0, 0), SourceRect;
                switch (b.tileType)
                {
                    case BackgroundDef.TileType.None:
                        DestinationRect = b.gameTexture.GetTextureArea();
                        DestinationRect.Offset(new Point((int)Math.Round(b.gameTexture.Origin.X), (int)Math.Round(b.gameTexture.Origin.Y)));
                        break;
                    case BackgroundDef.TileType.Vertical:
                        DestinationRect.X = (int)Math.Round(b.gameTexture.Position.X + displacement.X);
                        DestinationRect.Y = (int)Math.Round(camera.CameraPosition.Y - DrawViewport.Height / 2 + displacement.Y);
                        DestinationRect.Height = (int)Math.Round(camera.CameraPosition.Y + DrawViewport.Height);
                        DestinationRect.Width = b.gameTexture.Texture2d.Width;
                        break;
                    case BackgroundDef.TileType.Horizontal:
                        DestinationRect.X = (int)Math.Round(camera.CameraPosition.X - DrawViewport.Width / 2 + displacement.X);
                        DestinationRect.Y = (int)Math.Round(b.gameTexture.Position.Y + displacement.Y);
                        DestinationRect.Height = b.gameTexture.Texture2d.Height;
                        DestinationRect.Width = (int)Math.Round(camera.CameraPosition.X + DrawViewport.Width);
                        break;
                    case BackgroundDef.TileType.Both:
                        DestinationRect = DrawViewport;
                        break;
                    default:
                        break;
                }
                SourceRect = DestinationRect;
                SourceRect.X = (int)Math.Round(DestinationRect.X - b.gameTexture.Position.X);
                SourceRect.Y = (int)Math.Round(DestinationRect.Y - b.gameTexture.Position.Y);
                SourceRect.Offset(new Point(-(int)Math.Round(displacement.X), -(int)Math.Round(displacement.Y)));

                float bAmbient = b.brightness / 255.0f;
                if (isSkyBackground) bAmbient *= GAME.GameManager.LvlController.AmbientLight;

                Color color = new Color(bAmbient, bAmbient, bAmbient);
                Depository.SpriteBatch.Draw(b.gameTexture.Texture2d, DestinationRect, SourceRect, color, 0, Vector2.Zero, SpriteEffects.None, b.layer);
            }
            Depository.SpriteBatch.End();
        }

        public void AddTexture(string folder)
        {
            GameTexture t = new GameTexture(true, folder);
            if (backgrounds.Count > 0)
            {
                t.Position = backgrounds[backgrounds.Count - 1].gameTexture.Position;
                t.Position.Y += backgrounds[backgrounds.Count - 1].gameTexture.TextureResolution.height;
            }
            backgrounds.Add(new BackgroundDef(t));
        }

        public void DeleteBackground(BackgroundDef background)
        {
            backgrounds.Remove(background);
        }

        public BackgroundDef GetBackground(string s)
        {
            foreach (var item in backgrounds)
            {
                if (s == item.ToString())
                {
                    return item;
                }
            }
            return null;
        }
    }
}

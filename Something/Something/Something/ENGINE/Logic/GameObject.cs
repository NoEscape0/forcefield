﻿using System.Collections.Generic;
using ENGINE.Graphics;
using ENGINE.Logic.ObjAttributes;
using System;
using Microsoft.Xna.Framework;

namespace ENGINE.Logic
{
    [Serializable]
    public class GameObject
    {
        public GameObject(string folder, bool gamecontent = true)
        {
            data = new DataPool();

            Gtexture = new GameTexture(true, folder);
            if (!gamecontent) Gtexture = new GameTexture(false, folder);

            AddedAttributes = new List<Functions.Attr>();
            attachedObjects = new List<GameObject>();
            attachedTo = null;

            data.WriteDataSimple("editorOnly_attr", new List<Functions.Attr>());

            AddDefaultAttributes();
        }

        public GameObject()
        {
        }

        public DataPool data;
        public GameTexture Gtexture;
        public List<Functions.Attr> AddedAttributes;

        [NonSerialized]
        private List<GameObject> attachedObjects;
        [NonSerialized]
        public GameObject attachedTo;
        [NonSerialized]
        public FarseerPhysics.Dynamics.Body body;

        public float rotation;

        public delegate void Del(GameObject sender);
        public event Del OnDraw;
        public event Del OnUpdate;
        public bool isBullet;
        public bool isEnemy;
        public bool isShadowCaster = false;
        //public bool HasLight { get { return (this.data.ReadData("ALight") as ALight) != null; } }
        public bool HasLight = false;
        public bool isCircleBody = false;

        public virtual void Update()
        {
            if (OnUpdate != null) OnUpdate(this);
        }

        public virtual void Draw()
        {
            if (OnDraw != null) OnDraw(this);
            //DEBUG
            if (STORAGE.Config.EditorMode && attachedTo != null && Gtexture != null) Grid.BatchLine(Gtexture.Position, attachedTo.Gtexture.Position, Microsoft.Xna.Framework.Color.Gold);
        }

        private void AddDefaultAttributes()
        {
            Functions.AddAttribute(Functions.Attr.Select, this, true);
            Functions.AddAttribute(Functions.Attr.Drag, this, true);
            Functions.AddAttribute(Functions.Attr.Draw, this);
        }
        #region AttachedObjects
        public List<GameObject> AttachedObjects { get { if (attachedObjects == null) attachedObjects = new List<GameObject>(); return attachedObjects; } }

        /// <summary>
        /// Прикрепляет к текущему объекту Gobject
        /// </summary>
        /// <param name="Gobject">Прикрепляемый объект</param>
        public void AttachObject(GameObject Gobject)
        {
            if (!Functions.HaveAttribute(Functions.Attr.Barrier, this)) return; // прикрепляться только к барьерам
            if (Gobject.attachedTo != null && Gobject.attachedTo != this) Gobject.DeAttach();
            if (!AttachedObjects.Contains(Gobject)) { Gobject.attachedTo = this; AttachedObjects.Add(Gobject); }
        }
        /// <summary>
        /// Отсоединяет Gobject от текущего объекта
        /// </summary>
        /// <param name="Gobject">Отсоединяемый объект</param>
        public void DeAttachObject(GameObject Gobject)
        {
            AttachedObjects.Remove(Gobject);
            Gobject.attachedTo = null;
        }

        /// <summary>
        /// Если этот объект прикреплен, открепляет его
        /// </summary>
        public void DeAttach()
        {
            if (attachedTo != null) attachedTo.DeAttachObject(this);
        }
        /// <summary>
        /// Обрывает все связи
        /// </summary>
        public void DeAttachAll()
        {
            int i = 0;
            while (AttachedObjects.Count > 0)
            {
                AttachedObjects[i].DeAttach();
            }
            AttachedObjects.Clear(); // на всякий случай :p
        }
        #endregion

        public void DeleteBody(FarseerPhysics.Dynamics.World world)
        {
            body.UserData = null;
            world.RemoveBody(body);
            if (Functions.HaveAttribute(Functions.Attr.MovementHero, this))
            {
                (this.data.ReadData("MController") as MoveController).InitComplete = false;
            }
        }

        public void DrawShadowCasters(Vector2 relativePosition, float scale)
        {
            if (isShadowCaster && STORAGE.Depository.DrawViewport.Intersects(Gtexture.GetTextureArea()))
            {
                Color c = Gtexture.Color;
                Gtexture.Color = Color.Black;
                Gtexture.DrawScale(relativePosition, scale);
                Gtexture.Color = c;
            }
        }
    }
}

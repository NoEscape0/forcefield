﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using ENGINE.Logic;
using ENGINE.Logic.ObjAttributes;

namespace ENGINE.Logic
{
    public static class EditorEventHandler
    {
        public static void AddEvent(string name)
        {
            if (STORAGE.Config.EditorMode)
            {
                Thread t = new Thread(delegate()                     
                    {
                        events.Add(new EditorEvent((LevelController)Functions.DeepClone(GAME.GameManager.LvlController), name));

                        int r = rnd.Next(200);

                        FloatingText ft = FloatingText.Create(FloatingText.TextType.SlideRight, "[+EVENT]" + name,
                            1.0f, Microsoft.Xna.Framework.Color.Green, new Microsoft.Xna.Framework.Vector2(STORAGE.Depository.ScreenResolution.X / 2, STORAGE.Depository.ScreenResolution.Y - rnd.Next(200) - 50), 0, 9, true);


                        while (events.Count > maxEvents)
                        {
                            events.RemoveAt(0);
                        }
                    });
              
                
                t.Start();
            }
        }

        public static void UndoEvent()
        {
            if (STORAGE.Config.EditorMode & events.Count >= 1)
            {
                GAME.GameManager.LvlController = (LevelController)Functions.DeepClone(events[events.Count - 1].Controller);

                //09876543 тут нужно удалить все FloatingText в GAME.GameManager.LvlController
                for (int i = 0; i < GAME.GameManager.LvlController.ObjectArray.Count; i++)
                {
                    GameObject go = GAME.GameManager.LvlController.ObjectArray[i];
                    if (go.GetType() == typeof(ENGINE.Logic.FloatingText))
                    {
                        GAME.GameManager.LvlController.ObjectArray.RemoveAt(i--);
                    }
                }

                FloatingText ft = FloatingText.Create(FloatingText.TextType.SlideRight, "[UNDO EVENT]" + events[events.Count - 1].name,
                          1.0f, Microsoft.Xna.Framework.Color.Blue, new Microsoft.Xna.Framework.Vector2(STORAGE.Depository.ScreenResolution.X / 2, STORAGE.Depository.ScreenResolution.Y - rnd.Next(200) - 50), 0, 9, true);

             
                events.RemoveAt(events.Count -1);


            }
        }
        private static Random rnd = new Random();

        public static int MaxEvents
        {
            get { return maxEvents; }
            set
            {
                int was = maxEvents;
                maxEvents = value;
                if (maxEvents <= 1) maxEvents = 1;
                if (maxEvents >= 50) maxEvents = 50;


                if (maxEvents < was)
                {
                    while (events.Count > maxEvents)
                    {
                        events.RemoveAt(0);
                    }
                }
            }
        }
        static int maxEvents = 30;


        public static List<EditorEvent> events = new List<EditorEvent>();
    }

    public class EditorEvent
    {
        public EditorEvent(LevelController lvC, string Name)
        {
            Controller = lvC;
            name = Name;
        }

        public LevelController Controller;
        public string name = "";
    }
}

﻿using System;
using ENGINE.Logic.ObjAttributes;

namespace ENGINE.Logic.Bonus
{
    [Serializable]
    public static class BonusAction
    {
        public static void Action (ARpgCharacter.CharacterParams rpgCharacter, BonusStandart bonus)
        {
            switch (bonus.type)
            {
                case  BonusType.coin:
                    {
                        rpgCharacter.Coins += (int)bonus.value;
                        break;
                    }
                case BonusType.health:
                    {
                        rpgCharacter.HP += (int)bonus.value;
                        break;
                    }
                case BonusType.mana:
                    {
                        rpgCharacter.MP += (int)bonus.value;
                        break;
                    }
            }

        }
    }
}

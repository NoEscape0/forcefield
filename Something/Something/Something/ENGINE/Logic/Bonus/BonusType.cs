﻿using System;

namespace ENGINE.Logic.Bonus
{
    [Serializable]
    public enum BonusType
    {
        health,
        mana,
        coin
    }

    [Serializable]
    public class BonusStandart
    {
        public BonusType type;
        public float value;
    }
}

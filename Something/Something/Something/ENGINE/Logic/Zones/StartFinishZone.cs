﻿using System;
using System.Collections.Generic;
using ENGINE.Logic.ObjAttributes;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;

namespace ENGINE.Logic.Zones
{
    [Serializable]
    public class StartFinishZone : GameObject
    {
        public StartFinishZone(bool startzone) : base(@"Textures\System\start_zone")
        {
            if (!startzone)
            {
                this.Gtexture.Folder = @"Textures\System\finish_zone";
                this.Gtexture.RestoreTexture();
            }


            List<Functions.Attr> edOnlyArrt = (List<Functions.Attr>)this.data.ReadData("editorOnly_attr"); //вызываем массив для изменения аттрибутов
            edOnlyArrt.Add(Functions.Attr.Draw);
            this.data.WriteDataSimple("editorOnly_attr", edOnlyArrt);

            InitComplete = true;

            InitBody(this);

            Startzone = startzone;

            this.OnUpdate += new Del(StartZone_OnUpdate);
        }

        public bool Startzone;

        void StartZone_OnUpdate(GameObject sender)
        {
            FarseerPhysics.Dynamics.Body MainBody = GetBody(sender);
            if (MainBody == null) return;

            #region Ручные изменения положения в редакторе
            if (oldPos != sender.Gtexture.Position) { MainBody.SetTransform(ConvertUnits.ToSimUnits(sender.Gtexture.Position + realCenter), sender.rotation); MainBody.ResetDynamics(); MainBody.Awake = true; ; }
            if (oldRot != sender.rotation) { MainBody.Rotation = sender.rotation; MainBody.ResetDynamics(); MainBody.Awake = true; ; }
            sender.rotation = MainBody.Rotation;
            sender.Gtexture.Position = ConvertUnits.ToDisplayUnits(MainBody.Position) - realCenter;
            oldPos = sender.Gtexture.Position;
            oldRot = sender.rotation;
            #endregion
        }

        public bool OnCollisionWithStartFinishZone(Fixture fixtureA, Fixture fixtureB, FarseerPhysics.Dynamics.Contacts.Contact contact)
        {
            GameObject hero = fixtureB.Body.UserData as GameObject;
            if (hero == null)
                return false;

            if (!Functions.HaveAttribute(Functions.Attr.MovementHero, hero))
                return true;

            GameObject StartFinishZoneObj = fixtureA.Body.UserData as GameObject;

            //collision
            if (Functions.HaveAttribute(Functions.Attr.MovementHero, hero) & !STORAGE.Config.EditorMode)
            {
                if (!Startzone)
                {
                    GAME.GameManager.Map.CompleteLevel();
                }                
            }

            return false;
        }

        protected void InitBody(GameObject owner)
        {
            Body mainBody = BodyFactory.CreateRectangle(GAME.GameManager.LvlController.PhysicWorld,
                                                  ConvertUnits.ToSimUnits(owner.Gtexture.TextureResolution.width) * owner.Gtexture.Scale.X,
                                                  ConvertUnits.ToSimUnits(owner.Gtexture.TextureResolution.height) * owner.Gtexture.Scale.Y,
                                                  1);
            mainBody.Position = ConvertUnits.ToSimUnits(owner.Gtexture.Position);
            mainBody.BodyType = BodyType.Static;
            mainBody.UserData = owner;
            mainBody.OnCollision += new OnCollisionEventHandler(OnCollisionWithStartFinishZone);
            mainBody.IsSensor = true;
            owner.body = mainBody;
        }

        public FarseerPhysics.Dynamics.Body GetBody(GameObject owner)
        {
            if (owner.body != null && owner.body.UserData != null && InitComplete) return owner.body;
            InitComplete = true;
            InitBody(owner);
            return owner.body;
        }

        [NonSerialized]
        protected bool InitComplete;
        protected Microsoft.Xna.Framework.Vector2 oldPos;
        protected Microsoft.Xna.Framework.Vector2 realCenter = Microsoft.Xna.Framework.Vector2.Zero;
        protected float oldRot;
    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using STORAGE;


//show in editor only
namespace ENGINE.Logic
{
    public static class TextMessager
    {
        public static void ShowStaticText(Vector2 absPos, string text)
        {
            if (!Config.EditorMode) return;

            txt = text;
            pos = absPos;
        }

        static string txt = "";
        static Vector2 pos = new Vector2(0, 0);


        public static void Update()
        {
            if (!Config.EditorMode) return;


        }


        public static void Draw()
        {
            if (!Config.EditorMode) return;

            Depository.SpriteBatch.DrawString(GAME.GUIManager.MenuFont, txt, pos, Color.Red, 0, Vector2.Zero, 1.0f, SpriteEffects.None, 0.01f);
        }
    }
}

﻿using System.Collections.Generic;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ENGINE.Logic
{
    [Serializable]
    public static class Library
    {
        private static List<LibNote> LibraryObjects = new List<LibNote>();
        public static void LoadFromFile()
        {
            List<LibNote> objToSerialize = null;
            try
            {
                FileStream fstream = File.Open("objectlibrary.lib", FileMode.Open);
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                objToSerialize = (List<LibNote>)binaryFormatter.Deserialize(fstream);
                fstream.Close();
                LibraryObjects = objToSerialize;
            }
            catch { }
        }
        public static void Add(GameObject obj, string name)
        {
            bool contains = false;
            foreach (LibNote note in LibraryObjects)
            {
                if (note.name == name) contains = true;
            }

            if (!contains)
            {
                LibraryObjects.Add(new LibNote(obj, name));
                save();
            }

        }
        public static void Remove(int index)
        {
            try
            {
                LibraryObjects.RemoveAt(index);
                save();
            }
            catch { };
        }
        public static List<LibNote> GetLibraryArray()
        {
            return LibraryObjects;
        }
        public static GameObject GetByName(string name)
        {
            foreach (LibNote note in LibraryObjects)
            {
                if (note.name == name) return note.obj;
            }
            return null;
        }

        public static void save()
        {
            FileStream fstream = File.Open("objectlibrary.lib", FileMode.Create);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(fstream, LibraryObjects);
            fstream.Close();
        }
    }

    [Serializable]
    public class LibNote
    {
        public LibNote(GameObject Obj, string Name)
        {
            obj = Obj;
            name = Name;
        }
        public GameObject obj;
        public string name;
    }
}

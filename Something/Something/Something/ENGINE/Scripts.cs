﻿using System;
using GAME;
using System.Linq;
using System.Reflection;



namespace ENGINE.Scripts
{
    public class Scripts
    {
        //#1 - проигрывает слушайный звук из списка 
        [Serializable]
        public class PlayRandomSound : ScriptController.IScript
        {
            public bool DoWork()
            {
                if (sound == null) return false;

                sound.Play();

                return true;
            }

            public RandomSound sound = new RandomSound();
        }
    }
}

namespace ENGINE
{
    public static class ScriptController
    {
        public interface IScript
        {
            bool DoWork();
        }

        public static Type[] GetScripts()
        {
            string namespac = typeof(ENGINE.Scripts.Scripts).Namespace;
            return Assembly.GetExecutingAssembly().GetTypes().Where(t => String.Equals(t.Namespace, namespac, StringComparison.Ordinal)).ToArray();
        }
    }
}

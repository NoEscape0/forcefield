﻿using STORAGE;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using System;

namespace GAME
{
    public static class SoundManager
    {
        //общая громкость (действует на всю остальную громкость)
        public static float Common_Volume = 1.0f;



        public static MP3song background = new MP3song(@"Sound\Music\main menu background", 0.3f);

        public static void StopAll(bool smooth)
        {
            if (smooth)
            {
                foreach (SoundEffectInstance i in playingSounds) smoothStoppingSounds.Add(i);
                playingSounds.Clear();
            }
            else
            {
                foreach (SoundEffectInstance i in playingSounds) i.Stop();
                playingSounds.Clear();
            }

            MediaPlayer.Stop();
        }

        public static void Update()
        {
            if (smoothStoppingSounds.Count > 0)
            {
                foreach (SoundEffectInstance i in smoothStoppingSounds)
                {
                    float f = i.Volume;
                    f -= (float)(Depository.Elapsed / 2);
                    if (f > 0) i.Volume = f; else i.Volume = 0;
                }

                foreach (SoundEffectInstance i in smoothStoppingSounds) if (i.Volume <= 0) i.Stop();
            }


            for (int i = 0; i < playingSounds.Count; ++i)
                if (playingSounds[i].State == SoundState.Paused | playingSounds[i].State == SoundState.Stopped) playingSounds.Remove(playingSounds[i]);

            for (int i = 0; i < smoothStoppingSounds.Count; ++i)
                if (smoothStoppingSounds[i].State == SoundState.Paused | smoothStoppingSounds[i].State == SoundState.Stopped) smoothStoppingSounds.Remove(smoothStoppingSounds[i]);
        }

        public static List<SoundEffectInstance> playingSounds = new List<SoundEffectInstance>();
        private static List<SoundEffectInstance> smoothStoppingSounds = new List<SoundEffectInstance>();
    }

    [Serializable]
    public class Sound
    {
        [NonSerialized]
        private SoundEffect effect;
        [NonSerialized]
        private SoundEffectInstance SoundEffectI;

        public Sound(string folder)
        {
            this.folder = folder;
            RestoreSound();
        }

        public Sound(string folder, float Volume)
        {
            this.folder = folder;
            RestoreSound();
            this.Volume = Volume;
        }

        public void RestoreSound()
        {
            effect = Depository.CommonContent.Load<SoundEffect>(folder);
            SoundEffectI = effect.CreateInstance();
        }
        public string folder;

        /// <summary>
        /// palyhard- оборвать если идет и запустить заного
        /// </summary>
        public void Play()
        {
            try
            {
                SoundEffectI.Volume = (SoundManager.Common_Volume * 100) * (volume / 100);
                if (playHard) SoundEffectI.Stop();
                SoundEffectI.Play();
                SoundManager.playingSounds.Add(SoundEffectI);
            }
            catch
            {
                RestoreSound();
                Play();
            }
        }
        public bool playHard = false;

        public void Stop()
        {
            try
            {
                SoundEffectI.Stop();
            }
            catch
            {
                RestoreSound();
            }
        }

        public SoundState state
        {
            get
            {
                return SoundEffectI.State;
            }
        }

        public float Volume
        {
            get { return volume; }
            set
            {
                if (value < 0) volume = 0;
                else if (value > 1) volume = 1;
                else volume = value;

                if (state == SoundState.Playing)
                {
                    SoundEffectI.Stop();
                    Play();
                }
            }
        }
        float volume = 1;
    }

    [Serializable]
    public class RandomSound
    {
        public RandomSound(params Sound[] sounds)
        {
            Sounds = sounds;
        }

        public Sound[] Sounds;

        public void Play()
        {
            bool createnew = false;

            if (current != null)
            {
                if (current.state != SoundState.Playing)
                {
                    createnew = true;
                }
            }
            else createnew = true;

            if (createnew & Sounds.Length > 0)
            {
                int r = ENGINE.Logic.LevelController.rnd.Next(Sounds.Length);
                Sounds[r].Play();
                current = Sounds[r];
            }
        }

        public void Stop()
        {
            if (current != null) current.Stop();
        }

        [NonSerialized]
        Sound current;
    }

    [Serializable]
    public class MP3song
    {
        public float Volume = 1.0f;
        private Song Song;
        string folder;

        public MP3song(string folder, float volume = 1.0f)
        {
            Volume = volume;
            try
            {
                Song = Depository.CommonContent.Load<Song>(folder);
            }
            catch (Exception ex) {/*невозможно загрузить*/ }
            this.folder = folder;

        }

        public void Play()
        {
            try
            {
                if (Song == null) Song = Depository.CommonContent.Load<Song>(folder);

                MediaPlayer.Play(Song);
                MediaPlayer.Volume = (SoundManager.Common_Volume * 100) * (Volume / 100);
            }
            catch (Exception ex)
            { 
                // нет аудио, ничего не играем
            }
        }
    }
}



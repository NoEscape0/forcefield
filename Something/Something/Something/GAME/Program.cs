﻿using System;


namespace GAME
{
    public static class Program
    {

        public static Main game;

        [STAThreadAttribute]
        static void Main(string[] args)
        {
            game = new Main();
            game.Run();
        }
    }
}


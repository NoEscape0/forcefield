﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Windows.Forms;
using GAME;
using STORAGE;
using ENGINE.Logic;

namespace GAME
{
    public class Main : Microsoft.Xna.Framework.Game
    {
        public Main()
        {
            //Content.RootDirectory = "Content";
            Depository.CommonContent = new Microsoft.Xna.Framework.Content.ContentManager(this.Services, "Content");
            Depository.GameContent = new Microsoft.Xna.Framework.Content.ContentManager(this.Services, "Content");

            Depository.MainForm = Control.FromHandle(this.Window.Handle) as Form;
            Depository.Graphics = new GraphicsDeviceManager(this);
            Depository.Game = this;
            IsFixedTimeStep = Config.isFixedTimeStep;
        }

        protected override void Initialize()
        {
            Application.EnableVisualStyles();
            Depository.Adapter = Depository.Graphics.GraphicsDevice.Adapter;
            Depository.GraphDevice = Depository.Graphics.GraphicsDevice;
            Depository.BasicEffect = new BasicEffect(Depository.GraphDevice);
            ScreenParametrs.SetScreenParams();
            CursorManager.Initialize();
            base.Initialize();
        }
        protected override void LoadContent()
        {
            ProfilesManager.CheckDirectories();
            Depository.SpriteBatch = new SpriteBatch(GraphicsDevice);
            GUIManager.LoadContent(GAME.Program.game);
            IsMouseVisible = Config.MouseVisible;
        }
        protected override void UnloadContent()
        {
            
        }
        protected override void Update(GameTime gameTime)
        {
            if (this.IsActive) Depository.GameFormActive = true;
             else Depository.GameFormActive = false;

            Depository.keyboardState = Keyboard.GetState();
            Depository.mouseState = Mouse.GetState();
            Depository.Elapsed = gameTime.ElapsedGameTime.TotalSeconds;
            Depository.gameTime = gameTime;

            GameManager.Update();



            Depository.oldKeyboardState = Keyboard.GetState();
            Depository.oldMouseState = Mouse.GetState();
            base.Update(gameTime);
        }
        protected override void Draw(GameTime gameTime)
        {
            GameManager.Draw();

            base.Draw(gameTime);
        }
    }
}

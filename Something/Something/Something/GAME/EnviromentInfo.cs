﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using ENGINE.Logic;
using ENGINE.Logic.ObjAttributes;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;

namespace GAME
{
    public static class EnviromentInfo
    {
        public static class HERO
        {
            public static GameObject Hero
            {
                get
                {
                    return GAME.GameManager.LvlController.HERO;
                }
            }

            public static Vector2 Position
            {
                get
                {
                    if (GAME.GameManager.LvlController.HERO != null) return GAME.GameManager.LvlController.HERO.Gtexture.Position;
                    else return new Vector2(0, 0);
                }
            }
        }

        public static side_horisontal HeroSide_horisontal(GameObject obj)
        {
            if (HERO.Hero != null)
            {
                if (HERO.Hero.Gtexture.Position.X < obj.Gtexture.Position.X) return side_horisontal.left;
                else return side_horisontal.right;
            }
            return side_horisontal.none;
        }

        public static float GetDistanceToHero_points(GameObject obj)
        {
            if (HERO.Hero == null) return float.MaxValue;

            float x = GetBarrierDist_points_x(obj, HERO.Hero);
            float y = GetBarrierDist_points_y(obj, HERO.Hero);

            if (x > y) return x; else return y;
        }

        static float GetDistanceToHero_points_Xcoord(GameObject obj)
        {
            if (HERO.Hero == null) return float.MaxValue;
            return GetBarrierDist_points_x(obj, HERO.Hero);
        }

        static float GetDistanceToHero_points_Ycoord(GameObject obj)
        {
            if (HERO.Hero == null) return float.MaxValue;
            return GetBarrierDist_points_y(obj, HERO.Hero);
        }

        //получает список точек в форме барьера
        static List<Vector2> GetBarrierVertexes(GameObject obj)
        {
            if (!Functions.HaveAttribute(Functions.Attr.Barrier, obj)) return null;

            var barrier = obj.data.ReadData("ABarrier") as ABarrier;
            if (barrier.GetBody(obj) == null) return null;
            List<FarseerPhysics.Dynamics.Fixture> fxl = barrier.GetBody(obj).FixtureList;
            if (fxl == null) return null;
            //все вершины барьера
            FarseerPhysics.Common.Vertices vert = (fxl[0].Shape as FarseerPhysics.Collision.Shapes.PolygonShape).Vertices;

            FarseerPhysics.Common.Transform t;
            barrier.GetBody(obj).GetTransform(out t);
            Matrix m = Matrix.CreateRotationZ(t.Angle);
            m *= Matrix.CreateTranslation(new Vector3(ConvertUnits.ToDisplayUnits(t.Position), 0));

            List<Vector2> vertexes = new List<Vector2>();

            for (int i = 0; i < vert.Count; i++)
            {
                vertexes.Add(Vector2.Transform(ConvertUnits.ToDisplayUnits(vert[i]), m));
            }
            return vertexes;
        }

        //кротчайшее расстояние между наиближайшими точками двух ректанглов по координате Х
        static float GetBarrierDist_points_x(GameObject o1, GameObject o2)
        {
            var v1 = GetBarrierVertexes(o1);
            var v2 = GetBarrierVertexes(o2);


            float closestXpoints = float.MaxValue;

            if (v1 == null | v2 == null) return closestXpoints;

            foreach (var a in v1)
            {
                foreach (var b in v2)
                {
                    float dist = (float)(Math.Sqrt((double)Math.Pow(a.X - b.X, 2)));
                    if (dist < closestXpoints) closestXpoints = dist;
                }
            }
            return closestXpoints;
        }

        //кротчайшее расстояние между наиближайшими точками двух ректанглов по координате У
        static float GetBarrierDist_points_y(GameObject o1, GameObject o2)
        {
            var v1 = GetBarrierVertexes(o1);
            var v2 = GetBarrierVertexes(o2);


            float closestYpoints = float.MaxValue;

            if (v1 == null | v2 == null) return closestYpoints;

            foreach (var a in v1)
            {
                foreach (var b in v2)
                {
                    float dist = (float)(Math.Sqrt((double)Math.Pow(a.Y - b.Y, 2)));
                    if (dist < closestYpoints) closestYpoints = dist;
                }
            }
            return closestYpoints;
        }



        public enum side_horisontal
        {
            none, right, left
        }
        public enum CollisionDirection
        {
            None, Top, Down, Left, Right
        }


        //?
        public static float GetDistanceToNearestBarrier(CollisionDirection direction, GameObject target)
        {
            float rayLength = 100;// в юнитах симуляции, в пикселях в 64 раза больше
            float distance = 100000f;
            Vector2 startPoint1, startPoint2;
            startPoint2 = startPoint1 = target.body.Position;
            Vector2 dir = Vector2.Zero;
            switch (direction)
            {
                case CollisionDirection.Top:
                    dir -= Vector2.UnitY;
                    startPoint1.X -= ConvertUnits.ToSimUnits(target.Gtexture.TextureResolution.width * 0.5f);
                    startPoint2.X += ConvertUnits.ToSimUnits(target.Gtexture.TextureResolution.width * 0.5f);
                    break;
                case CollisionDirection.Down:
                    dir += Vector2.UnitY;
                    startPoint1.X -= ConvertUnits.ToSimUnits(target.Gtexture.TextureResolution.width * 0.5f);
                    startPoint2.X += ConvertUnits.ToSimUnits(target.Gtexture.TextureResolution.width * 0.5f);
                    break;
                case CollisionDirection.Left:
                    dir -= Vector2.UnitX;
                    startPoint1.Y -= ConvertUnits.ToSimUnits(target.Gtexture.TextureResolution.height * 0.5f);
                    startPoint2.Y += ConvertUnits.ToSimUnits(target.Gtexture.TextureResolution.height * 0.5f);
                    break;
                case CollisionDirection.Right:
                    dir += Vector2.UnitX;
                    startPoint1.Y -= ConvertUnits.ToSimUnits(target.Gtexture.TextureResolution.height * 0.5f);
                    startPoint2.Y += ConvertUnits.ToSimUnits(target.Gtexture.TextureResolution.height * 0.5f);
                    break;
                default:
                    break;
            }
            dir *= rayLength;
            GameManager.LvlController.PhysicWorld.RayCast((f, p, n, fr) =>
            {
                if (distance > rayLength * fr) distance = rayLength * fr;
                return 1;
            }
            , startPoint1, startPoint1 + dir);
            GameManager.LvlController.PhysicWorld.RayCast((f, p, n, fr) =>
            {
                if (distance > rayLength * fr) distance = rayLength * fr;
                return 1;
            }
            , startPoint2, startPoint2 + dir);
            distance = ConvertUnits.ToDisplayUnits(distance);
            return distance;
        }

        public static float GetDistanceToBreak(CollisionDirection direction, GameObject target)
        {
            float distance = float.MaxValue;
            if (target.attachedTo == null) return distance;
            GameObject floor = target.attachedTo;
            switch (direction)
            {
                case CollisionDirection.Top:
                case CollisionDirection.Down:
                    break;
                case CollisionDirection.Left:
                    distance = target.Gtexture.Position.X - floor.Gtexture.Position.X;
                    break;
                case CollisionDirection.Right:
                    distance = (floor.Gtexture.Position.X + floor.Gtexture.TextureResolution.width) - target.Gtexture.Position.X;
                    break;
                default:
                    break;
            }
            return distance;
        }

        /// <summary>
        /// Проверяет контакты и выставляет флаги текущих коллизий
        /// </summary>
        /// <param name="subject">Проверяемый GameObject</param>
        /// <param name="direction">направление на барьер</param>
        public static bool CheckCollision(GameObject subject, CollisionDirection direction)
        {
            GameObject tmp = null;
            return CheckCollision(subject, direction, out tmp);
        }

        /// <summary>
        /// Проверяет контакты и выставляет флаги текущих коллизий
        /// </summary>
        /// <param name="subject">Проверяемый GameObject</param>
        /// <param name="direction">направление на барьер</param>
        /// <param name="barrier">null или GameObject барьера</param>
        public static bool CheckCollision(GameObject subject, CollisionDirection direction, out GameObject barrier)
        {

            Vector2 manifoldNormal;
            FarseerPhysics.Common.FixedArray2<Vector2> points;
            barrier = null;
            bool result = false;
            Vector2 directionNormal = Vector2.Zero;
            if (subject == null || subject.body == null) return false;
            switch (direction)
            {
                case CollisionDirection.Top:
                    directionNormal = -Vector2.UnitY;
                    break;
                case CollisionDirection.Down:
                    directionNormal = Vector2.UnitY;
                    break;
                case CollisionDirection.Left:
                    directionNormal = -Vector2.UnitX;
                    break;
                case CollisionDirection.Right:
                    directionNormal = Vector2.UnitX;
                    break;
                default:
                    break;
            }

            ContactEdge edge = subject.body.ContactList;
            while (edge != null)
            {
                float f = 0;
                edge.Contact.GetWorldManifold(out manifoldNormal, out points);
                Fixture otherFixture = edge.Contact.FixtureA == subject.body.FixtureList[0] ? edge.Contact.FixtureB : edge.Contact.FixtureA;
                if (subject.isCircleBody)
                {
                    manifoldNormal = ConvertUnits.ToDisplayUnits(points[0]) - subject.Gtexture.Position;
                    manifoldNormal.Normalize();
                    f = Vector2.Dot(manifoldNormal, directionNormal);
                }
                else
                {
                    int sign = edge.Contact.FixtureA == subject.body.FixtureList[0] ? 1 : -1;
                    f = Vector2.Dot(manifoldNormal, sign*directionNormal);
                }

                if (edge.Contact.IsTouching() && !otherFixture.IsSensor)
                {
                    if (f > 0.3f) { result = true; barrier = edge.Other.UserData as GameObject; }
                }
                edge = edge.Next;
            }
            return result;
        }




        //часть работает не так как надо
        static float GetClosestDistanceToHero(GameObject obj)
        {
            if (HERO.Hero == null) return float.MaxValue;
            return GetClosestDistBetwin_SQUARE_rectangles(HERO.Hero, obj);
        }
        static float GetClosestDistBetwin_SQUARE_rectangles(GameObject o1, GameObject o2)
        {
            //это наборы точек двух квадратов
            var points_1 = GetBarrierVertexes(o1);
            var points_2 = GetBarrierVertexes(o2);

            float closestPoints = float.MaxValue;
            if (points_1 == null | points_2 == null) return closestPoints;


            float CLOSEST = float.MaxValue;

            for (int i = 0; i < points_1.Count; ++i)
            {
                Vector2 segmentONE_1, segmentONE_2;
                //точки первого отрезка
                segmentONE_1 = points_1[i];
                int one_2 = (i == points_1.Count - 1) ? 0 : i + 1;
                segmentONE_2 = points_1[one_2];

                for (int i2 = 0; i2 < points_2.Count; ++i2)
                {
                    Vector2 segmentTWO_1, segmentTWO_2;

                    //точки второго отрезка
                    segmentTWO_1 = points_2[i2];
                    int two_2 = (i2 == points_2.Count - 1) ? 0 : i2 + 1;
                    segmentTWO_2 = points_2[two_2];


                    //найти кратчайшее расстояние между отрезками
                    float d = GetClosestDistBetwinTwo_SEGMENTS(segmentONE_1, segmentONE_2, segmentTWO_1, segmentTWO_2);
                    if (d < CLOSEST) CLOSEST = d;
                }
            }

            return CLOSEST;
        }
        static float GetClosestDistBetwinTwo_SEGMENTS(Vector2 segmentONE_1, Vector2 segmentONE_2, Vector2 segmentTWO_1, Vector2 segmentTWO_2)
        {
            float va1 = GetClosestDistBetwin_SEGMENT_AND_POINT(segmentONE_1, segmentONE_2, segmentTWO_1);
            float va2 = GetClosestDistBetwin_SEGMENT_AND_POINT(segmentONE_1, segmentONE_2, segmentTWO_2);

            if (va1 <= va2) return va1; else return va2;
        }
        static float GetClosestDistBetwin_SEGMENT_AND_POINT(Vector2 segmentONE_1, Vector2 segmentONE_2, Vector2 point)
        {
            float distance = float.MaxValue;

            double A = GetDistBetwin_POINTS(segmentONE_1, point);
            double B = GetDistBetwin_POINTS(segmentONE_2, point);
            double C = GetDistBetwin_POINTS(segmentONE_1, segmentONE_2);

            double skobki = Math.Pow(((B * B - A * A + C * C) / (2 * C)), 2);

            double h = Math.Sqrt(B * B - skobki);
            return (float)h;
        }
        static float GetDistBetwin_POINTS(Vector2 p1, Vector2 p2)
        {
            double pow1 = (p1.X - p2.X) * (p1.X - p2.X);
            double pow2 = (p1.Y - p2.Y) * (p1.Y - p2.Y);
            float d = (float)Math.Sqrt(pow1 + pow2);
            return d;
        }
    }
}

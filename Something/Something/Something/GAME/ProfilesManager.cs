﻿using Microsoft.Xna.Framework;

namespace GAME
{
    public static class ProfilesManager
    {
        /// <summary>
        /// Проверяет директори с профилями (\\Profiles) и со всеми уровнями иры и стандартной картой (\\Levels), если их нет то создает их
        /// </summary>
        public static void CheckDirectories()
        {
            string currentDir = System.IO.Directory.GetCurrentDirectory();
            string[] dirs = System.IO.Directory.GetDirectories(currentDir);

            bool Profiles = false;
            bool Levels = false;

            for (int i = 0; i < dirs.Length; ++i)
            {
                if (dirs[i] == System.IO.Directory.GetCurrentDirectory() + "\\Profiles") Profiles = true;
                if (dirs[i] == System.IO.Directory.GetCurrentDirectory() + "\\Levels") Levels = true;
            }

            if (!Levels) System.IO.Directory.CreateDirectory(System.IO.Directory.GetCurrentDirectory() + "\\Levels");          
            if (!Profiles) System.IO.Directory.CreateDirectory(System.IO.Directory.GetCurrentDirectory() + "\\Profiles");            
        }

        public static void LoadProfile(string dirFolder)
        {
            string[] Profilefiles = System.IO.Directory.GetFiles(dirFolder);
            

            foreach (string s in Profilefiles)
            {
                if (s.IndexOf(".map") != -1)
                {
                    GAME.GameManager.Map = ENGINE.Logic.WorldMap.Load(s);
                    break;
                }
            }


            GAME.GameManager.SetMap();            

            GUIManager._LoadProfileWindow.Visible = false;            
        }

        public static void CreateProfile(string dirFolder)
        {
            string[] filesInLevels = System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory() + @"\Levels");

            bool haveMap = false;
            foreach (string s in filesInLevels)
            {
                if (s.IndexOf(".map") != -1)
                {
                    haveMap = true;
                }
            }

            if (!haveMap)
            {
                ENGINE.GUI.AquaGUI.ShowMessage("ERROR : cant create profile (there is no .map file in 'Levels' directory!)");
                return;
            }



            //копируем карту

            System.IO.Directory.CreateDirectory(dirFolder);

            foreach (string s in filesInLevels)
            {
                if (s.IndexOf(".map") != -1)  System.IO.File.Copy(s, dirFolder + @"\" + s.Substring(s.LastIndexOf(@"\") + 1), true);
            }

        }
    }
}

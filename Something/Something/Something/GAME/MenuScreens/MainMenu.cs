﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ENGINE.Graphics;
using Microsoft.Xna.Framework.Content;
using ENGINE.GUI;
using STORAGE;
using Microsoft.Xna.Framework.Media;

namespace GAME.MenuScreens
{
    public class MainMenu : StandartScreen
    {
        public MainMenu(ContentManager Content, object LastMenu)
        {
            Marker = new GameTexture(false, @"Textures\Advanced\point", new Vector2(-2000, -2000));
            MenuBg = new GameTexture(false, @"Textures\Advanced\menu", new Vector2(-2000, -2000));


            Start = new Button(new GameTexture(false, @"Textures\Advanced\button", new Vector2(-2000, -2000)));
            Start.Text = "Загрузить игру";
            Start.OnButtonClick += delegate(object sender)
            {
                GUIManager.ActiveMenu = null;
                GUIManager._LoadProfileWindow.Visible = true;
                GUIManager.RefreshProfileForm();
            };
            Start.OnCursorUnderButton += delegate(object sender)
            {
                Marker.Position = new Vector2(Start.GTexture.TopLeft.X - (Marker.TextureResolution.width + 5), Start.GTexture.Position.Y);
            };

            //=======

            Loading = new Button(new GameTexture(false, @"Textures\Advanced\button", new Vector2(-2000, -2000)));
            Loading.Text = "";
            Loading.OnButtonClick += delegate(object sender)
            {

            };
            Loading.OnCursorUnderButton += delegate(object sender)
            {
                Marker.Position = new Vector2(Loading.GTexture.TopLeft.X - (Marker.TextureResolution.width + 5), Loading.GTexture.Position.Y);
            };

            //=======
            Settings = new Button(new GameTexture(false, @"Textures\Advanced\button", new Vector2(-2000, -2000)));
            Settings.Text = "Настройки";
            Settings.OnButtonClick += delegate(object sender)
            {
                GUIManager.ActiveMenu = new SettingsM(Content, this);
            };
            Settings.OnCursorUnderButton += delegate(object sender)
            {
                Marker.Position = new Vector2(Settings.GTexture.TopLeft.X - (Marker.TextureResolution.width + 5), Settings.GTexture.Position.Y);
            };

            //=======

            Exit = new Button(new GameTexture(false, @"Textures\Advanced\button", new Vector2(-2000, -2000)));
            Exit.Text = "Выход";
            Exit.OnButtonClick += delegate(object sender)
            {
                SoundManager.StopAll(false);
                GAME.Program.game.Exit();
            };
            Exit.OnCursorUnderButton += delegate(object sender)
            {
                Marker.Position = new Vector2(Exit.GTexture.TopLeft.X - (Marker.TextureResolution.width + 5), Exit.GTexture.Position.Y);
            };

            SoundManager.background.Play();
        }

        GameTexture MenuBg;
        GameTexture Marker;
        public Button Start;
        public Button Loading;
        public Button Settings;
        public Button Exit;
        public string MenuName = "Главное меню";

        public override void  Draw(SpriteFont font)
        {
            MenuBg.Draw();
            Start.Draw(font);
            Loading.Draw(font);
            Settings.Draw(font);
            Exit.Draw(font);
            if (Marker.Position.X != 0 | Marker.Position.Y != 0)  Marker.Draw();
            Depository.SpriteBatch.DrawString(font, MenuName, new Vector2(MenuBg.TopLeft.X + 50, MenuBg.TopLeft.Y + 10), new Color(80, 80, 80));
            base.Draw(font);
        }
        public override void Update(Microsoft.Xna.Framework.Vector2 MouseCoord, float camZoom, bool LbClick, Vector2 ScreenRes)
        {
            Vector2 mPos = new Vector2(ScreenRes.X / 2, ScreenRes.Y / 2);
            MenuBg.Position = mPos;

            //продолжить (старт)
            Start.Update(MouseCoord, camZoom, LbClick);
            Start.GTexture.Position =
                new Vector2(ScreenRes.X / 2, MenuBg.TopLeft.Y + 330);

            //загрузка
            Loading.Update(MouseCoord, camZoom, LbClick);
            Loading.GTexture.Position =
                new Vector2(ScreenRes.X / 2, MenuBg.TopLeft.Y + 430);

            //настройки
            Settings.Update(MouseCoord, camZoom, LbClick);
            Settings.GTexture.Position =
                new Vector2(ScreenRes.X / 2, MenuBg.TopLeft.Y + 530);


            //выход
            Exit.Update(MouseCoord, camZoom, LbClick);
            Exit.GTexture.Position =
                new Vector2(ScreenRes.X / 2, MenuBg.TopLeft.Y + 630);

            base.Update(MouseCoord, camZoom, LbClick, ScreenRes);
        }
    }
}

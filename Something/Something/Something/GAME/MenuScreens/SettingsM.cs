﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ENGINE.Graphics;
using Microsoft.Xna.Framework.Content;
using ENGINE.GUI;
using STORAGE;

namespace GAME.MenuScreens
{
    public class SettingsM : StandartScreen
    {
        public SettingsM(ContentManager Content, object LastMenu)
        {
            Back = new Button(new GameTexture(false, @"Textures\Advanced\button", new Vector2(-2000, -2000)));
            Back.Text = "Назад";
            Back.OnButtonClick += delegate(object sender)
            {
                GUIManager.ActiveMenu = LastMenu;
            };

            MenuBg = new GameTexture(false, @"Textures\Advanced\settings_menu", new Vector2(-2000, -2000));
        }

        GameTexture MenuBg;
        public Button Back;
        public string MenuName = "Настройки";

        public override void Draw(SpriteFont font)
        {
            MenuBg.Draw();
            Back.Draw(font);
            Depository.SpriteBatch.DrawString(font, MenuName, new Vector2(MenuBg.TopLeft.X + 50, MenuBg.TopLeft.Y + 10), Color.Blue);
            base.Draw(font);
        }

        public override void Update(Microsoft.Xna.Framework.Vector2 globMouseCoord, float camZoom, bool LbClick, Vector2 ScreenRes)
        {
            Vector2 mPos = new Vector2(ScreenRes.X / 2, ScreenRes.Y / 2);
            MenuBg.Position = mPos;

            //back
            Back.Update(globMouseCoord, camZoom, LbClick);
            Back.GTexture.TopLeft = MenuBg.TopLeft + new Vector2(320, 715);
            //new Vector2(mPos.X + 320, MenuBg.TopLeft.Y + 715);

            base.Update(globMouseCoord, camZoom, LbClick, ScreenRes);
        }
    }
}

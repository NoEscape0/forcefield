﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ENGINE.Graphics;
using Microsoft.Xna.Framework.Content;
using ENGINE.GUI;
using STORAGE;
using Microsoft.Xna.Framework.Media;

namespace GAME.MenuScreens
{
    public class PauseM : StandartScreen
    {
        public PauseM(ContentManager Content, object LastMenu)
        {
            SoundManager.StopAll(true);

            Marker = new GameTexture(false, @"Textures\Advanced\point", new Vector2(-2000, -2000));


            Back = new Button(new GameTexture(false, @"Textures\Advanced\button", new Vector2(-2000, -2000)));
            Back.Text = "Назад";
            Back.OnButtonClick += delegate(object sender)
            {
                if (GUIManager.escapedFromMap == false) GameManager.SetGame(); else GameManager.SetMap();
                SoundManager.StopAll(true);
            };
            Back.OnCursorUnderButton += delegate(object sender)
            {
                Marker.Position = new Vector2(Back.GTexture.TopLeft.X - (Marker.TextureResolution.width + 5), Back.GTexture.Position.Y);
            };

            MainMenu = new Button(new GameTexture(false, @"Textures\Advanced\button", new Vector2(-2000, -2000)));
            MainMenu.Text = "Главное меню";
            MainMenu.OnButtonClick += delegate(object sender)
            {
                GUIManager.ActiveMenu = new MainMenu(Depository.CommonContent, null);
            };
            MainMenu.OnCursorUnderButton += delegate(object sender)
            {
                Marker.Position = new Vector2(MainMenu.GTexture.TopLeft.X - (Marker.TextureResolution.width + 5), MainMenu.GTexture.Position.Y);
            };

            MenuBg = new GameTexture(false, @"Textures\Advanced\menu", new Vector2(-2000, -2000));

            SoundManager.background.Play();
        }

        GameTexture Marker;
        GameTexture MenuBg;
        public Button Back;
        public Button MainMenu;

        public string MenuName = "Пауза";

        public override void Draw(SpriteFont font)
        {
            MenuBg.Draw();
            Back.Draw(font);
            MainMenu.Draw(font);
            if (Marker.Position.X != 0 | Marker.Position.Y != 0) Marker.Draw();
            Depository.SpriteBatch.DrawString(font, MenuName, new Vector2(MenuBg.TopLeft.X + 50, MenuBg.TopLeft.Y + 10), new Color(30, 30, 30));
            base.Draw(font);
        }

        public override void Update(Microsoft.Xna.Framework.Vector2 MouseCoord, float camZoom, bool LbClick, Vector2 ScreenRes)
        {
            Vector2 mPos = new Vector2(ScreenRes.X / 2, ScreenRes.Y / 2);
            MenuBg.Position = mPos;

            Back.Update(MouseCoord, camZoom, LbClick);
            Back.GTexture.Position =
                new Vector2(ScreenRes.X / 2, MenuBg.TopLeft.Y + 330);

            MainMenu.Update(MouseCoord, camZoom, LbClick);
            MainMenu.GTexture.Position =
                new Vector2(ScreenRes.X / 2, MenuBg.TopLeft.Y + 630);

            base.Update(MouseCoord, camZoom, LbClick, ScreenRes);
        }
    }
}

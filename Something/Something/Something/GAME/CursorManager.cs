﻿using Microsoft.Xna.Framework;
using ENGINE.Graphics;
using STORAGE;
using Microsoft.Xna.Framework.Input;

namespace GAME
{
    public static class CursorManager
    {
        public static void Initialize()
        {
            TNormal = new GameTexture(false, "Textures\\Advanced\\normal");
        }

        private static GameTexture TNormal;


        public static void DrawCursor()
        {
            Depository.SpriteBatch.Draw(TNormal.Texture2d, GAME.CursorManager.ScreenPosition, Color.White);
        }

        public static Vector2 ScreenPosition
        {
            get { return new Vector2(Depository.mouseState.X + Depository.MainForm.Left, Depository.mouseState.Y + Depository.MainForm.Top); }
        }

        public static Vector2 GlobalPosition(Vector2 pos, float zoom)
        {
            Vector2 Position = new Vector2();
            double lpointX = (pos.X * zoom - Depository.ScreenResolution.X / 2);
            double lpointY = (pos.Y * zoom - Depository.ScreenResolution.Y / 2);
            Position.X = (int)((lpointX + Depository.mouseState.X) / zoom);
            Position.Y = (int)((lpointY + Depository.mouseState.Y) / zoom);
            return Position;
        }

        public static Vector2 ScreenToWorld(Vector2 pos, Matrix view)
        {
            Matrix invView = Matrix.Invert(view);
            return Vector2.Transform(pos, invView);
        }

        public static Rectangle ScreenToWorld(Rectangle rect, Matrix view)
        {
            //Matrix invView = Matrix.Invert(view);
            Vector2 corner = ScreenToWorld(new Vector2(rect.X, rect.Y), view);
            float scale = 1/view.M11;
            return new Rectangle((int)corner.X, (int)corner.Y, (int)(rect.Width * scale), (int)(rect.Height * scale));
        }


        public static Vector2 GetWorldMouse()
        {
            Matrix invView = Matrix.Invert(GameManager.LvlController.camera.GetTransformation());
            return Vector2.Transform(STORAGE.Input.MouseScreenVector(), invView);
        }

        public static bool LBclick
        {
            get
            {
                if (Depository.mouseState.LeftButton == ButtonState.Pressed) return true;
                else return false;
            }
        }

        public static bool RBclick
        {
            get
            {
                if (Depository.mouseState.RightButton == ButtonState.Pressed) return true;
                else return false;
            }
        }
    }
}

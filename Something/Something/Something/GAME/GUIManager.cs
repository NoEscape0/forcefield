﻿using Microsoft.Xna.Framework;
using ENGINE.GUI;
using Microsoft.Xna.Framework.Graphics;
using GAME.MenuScreens;
using STORAGE;
using ENGINE.Logic;
using ENGINE.Logic.ObjAttributes;
using ENGINE.Graphics;
using RamGecXNAControls;
using Microsoft.Xna.Framework.Input;
using System.Text;
using System.Collections.Generic;

namespace GAME
{
    public static class GUIManager
    {
        public static object ActiveMenu;
        public static Background BG;
        public static SpriteFont MenuFont;


        public static RamGecXNAControls.GUIManager AquaGUIgameManager;

        public static RamGecXNAControls.Window Console_W;
        public static RamGecXNAControls.Window _LoadProfileWindow; 
        public static RamGecXNAControls.Window _YesNoWindow; //при удалении профиля
        public static RamGecXNAControls.Window _CreateProfileWindow; //при создании профиля
        public static RamGecXNAControls.Window _YouAreDeadWindow; //при смерти


        public static GameButton MenuButton;
        public static GameButton MenuButtonFromMap;
        public static GameButton MapButton;
        public static GameButton GameButton;

        public static List<GameButton> buttons;

        public static void LoadContent(Microsoft.Xna.Framework.Game Game)
        {
            AquaGUIgameManager = new RamGecXNAControls.GUIManager(Game, "Themes", "DefaultGreen");

            #region console window
            AquaGUIgameManager.LoadControls("Content\\GUIcontrols\\Console_Window.xml");
            Console_W = (Window)AquaGUIgameManager.GetControl("Console_Window");
            Console_W.Bounds.X = 0;
            Console_W.Bounds.Y = 0;


            ENGINE.Logic.Console.Console.TextArea = (TextArea)Console_W.GetControl("TextArea");
            ENGINE.Logic.Console.Console.TextArea.Bounds.Height = Console_W.Bounds.Height - 40;
            ENGINE.Logic.Console.Console.TextArea.Colorize = true;

            TextBox textbox = (TextBox)Console_W.GetControl("Textbox");
            textbox.Bounds.Y = Console_W.Bounds.Height - 25;
            textbox.OnKeyDown += (sender, key) =>
            {
                if (Depository.keyboardState.IsKeyDown(Config.KeyboardCfg.ConsoleButton))
                {
                    textbox.Text = "";
                }

                if (Depository.keyboardState.IsKeyDown(Keys.Enter) & textbox.Text != "")
                {
                    ENGINE.Logic.Console.Console.ReadCommand(textbox.Text);
                    ENGINE.GUI.AquaGUI.lastcommand = textbox.Text;
                    textbox.Text = "";
                }

                if (Depository.keyboardState.IsKeyDown(Keys.Up))
                {
                    textbox.Text = ENGINE.GUI.AquaGUI.lastcommand;
                }

                if (Depository.keyboardState.IsKeyDown(Keys.Tab) && !Depository.oldKeyboardState.IsKeyDown(Keys.Tab))
                {
                    System.Reflection.Assembly c = System.Reflection.Assembly.GetAssembly(textbox.GetType());
                    System.Reflection.PropertyInfo mi = System.Array.Find(c.GetExportedTypes(), a => a.Name == "TextBox").GetProperty("cursor", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance); ;
                    int cursor = (int)mi.GetValue(textbox, new object[] { });
                    textbox.Text = ENGINE.Logic.Console.Console.FindCommand(textbox.Text, cursor);
                }
            };
            Console_W.Visible = false;
            Depository.ConsoleWindowVisible = Console_W.Visible;

            ENGINE.GUI.AquaGUI.MessageBox = new RamGecXNAControls.ExtendedControls.MessageBox(AquaGUIgameManager, "");
            #endregion

            BG = new Background();
            BG.LoadBackgrounds();
            ActiveMenu = new MainMenu(Depository.CommonContent, null);
            MenuFont = Depository.CommonContent.Load<SpriteFont>("Source\\Fonts\\menu");


            //in game GUI
            t_health = new GameTexture(false, @"Textures\gameGUI\health");
            t_mana = new GameTexture(false, @"Textures\gameGUI\mana");
            t_clear = new GameTexture(false, @"Textures\gameGUI\clear");
            t_coin = new GameTexture(false, @"Textures\gameGUI\coin");

            
            GAME.GUIManager.AquaGUIgameManager.LoadControls("Content\\GUIcontrols\\Load_Profile_Window.xml");
            GUIManager._LoadProfileWindow = (RamGecXNAControls.Window)GAME.GUIManager.AquaGUIgameManager.GetControl("Load_Profile_Window");
            GUIManager._LoadProfileWindow.Visible = false;
            GUIManager._LoadProfileWindow.Bounds.X = (int)(Depository.ScreenResolution.X / 2 - GUIManager._LoadProfileWindow.Bounds.Width / 2);
            GUIManager._LoadProfileWindow.Bounds.Y = (int)(Depository.ScreenResolution.Y / 2 - GUIManager._LoadProfileWindow.Bounds.Height / 2);

            GAME.GUIManager.AquaGUIgameManager.LoadControls("Content\\GUIcontrols\\YesNoWindow.xml");
            ((RamGecXNAControls.Window)GAME.GUIManager.AquaGUIgameManager.GetControl("You sure")).Visible = false;

            GAME.GUIManager.AquaGUIgameManager.LoadControls("Content\\GUIcontrols\\YesNoWindow.xml");
            GUIManager._YesNoWindow = (RamGecXNAControls.Window)GAME.GUIManager.AquaGUIgameManager.GetControl("You sure");
            GUIManager._YesNoWindow.Bounds.X = (int)(Depository.ScreenResolution.X / 2 - GUIManager._YesNoWindow.Bounds.Width / 2);
            GUIManager._YesNoWindow.Bounds.Y = (int)(Depository.ScreenResolution.Y / 2 - GUIManager._YesNoWindow.Bounds.Height / 2);
            _YesNoWindow.Movable = false;
            _YesNoWindow.Visible = false;




            GAME.GUIManager.AquaGUIgameManager.LoadControls("Content\\GUIcontrols\\Create_Profile_Window.xml");
            GUIManager._CreateProfileWindow = (RamGecXNAControls.Window)GAME.GUIManager.AquaGUIgameManager.GetControl("Create profile");
            _CreateProfileWindow.Movable = false;
            _CreateProfileWindow.Visible = false;
            GUIManager._CreateProfileWindow.Bounds.X = (int)(Depository.ScreenResolution.X / 2 - GUIManager._CreateProfileWindow.Bounds.Width / 2);
            GUIManager._CreateProfileWindow.Bounds.Y = (int)(Depository.ScreenResolution.Y / 2 - GUIManager._CreateProfileWindow.Bounds.Height / 2);


            #region Back button
            _LoadProfileWindow.GetControl("Back").OnClick += (sender) =>
            {
                CloseDialogWindows();
                GUIManager.ActiveMenu = new MainMenu(Depository.CommonContent, null);
                _LoadProfileWindow.Visible = false;
                ENGINE.GUI.Button.Saccept.Play();
            };
            #endregion

            #region New profile button
            _LoadProfileWindow.GetControl("New").OnClick += (sender) =>
            {
                CloseDialogWindows();

                string folder = System.IO.Directory.GetCurrentDirectory() + "\\Profiles";
                string[] profiles = System.IO.Directory.GetDirectories(folder);

                if (profiles.Length < 10)
                {
                    _CreateProfileWindow.Visible = true;
                    _CreateProfileWindow.TopMost = true;
                }
                ENGINE.GUI.Button.Saccept.Play();
            };
            #region createprofile
            _CreateProfileWindow.GetControl("Cancel").OnClick += (sender) =>
            {
                CloseDialogWindows();
                ENGINE.GUI.Button.Saccept.Play();
            };

            _CreateProfileWindow.GetControl("Create").OnClick += (sender) =>
            {
                string name = (_CreateProfileWindow.GetControl("Textbox") as TextBox).Text;

                if (name == "")
                {
                    string folder = System.IO.Directory.GetCurrentDirectory() + "\\Profiles";
                    string[] profiles = System.IO.Directory.GetDirectories(folder);

                    name = (profiles.Length + 1).ToString();
                }

                ProfilesManager.CreateProfile(System.IO.Directory.GetCurrentDirectory() + "\\Profiles\\" + name);

                ENGINE.GUI.Button.Saccept.Play();
                RefreshProfileForm();
                CloseDialogWindows();
            };
            #endregion
            #endregion

            #region Load button
            _LoadProfileWindow.GetControl("Load").OnClick += (sender) =>
            {
                ENGINE.GUI.Button.Saccept.Play();
                string folder = System.IO.Directory.GetCurrentDirectory() + "\\Profiles";
                string[] profiles = System.IO.Directory.GetDirectories(folder);

                if (!_YesNoWindow.Visible & profiles.Length >= 1)
                {
                    bool ok = true;
                    int i = 1;

                    string delDir = "";

                    while (ok)
                    {
                        if ((_LoadProfileWindow.GetControl(i.ToString()) as RamGecXNAControls.RadioButton).Checked)
                        {
                            delDir = profiles[i - 1];
                            ok = false;
                        }
                        i++;
                    }


                    ProfilesManager.LoadProfile(delDir);
                    SoundManager.StopAll(true);
                }

                CloseDialogWindows();
            };
            #endregion

            #region Delete button
            _LoadProfileWindow.GetControl("Delete").OnClick += (sender) =>
            {
                ENGINE.GUI.Button.Saccept.Play();
                string folder = System.IO.Directory.GetCurrentDirectory() + "\\Profiles";
                string[] profiles = System.IO.Directory.GetDirectories(folder);

                if (!_YesNoWindow.Visible & profiles.Length >= 1)
                {
                    _YesNoWindow.Visible = true;
                    _YesNoWindow.TopMost = true;
                    bool ok = true;
                    int i = 1;

                    string delDir = "";

                    while (ok)
                    {
                        if ((_LoadProfileWindow.GetControl(i.ToString()) as RamGecXNAControls.RadioButton).Checked)
                        {
                            delDir = profiles[i - 1];
                            ok = false;
                        }
                        i++;
                    }


                    delfolder = delDir;
                }
            };
            #region yesnowindow
            _YesNoWindow.GetControl("Yes").OnClick += (sender) =>
            {
                System.IO.Directory.Delete(delfolder, true);
                RefreshProfileForm();

                delfolder = "";
                _YesNoWindow.Visible = false;
                ENGINE.GUI.Button.Saccept.Play();
            };

            _YesNoWindow.GetControl("No").OnClick += (sender) =>
            {
                RefreshProfileForm();

                delfolder = "";
                _YesNoWindow.Visible = false;
                ENGINE.GUI.Button.Saccept.Play();
            };
            #endregion
            #endregion
            
            

            #region u are dead window
            GAME.GUIManager.AquaGUIgameManager.LoadControls("Content\\GUIcontrols\\You_Are_Dead_Window.xml");
            GUIManager._YouAreDeadWindow = (RamGecXNAControls.Window)GAME.GUIManager.AquaGUIgameManager.GetControl("You are dead");
            GUIManager._YouAreDeadWindow.Bounds.X = (int)(Depository.ScreenResolution.X / 2 - GUIManager._YouAreDeadWindow.Bounds.Width / 2);
            GUIManager._YouAreDeadWindow.Bounds.Y = (int)(Depository.ScreenResolution.Y / 2 - GUIManager._YouAreDeadWindow.Bounds.Height / 2);
            _YouAreDeadWindow.Movable = false;
            _YouAreDeadWindow.Visible = false;


            _YouAreDeadWindow.GetControl("Restart").OnClick += (sender) =>
            {
                death_animation = false;
                GameManager.Map.ReopenLastLevel();

                _YouAreDeadWindow.TopMost = false;
                _YouAreDeadWindow.Visible = false;
                ENGINE.GUI.Button.Saccept.Play();
               
            };


            _YouAreDeadWindow.GetControl("Map").OnClick += (sender) =>
            {
                death_animation = false;
                GameManager.SetMap();
                _YouAreDeadWindow.TopMost = false;
                _YouAreDeadWindow.Visible = false;

                //очищаем старые ресурсы
                if (GameManager.LvlController != null) GameManager.LvlController.UnloadResources();
                GameManager.LvlController = null;
                ENGINE.GUI.Button.Saccept.Play();
               
            };
            #endregion

            #region buttons
            buttons = new List<GameButton>();


            MenuButton = new GameButton(@"Textures\Advanced\button_pause", new Vector2(0, 0), GameButton.bSpace.game);
            MenuButton.Texture.TopLeft = new Vector2(0, 0);
            MenuButton.HotKey = Keys.Escape;
            MenuButton.Mask_Up = true;
            MenuButton.OnButtonClick += (bool isHotkey) =>
            {
                if (!_YouAreDeadWindow.Visible)
                {
                    GameManager.SetMenu();
                    escapedFromMap = false;

                    GUIManager.ActiveMenu = new PauseM(Depository.CommonContent, ActiveMenu);
                }
            };

            buttons.Add(MenuButton);


            MenuButtonFromMap = new GameButton(@"Textures\Advanced\button_pause", new Vector2(0, 0), GameButton.bSpace.map);
            MenuButtonFromMap.Texture.TopLeft = new Vector2(0, 0);
            MenuButtonFromMap.HotKey = Keys.Escape;
            MenuButtonFromMap.Mask_Up = true;
            MenuButtonFromMap.OnButtonClick += (bool isHotkey) =>
            {
                GameManager.SetMenu();
                GUIManager.ActiveMenu = new PauseM(Depository.CommonContent, ActiveMenu);
                escapedFromMap = true;
            };

            buttons.Add(MenuButtonFromMap);


            MapButton = new GameButton(@"Textures\Advanced\button_map", new Vector2(0, 0), GameButton.bSpace.game);
            MapButton.Texture.TopLeft = new Vector2(Depository.ScreenResolution.X - MapButton.Texture.TextureResolution.width - 20, Depository.ScreenResolution.Y - MapButton.Texture.TextureResolution.height - 20);
            MapButton.Mask_Shine = true;
            MapButton.OnButtonClick += (bool isHotkey) =>
            {
                if (_YouAreDeadWindow.Visible)
                {
                    death_animation = false;
                    _YouAreDeadWindow.TopMost = false;
                    _YouAreDeadWindow.Visible = false;

                    //очищаем старые ресурсы
                    if (GameManager.LvlController != null) GameManager.LvlController.UnloadResources();
                    GameManager.LvlController = null;
                }

                GameManager.SetMap();                 
            };

            buttons.Add(MapButton);



            GameButton = new GameButton(@"Textures\Advanced\button_game", new Vector2(0, 0), GameButton.bSpace.map);
            GameButton.Texture.TopLeft = new Vector2(Depository.ScreenResolution.X - GameButton.Texture.TextureResolution.width - 20, Depository.ScreenResolution.Y - GameButton.Texture.TextureResolution.height - 20);
            GameButton.Mask_Shine = true;
            GameButton.OnButtonClick += (bool isHotkey) =>
            {
               if (GameManager.LvlController != null) GameManager.SetGame();
            };

            buttons.Add(GameButton);
            #endregion

            ENGINE.Logic.Console.Console.LoadContent();

            load_level_W_texture.TopLeft = new Vector2(Depository.ScreenResolution.X, 0);            
        }

        public static void RefreshProfileForm()
        {
            string folder = System.IO.Directory.GetCurrentDirectory() + "\\Profiles";
            string[] profiles = System.IO.Directory.GetDirectories(folder);

            for (int i = 1; i < 11; ++i)
            {
                if (profiles.Length >= i)
                {
                    _LoadProfileWindow.GetControl(i.ToString()).Visible = true;
                    (_LoadProfileWindow.GetControl(i.ToString()) as RamGecXNAControls.RadioButton).Text = profiles[i - 1].Substring(profiles[i - 1].LastIndexOf(@"\") + 1);
                }
                else
                {
                    _LoadProfileWindow.GetControl(i.ToString()).Visible = false;
                    (_LoadProfileWindow.GetControl(i.ToString()) as RamGecXNAControls.RadioButton).Text = "";
                }
            }

            if (profiles.Length >= 10) _LoadProfileWindow.GetControl("New").Enabled = false; else _LoadProfileWindow.GetControl("New").Enabled = true;

            if (profiles.Length >= 1) (_LoadProfileWindow.GetControl("1") as RamGecXNAControls.RadioButton).Checked = true;
        }
        public static void CloseDialogWindows()
        {
            _YesNoWindow.Visible = false;
            _YesNoWindow.TopMost = false;


            _CreateProfileWindow.Visible = false;
            _CreateProfileWindow.TopMost = false;
        }

        public static GameTexture load_level_W_texture = new GameTexture(false, @"Textures\Map\level_select");
        public static GameTexture start_game_b = new GameTexture(false, @"Textures\Map\start_game_b");
        static GameTexture Level_Preview_Image;


        //Menu
        public static void DrawMenuGUI()
        {
            Depository.SpriteBatch.Begin();

            BG.Draw();

            if (ActiveMenu != null) (ActiveMenu as StandartScreen).Draw(MenuFont);

            Depository.SpriteBatch.End();
        }
        public static void UpdateMenuGUI()
        {
            if (ActiveMenu != null) (ActiveMenu as StandartScreen).Update(GAME.CursorManager.ScreenPosition, 1.0f, GAME.CursorManager.LBclick, Depository.ScreenResolution);
        }

        //Game GUI
        public static void DrawGameGUI(LevelController LvController)
        {
            Depository.SpriteBatch.Begin();

            GameObject hero = GAME.EnviromentInfo.HERO.Hero;


            float hpClearSize = 1;
            float manaClearSize = 1;
            float hpSize = 0;
            float manaSize = 0;

            int coins = 0;


            if (hero != null)
            {
                if (Functions.HaveAttribute(Functions.Attr.RpgCharacter, hero))
                {
                    hpClearSize = (float)(hero.data.ReadData("ARpgCharacterParams") as ARpgCharacter.CharacterParams).MaxHP / (float)100;
                    manaClearSize = (hero.data.ReadData("ARpgCharacterParams") as ARpgCharacter.CharacterParams).MaxMp / (float)100;

                    hpSize = (float)(hero.data.ReadData("ARpgCharacterParams") as ARpgCharacter.CharacterParams).HP / (float)100;
                    manaSize = (hero.data.ReadData("ARpgCharacterParams") as ARpgCharacter.CharacterParams).MP / (float)100;

                    coins = (int)(hero.data.ReadData("ARpgCharacterParams") as ARpgCharacter.CharacterParams).Coins;


                    #region hp and mana
                    //mp

                    Depository.SpriteBatch.Draw(t_mana.Texture2d, new Vector2(10, Depository.ScreenResolution.Y - t_clear.TextureResolution.height * 2 - 25), null, Color.White,
                            0, Vector2.Zero, new Vector2(manaSize, 1), SpriteEffects.None, 0.0f);


                    Depository.SpriteBatch.Draw(t_clear.Texture2d, new Vector2(10, Depository.ScreenResolution.Y - t_clear.TextureResolution.height * 2 - 25), null, Color.White,
                        0, Vector2.Zero, new Vector2(manaClearSize, 1), SpriteEffects.None, 0.1f);


                    //hp

                    Depository.SpriteBatch.Draw(t_health.Texture2d, new Vector2(10, Depository.ScreenResolution.Y - t_clear.TextureResolution.height - 10), null, Color.White,
                        0, Vector2.Zero, new Vector2(hpSize, 1), SpriteEffects.None, 0.0f);

                    Depository.SpriteBatch.Draw(t_clear.Texture2d, new Vector2(10, Depository.ScreenResolution.Y - t_clear.TextureResolution.height - 10), null, Color.White,
                        0, Vector2.Zero, new Vector2(hpClearSize, 1), SpriteEffects.None, 0.1f);
                    #endregion

                    #region coin
                    Depository.SpriteBatch.Draw(t_coin.Texture2d, new Vector2(Depository.ScreenResolution.X /2 - t_coin.TextureResolution.width - 10,
                    Depository.ScreenResolution.Y - t_coin.TextureResolution.height - 10), Color.White);


                    Depository.SpriteBatch.DrawString(MenuFont, coins.ToString(),
                        new Vector2(Depository.ScreenResolution.X/2 - t_coin.TextureResolution.width * 2 - 10,
                            Depository.ScreenResolution.Y - t_coin.TextureResolution.height / 2 - 20), Color.LightGray);

                    #endregion
                }
            }

            if (death_animation)
            {
                splash_death_screen.Draw();
            }

            Depository.SpriteBatch.End();
        }
        public static void UpdateGameGUI()
        {
            if (death_animation)
            {
                int tr = splash_death_screen.Color.A + 1;
                if (tr > 255) tr = 255;

                splash_death_screen.Color.A = (byte)tr;

            } 
        }


        public static void HeroDie()
        {
              GAME.GUIManager._YouAreDeadWindow.Visible = true;
              GAME.GUIManager._YouAreDeadWindow.TopMost = true;
              death_animation = true;

              splash_death_screen.Scale.X = Depository.ScreenResolution.X / splash_death_screen.TextureResolution.width +0.1f;
              splash_death_screen.Scale.Y = Depository.ScreenResolution.Y / splash_death_screen.TextureResolution.height + 0.1f;

              splash_death_screen.Position = new Vector2(Depository.ScreenResolution.X / 2, Depository.ScreenResolution.Y / 2);
              splash_death_screen.Color.A = 0;
        }
        static bool death_animation = false;
        static GameTexture splash_death_screen = new GameTexture(false, @"Textures\Advanced\splash_death_screen");

        //Map GUI
        public static void DrawMapGUI()
        {
            Depository.SpriteBatch.Begin();

            if (!Config.EditorMode)
            {
                load_level_W_texture.Draw();

                if (Level_Preview_Image != null) Level_Preview_Image.Draw();

                STORAGE.Depository.SpriteBatch.DrawString(MenuFont, title, new Vector2(load_level_W_texture.TopLeft.X + 60 + 10, 245),
                   new Color(162, 153, 144), 0, Vector2.Zero, 1.0f, SpriteEffects.None, 0);


                STORAGE.Depository.SpriteBatch.DrawString(MenuFont, DescriptionText, new Vector2(load_level_W_texture.TopLeft.X + 60, 280),
                    new Color(40, 40, 40), 0, Vector2.Zero, 0.8f, SpriteEffects.None, 0);

                start_game_b.Draw();

            }
            Depository.SpriteBatch.End();
        }
        public static void UpdateMapGUI()
        {           
            if (!Config.EditorMode)
            {
                //высчитывание позиций, установка теста и прочего

                if (GAME.GameManager.Map.SelectedLevel != null)
                {
                    if (load_level_W_texture.TopLeft.X >= Depository.ScreenResolution.X - load_level_W_texture.TextureResolution.width)
                        load_level_W_texture.TopLeft -= new Vector2((float)(1000 * Depository.Elapsed), 0);
                    Level_Preview_Image = GAME.GameManager.Map.SelectedLevel.Preview_Image;

                    DescriptionText = GAME.GameManager.Map.SelectedLevel.description;
                    title = GAME.GameManager.Map.SelectedLevel.title;
                }
                else
                {
                    if (load_level_W_texture.TopLeft.X <= Depository.ScreenResolution.X)
                        load_level_W_texture.TopLeft += new Vector2((float)(500 * Depository.Elapsed), 0);
                }

                if (Level_Preview_Image != null)
                {
                    //Level_Preview_Image.Position.Y = 60;
                    //Level_Preview_Image.Position.X = load_level_W_texture.Position.X + load_level_W_texture.TextureResolution.width / 2 - Level_Preview_Image.TextureResolution.width / 2;
                    Level_Preview_Image.TopLeft = new Vector2(load_level_W_texture.Position.X - Level_Preview_Image.TextureResolution.width / 2, 60);
                }

                start_game_b.TopLeft = load_level_W_texture.TopLeft + new Microsoft.Xna.Framework.Vector2(400, 530);//new Microsoft.Xna.Framework.Vector2(load_level_W_texture.Position.X + 400, load_level_W_texture.Position.Y + 530);

                Rectangle startRec = start_game_b.GetTextureArea();
                Vector2 mouse = Input.MouseScreenVector();
                if (CursorManager.LBclick & Input.isHover(startRec, mouse))
                {
                    //start selected level
                    GAME.GameManager.Map.OpenLevel(GAME.GameManager.Map.SelectedLevel);
                }

            }

        }

        //Common GUI
        public static void UpdateCommonGUI()
        {
            AquaGUIgameManager.Update(Depository.gameTime);

            #region console
            TextBox textbox = (TextBox)Console_W.GetControl("Textbox");

            if (Depository.keyboardState.IsKeyDown(Config.KeyboardCfg.ConsoleButton) & !ENGINE.GUI.AquaGUI.console_button_was_pressed)
            {
                Console_W.Visible = !Console_W.Visible;
                textbox.Focused = true;
                Depository.ConsoleWindowVisible = Console_W.Visible;
            }

            ENGINE.GUI.AquaGUI.console_button_was_pressed = Depository.keyboardState.IsKeyDown(Config.KeyboardCfg.ConsoleButton);
            #endregion

            foreach (GameButton b in buttons)
            {
                 if (b == GameButton) if (GAME.GameManager.LvlController == null) continue;
                 b.Update();
            }

            ENGINE.Logic.Fps.UpdateUps(Depository.gameTime.ElapsedGameTime.TotalSeconds);
        }
        public static void DrawCommonGUI()
        {
            Depository.SpriteBatch.Begin();
            AquaGUIgameManager.Draw(Depository.SpriteBatch);

            ENGINE.Logic.Fps.UpdateFps(Depository.gameTime.ElapsedGameTime.TotalSeconds);

            foreach (GameButton b in buttons)
            {
                if (b == GameButton) if (GAME.GameManager.LvlController == null) continue;
                b.Draw();
            }


            if (Config.show_fps)
            {
                Depository.SpriteBatch.DrawString(MenuFont, "FPS: " + ENGINE.Logic.Fps.FramesPerSecond.ToString(), new Vector2(5, 50 + 5), Color.LightGreen, 0, Vector2.Zero, 0.8f, SpriteEffects.None, 0.5f);
                Depository.SpriteBatch.DrawString(MenuFont, "UPS: " + ENGINE.Logic.Fps.UpdatesPerSecond.ToString(), new Vector2(5, 50 + 20), Color.LightGreen, 0, Vector2.Zero, 0.8f, SpriteEffects.None, 0.5f);
            }

           

            if (!Config.EditorMode) CursorManager.DrawCursor();

            Depository.SpriteBatch.End();
        }

        public static bool escapedFromMap = false;
        #region private
        static GameTexture t_health;
        static GameTexture t_mana;
        static GameTexture t_clear;
        static GameTexture t_coin;

        static string delfolder = "";

        static StringBuilder description = new StringBuilder();

        static string title = "";

        public static string DescriptionText
        {
            get
            {
                return description.ToString();
            }
            set
            {
                text = value;
                Wrap();
            }
        }
        static string text;

        public static void Wrap()
        {
            description.Clear();
            int index = 0;
            while (index < text.Length)
            {
                string nextline = text.Substring(index, PixelToCharLength(index));
                description.Append(nextline);
                index += PixelToCharLength(index);
                if (index < text.Length) description.AppendLine();
            }
            string res = description.ToString();
        }

        public static int PixelToCharLength(int startindex)
        {
            int safeWidth = (int)(503 - 40); // 20- отступ 503- ширина текстуры
            if (safeWidth / MenuFont.MeasureString("O").X < 1) return 1;//для узких текстур, которых никто и не должен использовать
            float res = 0;
            int len = 0;
            while (res < safeWidth && len + startindex < text.Length)
            {
                res = MenuFont.MeasureString(text.Substring(startindex, ++len)).X;
            }
            if (len + startindex < text.Length || res > safeWidth) len--;
            return len;
        }
        #endregion
    }
}

﻿using Microsoft.Xna.Framework;
using STORAGE;
using ENGINE.Logic;

namespace GAME
{
    public static class GameManager
    {
        public static LevelController LvlController;

        public static WorldMap Map = new WorldMap();


        /// <summary>
        /// пересоздает LevelController
        /// </summary>
        public static void RecreateLevelController()
        {
            if (GameManager.LvlController != null) GameManager.LvlController.UnloadResources();
            LvlController = new LevelController();
        }


        public static void Draw()
        {

            if (STATUS == gamestatus.game)
            {
                Depository.GraphDevice.Clear(LvlController.BackgroundFillColor);


                Depository.DrawViewport = new Rectangle(
                   (int)(LvlController.camera.CameraPosition.X - Depository.ScreenResolution.X / 2 / LvlController.camera.Zoom),
                   (int)(LvlController.camera.CameraPosition.Y - Depository.ScreenResolution.Y / 2 / LvlController.camera.Zoom),
                   (int)(Depository.ScreenResolution.X / LvlController.camera.Zoom),
                   (int)(Depository.ScreenResolution.Y / LvlController.camera.Zoom));
                Depository.ScreenRectangle = new Rectangle(0, 0, (int)Depository.ScreenResolution.X, (int)Depository.ScreenResolution.Y);

                LvlController.DrawAll();

                GUIManager.DrawGameGUI(LvlController);
            }
            else Depository.GraphDevice.Clear(Color.Gray);


            if (STATUS == gamestatus.menu)
            {
                GUIManager.DrawMenuGUI();
            }

            if (STATUS == gamestatus.map)
            {
                Map.Draw();
                GUIManager.DrawMapGUI();
            }


            GUIManager.DrawCommonGUI();
        }


        public static void Update()
        {
            GUIManager.UpdateCommonGUI();

            SoundManager.Update();

            if (STATUS == gamestatus.game)
            {
                LvlController.UpdateAll();
                GUIManager.UpdateGameGUI();
            }

            if (STATUS == gamestatus.menu)
            {
                GUIManager.UpdateMenuGUI();
            }

            if (STATUS == gamestatus.map)
            {
                Map.Update();
                GUIManager.UpdateMapGUI();
            }            
        }


        public static void SetGame()
        {
            STATUS = gamestatus.game;
            LvlController.camera.scrollValueWas = Depository.mouseState.ScrollWheelValue;
        }
        public static void SetMenu()
        {
            STATUS = gamestatus.menu;
            GUIManager.BG.ChangeTexture();

            GUIManager._YouAreDeadWindow.Visible = false;
            GUIManager._YouAreDeadWindow.TopMost = false;
        }
        public static void SetMap()
        {
            STATUS = gamestatus.map;
        }

        public static gamestatus GetGameStatus()
        {
            return STATUS;
        }
        private static gamestatus STATUS = gamestatus.menu;

        public enum gamestatus
        {
            menu, game, map
        }
    }
}
